package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	// "runtime/trace"
	"time"

	"github.com/docker/libchan/spdy"
	metrics "github.com/tevjef/go-runtime-metrics"
	"gitlab.com/arbke/bot"
	"gitlab.com/arbke/scraper"
	"gitlab.com/arbke/utils"
	tbot "gopkg.in/tucnak/telebot.v2"
)

var (
	princessPeach *tbot.Bot
	arbke         *tbot.Bot
	cacheID       string
	cwd           string
	premium       = "127.0.0.1:9323"
)

//returns true if arb percentages are equal
//false if not
func checkEqual(current, new []bot.Arb) (bool, bool) {
	currentTotal, newTotal := 0.0, 0.0
	for _, arb := range current {
		currentTotal += arb.Percentage
	}
	for _, arb := range new {
		newTotal += arb.Percentage
	}

	utils.Debug(currentTotal, newTotal)
	zero := false
	if newTotal == 0 && currentTotal != 0 {
		zero = true
	}
	return currentTotal == newTotal, zero

}

func getJSONBinCache() []bot.Arb {
	c, err := ioutil.ReadFile(fmt.Sprintf("%s/.arbcache", cwd))
	if err != nil {
		utils.Error(err)
		return []bot.Arb{}
	}
	cacheID = string(c)
	utils.Debug(cacheID)
	if len(cacheID) == 0 {
		return []bot.Arb{}
	}
	resp, err := http.DefaultClient.Get(fmt.Sprintf("https://api.jsonbin.io/b/%s", cacheID))
	if err != nil {
		utils.Error(err)
		return []bot.Arb{}
	}
	r, _ := ioutil.ReadAll(resp.Body)
	tmp := []bot.Arb{}
	json.Unmarshal(r, &tmp)
	return tmp
}
func createJSONBinCache(arbs []bot.Arb) {
	if len(arbs) == 0 {
		os.Remove(fmt.Sprintf("%s/.arbcache", cwd))
		return
	}
	data, _ := json.Marshal(arbs)
	// utils.Debug(string(data))

	resp, err := http.DefaultClient.Post("https://api.jsonbin.io/b", "application/json", bytes.NewBuffer(data))
	if err != nil {
		utils.Error(err)
		return
	}
	r, _ := ioutil.ReadAll(resp.Body)

	respData := map[string]interface{}{}
	json.Unmarshal(r, &respData)

	if respData["success"].(bool) == true {
		cacheID = respData["id"].(string)
		ioutil.WriteFile(".arbcache", []byte(cacheID), 0777)
	}

}

type prempayload struct {
	arbs []bot.Arb
	vbs  []bot.ValueBet
}

func startPremiumConnection(arbc chan prempayload) {
	for {
		var client net.Conn
		var err error

		var sleep int64
		//Linear backoff connection
		for {
			client, err = net.Dial("tcp", premium)
			if err != nil {
				utils.Error(err)
			} else {
				utils.Debug("CONNECTED")
				break
			}

			sleep++
			utils.Debug("Backing off for", sleep)
			time.Sleep(time.Duration(sleep) * time.Second * 2)

		}

		// p, err := spdy.NewSpdyStreamProvider(client, false)
		// if err != nil {
		// 	utils.Error(err)
		// }

		transport, _ := spdy.NewClientTransport(client)
		sender, err := transport.NewSendChannel()
		if err != nil {
			utils.Error(err)
		}

		utils.Debug("WAITING")
		for payload := range arbc {
			utils.Debug("SENDING ARBS", payload.arbs)
			utils.Debug("SENDING VALUE BETS", payload.vbs)
			err = sender.Send(&bot.PremiumPayload{Arbs: payload.arbs, ValueBets: payload.vbs})
			if err != nil {
				utils.Error(err)
				break
			}
		}
	}
}

func main() {
	//ENABLE FOR SYSTRACE
	metrics.DefaultConfig.CollectionInterval = time.Second
	metrics.DefaultConfig.Database = "stats_db"
	metrics.DefaultConfig.Logger = &metrics.DefaultLogger{}
	metricStop := make(chan struct{})
	if err := metrics.RunCollector(metrics.DefaultConfig, metricStop); err != nil {
		utils.Error(err)
	}

	utils.LogDebug, utils.LogError = true, true
	cwd, _ = os.Getwd()
	stop := make(chan struct{})
	resp := make(chan map[string][]scraper.Aggregate)
	sites := []scraper.Scraper{
		// scraper.NewFake(), scraper.NewFake2(), scraper.NewFake3()
		scraper.NewDafabet(),
		scraper.NewXbet(),
		scraper.NewElitebet(),
		// scraper.NewBetika(),
		scraper.NewBetIn(),
		scraper.NewBetpawa(),
		scraper.NewSpesa(),
		scraper.NewKwikBet(),
		scraper.NewChezacash(),
		scraper.NewBetway(),
		scraper.NewSportybet(),
		scraper.NewPowerbet(),
		scraper.NewGMania(),
		scraper.NewOdibet(),
		// scraper.NewWSBet(),
		// scraper.NewMozzart(),
		// scraper.NewLollyybet(),
	}

	b := bot.NewBot(300, 6, stop, sites...)
	bot.WORKER_TIMEOUT = 240

	princessPeach = newTBOTServer("563927613:AAE7THPjOQliL0KwtjE7d9Ct3qwLJ0FfTLA")
	arbke = newTBOTServer("592329288:AAEK0nWKB5fB6sjSD6oWIXrtgrK6rKSG8do")
	go startMessageServers(b, arbke)
	// postToTelegramCustom("Previous arbs now obsolete!")
	// return
	go b.Start(resp)

	// postToTelegram(nil, false)
	currentArbs := getJSONBinCache()
	currCustom := []bot.Arb{}
	// checkEqual(currentArbs, []bot.Arb{})

	count := 0

	//start premium connection
	prov, pchan := make(chan prempayload, 50), make(chan prempayload)
	go startPremiumConnection(pchan)
	go func() {
		for p := range prov {
			pchan <- p
		}
	}()

	for {
		data := <-resp
		// utils.Debug(data["football"])
		// utils.Debug(data["football"][0].Hash)
		bot.CurrentProxy = b.CurrentProxyAddr()
		//for pp,premium and kwikbet recovery, with elitebet
		arbs, customArbs, valueBets := bot.Combine(data["football"], "Betin", false, "Kwikbet")

		// bot.CombineMiddles(data["football"])

		// premArbs, _ := bot.Combine(data["football"], "", "Kwikbet")

		// arbs, _ := bot.Combine(data["football"], false, "Kwikbet")

		// utils.Debug(middles)
		// arbs = append(arbs, middles...)

		equal, zero := checkEqual(currentArbs, arbs)
		currentArbs = arbs

		/// CUSTOM ARBS
		eqb, zb := checkEqual(currCustom, customArbs)
		currCustom = customArbs
		if !eqb {
			postCustomTel(currCustom, zb)
		}
		///

		if !equal {
			postToTelegram(currentArbs, zero)
			createJSONBinCache(currentArbs)
		}

		prov <- prempayload{
			arbs: arbs,
			vbs:  valueBets,
		}

		count++
	}
}
