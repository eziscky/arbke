package bot

import (
	"strings"
	"time"

	"gitlab.com/arbke/scraper"
)

//Tway dc hybrids
func CombineMiddles(games []scraper.Aggregate) {
	for id, arb := range CurrentArbs {
		if strings.Contains(arb.Market, "MIDDLE") {
			CurrentArbs[id].Updated = false
		}

	}

	for _, game := range games {
		dnb := game.HighestDNB()[0]
		tway := game.HighestTWAY()[0]
		gng := game.HighestGNG()[0]
		dc := game.HighestDC()[0]
		ou15 := game.HighestOU15()[0]
		// ou25 := game.HighestOU25()
		// ou35 := game.HighestOU35()
		// ou45 := game.HighestOU45()

		gameTime := ""

		if game.Time != nil {
			gameTime = game.Time.Format(time.ANSIC)
		}

		//1 - X2
		perc := computeMiddle(tway.Home, dc.Away)
		if perc >= 1.01 {
			// utils.Debug(game, ou2)
			registerArb(
				game.Home,
				game.Away,
				gameTime,
				"#MIDDLE# ThreeWay/DoubleChance",
				game.League,
				game.Sport,
				*game.Time,
				(perc-1)*100,
				[]Container{
					Container{"Site": tway.HomeSite, "Outcome": "1", "link": tway.HomeSiteLink, "api": tway.HomeSiteAPILink, "Odd": tway.Home, "Stake": ((1 / tway.Home) * perc)},
					Container{"Site": dc.AwaySite, "Outcome": "X2", "link": dc.AwaySiteLink, "api": dc.AwaySiteAPILink, "Odd": dc.Away, "Stake": ((1 / dc.Away) * perc)},
				},
			)
		}

		//2 - 1X
		perc = computeMiddle(tway.Away, dc.Home)
		if perc >= 1.01 {
			// utils.Debug(game, ou2)
			registerArb(
				game.Home,
				game.Away,
				gameTime,
				"#MIDDLE# ThreeWay/DoubleChance",
				game.League,
				game.Sport,
				*game.Time,
				(perc-1)*100,
				[]Container{
					Container{"Site": tway.AwaySite, "Outcome": "2", "Odd": tway.Away, "link": tway.AwaySiteLink, "api": tway.AwaySiteAPILink, "Stake": ((1 / tway.Away) * perc)},
					Container{"Site": dc.HomeSite, "Outcome": "1X", "Odd": dc.Home, "link": dc.HomeSiteLink, "api": dc.HomeSiteAPILink, "Stake": ((1 / dc.Home) * perc)},
				},
			)
		}

		//DNB1 - X -2
		perc = computeMiddle(dnb.Home, tway.Draw, tway.Away)
		if perc >= 1.01 {
			// utils.Debug(game, ou2)
			registerArb(
				game.Home,
				game.Away,
				gameTime,
				"#MIDDLE# DNB/ThreeWay",
				game.League,
				game.Sport,
				*game.Time,
				(perc-1)*100,
				[]Container{
					Container{"Site": dnb.HomeSite, "Outcome": "DNB - 1", "Odd": dnb.Home, "link": dnb.HomeSiteLink, "api": dnb.HomeSiteAPILink, "Stake": ((1 / dnb.Home) * perc)},
					Container{"Site": tway.DrawSite, "Outcome": "X", "Odd": tway.Draw, "link": tway.DrawSiteLink, "api": tway.DrawSiteAPILink, "Stake": ((1 / tway.Draw) * perc)},
					Container{"Site": tway.AwaySite, "Outcome": "2", "Odd": tway.Away, "link": tway.HomeSiteLink, "api": tway.HomeSiteAPILink, "Stake": ((1 / tway.Away) * perc)},
				},
			)
		}

		//DNB2 - X - 1
		perc = computeMiddle(dnb.Away, tway.Draw, tway.Home)
		if perc >= 1.01 {
			// utils.Debug(game, ou2)
			registerArb(
				game.Home,
				game.Away,
				gameTime,
				"#MIDDLE# DNB/ThreeWay",
				game.League,
				game.Sport,
				*game.Time,
				(perc-1)*100,
				[]Container{
					Container{"Site": dnb.AwaySite, "Outcome": "DNB - 2", "Odd": dnb.Away, "link": dnb.AwaySiteLink, "api": dnb.AwaySiteAPILink, "Stake": ((1 / dnb.Away) * perc)},
					Container{"Site": tway.DrawSite, "Outcome": "X", "Odd": tway.Draw, "link": tway.DrawSiteLink, "api": tway.DrawSiteAPILink, "Stake": ((1 / tway.Draw) * perc)},
					Container{"Site": tway.HomeSite, "Outcome": "1", "Odd": tway.Home, "link": tway.HomeSiteLink, "api": tway.HomeSiteAPILink, "Stake": ((1 / tway.Home) * perc)},
				},
			)
		}

		//DNB2 - 1X
		perc = computeMiddle(dnb.Away, dc.Home)
		if perc >= 1.01 {
			// utils.Debug(game, ou2)
			registerArb(
				game.Home,
				game.Away,
				gameTime,
				"#MIDDLE# DNB/DoubleChance",
				game.League,
				game.Sport,
				*game.Time,
				(perc-1)*100,
				[]Container{
					Container{"Site": dnb.AwaySite, "Outcome": "DNB - 2", "Odd": dnb.Away, "link": dnb.AwaySiteLink, "api": dnb.AwaySiteAPILink, "Stake": ((1 / dnb.Away) * perc)},
					Container{"Site": dc.HomeSite, "Outcome": "1X", "Odd": dc.Home, "link": dc.HomeSiteLink, "api": dc.HomeSiteAPILink, "Stake": ((1 / dc.Home) * perc)},
				},
			)
		}

		//DNB1 - X2
		perc = computeMiddle(dnb.Home, dc.Away)
		if perc >= 1.01 {
			// utils.Debug(game, ou2)
			registerArb(
				game.Home,
				game.Away,
				gameTime,
				"#MIDDLE# DNB/DoubleChance",
				game.League,
				game.Sport,
				*game.Time,
				(perc-1)*100,
				[]Container{
					Container{"Site": dnb.HomeSite, "Outcome": "DNB - 1", "Odd": dnb.Home, "link": dnb.HomeSiteLink, "api": dnb.HomeSiteAPILink, "Stake": ((1 / dnb.Home) * perc)},
					Container{"Site": dc.AwaySite, "Outcome": "X2", "Odd": dc.Away, "link": dc.AwaySiteLink, "api": dc.AwaySiteAPILink, "Stake": ((1 / dc.Away) * perc)},
				},
			)
		}

		//GG -NG-O1.5
		perc = computeMiddle(gng.Goal, gng.NoGoal, ou15.Over)
		if perc >= 1.01 {
			// utils.Debug(game, ou2)
			registerArb(
				game.Home,
				game.Away,
				gameTime,
				"#MIDDLE# GoalNoGoal/OverUnder1.5",
				game.League,
				game.Sport,
				*game.Time,
				(perc-1)*100,
				[]Container{
					Container{"Site": gng.GoalSite, "Outcome": "GG - YES", "Odd": gng.Goal, "link": gng.GoalSiteLink, "api": gng.GoalSiteAPILink, "Stake": ((1 / gng.Goal) * perc)},
					Container{"Site": gng.NoGoalSite, "Outcome": "GG -NO", "Odd": gng.NoGoal, "link": gng.NoGoalSiteLink, "api": gng.NoGoalSiteAPILink, "Stake": ((1 / gng.NoGoal) * perc)},
					Container{"Site": ou15.OverSite, "Outcome": "Over 1.5", "Odd": ou15.Over, "link": ou15.OverSiteLink, "api": ou15.OverSiteAPILink, "Stake": ((1 / ou15.Over) * perc)},
				},
			)
		}

		//NG -X- O1.5
		perc = computeMiddle(tway.Draw, gng.NoGoal, ou15.Over)
		if perc >= 1.01 {
			// utils.Debug(game, ou2)
			registerArb(
				game.Home,
				game.Away,
				gameTime,
				"#MIDDLE# GoalNoGoal/ThreeWay/OverUnder1.5",
				game.League,
				game.Sport,
				*game.Time,
				(perc-1)*100,
				[]Container{
					Container{"Site": tway.DrawSite, "Outcome": "X", "Odd": tway.Draw, "link": tway.DrawSiteLink, "api": tway.DrawSiteAPILink, "Stake": ((1 / tway.Draw) * perc)},
					Container{"Site": gng.NoGoalSite, "Outcome": "GG -NO", "Odd": gng.NoGoal, "link": gng.NoGoalSiteLink, "api": gng.NoGoalSiteAPILink, "Stake": ((1 / gng.NoGoal) * perc)},
					Container{"Site": ou15.OverSite, "Outcome": "Over 1.5", "Odd": ou15.Over, "link": ou15.OverSiteLink, "api": ou15.OverSiteAPILink, "Stake": ((1 / ou15.Over) * perc)},
				},
			)
		}
	}

}

func computeMiddle(odds ...float64) float64 {
	interimT := 0.0
	for _, odd := range odds {
		interimT += 1 / odd
	}
	preturn := 1.0 / interimT

	return Round(preturn, 0.0005)
}
