package bot

import "gitlab.com/arbke/scraper"

func computeTWAYValue(opp scraper.HThreeWay) (string, float64) {
	initial := 1000.0

	probabilities := []float64{
		1 / opp.Home,
		1 / opp.Draw,
		1 / opp.Away,
	}

	highestProbability := 0.0
	hvIndex := -1
	value := 0.0

	for i, prob := range probabilities {
		losingProb := 0.0
		if prob > highestProbability {
			highestProbability = prob
			hvIndex = i
		}
		for j, p := range probabilities {
			if i == j {
				continue
			}
			losingProb += p

		}
		profit := (initial * 1 / prob) - initial
		v := profit*prob - initial*(losingProb)
		value = v / initial * 100

	}

	switch hvIndex {
	case 0:
		return "1", value
	case 1:
		return "X", value
	case 2:
		return "2", value
	default:
		return "", -1.0
	}

}

func computeDCValue(opp scraper.HDoubleChance) (string, float64) {
	initial := 1000.0

	probabilities := []float64{
		1 / opp.Home,
		1 / opp.HomeOrAway,
		1 / opp.Away,
	}

	highestProbability := 0.0
	hvIndex := -1
	value := 0.0

	for i, prob := range probabilities {
		losingProb := 0.0
		if prob > highestProbability {
			highestProbability = prob
			hvIndex = i
		}
		for j, p := range probabilities {
			if i == j {
				continue
			}
			losingProb += p

		}
		profit := (initial * 1 / prob) - initial
		v := profit*prob - initial*(losingProb)
		value = v / initial * 100

	}

	switch hvIndex {
	case 0:
		return "1X", value
	case 1:
		return "12", value
	case 2:
		return "X2", value
	default:
		return "", -1.0
	}
}

func computeDNBValue(opp scraper.HDrawNoBet) (string, float64) {
	initial := 1000.0

	probabilities := []float64{
		1 / opp.Home,
		1 / opp.Away,
	}

	highestProbability := 0.0
	hvIndex := -1
	value := 0.0

	for i, prob := range probabilities {
		losingProb := 0.0
		if prob > highestProbability {
			highestProbability = prob
			hvIndex = i
		}
		for j, p := range probabilities {
			if i == j {
				continue
			}
			losingProb += p

		}
		profit := (initial * 1 / prob) - initial
		v := profit*prob - initial*(losingProb)
		value = v / initial * 100

	}

	switch hvIndex {
	case 0:
		return "1", value
	case 2:
		return "2", value
	default:
		return "", -1.0
	}

}

func computeGNGValue(opp scraper.HGoalNoGoal) (string, float64) {
	initial := 1000.0

	probabilities := []float64{
		1 / opp.Goal,
		1 / opp.NoGoal,
	}

	highestProbability := 0.0
	hvIndex := -1
	value := 0.0

	for i, prob := range probabilities {
		losingProb := 0.0
		if prob > highestProbability {
			highestProbability = prob
			hvIndex = i
		}
		for j, p := range probabilities {
			if i == j {
				continue
			}
			losingProb += p

		}
		profit := (initial * 1 / prob) - initial
		v := profit*prob - initial*(losingProb)
		value = v / initial * 100

	}

	switch hvIndex {
	case 0:
		return "GG", value
	case 2:
		return "NG", value
	default:
		return "", -1.0
	}

}
