package bot

import (
	"strings"

	"gitlab.com/arbke/scraper"
)

func rmSpecial(team string) string {
	team = strings.Replace(team, "(", "", -1)
	team = strings.Replace(team, ")", "", -1)
	return strings.Trim(team, " ")
}

func short(name string) bool {
	n := strings.ToUpper(name)

	for _, f := range scraper.ShortForms() {
		if f == n {
			return true
		}
	}
	return false
}
func rm(team string) string {
	arr := strings.Split(team, " ")
	index := -1
	for i, j := range arr {
		if short(j) {
			index = i
			break
		}
	}
	if index >= 0 {
		arr = append(arr[:index], arr[index+1:]...)
		// utils.Debug(team, "--->", strings.Join(arr, " "))
	}
	return strings.Join(arr, " ")
}
