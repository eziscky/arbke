package bot

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/arbke/scraper"
)

func testWorker(t *testing.T) {
	sites := []scraper.Scraper{
		// scraper.NewFake(), scraper.NewFake2(), scraper.NewFake3()
		scraper.NewFake(),
	}
	w := NewWorker(1)
	// w2 := NewWorker(2)
	workerChan := make(chan chan map[string]interface{})
	results := make(chan map[string]interface{})
	WORKER_TIMEOUT = 30
	w.sports = []int{0} //1, 2, 3, 4, 5}
	go w.Start(workerChan, results)
	// go w2.Start(workerChan, results)
	proxy := NewProxy()
	data := map[string][]scraper.Game{}
	// num := len(sites)
	done := make(chan int)
	go func() {
		for result := range results {
			fmt.Println(result["name"].(string))
			data[result["name"].(string)] = result["data"].([]scraper.Game)

		}
		done <- 1
	}()
	for _, site := range sites {
		// proxy := false
		worker := <-workerChan
		worker <- map[string]interface{}{
			"site":  site,
			"proxy": proxy,
		}
	}
	time.Sleep(65 * time.Second)
}
