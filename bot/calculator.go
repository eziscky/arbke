package bot

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/nu7hatch/gouuid"

	"gitlab.com/arbke/scraper"
	"gitlab.com/arbke/utils"
)

var (
	CurrentArbs      map[string]*Arb //nolint
	CurrentValueBets map[string]*ValueBet
	CurrentProxy     string
)

func resetCurrentArbs() {
	for id, arb := range CurrentArbs {
		if strings.Contains(arb.Market, "MIDDLE") {
			continue
		}
		CurrentArbs[id].Updated = false
	}

}

//Combine returns new,custom arbs
func Combine(games []scraper.Aggregate, custom string, v bool, exclude ...string) ([]Arb, []Arb, []ValueBet) {
	customArbs := []Arb{}
	resetCurrentArbs()
	// utils.Debug(games)

	//compute and populate arbs and value bets
	for _, game := range games {
		gameTime := ""
		if game.Time != nil {
			gameTime = game.Time.Format(time.ANSIC)
		}

		//custom criteria
		if len(custom) > 0 {
			customArbs = append(customArbs, CustomCriteriaArbs(custom, game)...)
		}

		//ARB CALCULATIONS
		//check for three way arb
		for _, tw := range game.HighestTWAY(exclude...) {
			perc := computeTWArb(tw)

			if perc >= 1.01 {
				// utils.Debug(game)
				hsiteInd, asiteInd, dsiteInd := "Site", "Site", "Site"
				if tw.HDeviation > tw.ADeviation {
					if tw.HDeviation > tw.DDeviation {
						hsiteInd = "*Site"
					} else {
						dsiteInd = "*Site"
					}
				} else {
					if tw.ADeviation > tw.DDeviation {
						asiteInd = "*Site"
					} else {
						dsiteInd = "*Site"
					}
				}

				registerArb(
					game.Home,
					game.Away,
					gameTime,
					"Three Way (1X2)",
					game.League,
					game.Sport,
					*game.Time,
					(perc-1)*100,
					[]Container{
						Container{hsiteInd: tw.HomeSite, "link": tw.HomeSiteLink, "api": tw.HomeSiteAPILink, "Outcome": "1", "Odd": tw.Home, "Stake": ((1 / tw.Home) * perc)},
						Container{dsiteInd: tw.DrawSite, "link": tw.DrawSiteLink, "api": tw.DrawSiteAPILink, "Outcome": "X", "Odd": tw.Draw, "Stake": ((1 / tw.Draw) * perc)},
						Container{asiteInd: tw.AwaySite, "link": tw.AwaySiteLink, "api": tw.AwaySiteAPILink, "Outcome": "2", "Odd": tw.Away, "Stake": ((1 / tw.Away) * perc)},
					},
				)
			}
			if v {
				break
			}
		}
		for _, twow := range game.HighestTWOWAY(exclude...) {
			perc := computeTWOWArb(twow)
			if perc >= 1.01 {
				// utils.Debug(game)
				hsiteInd, asiteInd := "Site", "Site"
				if twow.HDeviation > twow.ADeviation {
					hsiteInd = "*Site"
				} else {
					asiteInd = "*Site"
				}

				registerArb(
					game.Home,
					game.Away,
					gameTime,
					"Two Way (Money Line/Winner)",
					game.League,
					game.Sport,
					*game.Time,
					(perc-1)*100,
					[]Container{
						Container{hsiteInd: twow.HomeSite, "link": twow.HomeSiteLink, "api": twow.HomeSiteAPILink, "Outcome": "1", "Odd": twow.Home, "Stake": ((1 / twow.Home) * perc)},
						// Container{dsiteInd: tw.DrawSite, "link": tw.DrawSiteLink, "api": tw.DrawSiteAPILink, "Outcome": "X", "Odd": tw.Draw, "Stake": ((1 / tw.Draw) * perc)},
						Container{asiteInd: twow.AwaySite, "link": twow.AwaySiteLink, "api": twow.AwaySiteAPILink, "Outcome": "2", "Odd": twow.Away, "Stake": ((1 / twow.Away) * perc)},
					},
				)
			}
			if v {
				break
			}
		}
		//check for double chance arbs
		for _, dc := range game.HighestDC(exclude...) {
			perc := computeDCArb(dc)
			if perc >= 1.01 {
				hsiteInd, asiteInd := "Site", "Site"
				if dc.HDeviation > dc.ADeviation {
					hsiteInd = "*Site"
				} else {
					asiteInd = "*Site"
				}
				registerArb(
					game.Home,
					game.Away,
					gameTime,
					"Double Chance",
					game.League,
					game.Sport,
					*game.Time,
					(perc-1)*100,
					[]Container{
						Container{hsiteInd: dc.HomeSite, "link": dc.HomeSiteLink, "api": dc.HomeSiteAPILink, "Outcome": "1X", "Odd": dc.Home, "Stake": ((1 / dc.Home) * perc)},
						Container{asiteInd: dc.AwaySite, "link": dc.AwaySiteLink, "api": dc.AwaySiteAPILink, "Outcome": "X2", "Odd": dc.Away, "Stake": ((1 / dc.Away) * perc)},
					},
				)
			}
			if v {
				break
			}

		}
		//check for dnb arbs
		for _, dnb := range game.HighestDNB(exclude...) {
			perc := computeDNBArb(dnb)
			if perc >= 1.01 {
				hsiteInd, asiteInd := "Site", "Site"
				if dnb.HDeviation > dnb.ADeviation {
					hsiteInd = "*Site"
				} else {
					asiteInd = "*Site"
				}
				// utils.Debug(game)
				registerArb(
					game.Home,
					game.Away,
					gameTime,
					"Draw No Bet",
					game.League,
					game.Sport,
					*game.Time,
					(perc-1)*100,
					[]Container{
						Container{hsiteInd: dnb.HomeSite, "link": dnb.HomeSiteLink, "api": dnb.HomeSiteAPILink, "Outcome": "1", "Odd": dnb.Home, "Stake": ((1 / dnb.Home) * perc)},
						Container{asiteInd: dnb.AwaySite, "link": dnb.AwaySiteLink, "api": dnb.AwaySiteAPILink, "Outcome": "2", "Odd": dnb.Away, "Stake": ((1 / dnb.Away) * perc)},
					},
				)
			}
			if v {
				break
			}
		}
		//check for gng arbs
		for _, gng := range game.HighestGNG(exclude...) {
			perc := computeGNGArb(gng)
			if perc >= 1.01 {
				gsiteInd, ngsiteInd := "Site", "Site"
				if gng.GDeviation > gng.NGDeviation {
					gsiteInd = "*Site"
				} else {
					ngsiteInd = "*Site"
				}
				registerArb(
					game.Home,
					game.Away,
					gameTime,
					"Goal/No Goal",
					game.League,
					game.Sport,
					*game.Time,
					(perc-1)*100,
					[]Container{
						Container{gsiteInd: gng.GoalSite, "link": gng.GoalSiteLink, "api": gng.GoalSiteAPILink, "Outcome": "Goal", "Odd": gng.Goal, "Stake": ((1 / gng.Goal) * perc)},
						Container{ngsiteInd: gng.NoGoalSite, "link": gng.NoGoalSiteLink, "api": gng.NoGoalSiteAPILink, "Outcome": "NoGoal", "Odd": gng.NoGoal, "Stake": ((1 / gng.NoGoal) * perc)},
					},
				)
			}
			if v {
				break
			}
		}
		//check for ou15 arbs
		for _, ou := range game.HighestOU15(exclude...) {
			perc := computeOUArb(ou)
			if perc >= 1.01 {
				ositeInd, usiteInd := "Site", "Site"
				if ou.ODeviation > ou.UDeviation {
					ositeInd = "*Site"
				} else {
					usiteInd = "*Site"
				}
				registerArb(
					game.Home,
					game.Away,
					gameTime,
					"Over/Under 1.5",
					game.League,
					game.Sport,
					*game.Time,
					(perc-1)*100,
					[]Container{
						Container{ositeInd: ou.OverSite, "link": ou.OverSiteLink, "api": ou.OverSiteAPILink, "Outcome": "Over 1.5", "Odd": ou.Over, "Stake": ((1 / ou.Over) * perc)},
						Container{usiteInd: ou.UnderSite, "link": ou.UnderSiteLink, "api": ou.UnderSiteAPILink, "Outcome": "Under 1.5", "Odd": ou.Under, "Stake": ((1 / ou.Under) * perc)},
					},
				)
			}
			if v {
				break
			}
		}
		//check for ou25 arbs
		for _, ou2 := range game.HighestOU25(exclude...) {
			perc := computeOUArb(ou2)
			if perc >= 1.01 {
				ositeInd, usiteInd := "Site", "Site"
				if ou2.ODeviation > ou2.UDeviation {
					ositeInd = "*Site"
				} else {
					usiteInd = "*Site"
				}
				// utils.Debug(game, ou2)
				registerArb(
					game.Home,
					game.Away,
					gameTime,
					"Over/Under 2.5",
					game.League,
					game.Sport,
					*game.Time,
					(perc-1)*100,
					[]Container{
						Container{ositeInd: ou2.OverSite, "link": ou2.OverSiteLink, "api": ou2.OverSiteAPILink, "Outcome": "Over 2.5", "Odd": ou2.Over, "Stake": ((1 / ou2.Over) * perc)},
						Container{usiteInd: ou2.UnderSite, "link": ou2.UnderSiteLink, "api": ou2.UnderSiteAPILink, "Outcome": "Under 2.5", "Odd": ou2.Under, "Stake": ((1 / ou2.Under) * perc)},
					},
				)
			}
			if v {
				break
			}
		}
		//check for ou35 arbs
		for _, ou3 := range game.HighestOU35(exclude...) {
			perc := computeOUArb(ou3)
			if perc >= 1.01 {
				ositeInd, usiteInd := "Site", "Site"
				if ou3.ODeviation > ou3.UDeviation {
					ositeInd = "*Site"
				} else {
					usiteInd = "*Site"
				}
				registerArb(
					game.Home,
					game.Away,
					gameTime,
					"Over/Under 3.5",
					game.League,
					game.Sport,
					*game.Time,
					(perc-1)*100,
					[]Container{
						Container{ositeInd: ou3.OverSite, "link": ou3.OverSiteLink, "api": ou3.OverSiteAPILink, "Outcome": "Over 3.5", "Odd": ou3.Over, "Stake": ((1 / ou3.Over) * perc)},
						Container{usiteInd: ou3.UnderSite, "link": ou3.UnderSiteLink, "api": ou3.UnderSiteAPILink, "Outcome": "Under 3.5", "Odd": ou3.Under, "Stake": ((1 / ou3.Under) * perc)},
					},
				)
			}
			if v {
				break
			}
		}
		//check for ou45 arbs
		for _, ou4 := range game.HighestOU45(exclude...) {
			perc := computeOUArb(ou4)
			if perc >= 1.01 {
				ositeInd, usiteInd := "Site", "Site"
				if ou4.ODeviation > ou4.UDeviation {
					ositeInd = "*Site"
				} else {
					usiteInd = "*Site"
				}
				registerArb(
					game.Home,
					game.Away,
					gameTime,
					"Over/Under 4.5",
					game.League,
					game.Sport,
					*game.Time,
					(perc-1)*100,
					[]Container{
						Container{ositeInd: ou4.OverSite, "link": ou4.OverSiteLink, "api": ou4.OverSiteAPILink, "Outcome": "Over 4.5", "Odd": ou4.Over, "Stake": ((1 / ou4.Over) * perc)},
						Container{usiteInd: ou4.UnderSite, "link": ou4.UnderSiteLink, "api": ou4.UnderSiteAPILink, "Outcome": "Under 4.5", "Odd": ou4.Under, "Stake": ((1 / ou4.Under) * perc)},
					},
				)
			}
			if v {
				break
			}
		}

		//VALUE BET CALCULATIONS

		tw := game.HighestTWAY(exclude...)
		if len(tw) > 0 {
			outcome, perc := computeTWAYValue(tw[0])
			// utils.Debug(outcome, perc)
			if perc >= 5 {
				registerValueBet(
					game.Home,
					game.Away,
					gameTime,
					"Three Way",
					game.League,
					game.Sport,
					*game.Time,
					perc,
					outcome,
				)
			}
		}
		dc := game.HighestDC(exclude...)
		if len(dc) > 0 {
			outcome, perc := computeDCValue(dc[0])
			if perc >= 5 {
				registerValueBet(
					game.Home,
					game.Away,
					gameTime,
					"Double Chance",
					game.League,
					game.Sport,
					*game.Time,
					perc,
					outcome,
				)
			}
		}
		dnb := game.HighestDNB(exclude...)
		if len(dnb) > 0 {
			outcome, perc := computeDNBValue(dnb[0])
			if perc >= 5 {
				registerValueBet(
					game.Home,
					game.Away,
					gameTime,
					"DNB",
					game.League,
					game.Sport,
					*game.Time,
					perc,
					outcome,
				)
			}
		}
		gng := game.HighestGNG(exclude...)
		if len(gng) > 0 {
			outcome, perc := computeGNGValue(gng[0])
			if perc >= 5 {
				registerValueBet(
					game.Home,
					game.Away,
					gameTime,
					"GG/NG",
					game.League,
					game.Sport,
					*game.Time,
					perc,
					outcome,
				)
			}
		}
	}

	//compute and populate value bets

	arbs := []Arb{}
	valueBets := []ValueBet{}
	toRemove := []string{}

	for _, v := range CurrentArbs {

		if !v.Updated { //arb is obsolete
			continue
		}
		arbs = append(arbs, *v)
	}

	for id, vb := range CurrentValueBets {
		if time.Now().Unix() >= vb.TimeObj.Unix() {
			toRemove = append(toRemove, id)
			continue
		}
		valueBets = append(valueBets, *vb)
	}

	for _, id := range toRemove {
		delete(CurrentArbs, id)
	}

	return arbs, customArbs, valueBets

}

func genArbID() string {
	id, err := uuid.NewV4()
	if err != nil {
		utils.Error(err)
	}
	return id.String()
}

func createArb(home, away, timeStr, market string, perc float64, comb []Container) Arb {
	return Arb{
		Name:         fmt.Sprintf("%s V %s", home, away),
		Home:         home,
		Away:         away,
		Time:         timeStr,
		Market:       market,
		Combinations: comb,
		Percentage:   perc,
	}
}

func getCombinationHash(combs []Container) string {
	siteStr := ""
	for _, c := range combs {
		if site, ok := c["*Site"]; ok {
			siteStr += site.(string)
		}
		if site, ok := c["Site"]; ok {
			siteStr += site.(string)
		}
	}

	return utils.GetB64(siteStr)
}

func registerValueBet(home, away, timeStr, market, league, sport string, _time time.Time, perc float64, outcome string) {
	home = rmSpecial(rm(strings.ToUpper(home)))
	away = rmSpecial(rm(strings.ToUpper(away)))

	vbHash := utils.GetB64(home + away)

	if _, ok := CurrentValueBets[vbHash]; ok {
		CurrentValueBets[vbHash].Percentage = perc
		CurrentValueBets[vbHash].Updated = true
		CurrentValueBets[vbHash].Outcome = outcome
		return
	}
	CurrentValueBets[vbHash] = &ValueBet{
		ID:         vbHash,
		Name:       fmt.Sprintf("%s V %s", home, away),
		Home:       home,
		Away:       away,
		League:     league,
		Sport:      sport,
		Updated:    true,
		Time:       timeStr,
		TimeObj:    &_time,
		Market:     market,
		Percentage: perc,
		Outcome:    outcome,
	}

	// vbID := ""
}
func registerArb(home, away, timeStr, market, league, sport string, time time.Time, perc float64, comb []Container) {
	arbID := ""
	//EXCLUDE LEAGUES/GAMES here

	// if strings.Contains(league, "Trinidad") {
	// 	return
	// }
	hash := getCombinationHash(comb)
	for id, arb := range CurrentArbs {
		if arb.Sport == sport {
			if market == arb.Market {

				if hash != arb.CombinationHash {
					// utils.Debug("HASH MISMATCH")
					// utils.Debug(arb.Combinations)
					// utils.Debug(comb)
					continue
				}
				rhome := rmSpecial(rm(strings.ToUpper(arb.Home)))
				raway := rmSpecial(rm(strings.ToUpper(arb.Away)))
				home = rmSpecial(rm(strings.ToUpper(home)))
				away = rmSpecial(rm(strings.ToUpper(away)))

				homematch := strings.Contains(rhome, home) || strings.Contains(home, rhome)
				awaymatch := strings.Contains(raway, away) || strings.Contains(away, raway)

				// utils.Debug(homematch, awaymatch, rec.Time.Equal(*game.Time))
				if (homematch && awaymatch) && arb.TimeObj.Equal(time) {
					utils.Debug("IDENTIFIER MATCH", home, rhome, away, raway, market, arb.Market)
					// return k, true
					arbID = id
					break

				}
			}
		}
	}
	if len(arbID) == 0 {
		arbID = genArbID()
	}

	//Check reliability
	reliability := 3 //max
	reliabilityStr := ""

	for _, combination := range comb {
		if val, ok := combination["Site"]; ok {
			if val == "Elitebet" || val == "Chezacash" || val == "Sportpesa" {
				reliability--
			}
		}
		if val, ok := combination["*Site"]; ok {
			if val == "Elitebet" || val == "Chezacash" || val == "Sportpesa" {
				reliability--
			}
		}
	}

	switch reliability {
	case 0:
		reliabilityStr = "VERY LOW"
	case 1:
		reliabilityStr = "LOW"
	case 2:
		reliabilityStr = "MEDIUM"
	case 3:
		reliabilityStr = "HIGH"
	}

	if _, ok := CurrentArbs[arbID]; ok {
		CurrentArbs[arbID].Reliability = reliabilityStr
		CurrentArbs[arbID].Percentage = perc
		CurrentArbs[arbID].Updated = true
		CurrentArbs[arbID].Combinations = comb
		CurrentArbs[arbID].Proxy = CurrentProxy
	} else {
		CurrentArbs[arbID] = &Arb{
			ID:              arbID,
			Name:            fmt.Sprintf("%s V %s", home, away),
			Home:            home,
			Away:            away,
			Reliability:     reliabilityStr,
			League:          league,
			Sport:           sport,
			Proxy:           CurrentProxy,
			CombinationHash: hash,
			Updated:         true,
			Time:            timeStr,
			TimeObj:         &time,
			Market:          market,
			Combinations:    comb,
			Percentage:      perc,
		}

	}
}

func Round(x, unit float64) float64 {
	rounded, _ := strconv.ParseFloat(fmt.Sprintf("%.4f", float64(int64(x/unit+0.5))*unit), 64)
	return rounded
}

func computeTWArb(opp scraper.HThreeWay) float64 {
	interimT := 0.0
	outcomes := []float64{
		opp.Home, opp.Draw, opp.Away,
	}
	for _, v := range outcomes {
		interimT += 1 / v

	}
	preturn := 1.0 / interimT

	return Round(preturn, 0.0005)
}
func computeTWOWArb(opp scraper.HTwoWay) float64 {
	interimT := 0.0
	outcomes := []float64{
		opp.Home, opp.Away,
	}
	for _, v := range outcomes {
		interimT += 1 / v

	}
	preturn := 1.0 / interimT

	return Round(preturn, 0.0005)
}
func computeDCArb(opp scraper.HDoubleChance) float64 {
	interimT := 0.0
	outcomes := []float64{
		opp.Home, opp.Away,
	}
	for _, v := range outcomes {
		interimT += 1.0 / v

	}
	preturn := 1.0 / interimT

	return Round(preturn, 0.0005)
}

func computeDNBArb(opp scraper.HDrawNoBet) float64 {
	interimT := 0.0
	outcomes := []float64{
		opp.Home, opp.Away,
	}
	for _, v := range outcomes {
		interimT += 1.0 / v

	}
	preturn := 1.0 / interimT

	return Round(preturn, 0.0005)
}

func computeGNGArb(opp scraper.HGoalNoGoal) float64 {
	interimT := 0.0
	outcomes := []float64{
		opp.Goal, opp.NoGoal,
	}
	for _, v := range outcomes {
		interimT += 1.0 / v

	}
	preturn := 1.0 / interimT

	return Round(preturn, 0.0005)
}

func computeOUArb(opp scraper.OU) float64 {
	interimT := 0.0
	outcomes := []float64{
		opp.Over_(), opp.Under_(),
	}
	for _, v := range outcomes {
		interimT += 1.0 / v

	}
	preturn := 1.0 / interimT

	return Round(preturn, 0.0005)
}

//1.Betin - for bonus
//2. Kwikbet - to withdraw money
func CustomCriteriaArbs(site string, game scraper.Aggregate) []Arb {
	condition := func(val float64) bool {
		return val >= 3.0
	}

	validArb := func(arb float64) bool {
		return arb >= 0.99 //3% acceptable loss
	}

	validArbs := []Arb{}
	gameTime := ""
	if game.Time != nil {
		gameTime = game.Time.Format(time.ANSIC)
	}

	if tw, ok := game.TWAY[site]; ok {
		htw := game.HighestTWAY()[0]
		s := false
		if condition(tw.Home) {
			s = true
			htw.Home = tw.Home
			htw.HomeSite = site
			htw.HomeSiteLink = tw.Link
		}
		if condition(tw.Away) {
			s = true
			htw.Away = tw.Away
			htw.AwaySite = site
			htw.AwaySiteLink = tw.Link
		}
		if condition(tw.Draw) {
			s = true
			htw.Draw = tw.Draw
			htw.DrawSite = site
			htw.DrawSiteLink = tw.Link
		}
		p := computeTWArb(htw)
		if !s {
			p = 0
		}
		if validArb(p) {
			validArbs = append(validArbs, createArb(
				game.Home,
				game.Away,
				gameTime,
				"Three WaY",
				(p-1)*100,
				[]Container{
					Container{"Site": htw.HomeSite, "Outcome": "1", "Odd": htw.Home, "link": htw.HomeSiteLink, "Stake": ((1 / htw.Home) * p)},
					Container{"Site": htw.DrawSite, "Outcome": "X", "Odd": htw.Draw, "link": htw.DrawSiteLink, "Stake": ((1 / htw.Draw) * p)},
					Container{"Site": htw.AwaySite, "Outcome": "2", "Odd": htw.Away, "link": htw.AwaySiteLink, "Stake": ((1 / htw.Away) * p)},
				},
			))
		}
	}

	if dc, ok := game.DC[site]; ok {
		hdc := game.HighestDC()[0]
		s := false
		if condition(dc.Home) {
			s = true
			hdc.Home = dc.Home
			hdc.HomeSite = site
			hdc.HomeSiteLink = dc.Link
		}
		if condition(dc.Away) {
			s = true
			hdc.Away = dc.Away
			hdc.AwaySite = site
			hdc.AwaySiteLink = dc.Link
		}
		p := computeDCArb(hdc)

		if !s {
			p = 0
		}
		if validArb(p) {
			validArbs = append(validArbs, createArb(
				game.Home,
				game.Away,
				gameTime,
				"Double Chance",
				(p-1)*100,
				[]Container{
					Container{"Site": hdc.HomeSite, "Outcome": "1X", "Odd": hdc.Home, "link": hdc.HomeSiteLink, "Stake": ((1 / hdc.Home) * p)},
					// Container{"Site": dc.HASite, "Outcome": "12", "Odd": dc.HomeOr"link":hdc.HomeSiteLinkAway},
					Container{"Site": hdc.AwaySite, "Outcome": "X2", "Odd": hdc.Away, "link": hdc.AwaySiteLink, "Stake": ((1 / hdc.Away) * p)},
				},
			))
		}
	}

	if gng, ok := game.GNG[site]; ok {
		hgng := game.HighestGNG()[0]
		s := false
		if condition(gng.Goal) {
			s = true
			hgng.Goal = gng.Goal
			hgng.GoalSite = site
			hgng.GoalSiteLink = gng.Link
		}
		if condition(gng.NoGoal) {
			s = true
			hgng.NoGoal = gng.NoGoal
			hgng.NoGoalSite = site
			hgng.GoalSiteLink = gng.Link
		}
		p := computeGNGArb(hgng)

		if !s {
			p = 0
		}
		if validArb(p) {
			validArbs = append(validArbs, createArb(
				game.Home,
				game.Away,
				gameTime,
				"Goal/No Goal",
				(p-1)*100,
				[]Container{
					Container{"Site": hgng.GoalSite, "Outcome": "Goal", "link": hgng.GoalSiteLink, "Odd": hgng.Goal, "Stake": ((1 / hgng.Goal) * p)},
					Container{"Site": hgng.NoGoalSite, "Outcome": "NoGoal", "link": hgng.NoGoalSiteLink, "Odd": hgng.NoGoal, "Stake": ((1 / hgng.NoGoal) * p)},
				},
			))
		}
	}

	if dnb, ok := game.DNB[site]; ok {
		hdnb := game.HighestDNB()[0]
		s := false
		if condition(dnb.Home) {
			s = true
			hdnb.Home = dnb.Home
			hdnb.HomeSite = site
			hdnb.HomeSiteLink = dnb.Link
		}
		if condition(dnb.Away) {
			s = true
			hdnb.Away = dnb.Away
			hdnb.AwaySite = site
			hdnb.AwaySiteLink = dnb.Link
		}
		p := computeDNBArb(hdnb)
		if !s {
			p = 0
		}
		if validArb(p) {
			validArbs = append(validArbs, createArb(
				game.Home,
				game.Away,
				gameTime,
				"DNB",
				(p-1)*100,
				[]Container{
					Container{"Site": hdnb.HomeSite, "Outcome": "1", "Odd": hdnb.Home, "link": hdnb.HomeSiteLink, "Stake": ((1 / hdnb.Home) * p)},
					Container{"Site": hdnb.AwaySite, "Outcome": "2", "Odd": hdnb.Away, "link": hdnb.AwaySiteLink, "Stake": ((1 / hdnb.Away) * p)},
				},
			))
		}
	}

	if ou, ok := game.OU15[site]; ok {
		hou := game.HighestOU15()[0]
		s := false
		if condition(ou.Over) {
			s = true
			hou.Over = ou.Over
			hou.OverSite = site
			hou.OverSiteLink = ou.Link
		}
		if condition(ou.Under) {
			s = true
			hou.Under = ou.Under
			hou.UnderSite = site
			hou.UnderSiteLink = ou.Link
		}
		p := computeOUArb(hou)
		if !s {
			p = 0
		}
		if validArb(p) {
			validArbs = append(validArbs, createArb(
				game.Home,
				game.Away,
				gameTime,
				"Over/Under 1.5",
				(p-1)*100,
				[]Container{
					Container{"Site": hou.OverSite, "Outcome": "Over 1.5", "link": hou.OverSiteLink, "Odd": hou.Over, "Stake": ((1 / hou.Over) * p)},
					Container{"Site": hou.UnderSite, "Outcome": "Under 1.5", "link": hou.UnderSiteLink, "Odd": hou.Under, "Stake": ((1 / hou.Under) * p)},
				},
			))
		}
	}

	if ou, ok := game.OU25[site]; ok {
		hou := game.HighestOU25()[0]
		s := false
		if condition(ou.Over) {
			s = true
			hou.Over = ou.Over
			hou.OverSite = site
			hou.OverSiteLink = ou.Link
		}
		if condition(ou.Under) {
			s = true
			hou.Under = ou.Under
			hou.UnderSite = site
			hou.UnderSiteLink = ou.Link
		}
		p := computeOUArb(hou)
		if !s {
			p = 0
		}
		if validArb(p) {
			validArbs = append(validArbs, createArb(
				game.Home,
				game.Away,
				gameTime,
				"Over/Under 2.5",
				(p-1)*100,
				[]Container{
					Container{"Site": hou.OverSite, "Outcome": "Over 2.5", "Odd": hou.Over, "link": hou.OverSiteLink, "Stake": ((1 / hou.Over) * p)},
					Container{"Site": hou.UnderSite, "Outcome": "Under 2.5", "Odd": hou.Under, "link": hou.UnderSiteLink, "Stake": ((1 / hou.Under) * p)},
				},
			))
		}
	}

	if ou, ok := game.OU35[site]; ok {
		hou := game.HighestOU35()[0]
		s := false
		if condition(ou.Over) {
			s = true
			hou.Over = ou.Over
			hou.OverSite = site
			hou.OverSiteLink = ou.Link
		}
		if condition(ou.Under) {
			s = true
			hou.Under = ou.Under
			hou.UnderSite = site
			hou.UnderSiteLink = ou.Link
		}
		p := computeOUArb(hou)
		if !s {
			p = 0
		}
		if validArb(p) {
			validArbs = append(validArbs, createArb(
				game.Home,
				game.Away,
				gameTime,
				"Over/Under 3.5",
				(p-1)*100,
				[]Container{
					Container{"Site": hou.OverSite, "Outcome": "Over 3.5", "Odd": hou.Over, "link": hou.OverSiteLink, "Stake": ((1 / hou.Over) * p)},
					Container{"Site": hou.UnderSite, "Outcome": "Under 3.5", "Odd": hou.Under, "link": hou.UnderSiteLink, "Stake": ((1 / hou.Under) * p)},
				},
			))
		}
	}

	if ou, ok := game.OU45[site]; ok {
		hou := game.HighestOU45()[0]
		s := false
		if condition(ou.Over) {
			s = true
			hou.Over = ou.Over
			hou.OverSite = site
			hou.OverSiteLink = ou.Link
		}
		if condition(ou.Under) {
			s = true
			hou.Under = ou.Under
			hou.UnderSite = site
			hou.UnderSiteLink = ou.Link
		}
		p := computeOUArb(hou)
		if !s {
			p = 0
		}

		if validArb(p) {
			validArbs = append(validArbs, createArb(
				game.Home,
				game.Away,
				gameTime,
				"Over/Under 4.5",
				(p-1)*100,
				[]Container{
					Container{"Site": hou.OverSite, "Outcome": "Over 4.5", "link": hou.OverSiteLink, "Odd": hou.Over, "Stake": ((1 / hou.Over) * p)},
					Container{"Site": hou.UnderSite, "Outcome": "Under 4.5", "link": hou.UnderSiteLink, "Odd": hou.Under, "Stake": ((1 / hou.Under) * p)},
				},
			))
		}
	}

	return validArbs

}
