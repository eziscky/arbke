package bot

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/arbke/scraper"
	"gitlab.com/arbke/utils"
)

var (
	stop chan struct{}
)

func ltToNum(i int) string {
	switch i {
	case 0:
		return "A"
	case 1:
		return "B"
	case 2:
		return "C"
	case 3:
		return "D"
	case 4:
		return "E"
	case 5:
		return "F"
	case 6:
		return "G"
	case 7:
		return "H"
	case 8:
		return "I"
	case 9:
		return "J"
	case 10:
		return "K"
	case 11:
		return "L"
	case 12:
		return "M"
	case 13:
		return "N"
	case 14:
		return "O"
	case 15:
		return "P"
	case 16:
		return "Q"
	case 17:
		return "R"
	case 18:
		return "S"
	case 19:
		return "T"
	case 20:
		return "U"
	case 21:
		return "V"
	case 22:
		return "W"
	case 23:
		return "X"
	case 24:
		return "Y"
	case 25:
		return "Z"
	default:
		return fmt.Sprintf("%d", i)
	}
}
func newBot() *Bot {
	stop = make(chan struct{})
	sites := []scraper.Scraper{
		// scraper.NewFake(), scraper.NewFake2(), scraper.NewFake3()
		scraper.NewDafabet(),
		scraper.NewElitebet(),
		scraper.NewBetika(),
		scraper.NewXbet(),
		scraper.NewBetIn(),
		scraper.NewBetpawa(),
		scraper.NewSpesa(),
		scraper.NewKwikBet(),
		scraper.NewChezacash(),
		scraper.NewBetway(),
	}
	return NewBot(1, 1, stop, sites...)
}

func genGames(site string, num int, today time.Time) []scraper.Game {
	games := []scraper.Game{}

	for i := 0; i < num; i++ {
		games = append(games, scraper.Game{
			Site: site,
			Home: fmt.Sprintf("Home%s", ltToNum(i)),
			Away: fmt.Sprintf("Away%s", ltToNum(i)),
			Time: &today,
		})
	}
	return games
}
func genDGames(site string, num int, today time.Time) []scraper.Game {
	games := []scraper.Game{}

	for i := 0; i < num; i++ {
		games = append(games, scraper.Game{
			Site: site,
			Home: fmt.Sprintf("Team Home%s", ltToNum(i)),
			Away: fmt.Sprintf("Team Away%s", ltToNum(i)),
			Time: &today,
		})
	}
	return games
}

func testValue(t *testing.T) {
	utils.LogError, utils.LogDebug = true, true
	x, y := computeTWAYValue(scraper.HThreeWay{
		Home: 3.25,
		Draw: 3.6,
		Away: 2.89,
	})
	utils.Debug(x, y)

}
func testEnsure(t *testing.T) {
	bot := newBot()
	today := time.Now()

	// p, _ := url.Parse("http://212.49.84.113:65103")
	// transport := &http.Transport{
	// Proxy: http.ProxyURL(p),
	// }

	// sbet := scraper.NewSportybet().GetFootball(transport)
	// utils.Debug(sbet)
	agg := bot.Classify(map[string][]scraper.Game{
		"1XBet": []scraper.Game{
			scraper.Game{
				Home: "KASIMPASA",
				Away: "GENCLERBIRLIGI",
				Site: "1XBet",
				Time: &today,
			},
		},
		"Betway": []scraper.Game{
			scraper.Game{
				Home: "GENCLERBIRLIGI",
				Away: "KASIMPASA",
				Site: "Betway",
				Time: &today,
			},
		},
	})

	utils.Debug(agg)
}
func testClassifier(t *testing.T) {
	bot := newBot()
	today := time.Now()
	n := time.Now().UnixNano()
	utils.Debug(CurrentArbs)
	// num := 2
	// one := time.Now().Add(6 * time.Hour)

	// p, _ := url.Parse("http://212.49.84.113:65103")
	// transport := &http.Transport{
	// 	Proxy: http.ProxyURL(p),
	// }

	// four := time.Now().Add(8 * time.Hour)
	// tomorrow := time.Now().Add(time.Hour * 1)
	res := bot.Classify(map[string][]scraper.Game{
		"Fake1": []scraper.Game{
			scraper.Game{
				Sport: "Football",
				Home:  "HOME1 TEAM",
				Away:  "HOME2 TEAM",
				Site:  "Fake1",
				Time:  &today,
			},
		},
		"Fake2": []scraper.Game{
			scraper.Game{
				Sport: "Football",
				Home:  "HOME1 TEAM",
				Away:  "HOME2 TEAM",
				Site:  "Fake2",
				Time:  &today,
			},
		},
		"Fake3": []scraper.Game{
			scraper.Game{
				Sport: "Icehockey",
				Home:  "HOME1 TEAM",
				Away:  "HOME2 TEAM",
				Site:  "Fake3",
				Time:  &today,
			},
		},
		// "Fake2":  genDGames("Fake2", num, today),
		// "Fake3":  genGames("Fake3", num, today),
		// "Fake4":  genDGames("Fake4", num, today),
		// "Fake5":  genGames("Fake5", num, today),
		// "Fake6":  genDGames("Fake6", num, today),
		// "Fake7":  genGames("Fake7", num, today),
		// "Fake8":  genDGames("Fake8", num, today),
		// "Fake9":  genGames("Fake9", num, today),
		// "Fake10": genDGames("Fake10", num, today),
		// "Elitebet": scraper.NewXbet().GetFootball(nil),
		// "1Xbet":     scraper.NewElitebet().GetTennis(nil),
		// "Dafabet":   scraper.NewDafabet().GetTennis(transport),
		// "Betika":    scraper.NewBetika().GetTennis(nil),
		// "Betin":     scraper.NewBetIn().GetTennis(nil),
		// "Betpawa":   scraper.NewBetpawa().GetTennis(nil),
		// "Spesa":     scraper.NewSpesa().GetTennis(nil),
		// "Kwikbet":   scraper.NewKwikBet().GetTennis(nil),
		// "Chezacash": scraper.NewChezacash().GetTennis(nil),
		// "Betway":    scraper.NewBetway().GetTennis(nil),
		// "Powerbet":  scraper.NewPowerbet().GetTennis(nil),
		// "Lollybet":  scraper.NewLollyybet().GetTennis(nil),
		// "wsbetting": scraper.NewWSBet().GetTennis(nil),
		// "Sportybet": scraper.NewSportybet().GetTennis(nil),

		// "Sportpesa": []scraper.Game{
		// 	scraper.Game{
		// 		Home: "KASIMPASA",
		// 		Away: "GENCLERBIRLIGI",
		// 		Site: "Sportpesa",
		// 		Time: &four,
		// 	},
		// },

	})
	// utils.Debug(res)
	for _, r := range res {
		if len(r.TWAY) > 1 {
			utils.Debug(r.Home, r.Away, r.TWAY)
			utils.Debug(r.HighestTWAY())
		}
	}

	// CurrentArbs = map[string]*Arb{}
	// x, _ := Combine(res, "")
	// for _, i := range x {
	// 	utils.Debug(i)
	// }
	// if num != len(res) {
	// 	t.Errorf("Expected: %d Got: %d", num, len(res))
	// 	return
	// }

	utils.Debug(time.Now().UnixNano() - n)

}

func testStats(t *testing.T) {
	bot := newBot()
	tmp := map[string]string{
		"site":    "Fake",
		"num":     fmt.Sprintf("%d", 10),
		"latency": "2.2",
		"avusage": fmt.Sprintf("%.2f", 100.0),
		"usage":   fmt.Sprintf("%.2f", 100.0),
	}

	bot.stats = append(bot.stats, tmp)
	bot.execCount = 2

	bot.RecStat("Fake", 20, 100, "2.2")
	bot.execCount++
	bot.RecStat("Fake1", 0, 0, "1.1")
	utils.Debug(bot.stats)

}

func testZeroG(t *testing.T) {
	bot := newBot()
	bot.failure["Fake"] = 0

	games := []scraper.Game{
		scraper.Game{
			Home: "KASIMPASA",
			Away: "GENCLERBIRLIGI",
			Site: "Fake",
		},
	}
	bot.Update("Fake", games)
	utils.Debug(bot.failure, bot.footData)

	bot.Update("Fake", []scraper.Game{})
	utils.Debug(bot.failure, bot.footData)

	bot.Update("Fake", []scraper.Game{})
	utils.Debug(bot.failure, bot.footData)

	bot.Update("Fake", []scraper.Game{})
	utils.Debug(bot.failure, bot.footData)

}

func testBetinBonus(t *testing.T) {

	arbs := CustomCriteriaArbs("", scraper.Aggregate{
		Hash: "hash",
		Home: "TeamA",
		Away: "TeamB",
		TWAY: map[string]scraper.ThreeWay{
			"Betin": scraper.ThreeWay{
				Home: 1.09,
				Draw: 2.0,
				Away: 4.6,
			},
			"Betway": scraper.ThreeWay{
				Home: 2.09,
				Draw: 3.0,
				Away: 3.0,
			},
			"1XBet": scraper.ThreeWay{
				Home: 1.09,
				Draw: 2.0,
				Away: 10.0,
			},
		},
	})

	utils.Debug(arbs)
}

func TestVolatilityIndicator(t *testing.T) {
	tm := time.Now().Add(1 * time.Hour)
	CurrentArbs = map[string]*Arb{}
	CurrentValueBets = map[string]*ValueBet{}
	_, _, x := Combine([]scraper.Aggregate{
		scraper.Aggregate{
			Hash: "hash",
			Home: "TeamA",
			Away: "TeamB",
			Time: &tm,
			TWAY: map[string]scraper.ThreeWay{
				"Betin": scraper.ThreeWay{
					Home: 1.09,
					Draw: 2.0,
					Away: 10.6,
				},
				"Betway": scraper.ThreeWay{
					Home: 2.09,
					Draw: 3.0,
					Away: 3.0,
				},
				"1XBet": scraper.ThreeWay{
					Home: 1.09,
					Draw: 3.0,
					Away: 10.0,
				},
				"Betpawa": scraper.ThreeWay{
					Home: 1.08,
					Draw: 2.0,
					Away: 10.6,
				},
				"Betyetu": scraper.ThreeWay{
					Home: 2.09,
					Draw: 2.9,
					Away: 3.0,
				},
				"Betika": scraper.ThreeWay{
					Home: 1.09,
					Draw: 3.0,
					Away: 9.8,
				},
			},
		},
	}, "", false)

	fmt.Println(x)
}

func testZero(t *testing.T) {
	utils.Debug(computeTWOWArb(scraper.HTwoWay{}))
}
