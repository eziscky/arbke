package bot

import (
	"context"
	"crypto/tls"
	"fmt"
	"net/http"
	"runtime/debug"
	"sync"
	"time"

	"gitlab.com/arbke/scraper"
	"gitlab.com/arbke/utils"
)

var (
	WORKER_TIMEOUT time.Duration //seconds
)

type Worker struct {
	ID          int
	processChan chan map[string]interface{}
	sports      []int
}

func NewWorker(id int) *Worker {
	return &Worker{
		ID:          id,
		processChan: make(chan map[string]interface{}),
		sports:      []int{0, 1, 2, 3, 4},
	}
}

func (worker *Worker) Start(workerChan chan chan map[string]interface{}, footChan chan map[string]interface{}) {
	name := ""
	data := []scraper.Game{}
	defer func() {
		if e := recover(); e != nil {
			utils.Error(e, fmt.Sprintf("PANIC!!! %s", string(debug.Stack())))
			footChan <- map[string]interface{}{
				"data": data,
				"name": name,
			}

		}
	}()
	utils.Debug("Worker", worker.ID, " joining work queue")

	for {
		workerChan <- worker.processChan
		data = []scraper.Game{}

		select {
		case payload := <-worker.processChan:

			site := payload["site"].(scraper.Scraper)
			proxy := payload["proxy"].(*Proxy)
			transport := &http.Transport{}

			var _p *ProxyServer
			if site.Name() == "Dafabet" {
				p := proxy.FastestServer()
				utils.Debug("Setting Proxy:", p.Addr.String(), "Latency(ms):", p.ResponseTime, " For: ", site.Name())
				transport = &http.Transport{
					Proxy:           http.ProxyURL(p.Addr),
					TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
				}
				_p = p
			}

			utils.Debug("Worker", worker.ID, " processing ", site.Name())

			tnum := 0
			usage := 0
			recLocks := &sync.Mutex{}
			name = site.Name()
			site.UsageReset()

			start := time.Now().UnixNano()
			failed := false
			aggregator := make(chan []scraper.Game)
			aggDone := make(chan struct{})
			go func() {
				for games := range aggregator {
					data = append(data, games...)
				}
				aggDone <- struct{}{}
			}()
			wg := sync.WaitGroup{}
			wg.Add(len(worker.sports))

			for _, i := range worker.sports {
				go func(i int) {
					ctx, cf := context.WithTimeout(context.Background(), (WORKER_TIMEOUT * time.Second))
					num := 0
					data := []scraper.Game{}
					games := make(chan scraper.Game) //HAS TO BE A BLOCKING CHANNEL
					stopC := make(chan struct{})
					completed := make(chan struct{})
					sport := ""
					switch i {
					case 0:
						sport = "football"
					case 1:
						sport = "tennis"
					case 2:
						sport = "basketball"
					case 3:
						sport = "rugby"
					case 4:
						sport = "icehockey"
					case 5:
						sport = "handball"
					}
					go func(i int) {
						switch i {
						case 0:
							site.GetFootball(transport, games, stopC)
						case 1:
							site.GetTennis(transport, games, stopC)
						case 2:
							site.GetBasketBall(transport, games, stopC)
						case 3:
							site.GetRugby(transport, games, stopC)
						case 4:
							site.GetIceHockey(transport, games, stopC)
						case 5:
							site.GetHandball(transport, games, stopC)
						}

					}(i)

					go func(i int) {
						for game := range games {
							switch i {
							case 0:
								game.Sport = "football"
							case 1:
								game.Sport = "tennis"
							case 2:
								game.Sport = "basketball"
							case 3:
								game.Sport = "rugby"
							case 4:
								game.Sport = "icehockey"
							case 5:
								game.Sport = "handball"

							}
							// game.Site = site.Name()
							// utils.Debug(game.Site, game.Sport)
							data = append(data, game)
							num++
							// if num == 50 {
							// 	stopC <- struct{}{}
							// 	break
							// }
							//LIMIT MAX GAMES HERE
						}
						completed <- struct{}{}

					}(i)
					timedOut := false
					select {
					case <-completed:
						utils.Debug("COMPLETED")
						break
					case <-ctx.Done():
						utils.Error("Timeout", site.Name(), ctx.Err().Error())
						stopC <- struct{}{}
						timedOut = true
						break
					}
					cf()
					if timedOut {
						go func() {
							select {
							case <-completed:
								utils.Debug("TIMEOUT COMPLETED")
								break
							}
						}()
					}
					tmp := site.Usage()
					recLocks.Lock()
					usage += tmp
					tnum += num
					recLocks.Unlock()

					aggregator <- data

					wg.Done()
					if len(data) == 0 {
						// failed = true
						utils.Debug("No ", sport, "data on", name)
					}
				}(i)
			}
			wg.Wait()
			close(aggregator) //stop aggregator service
			<-aggDone
			stop := time.Now().UnixNano()

			utils.Debug(site.Name(), tnum, "Latency(ms):", (stop-start)/1000000)

			if _p != nil {
				_p.ResponseTime = (stop - start)
				_p.Up = !failed
				if len(data) == 0 { //assume no data is proxy failure
					_p.Up = false
				}
			}
			footChan <- map[string]interface{}{
				"data":    data,
				"name":    name,
				"usage":   usage,
				"num":     tnum,
				"latency": fmt.Sprintf("%.2f", float64((stop-start))/1000000000.0),
			}

		}
	}
}
