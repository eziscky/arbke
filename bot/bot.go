package bot

import (
	"crypto/sha1"
	"fmt"
	"math/rand"
	"runtime/debug"
	"strconv"
	"strings"
	"time"

	"gitlab.com/arbke/scraper"
	"gitlab.com/arbke/utils"
)

type Bot struct {
	interval   time.Duration
	sites      []scraper.Scraper
	execCount  int
	numWorkers int
	stop       chan struct{}
	proxy      *Proxy
	stats      []map[string]string
	failure    map[string]int

	//channel of worker channels
	workerChan chan chan map[string]interface{}

	//results from worker
	footChan chan map[string]interface{}

	//storage
	footData map[string][]scraper.Game
}

func NewBot(interval int64, numWorkers int, stop chan struct{}, sites ...scraper.Scraper) *Bot {
	return &Bot{
		interval:   time.Duration(interval),
		sites:      sites,
		stop:       stop,
		footData:   map[string][]scraper.Game{},
		numWorkers: numWorkers,
		proxy:      NewProxy(),
		failure:    map[string]int{},
		workerChan: make(chan chan map[string]interface{}),
		footChan:   make(chan map[string]interface{}, numWorkers),
	}
}

func (bot *Bot) Shuffle() {
	for i := range bot.sites {
		j := rand.Intn(i + 1)
		bot.sites[i], bot.sites[j] = bot.sites[j], bot.sites[i]
	}
}

func (bot *Bot) CurrentProxyAddr() string {
	return bot.proxy.currentAddr
}
func (bot *Bot) Start(resp chan map[string][]scraper.Aggregate) {
	fmt.Println("Starting bot")
	ticker := time.NewTicker(bot.interval * time.Second)
	bot.execCount = 1
	for _, site := range bot.sites {
		bot.failure[site.Name()] = 0
	}

	CurrentArbs = map[string]*Arb{}
	CurrentValueBets = map[string]*ValueBet{}
	//
	go func() {
		//initial exec
		bot.Execute(resp)
		for {
			select {
			//every interval
			case <-ticker.C:
				fmt.Println("Executing bot")
				//create wg

				bot.Execute(resp)
			}
			bot.execCount++
		}
	}()

	//recover from worker panics gracefully
	restartChan := make(chan int)

	//worker restarter
	go func(rc chan int) {
		for id := range rc {
			w := NewWorker(id)
			go func(id int) {
				defer func() {
					if e := recover(); e != nil {
						utils.Error(e, fmt.Sprintf("PANIC!!! %s", string(debug.Stack())))
					}
					rc <- id
				}()

				utils.Debug("RESTARTING WORKER", id)
				w.Start(bot.workerChan, bot.footChan)
			}(id)
		}
	}(restartChan)
	//

	//start workers
	fmt.Println("Starting workers")
	for i := 0; i < bot.numWorkers; i++ {
		id := i + 1
		w := NewWorker(id)
		go func(id int, rc chan int) {
			defer func() {
				if e := recover(); e != nil {
					utils.Error(e, fmt.Sprintf("PANIC!!! %s", string(debug.Stack())))
				}
				rc <- id
			}()
			w.Start(bot.workerChan, bot.footChan)
		}(id, restartChan)
	}

	<-bot.stop
	ticker.Stop()

}

func (bot *Bot) Update(name string, data []scraper.Game) {
	if len(data) == 0 {
		utils.Debug("Ignoring empty data", name)
		utils.Debug("Previous Data ", name, len(bot.footData[name]), bot.failure[name])
		bot.failure[name]++
		if bot.failure[name] >= 3 {
			bot.footData[name] = data
		}
		return
	}
	bot.footData[name] = data
	bot.failure[name] = 0
}

func (bot *Bot) Execute(resp chan map[string][]scraper.Aggregate) {
	defer func() {
		if e := recover(); e != nil {
			utils.Error(e, fmt.Sprintf("PANIC!!! %s", string(debug.Stack())))

		}
	}()
	bot.Shuffle()
	go func() {
		for _, site := range bot.sites {
			// proxy := false
			worker := <-bot.workerChan
			worker <- map[string]interface{}{
				"site":  site,
				"proxy": bot.proxy,
			}

		}
	}()

	usage := 0

	total := 0
	for {
		data := <-bot.footChan

		bot.Update(data["name"].(string), data["data"].([]scraper.Game))

		if _, ok := data["usage"]; ok {
			usage += data["usage"].(int)
		}
		total++

		bot.RecStat(data["name"].(string), data["num"].(int), data["usage"].(int), data["latency"].(string))
		// utils.Debug(bot.stats)
		if total >= len(bot.sites) {
			break
		}
		// Combine(footData)
	}

	utils.Debug(fmt.Sprintf("========= EXEC: %d USAGE: %d KB =========", bot.execCount, usage/1000))
	resp <- map[string][]scraper.Aggregate{
		"football": bot.Classify(bot.footData),
	}

}

//Aggregate similar games
//Empty game names are fatal
func (bot *Bot) Classify(data map[string][]scraper.Game) []scraper.Aggregate {

	records := map[string][]scraper.Game{}

	getValidTime := func(key string) *time.Time {
		for _, g := range records[key] {
			if g.Time != nil {
				return g.Time
			}
		}
		return nil
	}

	deepCheck := func(records map[string][]scraper.Game, game scraper.Game) (string, bool) {
		for k, v := range records {
			for _, rec := range v {
				rhome := rmSpecial(rm(strings.ToUpper(rec.Home)))
				raway := rmSpecial(rm(strings.ToUpper(rec.Away)))
				home := rmSpecial(rm(strings.ToUpper(game.Home)))
				away := rmSpecial(rm(strings.ToUpper(game.Away)))

				homematch := strings.Contains(rhome, home) || strings.Contains(home, rhome)
				awaymatch := strings.Contains(raway, away) || strings.Contains(away, raway)

				// utils.Debug(homematch, awaymatch, rec.Time.Equal(*game.Time))
				if (homematch && awaymatch) && rec.Time.Equal(*game.Time) && (rec.Sport == game.Sport) {
					// utils.Debug("DEEPEST MATCH", home, game.Site, rhome, rec.Site, away, raway)
					return k, true
				}
			}
		}
		return "", false
	}

	emptyName := func(home, away string) bool {
		h := strings.Replace(home, " ", "", -1)
		a := strings.Replace(away, " ", "", -1)

		return len(h) == 0 || len(a) == 0

	}
	//for each site
	for _, v := range data {

		//for each game within site
		for _, game := range v {

			//protect from empty game names
			if emptyName(game.Home, game.Away) {
				utils.Error("EMPTY NAME DETECTED:", game)
				continue
			}

			//remove short forms
			home := rmSpecial(rm(game.Home))
			away := rmSpecial(rm(game.Away))
			gameHash := fmt.Sprintf("%s%s", bot.hash(home, game.Sport), bot.hash(away, game.Sport))

			if _, ok := records[gameHash]; ok {

				// utils.Debug("MATCH", game.Home, game.Site, records[gameHash])
				if records[gameHash][0].Time.Equal(*game.Time) {
					if records[gameHash][0].Site == game.Site {
						utils.Debug("DUPLICATE DETECTED", game)
						continue
					}
					// if records[gameHash][0].Sport != game.Sport {
					// 	utils.Debug("Same name diff sport", game)
					// 	records[gameHash] = []scraper.Game{game}
					// 	continue
					// }
					records[gameHash] = append(records[gameHash], game)
					continue
				} else {
					if rec, ok := deepCheck(records, game); ok {
						records[rec] = append(records[rec], game)
						continue
					}
				}

			} else {
				if rec, ok := deepCheck(records, game); ok {
					records[rec] = append(records[rec], game)
					continue
				}
			}
			records[gameHash] = []scraper.Game{game}
		}
	}

	agg := []scraper.Aggregate{}

	//Aggregate
	for k, v := range records {
		// utils.Debug(k, v)
		tmp := scraper.Aggregate{}
		tmp.Hash = k
		tmp.HashRecs = v

		tmp.TWAY = map[string]scraper.ThreeWay{}
		tmp.TWOWAY = map[string]scraper.TwoWay{}
		tmp.DC = map[string]scraper.DoubleChance{}
		tmp.DNB = map[string]scraper.DrawNoBet{}
		tmp.GNG = map[string]scraper.GoalNoGoal{}
		tmp.OU15 = map[string]scraper.OU15{}
		tmp.OU25 = map[string]scraper.OU25{}
		tmp.OU35 = map[string]scraper.OU35{}
		tmp.OU45 = map[string]scraper.OU45{}
		tmp.Time = getValidTime(k)
		for _, g := range v {
			tmp.Home = g.Home
			tmp.Away = g.Away
			tmp.Sport = g.Sport
			if len(g.League) > 0 {
				tmp.League = g.League
			}

			tmp.TWAY[g.Site] = g.TWAY
			tmp.TWOWAY[g.Site] = g.TWOWAY
			tmp.DC[g.Site] = g.DC
			tmp.DNB[g.Site] = g.DNB
			tmp.GNG[g.Site] = g.GNG
			tmp.OU15[g.Site] = g.OU15
			tmp.OU25[g.Site] = g.OU25
			tmp.OU35[g.Site] = g.OU35
			tmp.OU45[g.Site] = g.OU45

		}

		if strings.Contains(strings.ToLower(tmp.Home), "ankaragucu") {
			utils.Debug(tmp)
		}
		// utils.Debug(tmp)
		//CHECK HERE TO SEE CLASSIFICATION

		agg = append(agg, tmp)
	}

	return agg
}

func (bot *Bot) RecStat(name string, num, _usage int, latency string) {
	usage := float64(_usage) / 1000000.0
	tmp := map[string]string{
		"site":      name,
		"total":     fmt.Sprintf("%d", num),
		"num":       fmt.Sprintf("%d", num),
		"latency":   latency,
		"avlatency": latency,
		"avusage":   fmt.Sprintf("%.2f", float64(_usage)/1000000.0),
		"usage":     fmt.Sprintf("%.2f", float64(_usage)/1000000.0),
	}
	index := -1
	for i, stat := range bot.stats {
		if stat["site"] == name {
			index = i

		}
	}
	if index < 0 {
		bot.stats = append(bot.stats, tmp)
		return
	}
	// avnum :=
	if num != 0 {
		ptotal, _ := strconv.Atoi(bot.stats[index]["total"])
		ptotal += num
		avnum := ptotal / bot.execCount
		bot.stats[index]["num"] = fmt.Sprintf("%d", avnum)
		bot.stats[index]["total"] = fmt.Sprintf("%d", ptotal)
	}

	if _usage != 0 {
		pusage, _ := strconv.ParseFloat(bot.stats[index]["usage"], 64)
		pusage += usage
		avusage := pusage / float64(bot.execCount)
		bot.stats[index]["avusage"] = fmt.Sprintf("%.2f", avusage)
		bot.stats[index]["usage"] = fmt.Sprintf("%.2f", pusage)
	}

	lat, _ := strconv.ParseFloat(latency, 64)
	plat, _ := strconv.ParseFloat(bot.stats[index]["latency"], 64)
	plat += lat
	avlat := plat / float64(bot.execCount)
	bot.stats[index]["latency"] = fmt.Sprintf("%.2f", plat)
	bot.stats[index]["avlatency"] = fmt.Sprintf("%.2f", avlat)

	// bot.sites[i] =

}
func (bot *Bot) Stats() []map[string]string {
	return bot.stats
}
func (bot *Bot) GetProxies() []*ProxyServer {
	return bot.proxy.servers
}

func (bot *Bot) prep(str string) string {
	str = strings.ToLower(str)
	str = strings.Trim(str, " ")
	str = strings.Replace(str, " ", "", -1)
	return str
}

func (bot *Bot) hash(str, app string) string {
	s := fmt.Sprintf("%x", sha1.Sum([]byte(bot.prep(str)+app)))
	return s
}

type Recipient struct {
	ID int64
}

func (r Recipient) Recipient() string {
	return fmt.Sprintf("%d", r.ID)
}

func GetRecipient(id int64) Recipient {
	return Recipient{
		ID: id,
	}
}
