package bot

import (
	"time"
)

type Container map[string]interface{}
type Arb struct {
	ID              string
	Sport           string
	Reliability     string
	Name            string
	Home            string
	Away            string
	League          string
	Time            string
	Proxy           string
	CombinationHash string
	TimeObj         *time.Time
	Updated         bool
	Market          string
	Combinations    []Container
	Percentage      float64
}
type ValueBet struct {
	ID         string
	Sport      string
	Name       string
	Home       string
	Away       string
	League     string
	Time       string
	TimeObj    *time.Time
	Updated    bool
	Market     string
	Outcome    string
	Percentage float64
}

type PremiumPayload struct {
	Arbs      []Arb
	ValueBets []ValueBet
}
