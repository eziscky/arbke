package bot

import (
	"net/url"
	"sync"

	"gitlab.com/arbke/utils"
)

type ProxyServer struct {
	Addr         *url.URL
	Up           bool
	ResponseTime int64
	Selections   int
}

type Proxy struct {
	current     int
	currentAddr string
	proxies     []string
	servers     []*ProxyServer
	length      int
	lock        *sync.Mutex
}

func NewProxy() *Proxy {
	p := Proxy{
		lock: &sync.Mutex{},
		proxies: []string{
			"http://197.232.21.141:59075",
			"http://212.49.84.113:65103",
			"http://41.79.169.89:8080",
			"http://197.232.36.43:8080",
			"http://41.203.215.126:60124",
			"http://41.217.220.26:8080",
			"http://196.202.176.114:23500",
		},
	}
	for _, proxy := range p.proxies {
		parsed, _ := url.Parse(proxy)
		p.servers = append(p.servers, &ProxyServer{
			Addr: parsed,
			Up:   true,
		})
	}
	p.current = -1
	p.length = len(p.proxies)
	return &p
}

func (proxy *Proxy) Next() int {
	proxy.lock.Lock()
	defer proxy.lock.Unlock()

	proxy.current++

	if proxy.current >= proxy.length {
		proxy.current = 0
	}
	// utils.Debug("Proxy index", proxy.current, " Link:", proxy.proxies[proxy.current])

	return proxy.current
}
func (proxy *Proxy) GetServer(addr string) *ProxyServer {
	for _, server := range proxy.servers {
		if server.Addr.String() == addr {
			return server
		}
	}
	parsed, _ := url.Parse(addr)
	return &ProxyServer{
		Addr: parsed,
		Up:   true,
	}
}
func (proxy *Proxy) FastestServer() *ProxyServer {
	candidates := []*ProxyServer{}
	for _, server := range proxy.servers {
		if server.Up {
			candidates = append(candidates, server)
		}
	}
	if len(candidates) == 0 {
		utils.Error("All proxies down")
		tmp := proxy.Next()
		proxy.servers[tmp].Selections++
		proxy.currentAddr = proxy.servers[tmp].Addr.String()
		return proxy.servers[tmp]
	}

	sel := proxy._select(candidates)
	for {
		if candidates[sel].Selections >= 30 {
			if (len(candidates) - 1) > 0 {
				candidates[sel].Selections = 0
				candidates = append(candidates[:sel], candidates[sel+1:]...)
				sel = proxy._select(candidates)
			} else {
				tmp := proxy.Next()
				proxy.servers[tmp].Selections++
				proxy.currentAddr = proxy.servers[tmp].Addr.String()
				return proxy.servers[tmp]
			}

		} else {
			break
		}
	}
	proxy.servers[sel].Selections++
	for _, s := range proxy.servers {
		utils.Debug(*s)
	}
	proxy.currentAddr = candidates[sel].Addr.String()
	return candidates[sel]
}
func (proxy *Proxy) _select(servers []*ProxyServer) int {
	least := int64(0)
	index := 0
	for i, server := range servers {
		if i == 0 {
			least = server.ResponseTime
			continue
		}
		if server.ResponseTime < least {
			index = i
			least = server.ResponseTime
		}
	}
	return index
}
