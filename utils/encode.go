package utils

import (
	"encoding/base64"
)

func GetB64(data string) string {

	return base64.StdEncoding.EncodeToString([]byte(data))
}
