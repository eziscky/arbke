package utils

import (
	"fmt"
	"log"
	"runtime"
	"sync"

	"github.com/fatih/color"
)

var (
	//LogDebug enables debug log display, off by default
	LogDebug = true
	//LogError enables error log display,on by default
	LogError = true
	//LogInfo enables info log display,off by default
	LogInfo bool
	red     = color.New(color.FgRed)
	blue    = color.New(color.FgBlue)
	green   = color.New(color.FgGreen)
	Debugs  = []string{}
	Errors  = []string{}

	//ErrCountChan is used for communication to count systemwide errors
	ErrCountChan chan int
	errCount     = 0
	lock         = sync.RWMutex{}
)

//Error logs serious application errors
func Error(data ...interface{}) {
	if !LogError {
		return
	}
	_, file, line, ok := runtime.Caller(1)
	if ok {
		red.Print("[ERROR]:")
		log.Println(fmt.Sprintf("%s Line: %d ", file, line), data)
	}
	// go func() {
	// 	lock.Lock()
	// 	defer lock.Unlock()
	// 	Errors = append(Errors, fmt.Sprintf("%v", data))
	// }()
	// ErrCountChan <- 1
}

//Debug logs application debug data
func Debug(data ...interface{}) {
	if !LogDebug {
		return
	}
	_, file, line, ok := runtime.Caller(1)
	if ok {
		blue.Print("[DEBUG]:")
		log.Println(fmt.Sprintf("%s Line: %d ", file, line), data)
	}
	// go func() {
	// 	lock.Lock()
	// 	defer lock.Unlock()
	// 	Debugs = append(Debugs, fmt.Sprintf("%v", data))
	// }()
}

//Info logs application general information
func Info(data ...interface{}) {
	if !LogInfo {
		return
	}
	_, file, line, ok := runtime.Caller(1)
	if ok {
		green.Print("[INFO]:")
		log.Println(fmt.Sprintf("%s Line: %d ", file, line), data)
	}
}

//GetErrCount gets the current systemwide error count
func GetErrCount() int {
	lock.Lock()
	defer lock.Unlock()
	return errCount
}

//AddErrCount increaments the systemwide error count
func AddErrCount() {
	lock.Lock()
	defer lock.Unlock()
	errCount++
}

func GetLogs() []string {
	lock.Lock()
	defer lock.Unlock()
	errs := Errors
	debs := Debugs
	comb := []string{}
	comb = append(comb, debs...)
	comb = append(comb, errs...)
	return comb
}

//StartErrorCounter starts the error counting service
func StartErrorCounter() {
	for range ErrCountChan {
		AddErrCount()
	}

}
