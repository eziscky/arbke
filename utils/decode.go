package utils

import (
	"compress/gzip"
	"fmt"
	"io"
	"net/url"
	"strconv"
	"strings"
	"time"
)

func DecodeGZIP(resp io.ReadCloser) io.Reader {
	reader, err := gzip.NewReader(resp)
	if err != nil {
		Error(err)
		return nil
	}

	if err1 := reader.Close(); err1 != nil || err != nil {
		Error(err)
		return nil
	}
	return reader

}

func GetPostVars(data map[string]string) (*url.Values, *strings.Reader) {
	form := &url.Values{}
	for k, v := range data {
		form.Add(k, v)
	}
	return form, strings.NewReader(form.Encode())
}

func DecodeTime(name, timestr string) *time.Time {
	year, month, day, hour, minute := 0, 0, 0, 0, 0
	today := time.Now()
	datetime := strings.Split(timestr, " ")
	switch name {
	case "Betika", "Betway", "Betpawa":
		//2018-05-10 15:00:00
		date := strings.Split(datetime[0], "-")
		t := strings.Split(datetime[1], ":")

		year, _ = strconv.Atoi(date[0])
		month, _ = strconv.Atoi(date[1])
		day, _ = strconv.Atoi(date[2])
		hour, _ = strconv.Atoi(t[0])
		minute, _ = strconv.Atoi(t[1])

	case "Betin":
		//10 May 13:00
		day, _ = strconv.Atoi(datetime[0])
		month = monthToInt(datetime[1])
		year = today.Year()
		t := strings.Split(datetime[2], ":")
		hour, _ = strconv.Atoi(t[0])
		minute, _ = strconv.Atoi(t[1])

	case "Elitebet":
		//Thu 10 May 16:00
		day, _ = strconv.Atoi(datetime[1])
		month = monthToInt(datetime[2])
		t := strings.Split(datetime[3], ":")
		hour, _ = strconv.Atoi(t[0])
		minute, _ = strconv.Atoi(t[1])
		year = today.Year()
	case "Dafabet":
		year, _ = strconv.Atoi(fmt.Sprintf("20%s", timestr[0:2]))
		month, _ = strconv.Atoi(fmt.Sprintf("%s", timestr[2:4]))
		day, _ = strconv.Atoi(fmt.Sprintf("%s", timestr[4:6]))
		hour, _ = strconv.Atoi(fmt.Sprintf("%s", timestr[6:8]))
		minute, _ = strconv.Atoi(fmt.Sprintf("%s", timestr[8:]))
	case "Kwikbet":
		datetime := strings.Split(strings.Trim(strings.Replace(timestr, "\t", "", -1), " "), "\n")
		s := strings.Split(strings.Trim(datetime[2], " "), " ")[0]
		my := strings.Split(strings.Trim(datetime[1], " "), "/")
		day, _ = strconv.Atoi(my[0])
		month, _ = strconv.Atoi(my[1])
		year = today.Year()
		t := strings.Split(s, ":")
		hour, _ = strconv.Atoi(t[0])
		minute, _ = strconv.Atoi(t[1])
	case "Chezacash":
		// 18-07-18 10:00
		date := strings.Split(strings.Trim(datetime[0], " "), "-")
		year, _ = strconv.Atoi(fmt.Sprintf("20%s", date[2]))
		month, _ = strconv.Atoi(fmt.Sprintf("%s", date[1]))
		day, _ = strconv.Atoi(fmt.Sprintf("%s", date[0]))

		t := strings.Split(strings.Trim(datetime[1], " "), ":")
		hour, _ = strconv.Atoi(t[0])
		minute, _ = strconv.Atoi(t[1])
	case "Lollybet":
		//2018-07-18T20:00:00

		datetime := strings.Split(timestr, "T")

		date := strings.Split(datetime[0], "-")

		year, _ = strconv.Atoi(date[0])
		month, _ = strconv.Atoi(date[1])
		day, _ = strconv.Atoi(date[2])

		// Debug(timestr, date, year, month, day)
		t := strings.Split(strings.Trim(datetime[1], " "), ":")
		hour, _ = strconv.Atoi(t[0])
		minute, _ = strconv.Atoi(t[1])

	case "Powerbet":
		datetime := strings.Split(timestr, "T")

		date := strings.Split(datetime[0], "-")

		year, _ = strconv.Atoi(date[0])
		month, _ = strconv.Atoi(date[1])
		day, _ = strconv.Atoi(date[2])

		// Debug(timestr, date, year, month, day)
		t := strings.Split(strings.Trim(datetime[1], " "), ":")
		hour, _ = strconv.Atoi(t[0])
		minute, _ = strconv.Atoi(t[1])

		//17:00 27/09
	case "Odibet":
		// Debug(timestr, datetime)
		t := strings.Split(strings.Trim(datetime[0], " "), ":")
		hour, _ = strconv.Atoi(t[0])
		minute, _ = strconv.Atoi(t[1])
		date := strings.Split(datetime[1], "/")

		year = today.Year()
		month, _ = strconv.Atoi(date[1])
		day, _ = strconv.Atoi(date[0])

	}
	parsed := time.Date(year, time.Month(month), day, hour, minute, 0, 0, today.Location())
	return &parsed
}

func monthToInt(month string) int {
	switch strings.ToLower(month) {
	case "jan", "january":
		return 1
	case "feb", "february":
		return 2
	case "mar", "march":
		return 3
	case "apr", "april":
		return 4
	case "may":
		return 5
	case "jun", "june":
		return 6
	case "jul", "july":
		return 7
	case "aug", "august":
		return 8
	case "sept", "september":
		return 9
	case "oct", "october":
		return 10
	case "nov", "november":
		return 11
	case "dec", "december":
		return 12
	default:
		return -1
	}
}
