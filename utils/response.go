package utils

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os/exec"
	"time"
)

//Response represents the general response format for all requests
type Response struct {
	Success int         `json:"success"`
	Message interface{} `json:"message"`
}

//Rerror responds with a server error message
func Rerror(rw http.ResponseWriter) {
	data, _ := json.Marshal(Response{Success: 0, Message: "Server Error"})
	rw.Write(data)
}

//Rsuccess responds with a 200 and a generic message
func Rsuccess(rw http.ResponseWriter, message interface{}) {
	data, _ := json.Marshal(Response{Success: 1, Message: message})
	rw.Write(data)
}

//GetRsuccess returns the success response template
func GetRsuccess(message interface{}) []byte {
	data, _ := json.Marshal(Response{Success: 1, Message: message})
	return data
}

//GetRerror returns the error response template
func GetRerror(message interface{}) []byte {
	data, _ := json.Marshal(Response{Success: 0, Message: message})
	return data
}

//FetchExternalResponse carries out a GET request using the linux 'curl' tool
func FetchExternalResponse(url string) ([]byte, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Second)
	defer cancel()

	cmd := exec.CommandContext(ctx, "curl", fmt.Sprintf("%s", url))

	out, err := cmd.Output()
	if err != nil {
		Error(string(out), err)
		return nil, err
	}
	if ctx.Err() == context.DeadlineExceeded {
		return nil, errors.New("timeout")
	}

	return out, nil
}

///constructParams converts a map to a url encoded parameter string
func constructParams(data map[string]string) string {
	js, _ := json.Marshal(data)
	return string(js)

}

//FetchExternalPostResponse carries out a POST request using the linux 'curl' tool
func FetchExternalPostResponse(url string, data map[string]string) ([]byte, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Second)
	defer cancel()

	str := constructParams(data)
	Debug(str)
	cmd := exec.CommandContext(ctx, `curl -X POST --header "Content-Type:application/json" --header "Authorization:Bearer QGT0yirxIVmsj90hXQnQHrQbyygx" -d '{"ConfirmationURL":"http://google.com","ResponseType":"Completed","ShortCode":"910068","ValidationURL":"http://google.com"}' https://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl`)

	out, err := cmd.Output()
	if err != nil {
		Error(string(out))
		return nil, err
	}
	if ctx.Err() == context.DeadlineExceeded {
		return nil, errors.New("timeout")
	}

	return out, nil
}
