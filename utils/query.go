package utils

import (
	"fmt"
	"log"

	r "gopkg.in/gorethink/gorethink.v3"
	"gopkg.in/mgo.v2/bson"
)

type DB struct {
	*r.Session
}

//variable for ability to programmatically change during tests
var DB_NAME = "arb"

const (
	USER_TBL = "users"
)

type MergeOpts struct {
	FieldName string
	Table     string
}
type Query struct {
	dbSession *DB
}

func NewQuery(db *DB) *Query {
	return &Query{db}
}

func (q *Query) GetOne(col string, query map[string]interface{}, target interface{}, mergeOpts ...MergeOpts) error {
	var s *r.Cursor
	var err error
	res := r.DB(DB_NAME).Table(col).Filter(query)
	if len(mergeOpts) > 0 {
		s, err = res.Merge(func(t r.Term) interface{} {
			merge := map[string]interface{}{}
			for _, v := range mergeOpts {
				merge[v.FieldName] = r.Table(v.Table).Get(t.Field(v.FieldName))
			}
			return merge
		}).Run(q.dbSession)
	} else {
		s, err = res.Run(q.dbSession)
	}
	if err != nil {
		Error(err)
		return err
	}
	if s.IsNil() {
		return fmt.Errorf("%#v returned no result", query)
	}
	return s.One(target)

}

//GetManyMatch gets many results from a regex match of a particular field
//expects a map[string]interface query with field and pattern keys
//prefix pattern with (?i) to have a case insensitive match
func (q *Query) GetManyMatch(col string, query bson.M, target interface{}, mergeOpts ...MergeOpts) error {
	var s *r.Cursor
	var err error
	res := r.DB(DB_NAME).Table(col).Filter(func(row r.Term) r.Term {
		return row.Field(query["field"]).Match(query["pattern"])
	})
	if len(mergeOpts) > 0 {
		s, err = res.Merge(func(t r.Term) interface{} {
			merge := map[string]interface{}{}
			for _, v := range mergeOpts {
				merge[v.FieldName] = r.Table(v.Table).GetAll(r.Args(t.Field(v.FieldName))).CoerceTo("array")
			}
			return merge
		}).Run(q.dbSession)
	} else {
		s, err = res.Run(q.dbSession)
	}
	if err != nil {
		Error(err)
		return err
	}
	if s.IsNil() {
		return fmt.Errorf("%#v returned no result", query)
	}
	return s.All(target)

}

func (q *Query) GetMany(col string, query bson.M, target interface{}, mergeOpts ...MergeOpts) error {
	var s *r.Cursor
	var err error
	res := r.DB(DB_NAME).Table(col).Filter(query)
	if len(mergeOpts) > 0 {
		s, err = res.Merge(func(t r.Term) interface{} {
			merge := map[string]interface{}{}
			for _, v := range mergeOpts {
				merge[v.FieldName] = r.Table(v.Table).Get(t.Field(v.FieldName))
			}
			return merge
		}).Run(q.dbSession)
	} else {
		s, err = res.Run(q.dbSession)
	}

	if err != nil {
		return err
	}
	if s.IsNil() {
		return fmt.Errorf("%#v returned no result", query)
	}
	return s.All(target)
}

func (q *Query) checkIndex(col, index string) error {
	res := r.DB(DB_NAME).Table(col).IndexList()
	data, err := res.Run(q.dbSession)
	defer data.Close()
	if err != nil {
		Error(err)
		return err
	}
	in := ""
	ins := []string{}
	for data.Next(&in) {
		ins = append(ins, in)
	}

	exists := func() bool {
		for _, in := range ins {
			if in == index {
				return true
			}
		}
		return false
	}
	if !exists() {
		_, err = r.DB(DB_NAME).Table(col).IndexCreate(index).RunWrite(q.dbSession)
		if err != nil {
			Error(err)
			// return err
		}
	}
	return nil
}

func (q *Query) GetManyOrdered(col string, query bson.M, target interface{}, index string, descending, primary bool) error {
	var s *r.Cursor
	var err error
	var res r.Term

	if !primary { //if the index is not a primary key
		err = q.checkIndex(col, index)
		if err != nil {
			return err
		}
	}

	res = r.DB(DB_NAME).Table(col)

	if descending {
		res = res.OrderBy(r.OrderByOpts{Index: r.Desc(index)})
	} else {
		res = res.OrderBy(r.OrderByOpts{Index: r.Asc(index)})
	}
	res = res.Filter(query)

	s, err = res.Run(q.dbSession)

	if err != nil {
		return err
	}
	if s.IsNil() {
		return fmt.Errorf("%#v returned no result", query)
	}
	return s.All(target)
}
func (q *Query) Insert(col string, data ...interface{}) error {
	_, err := r.DB(DB_NAME).Table(col).Insert(data).RunWrite(q.dbSession)
	return err
}

func (q *Query) Update(col string, query bson.M, target interface{}) error {
	_, err := r.DB(DB_NAME).Table(col).Filter(query).Update(target).RunWrite(q.dbSession)
	return err
}

func (q *Query) Delete(col string, query bson.M) error {
	return r.DB(DB_NAME).Table(col).Filter(query).Delete().Exec(q.dbSession)
}

func Setup(db *DB) {
	var tbls []string
	tables := map[string]string{
		USER_TBL: "email",
	}
	t, err := r.DB(DB_NAME).TableList().Run(db)
	if err != nil {
		log.Fatal(err)
	}
	defer t.Close()
	err = t.All(&tbls)
	if err != nil {
		log.Fatal(err)
	}
	exists := func(name string) bool {
		for _, tbl := range tbls {
			if tbl == name {
				return true
			}
		}
		return false
	}
	for k, v := range tables {
		if exists(k) {
			continue
		}
		Debug("Creating", k, v)

		if len(v) > 0 {
			err = r.DB(DB_NAME).TableCreate(k, r.TableCreateOpts{
				PrimaryKey: v,
			}).Exec(db)
		} else {
			err = r.DB(DB_NAME).TableCreate(k).Exec(db)
		}

		if err != nil {
			log.Fatal(err)
		}
	}
}
