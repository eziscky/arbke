package scraper

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"runtime/trace"
	"strings"
	"testing"
	"time"

	"github.com/sclevine/agouti"
	"gitlab.com/arbke/utils"
)

func TestScraper(t *testing.T) {
	f, err := os.Create("trace.out")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	err = trace.Start(f)
	if err != nil {
		panic(err)
	}
	defer trace.Stop()
	utils.LogError, utils.LogDebug = true, true
	sites := []Scraper{

		// === no GZIP + proxy required outside 254 === //
		// NewDafabet(),

		// === TEST SITE === //
		// NewFake(),

		// NewSportybet(),

		// === no GZIP + no proxy required === //

		// NewElitebet(),
		// NewBetpawa(),
		// NewBetIn(),
		// NewKwikBet(),
		// NewChezacash(),
		// NewXbet(),
		// NewLollyybet(),
		// NewPowerbet(),
		// NewWSBet(),

		// NewGMania(),
		// NewMozzart(),

		// NewOdibet(),
		//======== GZIP + no proxy==========//
		// NewBetway(),
		// NewSpesa(),
		// // //
		NewBetika(),
	}
	p, err := url.Parse("http://196.202.176.114:23500")
	utils.Error(err, p)
	transport := &http.Transport{
		// Proxy: http.ProxyURL(p),
	}

	// browser := surf.NewBrowser()
	// browser.Open("https://betyetu.co.ke/sportsbook/SOCCER/EN_D1/1375550/")
	// ioutil.WriteFile("out.html", []byte(browser.Body()), 0777)
	// return
	// pb := NewPowerbet()
	// utils.Debug(pb.GetRugbyGame(transport, "", "{\"subEventId\":60728921,\"offset\":0,\"limit\":100}"))
	// return
	// numGames := 5

	// for _, site := range sites {
	// 	utils.Debug(site.GetFootballGame(transport, "", "https://www.chezacash.com/main/index/details/16142220"))
	// }
	// return

	// betika := NewBetika()
	// utils.Debug(betika.getCategories(28))
	// utils.Debug(betika.getCategories(30))
	// utils.Debug(betika.getCategories(41))

	// return

	for _, site := range sites {
		fmt.Println("---------------", site.Name(), "------------------")
		data := []Game{}
		games := make(chan Game)
		stop := make(chan struct{})
		completed := make(chan struct{})
		ctx, cf := context.WithTimeout(context.Background(), time.Minute*30)
		go func() {
			site.GetFootball(transport, games, stop)
			// site.GetHandball(transport, games, stop)
			// site.GetBasketBall(transport, games, stop)
			// site.GetTennis(transport, games, stop)
			// site.GetIceHockey(transport, games, stop)
			// site.GetRugby(transport, games, stop)
		}()
		go func() {
			utils.Debug("LISTENING FOR GAMES")
			num := 0
			for g := range games {
				data = append(data, g)
				fmt.Println(g)
				num++
				// if strings.Contains(g.Home, "Kosice") {
				// 	stop <- struct{}{}

				// 	break

				// }
				// if num == 2 {
				// 	stop <- struct{}{}

				// 	break
				// }
			}
			completed <- struct{}{}
		}()
		select {
		case <-completed:
			utils.Debug("COMPLETED")
			break
		case <-ctx.Done():
			utils.Error("TIMEOUT")
			stop <- struct{}{}
			close(games)
			break
		}
		cf()

		// fmt.Println(games)
		fmt.Println(site.Usage())
		// fmt.Println("2", site.MainPageUsage())
		fmt.Println(len(data))

		fmt.Println("--------------- DONE ------------------")

	}

	home := "Brighton & Hove Albion"
	away := "Kilkenny (Women)"
	eventId := 1735063
	h := strings.Replace(strings.Trim(home, " "), " ", "-", -1)
	h = strings.Replace(h, "(", "", -1)
	h = strings.Replace(h, ")", "", -1)
	a := strings.Replace(strings.Trim(away, " "), " ", "-", -1)
	a = strings.Replace(a, "(", "", -1)
	a = strings.Replace(a, ")", "", -1)

	h = strings.Replace(strings.Trim(h, " "), " ", "-", -1)
	a = strings.Replace(strings.Trim(a, " "), " ", "-", -1)

	form := func(name string) (string, string) {
		lname := strings.Replace(name, ".", "", -1)
		lname = strings.Trim(lname, " ")
		lname = strings.Replace(lname, " ", "-", -1)
		lname = strings.Replace(lname, "(", "", -1)
		lname = strings.Replace(lname, ")", "", -1)
		lname = strings.Trim(lname, " ")
		lname = strings.Replace(lname, "&", "", -1)
		lname = strings.Replace(lname, " ", "-", -1)
		return lname, name
	}

	h, _ = form(home)
	a, _ = form(away)
	gameLink := fmt.Sprintf("http://www.betpawa.co.ke/event-%d-%s-%s", eventId, h, a)
	utils.Debug(gameLink)

	utils.Debug(time.Parse(time.RFC3339, "2018-04-27T21:00:00+03:00"))

}

func testSelenium(t *testing.T) {
	driver := agouti.ChromeDriver()
	// driver := agouti.ChromeDriver(
	//   agouti.ChromeOptions("args", []string{"--headless", "--disable-gpu", "--no-sandbox"}),
	// )

	if err := driver.Start(); err != nil {
		log.Fatal("Failed to start driver:", err)
	}

	page, err := driver.NewPage()
	if err != nil {
		log.Fatal("Failed to open page:", err)
	}

	if err := page.Navigate("https://betyetu.co.ke/sportsbook/SOCCER/EN_D1/1375550/"); err != nil {
		log.Fatal("Failed to navigate:", err)
	}
	time.Sleep(5 * time.Second)

	// page.FindByClass(".ng-binding")
	data, _ := page.HTML()
	ioutil.WriteFile("out.html", []byte(data), 0777)

	if err := driver.Stop(); err != nil {
		log.Fatal("Failed to close pages and stop WebDriver:", err)
	}

}
func testHighest(t *testing.T) {
	agg := Aggregate{
		TWOWAY: map[string]TwoWay{
			"Mozzart": TwoWay{
				Home: 2.85,
				// Draw: 3.30,
				Away: 1.83,
			},
			"Odibet": TwoWay{
				Home: 1.45,
				// Draw: 3.27,
				Away: 3.3,
			},
			// "betin": ThreeWay{
			// 	Home: 2.84,
			// 	Draw: 3.29,
			// 	Away: 2.3,
			// },
			// "powerbet": ThreeWay{
			// 	Home: 4.82,
			// 	Draw: 2.27,
			// 	Away: 1.3,
			// },
			// "gamemania": ThreeWay{
			// 	Home: 0,
			// 	Draw: 0,
			// 	Away: 0,
			// },
		},
	}

	utils.Debug(agg.HighestTWOWAY(""))
}

//"Accept-Encoding", "gzip, deflate, br"
