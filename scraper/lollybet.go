package scraper

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"
	"time"

	"gitlab.com/arbke/utils"
)

type Lollybet struct {
	name     string
	link     string
	usage    int
	mainpage int

	lock *sync.Mutex
}

func NewLollyybet() *Lollybet {
	return &Lollybet{
		name: "Lollybet",
		link: "https://api.lollybet.co.ke",
		lock: &sync.Mutex{},
	}
}

func (lbet *Lollybet) Usage() int {
	lbet.lock.Lock()
	defer lbet.lock.Unlock()
	return lbet.usage
}

func (lbet *Lollybet) addUsage(u int) {
	lbet.lock.Lock()
	defer lbet.lock.Unlock()
	lbet.usage += u
}

func (lbet *Lollybet) Name() string {
	return lbet.name
}

func (lbet *Lollybet) GetFootball(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	//get all upcoming games

	//daymonthyearhourminute
	today := time.Now()
	tomorrow := today.Add(24 * time.Hour * 5)

	from := fmt.Sprintf("%2d%2d%2d0005", today.Day(), int(today.Month()), today.Year())
	to := fmt.Sprintf("%2d%2d%2d2359", tomorrow.Day(), int(tomorrow.Month()), tomorrow.Year())
	utils.Info(from, to)
	// https: //api.lollybet.co.ke/external/as/initialize
	// https://api.lollybet.co.ke/external/as/chronologicalFootball/2608182305/2608182359/
	resp, err := http.DefaultClient.Get(fmt.Sprintf("%s/external/as/chronologicalFootball/%s/%s/", lbet.link, from, to))
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	lbet.addUsage(len(data))
	intermediate := []interface{}{}
	// parsed := []interface{}{}

	if err := json.Unmarshal(data, &intermediate); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	// parsed = intermediate["topBets"].([]interface{})
	for _, _league := range intermediate {
		league := _league.(map[string]interface{})
		leagueName := league["League"].(map[string]interface{})["Name"].(string)

		if _, ok := league["Fixtures"]; !ok {
			continue
		}

		for _, _fix := range league["Fixtures"].([]interface{}) {
			fix := _fix.(map[string]interface{})
			id := int(fix["Id"].(float64))

			game := Game{}
			game.League = leagueName
			gameLink := fmt.Sprintf("https://lollybet.co.ke/fixture/%d", id)

			// utils.Debug(gameLink)

			game.Site = lbet.name

			game.Home = fix["HomeName"].(string) + " " + Allowed(leagueName)
			game.Away = fix["AwayName"].(string) + " " + Allowed(leagueName)

			dateStr := fix["Date"].(string)
			_t := utils.DecodeTime(lbet.name, dateStr)
			// t := _t.Add(3 * time.Hour)
			game.Time = _t

			apiLink := fmt.Sprintf("%s/external/as/sportFixtureMarkets/%d/true", lbet.link, id)
			g := lbet.getGame(apiLink, gameLink, game)

			if isStopped() {
				return
			}
			if g == nil {
				continue
			}

			// utils.Debug(*g)
			games <- *g
		}

	}

	if isStopped() {
		return
	}

}

func (lbet *Lollybet) getGame(apiLink, gameLink string, game Game) *Game {

	tw := ThreeWay{Link: gameLink, APILink: apiLink}
	dnb := DrawNoBet{Link: gameLink, APILink: apiLink}
	dc := DoubleChance{Link: gameLink, APILink: apiLink}

	ou15 := OU15{Link: gameLink, APILink: apiLink}
	ou25 := OU25{Link: gameLink, APILink: apiLink}
	ou35 := OU35{Link: gameLink, APILink: apiLink}
	ou45 := OU45{Link: gameLink, APILink: apiLink}
	gng := GoalNoGoal{Link: gameLink, APILink: apiLink}

	resp, err := http.DefaultClient.Get(apiLink)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	ioutil.WriteFile("out2.json", data, 0777)
	lbet.addUsage(len(data))

	markets := []map[string]interface{}{}
	if err := json.Unmarshal(data, &markets); err != nil {
		fmt.Println(string(data))
		utils.Error(err)
		return nil
	}

	for _, market := range markets {
		name := market["BetName"].(string)
		outcomes := market["Odds"].([]interface{})
		switch name {
		case "Match Result":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd := outcome["Value"].(float64)
				switch outcome["Symbol"].(string) {
				case "1":
					tw.Home = odd
				case "X":
					tw.Draw = odd
				case "2":
					tw.Away = odd

				}
			}
		case "Double Chance":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd := outcome["Value"].(float64)
				switch outcome["Symbol"].(string) {
				case "1X":
					dc.Home = odd
				case "X2":
					dc.Away = odd
				case "12":
					dc.HomeOrAway = odd

				}
			}
		case "Both Teams To Score":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd := outcome["Value"].(float64)
				switch outcome["Symbol"].(string) {
				case "Yes":
					gng.Goal = odd
				case "No":
					gng.NoGoal = odd

				}
			}
		case "Draw No Bet":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd := outcome["Value"].(float64)
				switch outcome["Symbol"].(string) {
				case "DNB1":
					dnb.Home = odd
				case "DNB2":
					dnb.Away = odd

				}
			}
		case "Total Goals":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd := outcome["Value"].(float64)
				switch outcome["Symbol"].(string) {
				case "Over 1.50":
					ou15.Over = odd
				case "Under 1.50":
					ou15.Under = odd

				case "Over 2.50":
					ou25.Over = odd
				case "Under 2.50":
					ou25.Under = odd

				case "Over 3.50":
					ou35.Over = odd
				case "Under 3.50":
					ou35.Under = odd

				case "Over 4.50":
					ou45.Over = odd
				case "Under 4.50":
					ou45.Under = odd

				}
			}
		}
	}

	game.TWAY = tw
	game.DC = dc
	game.DNB = dnb
	game.GNG = gng
	game.OU15 = ou15
	game.OU25 = ou25
	game.OU35 = ou35
	game.OU45 = ou45

	// utils.Debug(game)
	return &game
}
func (lbet *Lollybet) GetFootballGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := lbet.getGame(apiLink, gameLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (lbet *Lollybet) GetTennis(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	//get all upcoming games

	//daymonthyearhourminute
	today := time.Now()
	tomorrow := today.Add(24 * time.Hour * 5)

	from := fmt.Sprintf("%2d%2d%2d0005", today.Day(), int(today.Month()), today.Year())
	to := fmt.Sprintf("%2d%2d%2d2359", tomorrow.Day(), int(tomorrow.Month()), tomorrow.Year())
	utils.Info(from, to)
	// https: //api.lollybet.co.ke/external/as/initialize
	// https://api.lollybet.co.ke/external/as/chronologicalTennis/2608182305/2608182359/
	//https://api.lollybet.co.ke/external/as/changeLeague/4/0/0/0909180000/0909182359/
	resp, err := http.DefaultClient.Get(fmt.Sprintf("%s/external/as/changeLeague/4/0/0/%s/%s/", lbet.link, from, to))
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	lbet.addUsage(len(data))
	intermediate := []interface{}{}
	// parsed := []interface{}{}

	if err := json.Unmarshal(data, &intermediate); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	// parsed = intermediate["topBets"].([]interface{})
	for _, _league := range intermediate {
		league := _league.(map[string]interface{})
		leagueName := league["League"].(map[string]interface{})["Name"].(string)

		if _, ok := league["Fixtures"]; !ok {
			continue
		}

		for _, _fix := range league["Fixtures"].([]interface{}) {
			fix := _fix.(map[string]interface{})
			id := int(fix["Id"].(float64))

			game := Game{}
			game.League = leagueName
			gameLink := fmt.Sprintf("https://lollybet.co.ke/fixture/%d", id)

			// utils.Debug(gameLink)

			game.Site = lbet.name

			game.Home = fix["HomeName"].(string)
			game.Away = fix["AwayName"].(string)

			dateStr := fix["Date"].(string)
			_t := utils.DecodeTime(lbet.name, dateStr)
			// t := _t.Add(3 * time.Hour)
			game.Time = _t

			apiLink := fmt.Sprintf("%s/external/as/sportFixtureMarkets/%d/true", lbet.link, id)
			g := lbet.getTGame(apiLink, gameLink, game)

			if isStopped() {
				return
			}
			if g == nil {
				continue
			}

			// utils.Debug(*g)
			games <- *g
		}

	}

	if isStopped() {
		return
	}

}

func (lbet *Lollybet) getTGame(apiLink, gameLink string, game Game) *Game {
	tw := TwoWay{Link: gameLink, APILink: apiLink}

	resp, err := http.DefaultClient.Get(apiLink)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	// ioutil.WriteFile("out2.json", data, 0777)
	lbet.addUsage(len(data))

	markets := []map[string]interface{}{}
	if err := json.Unmarshal(data, &markets); err != nil {
		fmt.Println(string(data))
		utils.Error(err)
		return nil
	}

	for _, market := range markets {
		name := market["BetName"].(string)
		outcomes := market["Odds"].([]interface{})
		switch name {
		case "Match":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd := outcome["Value"].(float64)
				switch outcome["Symbol"].(string) {
				case "1":
					tw.Home = odd
				// case "X":
				// 	tw.Draw = odd
				case "2":
					tw.Away = odd

				}
			}

		}
	}

	game.TWOWAY = tw

	// utils.Debug(game)
	return &game
}
func (lbet *Lollybet) GetTennisGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := lbet.getTGame(apiLink, gameLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (lbet *Lollybet) GetBasketBall(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	//get all upcoming games

	//daymonthyearhourminute
	today := time.Now()
	tomorrow := today.Add(24 * time.Hour * 5)

	from := fmt.Sprintf("%2d%2d%2d0005", today.Day(), int(today.Month()), today.Year())
	to := fmt.Sprintf("%2d%2d%2d2359", tomorrow.Day(), int(tomorrow.Month()), tomorrow.Year())
	utils.Info(from, to)
	// https: //api.lollybet.co.ke/external/as/initialize
	// https://api.lollybet.co.ke/external/as/chronologicalBasketBall/2608182305/2608182359/
	//https://api.lollybet.co.ke/external/as/changeLeague/4/0/0/0909180000/0909182359/
	resp, err := http.DefaultClient.Get(fmt.Sprintf("%s/external/as/changeLeague/3/0/0/%s/%s/", lbet.link, from, to))
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	lbet.addUsage(len(data))
	intermediate := []interface{}{}
	// parsed := []interface{}{}

	if err := json.Unmarshal(data, &intermediate); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	// parsed = intermediate["topBets"].([]interface{})
	for _, _league := range intermediate {
		league := _league.(map[string]interface{})
		leagueName := league["League"].(map[string]interface{})["Name"].(string)

		if _, ok := league["Fixtures"]; !ok {
			continue
		}

		for _, _fix := range league["Fixtures"].([]interface{}) {
			fix := _fix.(map[string]interface{})
			id := int(fix["Id"].(float64))

			game := Game{}
			game.League = leagueName
			gameLink := fmt.Sprintf("https://lollybet.co.ke/fixture/%d", id)

			// utils.Debug(gameLink)

			game.Site = lbet.name

			game.Home = fix["HomeName"].(string)
			game.Away = fix["AwayName"].(string)

			dateStr := fix["Date"].(string)
			_t := utils.DecodeTime(lbet.name, dateStr)
			// t := _t.Add(3 * time.Hour)
			game.Time = _t

			apiLink := fmt.Sprintf("%s/external/as/sportFixtureMarkets/%d/true", lbet.link, id)
			g := lbet.getBGame(apiLink, gameLink, game)

			if isStopped() {
				return
			}
			if g == nil {
				continue
			}

			// utils.Debug(*g)
			games <- *g
		}

	}

	if isStopped() {
		return
	}

}

func (lbet *Lollybet) getBGame(apiLink, gameLink string, game Game) *Game {
	tw := TwoWay{Link: gameLink, APILink: apiLink}

	resp, err := http.DefaultClient.Get(apiLink)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	// ioutil.WriteFile("out2.json", data, 0777)
	lbet.addUsage(len(data))

	markets := []map[string]interface{}{}
	if err := json.Unmarshal(data, &markets); err != nil {
		fmt.Println(string(data))
		utils.Error(err)
		return nil
	}

	for _, market := range markets {
		name := market["BetName"].(string)
		outcomes := market["Odds"].([]interface{})
		switch name {
		case "Money Line":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd := outcome["Value"].(float64)
				switch outcome["Symbol"].(string) {
				case "1":
					tw.Home = odd
				// case "X":
				// 	tw.Draw = odd
				case "2":
					tw.Away = odd

				}
			}

		}
	}

	game.TWOWAY = tw

	// utils.Debug(game)
	return &game
}
func (lbet *Lollybet) GetBasketBallGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := lbet.getBGame(apiLink, gameLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (lbet *Lollybet) GetRugby(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	//get all upcoming games

	//daymonthyearhourminute
	today := time.Now()
	tomorrow := today.Add(24 * time.Hour * 5)

	from := fmt.Sprintf("%2d%2d%2d0005", today.Day(), int(today.Month()), today.Year())
	to := fmt.Sprintf("%2d%2d%2d2359", tomorrow.Day(), int(tomorrow.Month()), tomorrow.Year())
	utils.Info(from, to)
	// https: //api.lollybet.co.ke/external/as/initialize
	// https://api.lollybet.co.ke/external/as/chronologicalRugby/2608182305/2608182359/
	//https://api.lollybet.co.ke/external/as/changeLeague/4/0/0/0909180000/0909182359/
	resp, err := http.DefaultClient.Get(fmt.Sprintf("%s/external/as/changeLeague/16/0/0/%s/%s/", lbet.link, from, to))
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	lbet.addUsage(len(data))
	intermediate := []interface{}{}
	// parsed := []interface{}{}

	if err := json.Unmarshal(data, &intermediate); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	// parsed = intermediate["topBets"].([]interface{})
	for _, _league := range intermediate {
		league := _league.(map[string]interface{})
		leagueName := league["League"].(map[string]interface{})["Name"].(string)

		if _, ok := league["Fixtures"]; !ok {
			continue
		}

		for _, _fix := range league["Fixtures"].([]interface{}) {
			fix := _fix.(map[string]interface{})
			id := int(fix["Id"].(float64))

			game := Game{}
			game.League = leagueName
			gameLink := fmt.Sprintf("https://lollybet.co.ke/fixture/%d", id)

			// utils.Debug(gameLink)

			game.Site = lbet.name

			game.Home = fix["HomeName"].(string)
			game.Away = fix["AwayName"].(string)

			dateStr := fix["Date"].(string)
			_t := utils.DecodeTime(lbet.name, dateStr)
			// t := _t.Add(3 * time.Hour)
			game.Time = _t

			apiLink := fmt.Sprintf("%s/external/as/sportFixtureMarkets/%d/true", lbet.link, id)
			g := lbet.getRGame(apiLink, gameLink, game)

			if isStopped() {
				return
			}
			if g == nil {
				continue
			}

			// utils.Debug(*g)
			games <- *g
		}

	}

	if isStopped() {
		return
	}

}

func (lbet *Lollybet) getRGame(apiLink, gameLink string, game Game) *Game {
	tw := ThreeWay{Link: gameLink, APILink: apiLink}

	resp, err := http.DefaultClient.Get(apiLink)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	// ioutil.WriteFile("out2.json", data, 0777)
	lbet.addUsage(len(data))
	markets := []map[string]interface{}{}
	if err := json.Unmarshal(data, &markets); err != nil {
		fmt.Println(string(data))
		utils.Error(err)
		return nil
	}

	for _, market := range markets {
		name := market["BetName"].(string)
		outcomes := market["Odds"].([]interface{})
		switch name {
		case "Match":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd := outcome["Value"].(float64)
				switch outcome["Symbol"].(string) {
				case "1":
					tw.Home = odd
				case "X":
					tw.Draw = odd
				case "2":
					tw.Away = odd

				}
			}

		}
	}

	game.TWAY = tw

	// utils.Debug(game)
	return &game
}
func (lbet *Lollybet) GetRugbyGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := lbet.getRGame(apiLink, gameLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

//https://api.lollybet.co.ke/external/as/changeLeague/4/0/0/0909180000/0909182359/

func (lbet *Lollybet) GetIceHockey(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	//get all upcoming games

	//daymonthyearhourminute
	today := time.Now()
	tomorrow := today.Add(24 * time.Hour * 5)

	from := fmt.Sprintf("%2d%2d%2d0005", today.Day(), int(today.Month()), today.Year())
	to := fmt.Sprintf("%2d%2d%2d2359", tomorrow.Day(), int(tomorrow.Month()), tomorrow.Year())
	utils.Info(from, to)
	// https: //api.lollybet.co.ke/external/as/initialize
	// https://api.lollybet.co.ke/external/as/chronologicalIceHockey/2608182305/2608182359/
	//https://api.lollybet.co.ke/external/as/changeLeague/4/0/0/0909180000/0909182359/
	resp, err := http.DefaultClient.Get(fmt.Sprintf("%s/external/as/changeLeague/5/0/0/%s/%s/", lbet.link, from, to))
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	lbet.addUsage(len(data))
	intermediate := []interface{}{}
	// parsed := []interface{}{}

	if err := json.Unmarshal(data, &intermediate); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	// parsed = intermediate["topBets"].([]interface{})
	for _, _league := range intermediate {
		league := _league.(map[string]interface{})
		leagueName := league["League"].(map[string]interface{})["Name"].(string)

		if _, ok := league["Fixtures"]; !ok {
			continue
		}

		for _, _fix := range league["Fixtures"].([]interface{}) {
			fix := _fix.(map[string]interface{})
			id := int(fix["Id"].(float64))

			game := Game{}
			game.League = leagueName
			gameLink := fmt.Sprintf("https://lollybet.co.ke/fixture/%d", id)

			// utils.Debug(gameLink)

			game.Site = lbet.name

			game.Home = fix["HomeName"].(string)
			game.Away = fix["AwayName"].(string)

			dateStr := fix["Date"].(string)
			_t := utils.DecodeTime(lbet.name, dateStr)
			// t := _t.Add(3 * time.Hour)
			game.Time = _t

			apiLink := fmt.Sprintf("%s/external/as/sportFixtureMarkets/%d/true", lbet.link, id)
			g := lbet.getIHGame(apiLink, gameLink, game)

			if isStopped() {
				return
			}
			if g == nil {
				continue
			}

			// utils.Debug(*g)
			games <- *g
		}

	}

	if isStopped() {
		return
	}

}

func (lbet *Lollybet) getIHGame(apiLink, gameLink string, game Game) *Game {
	tw := ThreeWay{Link: gameLink, APILink: apiLink}
	dnb := DrawNoBet{Link: gameLink, APILink: apiLink}
	dc := DoubleChance{Link: gameLink, APILink: apiLink}

	resp, err := http.DefaultClient.Get(apiLink)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	// ioutil.WriteFile("out2.json", data, 0777)
	lbet.addUsage(len(data))

	markets := []map[string]interface{}{}
	if err := json.Unmarshal(data, &markets); err != nil {
		fmt.Println(string(data))
		utils.Error(err)
		return nil
	}

	for _, market := range markets {
		name := market["BetName"].(string)
		outcomes := market["Odds"].([]interface{})
		switch name {
		case "Match Result":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd := outcome["Value"].(float64)
				switch outcome["Symbol"].(string) {
				case "1":
					tw.Home = odd
				case "X":
					tw.Draw = odd
				case "2":
					tw.Away = odd

				}
			}
		case "Double Chance":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd := outcome["Value"].(float64)
				switch outcome["Symbol"].(string) {
				case "1X":
					dc.Home = odd
				case "X2":
					dc.Away = odd
				case "12":
					dc.HomeOrAway = odd

				}
			}

		case "Money Line":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd := outcome["Value"].(float64)
				switch outcome["Symbol"].(string) {
				case "1":
					dnb.Home = odd
				case "2":
					dnb.Away = odd

				}
			}

		}
	}

	game.TWAY = tw
	game.DC = dc
	game.DNB = dnb
	// utils.Debug(game)
	return &game
}
func (lbet *Lollybet) GetIceHockeyGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := lbet.getIHGame(apiLink, gameLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

//

func (lbet *Lollybet) GetHandball(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	//get all upcoming games

	//daymonthyearhourminute
	today := time.Now()
	tomorrow := today.Add(24 * time.Hour * 5)

	from := fmt.Sprintf("%2d%2d%2d0005", today.Day(), int(today.Month()), today.Year())
	to := fmt.Sprintf("%2d%2d%2d2359", tomorrow.Day(), int(tomorrow.Month()), tomorrow.Year())
	utils.Info(from, to)
	// https: //api.lollybet.co.ke/external/as/initialize
	// https://api.lollybet.co.ke/external/as/chronologicalHandball/2608182305/2608182359/
	//https://api.lollybet.co.ke/external/as/changeLeague/4/0/0/0909180000/0909182359/
	resp, err := http.DefaultClient.Get(fmt.Sprintf("%s/external/as/changeLeague/6/0/0/%s/%s/", lbet.link, from, to))
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	lbet.addUsage(len(data))
	intermediate := []interface{}{}
	// parsed := []interface{}{}

	if err := json.Unmarshal(data, &intermediate); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	// parsed = intermediate["topBets"].([]interface{})
	for _, _league := range intermediate {
		league := _league.(map[string]interface{})
		leagueName := league["League"].(map[string]interface{})["Name"].(string)

		if _, ok := league["Fixtures"]; !ok {
			continue
		}

		for _, _fix := range league["Fixtures"].([]interface{}) {
			fix := _fix.(map[string]interface{})
			id := int(fix["Id"].(float64))

			game := Game{}
			game.League = leagueName
			gameLink := fmt.Sprintf("https://lollybet.co.ke/fixture/%d", id)

			// utils.Debug(gameLink)

			game.Site = lbet.name

			game.Home = fix["HomeName"].(string)
			game.Away = fix["AwayName"].(string)

			dateStr := fix["Date"].(string)
			_t := utils.DecodeTime(lbet.name, dateStr)
			// t := _t.Add(3 * time.Hour)
			game.Time = _t

			apiLink := fmt.Sprintf("%s/external/as/sportFixtureMarkets/%d/true", lbet.link, id)
			g := lbet.getHGame(apiLink, gameLink, game)

			if isStopped() {
				return
			}
			if g == nil {
				continue
			}

			// utils.Debug(*g)
			games <- *g
		}

	}

	if isStopped() {
		return
	}

}

func (lbet *Lollybet) getHGame(apiLink, gameLink string, game Game) *Game {
	tw := ThreeWay{Link: gameLink, APILink: apiLink}
	dnb := DrawNoBet{Link: gameLink, APILink: apiLink}
	resp, err := http.DefaultClient.Get(apiLink)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	// ioutil.WriteFile("out2.json", data, 0777)
	lbet.addUsage(len(data))

	markets := []map[string]interface{}{}
	if err := json.Unmarshal(data, &markets); err != nil {
		fmt.Println(string(data))
		utils.Error(err)
		return nil
	}

	for _, market := range markets {
		name := market["BetName"].(string)
		outcomes := market["Odds"].([]interface{})
		switch name {
		case "Match Result":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd := outcome["Value"].(float64)
				switch outcome["Symbol"].(string) {
				case "1":
					tw.Home = odd
				case "X":
					tw.Draw = odd
				case "2":
					tw.Away = odd

				}
			}
		case "Draw No Bet":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd := outcome["Value"].(float64)
				switch outcome["Symbol"].(string) {
				case "1":
					dnb.Home = odd
				case "2":
					dnb.Away = odd

				}
			}

		}
	}

	game.TWAY = tw
	game.DNB = dnb
	// utils.Debug(game)
	return &game
}
func (lbet *Lollybet) GetHandballGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := lbet.getIHGame(apiLink, gameLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}
func (lbet *Lollybet) UsageReset() {
	lbet.usage = 0
	lbet.mainpage = 0
}
func (lbet *Lollybet) MainPageUsage() int {
	return lbet.mainpage
}
