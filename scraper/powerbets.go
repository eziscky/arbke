package scraper

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitlab.com/arbke/utils"
)

type Powerbet struct {
	name     string
	link     string
	usage    int
	mainpage int

	lock *sync.Mutex
}

func NewPowerbet() *Powerbet {
	return &Powerbet{
		name: "Powerbet",
		link: "https://m.powerbets.co.ke/api",
		lock: &sync.Mutex{},
	}
}

func (pbet *Powerbet) addUsage(u int) {
	pbet.lock.Lock()
	defer pbet.lock.Unlock()
	pbet.usage += u
}

func (pbet *Powerbet) Usage() int {
	pbet.lock.Lock()
	defer pbet.lock.Unlock()
	return pbet.usage
}
func (sbet *Powerbet) Name() string {
	return sbet.name
}

func (pbet *Powerbet) getRequest(endpoint string, data string, referer string) *http.Request {
	req, _ := http.NewRequest(http.MethodPost, fmt.Sprintf("%s%s", pbet.link, endpoint), strings.NewReader(data))
	req.Header.Add("accept-language", "en-US,en;q=0.5")
	req.Header.Add("connection", "keep-alive")
	req.Header.Add("content-type", "application/json")
	req.Header.Add("host", "m.powerbets.co.ke")
	req.Header.Add("origin", "https://m.powerbets.co.ke")
	req.Header.Add("referer", referer)
	req.Header.Add("user-agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0")

	return req
}
func (pbet *Powerbet) GetFootball(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	//get all upcoming games

	// day := 0 //today
	eventGroups := []interface{}{}
	for day := 0; day <= 5; day++ {
		d := fmt.Sprintf("{\"sportID\":468,\"dayOffset\":%d,\"offset\":0,\"limit\":10,\"eventIDs\":[]}", day)
		req := pbet.getRequest("/sport/upcoming/detail", d, "https://m.powerbets.co.ke/sport/468/upcoming?spdl=soccer")

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		resp.Body.Close()

		pbet.addUsage(len(data))
		parsed := map[string]interface{}{}

		if err := json.Unmarshal(data, &parsed); err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		if _events, ok := parsed["eventGroups"]; ok {
			switch _events.(type) {
			case []interface{}:
				events := _events.([]interface{})
				eventGroups = append(eventGroups, events...)
			}
		}

	}
	for _, event := range eventGroups {
		league := event.(map[string]interface{})
		leagueName := fmt.Sprintf("%s %s", league["groupName"].(string), league["eventName"].(string))

		if _, ok := league["subEvents"]; !ok {
			continue
		}

		for _, _fix := range league["subEvents"].([]interface{}) {
			fix := _fix.(map[string]interface{})
			game := Game{}
			game.League = leagueName

			game.Site = pbet.name

			id := int(fix["subEventId"].(float64))
			name := strings.Split(fix["subEventName"].(string), " - ")

			if len(name) < 2 {
				// utils.Error(fix["subEventName"].(string))
				continue
			}
			game.Home = name[0] + " " + Allowed(leagueName)
			game.Away = name[1] + " " + Allowed(leagueName)

			dateStr := fix["date"].(string)
			_t := utils.DecodeTime(pbet.name, dateStr)

			// utils.Debug(game.Home, game.Away, dateStr)
			t := _t.Add(1 * time.Hour)
			game.Time = &t

			reqData := fmt.Sprintf("{\"subEventId\":%d,\"offset\":%d,\"limit\":%d}", id, 0, 100)
			// utils.Debug(reqData)
			gameLink := "" //fmt.Sprintf("https://m.powerbets.co.ke/market/%d", id)

			g := pbet.getGame(gameLink, reqData, game)
			// utils.Debug(gameLink)
			if isStopped() {
				return
			}
			if g == nil {
				continue
			}

			// utils.Debug(*g)
			games <- *g
		}

	}

	if isStopped() {
		return
	}

}

func (pbet *Powerbet) getGame(gameLink, reqData string, game Game) *Game {
	apiLink := reqData
	tw := ThreeWay{Link: gameLink, APILink: apiLink}
	dnb := DrawNoBet{Link: gameLink, APILink: apiLink}
	dc := DoubleChance{Link: gameLink, APILink: apiLink}

	ou15 := OU15{Link: gameLink, APILink: apiLink}
	ou25 := OU25{Link: gameLink, APILink: apiLink}
	ou35 := OU35{Link: gameLink, APILink: apiLink}
	ou45 := OU45{Link: gameLink, APILink: apiLink}
	gng := GoalNoGoal{Link: gameLink, APILink: apiLink}

	req := pbet.getRequest("/subevent/bets", reqData, gameLink)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	resp.Body.Close()

	// ioutil.WriteFile("pbet.json", data, 0777)
	pbet.addUsage(len(data))

	parsed := map[string]interface{}{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		// fmt.Println(string(data))
		utils.Error(err)
		return nil
	}
	// utils.Debug(string(data))
	if _, ok := parsed["bets"]; !ok {
		return nil
	}

	markets := parsed["bets"].([]interface{})

	for _, _market := range markets {
		market := _market.(map[string]interface{})
		name := market["caption"].(string)
		outcomes := market["odds"].([]interface{})
		switch name {
		case "Home/Draw/Away (FT)":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd, _ := strconv.ParseFloat(outcome["value"].(string), 64)

				switch int(outcome["oddTypeId"].(float64)) {
				case 1:
					tw.Home = odd
				case 2:
					tw.Draw = odd
				case 3:
					tw.Away = odd

				}
			}
		case "Double Chance":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd, _ := strconv.ParseFloat(outcome["value"].(string), 64)
				switch int(outcome["oddTypeId"].(float64)) {
				case 4:
					dc.HomeOrAway = odd
				case 5:
					dc.Home = odd
				case 6:
					dc.Away = odd

				}
			}
		case "Goal/No Goal":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd, _ := strconv.ParseFloat(outcome["value"].(string), 64)
				switch int(outcome["oddTypeId"].(float64)) {
				case 50:
					gng.Goal = odd
				case 51:
					gng.NoGoal = odd

				}
			}
		case "Draw No Bet":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd, _ := strconv.ParseFloat(outcome["value"].(string), 64)
				switch int(outcome["oddTypeId"].(float64)) {
				case 255:
					dnb.Home = odd
				case 257:
					dnb.Away = odd

				}
			}
		case "Over/Under FT":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd, _ := strconv.ParseFloat(outcome["value"].(string), 64)
				switch int(outcome["oddTypeId"].(float64)) {
				case 284:
					ou15.Over = odd
				case 285:
					ou15.Under = odd

				case 286:
					ou25.Over = odd
				case 287:
					ou25.Under = odd

				case 288:
					ou35.Over = odd
				case 289:
					ou35.Under = odd

				case 301:
					ou45.Over = odd
				case 302:
					ou45.Under = odd

				}
			}
		}
	}

	game.TWAY = tw
	game.DC = dc
	game.DNB = dnb
	game.GNG = gng
	game.OU15 = ou15
	game.OU25 = ou25
	game.OU35 = ou35
	game.OU45 = ou45

	return &game
}
func (pbet *Powerbet) GetFootballGame(t *http.Transport, gameLink string, apiLink string) Game {
	reqData := apiLink
	g := pbet.getGame(gameLink, reqData, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (pbet *Powerbet) GetBasketBall(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	//get all upcoming games

	// day := 0 //today
	eventGroups := []interface{}{}
	for day := 0; day <= 5; day++ {
		d := fmt.Sprintf("{\"sportID\":525,\"dayOffset\":%d,\"offset\":0,\"limit\":10,\"eventIDs\":[]}", day)
		req := pbet.getRequest("/sport/upcoming/detail", d, "https://m.powerbets.co.ke/sport/525/upcoming")

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		resp.Body.Close()

		pbet.addUsage(len(data))
		parsed := map[string]interface{}{}

		if err := json.Unmarshal(data, &parsed); err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		if _events, ok := parsed["eventGroups"]; ok {
			switch _events.(type) {
			case []interface{}:
				events := _events.([]interface{})
				eventGroups = append(eventGroups, events...)
			}
		}

	}
	for _, event := range eventGroups {
		league := event.(map[string]interface{})
		leagueName := league["eventName"].(string)

		if _, ok := league["subEvents"]; !ok {
			continue
		}

		for _, _fix := range league["subEvents"].([]interface{}) {
			fix := _fix.(map[string]interface{})
			game := Game{}
			game.League = leagueName

			game.Site = pbet.name

			id := int(fix["subEventId"].(float64))
			name := strings.Split(fix["subEventName"].(string), " - ")

			if len(name) < 2 {
				// utils.Error(fix["subEventName"].(string))
				continue
			}
			game.Home = name[0]
			game.Away = name[1]

			dateStr := fix["date"].(string)
			_t := utils.DecodeTime(pbet.name, dateStr)

			// utils.Debug(game.Home, game.Away, dateStr)
			t := _t.Add(1 * time.Hour)
			game.Time = &t

			reqData := fmt.Sprintf("{\"subEventId\":%d,\"offset\":%d,\"limit\":%d}", id, 0, 100)
			// utils.Debug(reqData)
			gameLink := fmt.Sprintf("https://m.powerbets.co.ke/market/%d", id)

			g := pbet.getBGame(gameLink, reqData, game)
			// utils.Debug(gameLink)
			if isStopped() {
				return
			}
			if g == nil {
				continue
			}

			// utils.Debug(*g)
			games <- *g
		}

	}

	if isStopped() {
		return
	}

}

func (pbet *Powerbet) getBGame(gameLink, reqData string, game Game) *Game {
	apiLink := reqData
	tww := TwoWay{Link: gameLink, APILink: apiLink}
	tw := ThreeWay{Link: gameLink, APILink: apiLink}
	req := pbet.getRequest("/subevent/bets", reqData, gameLink)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	resp.Body.Close()

	// ioutil.WriteFile("pbet.json", data, 0777)
	pbet.addUsage(len(data))

	parsed := map[string]interface{}{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		// fmt.Println(string(data))
		utils.Error(err)
		return nil
	}
	// utils.Debug(string(data))
	if _, ok := parsed["bets"]; !ok {
		return nil
	}

	markets := parsed["bets"].([]interface{})

	for _, _market := range markets {
		market := _market.(map[string]interface{})
		name := market["caption"].(string)
		outcomes := market["odds"].([]interface{})
		switch name {
		case "Home/Draw/Away (FT)":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd, _ := strconv.ParseFloat(outcome["value"].(string), 64)

				switch int(outcome["oddTypeId"].(float64)) {
				case 1:
					tw.Home = odd
				case 2:
					tw.Draw = odd
				case 3:
					tw.Away = odd

				}
			}
		case "Home/Away Incl OT":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd, _ := strconv.ParseFloat(outcome["value"].(string), 64)

				switch int(outcome["oddTypeId"].(float64)) {
				case 3899:
					tww.Home = odd
				// case 2:
				// 	tw.Draw = odd
				case 3900:
					tww.Away = odd

				}
			}

		}
	}

	game.TWOWAY = tww
	game.TWAY = tw
	return &game
}
func (pbet *Powerbet) GetBasketBallGame(t *http.Transport, gameLink string, apiLink string) Game {
	reqData := apiLink
	g := pbet.getBGame(gameLink, reqData, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (pbet *Powerbet) GetTennis(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	//get all upcoming games

	// day := 0 //today
	eventGroups := []interface{}{}
	for day := 0; day <= 5; day++ {
		d := fmt.Sprintf("{\"sportID\":523,\"dayOffset\":%d,\"offset\":0,\"limit\":10,\"eventIDs\":[]}", day)
		req := pbet.getRequest("/sport/upcoming/detail", d, "https://m.powerbets.co.ke/sport/523/upcoming")

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		resp.Body.Close()

		pbet.addUsage(len(data))
		parsed := map[string]interface{}{}

		if err := json.Unmarshal(data, &parsed); err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		if val, ok := parsed["eventGroups"]; !ok || val == nil {
			continue
		}

		if _events, ok := parsed["eventGroups"]; ok {
			switch _events.(type) {
			case []interface{}:
				events := _events.([]interface{})
				eventGroups = append(eventGroups, events...)
			}
		}

	}
	for _, event := range eventGroups {
		league := event.(map[string]interface{})
		leagueName := league["eventName"].(string)

		if _, ok := league["subEvents"]; !ok {
			continue
		}

		for _, _fix := range league["subEvents"].([]interface{}) {
			fix := _fix.(map[string]interface{})
			game := Game{}
			game.League = leagueName

			game.Site = pbet.name

			id := int(fix["subEventId"].(float64))
			name := strings.Split(fix["subEventName"].(string), " - ")

			if len(name) < 2 {
				// utils.Error(fix["subEventName"].(string))
				continue
			}
			game.Home = name[0]
			game.Away = name[1]

			dateStr := fix["date"].(string)
			_t := utils.DecodeTime(pbet.name, dateStr)

			// utils.Debug(game.Home, game.Away, dateStr)
			t := _t.Add(1 * time.Hour)
			game.Time = &t

			reqData := fmt.Sprintf("{\"subEventId\":%d,\"offset\":%d,\"limit\":%d}", id, 0, 100)
			// utils.Debug(reqData)
			gameLink := fmt.Sprintf("https://m.powerbets.co.ke/market/%d", id)

			g := pbet.getTGame(gameLink, reqData, game)
			// utils.Debug(gameLink)
			if isStopped() {
				return
			}
			if g == nil {
				continue
			}

			// utils.Debug(*g)
			games <- *g
		}

	}

	if isStopped() {
		return
	}

}

func (pbet *Powerbet) getTGame(gameLink, reqData string, game Game) *Game {
	apiLink := reqData
	tw := TwoWay{Link: gameLink, APILink: apiLink}

	req := pbet.getRequest("/subevent/bets", reqData, gameLink)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	resp.Body.Close()

	// ioutil.WriteFile("pbet.json", data, 0777)
	pbet.addUsage(len(data))

	parsed := map[string]interface{}{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		// fmt.Println(string(data))
		utils.Error(err)
		return nil
	}
	// utils.Debug(string(data))
	if _, ok := parsed["bets"]; !ok {
		return nil
	}

	markets := parsed["bets"].([]interface{})

	for _, _market := range markets {
		market := _market.(map[string]interface{})
		name := market["caption"].(string)
		outcomes := market["odds"].([]interface{})
		switch name {
		case "Home/Away":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd, _ := strconv.ParseFloat(outcome["value"].(string), 64)

				switch int(outcome["oddTypeId"].(float64)) {
				case 264:
					tw.Home = odd
				// case 2:
				// 	tw.Draw = odd
				case 265:
					tw.Away = odd

				}
			}

		}
	}

	game.TWOWAY = tw
	return &game
}

func (pbet *Powerbet) GetTennisGame(t *http.Transport, gameLink string, apiLink string) Game {
	reqData := apiLink
	g := pbet.getTGame(gameLink, reqData, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (pbet *Powerbet) GetIceHockey(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	//get all upcoming games

	// day := 0 //today
	eventGroups := []interface{}{}
	for day := 0; day <= 5; day++ {
		d := fmt.Sprintf("{\"sportID\":534,\"dayOffset\":%d,\"offset\":0,\"limit\":10,\"eventIDs\":[]}", day)
		req := pbet.getRequest("/sport/upcoming/detail", d, "https://m.powerbets.co.ke/sport/534/upcoming")

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		resp.Body.Close()

		pbet.addUsage(len(data))
		parsed := map[string]interface{}{}

		if err := json.Unmarshal(data, &parsed); err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		if _events, ok := parsed["eventGroups"]; ok {
			switch _events.(type) {
			case []interface{}:
				events := _events.([]interface{})
				eventGroups = append(eventGroups, events...)
			}
		}

	}
	for _, event := range eventGroups {
		league := event.(map[string]interface{})
		leagueName := league["eventName"].(string)

		if _, ok := league["subEvents"]; !ok {
			continue
		}

		for _, _fix := range league["subEvents"].([]interface{}) {
			fix := _fix.(map[string]interface{})
			game := Game{}
			game.League = leagueName

			game.Site = pbet.name

			id := int(fix["subEventId"].(float64))
			name := strings.Split(fix["subEventName"].(string), " - ")

			if strings.Contains(fix["subEventName"].(string), "~") { //is a handicap match
				utils.Debug("DETECTED Powerbet handicap")
				continue
			}
			if len(name) < 2 {
				// utils.Error(fix["subEventName"].(string))
				continue
			}

			game.Home = name[0]
			game.Away = name[1]

			dateStr := fix["date"].(string)
			_t := utils.DecodeTime(pbet.name, dateStr)

			// utils.Debug(game.Home, game.Away, dateStr)
			t := _t.Add(1 * time.Hour)
			game.Time = &t

			reqData := fmt.Sprintf("{\"subEventId\":%d,\"offset\":%d,\"limit\":%d}", id, 0, 100)
			// utils.Debug(reqData)
			gameLink := fmt.Sprintf("https://m.powerbets.co.ke/market/%d", id)

			g := pbet.getIHGame(gameLink, reqData, game)
			// utils.Debug(gameLink)
			if isStopped() {
				return
			}
			if g == nil {
				continue
			}

			// utils.Debug(*g)
			games <- *g
		}

	}

	if isStopped() {
		return
	}

}

func (pbet *Powerbet) getIHGame(gameLink, reqData string, game Game) *Game {
	apiLink := reqData
	tw := ThreeWay{Link: gameLink, APILink: apiLink}
	dnb := DrawNoBet{Link: gameLink, APILink: apiLink}
	dc := DoubleChance{Link: gameLink, APILink: apiLink}
	gng := GoalNoGoal{Link: gameLink, APILink: apiLink}

	req := pbet.getRequest("/subevent/bets", reqData, gameLink)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	resp.Body.Close()

	// ioutil.WriteFile("pbet.json", data, 0777)
	pbet.addUsage(len(data))

	parsed := map[string]interface{}{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		// fmt.Println(string(data))
		utils.Error(err)
		return nil
	}
	// utils.Debug(string(data))
	if _, ok := parsed["bets"]; !ok {
		return nil
	}

	markets := parsed["bets"].([]interface{})

	for _, _market := range markets {
		market := _market.(map[string]interface{})
		name := market["caption"].(string)
		outcomes := market["odds"].([]interface{})
		switch name {
		case "Home/Draw/Away (FT)":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd, _ := strconv.ParseFloat(outcome["value"].(string), 64)

				switch int(outcome["oddTypeId"].(float64)) {
				case 1:
					tw.Home = odd
				case 2:
					tw.Draw = odd
				case 3:
					tw.Away = odd

				}
			}
		case "Double Chance":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd, _ := strconv.ParseFloat(outcome["value"].(string), 64)
				switch int(outcome["oddTypeId"].(float64)) {
				case 4:
					dc.HomeOrAway = odd
				case 5:
					dc.Home = odd
				case 6:
					dc.Away = odd

				}
			}
		case "Goal/No Goal":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd, _ := strconv.ParseFloat(outcome["value"].(string), 64)
				switch int(outcome["oddTypeId"].(float64)) {
				case 50:
					gng.Goal = odd
				case 51:
					gng.NoGoal = odd

				}
			}
		case "Draw No Bet":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd, _ := strconv.ParseFloat(outcome["value"].(string), 64)
				switch int(outcome["oddTypeId"].(float64)) {
				case 255:
					dnb.Home = odd
				case 257:
					dnb.Away = odd

				}
			}

		}
	}

	game.TWAY = tw
	game.DC = dc
	game.DNB = dnb
	game.GNG = gng

	return &game
}
func (pbet *Powerbet) GetIceHockeyGame(t *http.Transport, gameLink string, apiLink string) Game {
	reqData := apiLink
	g := pbet.getIHGame(gameLink, reqData, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (pbet *Powerbet) GetHandball(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	//get all upcoming games

	// day := 0 //today
	eventGroups := []interface{}{}
	for day := 0; day <= 5; day++ {
		d := fmt.Sprintf("{\"sportID\":532,\"dayOffset\":%d,\"offset\":0,\"limit\":10,\"eventIDs\":[]}", day)
		req := pbet.getRequest("/sport/upcoming/detail", d, "https://m.powerbets.co.ke/sport/532/upcoming")

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		resp.Body.Close()

		pbet.addUsage(len(data))
		parsed := map[string]interface{}{}

		if err := json.Unmarshal(data, &parsed); err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		if _events, ok := parsed["eventGroups"]; ok {
			switch _events.(type) {
			case []interface{}:
				events := _events.([]interface{})
				eventGroups = append(eventGroups, events...)
			}
		}
		// events := parsed["eventGroups"].([]interface{})

		// eventGroups = append(eventGroups, events...)

	}
	for _, event := range eventGroups {
		league := event.(map[string]interface{})
		leagueName := league["eventName"].(string)

		if _, ok := league["subEvents"]; !ok {
			continue
		}

		for _, _fix := range league["subEvents"].([]interface{}) {
			fix := _fix.(map[string]interface{})
			game := Game{}
			game.League = leagueName

			game.Site = pbet.name

			id := int(fix["subEventId"].(float64))
			name := strings.Split(fix["subEventName"].(string), " - ")

			if strings.Contains(fix["subEventName"].(string), "~") { //is a handicap match
				utils.Debug("DETECTED Powerbet handicap")
				continue
			}
			if len(name) < 2 {
				// utils.Error(fix["subEventName"].(string))
				continue
			}

			game.Home = name[0]
			game.Away = name[1]

			dateStr := fix["date"].(string)
			_t := utils.DecodeTime(pbet.name, dateStr)

			// utils.Debug(game.Home, game.Away, dateStr)
			t := _t.Add(1 * time.Hour)
			game.Time = &t

			reqData := fmt.Sprintf("{\"subEventId\":%d,\"offset\":%d,\"limit\":%d}", id, 0, 100)
			// utils.Debug(reqData)
			gameLink := fmt.Sprintf("https://m.powerbets.co.ke/market/%d", id)

			g := pbet.getHGame(gameLink, reqData, game)
			// utils.Debug(gameLink)
			if isStopped() {
				return
			}
			if g == nil {
				continue
			}

			// utils.Debug(*g)
			games <- *g
		}

	}

	if isStopped() {
		return
	}

}

func (pbet *Powerbet) getHGame(gameLink, reqData string, game Game) *Game {
	apiLink := reqData
	tw := ThreeWay{Link: gameLink, APILink: apiLink}
	dc := DoubleChance{Link: gameLink, APILink: apiLink}

	req := pbet.getRequest("/subevent/bets", reqData, gameLink)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	resp.Body.Close()

	// utils.Debug(ioutil.WriteFile("pbet.json", data, 0777))
	pbet.addUsage(len(data))

	parsed := map[string]interface{}{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		// fmt.Println(string(data))
		utils.Error(err)
		return nil
	}
	// utils.Debug(string(data))
	if _, ok := parsed["bets"]; !ok {
		return nil
	}

	markets := parsed["bets"].([]interface{})

	for _, _market := range markets {
		market := _market.(map[string]interface{})
		name := market["caption"].(string)
		outcomes := market["odds"].([]interface{})
		switch name {
		case "Home/Draw/Away (FT)":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd, _ := strconv.ParseFloat(outcome["value"].(string), 64)

				switch int(outcome["oddTypeId"].(float64)) {
				case 1:
					tw.Home = odd
				case 2:
					tw.Draw = odd
				case 3:
					tw.Away = odd

				}
			}
		case "Double Chance":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd, _ := strconv.ParseFloat(outcome["value"].(string), 64)

				switch int(outcome["oddTypeId"].(float64)) {
				case 5:
					dc.Home = odd
				case 4:
					dc.HomeOrAway = odd
				case 6:
					dc.Away = odd

				}
			}

		}
	}

	game.TWAY = tw
	game.DC = dc
	return &game
}
func (pbet *Powerbet) GetHandballGame(t *http.Transport, gameLink string, apiLink string) Game {
	reqData := apiLink
	g := pbet.getHGame(gameLink, reqData, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (pbet *Powerbet) GetRugby(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	//get all upcoming games

	// day := 0 //today
	eventGroups := []interface{}{}
	for day := 0; day <= 5; day++ {
		d := fmt.Sprintf("{\"sportID\":521,\"dayOffset\":%d,\"offset\":0,\"limit\":10,\"eventIDs\":[]}", day)
		req := pbet.getRequest("/sport/upcoming/detail", d, "https://m.powerbets.co.ke/sport/521/upcoming")

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		resp.Body.Close()

		pbet.addUsage(len(data))
		parsed := map[string]interface{}{}

		if err := json.Unmarshal(data, &parsed); err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		if _events, ok := parsed["eventGroups"]; ok {
			switch _events.(type) {
			case []interface{}:
				events := _events.([]interface{})
				eventGroups = append(eventGroups, events...)
			}
		}

	}
	for _, event := range eventGroups {
		league := event.(map[string]interface{})
		leagueName := league["eventName"].(string)

		if _, ok := league["subEvents"]; !ok {
			continue
		}

		for _, _fix := range league["subEvents"].([]interface{}) {
			fix := _fix.(map[string]interface{})
			game := Game{}
			game.League = leagueName

			game.Site = pbet.name

			id := int(fix["subEventId"].(float64))
			name := strings.Split(fix["subEventName"].(string), " - ")

			if strings.Contains(fix["subEventName"].(string), "~") { //is a handicap match
				utils.Debug("DETECTED Powerbet handicap")
				continue
			}
			if len(name) < 2 {
				// utils.Error(fix["subEventName"].(string))
				continue
			}

			game.Home = name[0]
			game.Away = name[1]

			dateStr := fix["date"].(string)
			_t := utils.DecodeTime(pbet.name, dateStr)

			// utils.Debug(game.Home, game.Away, dateStr)
			t := _t.Add(1 * time.Hour)
			game.Time = &t

			reqData := fmt.Sprintf("{\"subEventId\":%d,\"offset\":%d,\"limit\":%d}", id, 0, 100)
			// utils.Debug(reqData)
			gameLink := fmt.Sprintf("https://m.powerbets.co.ke/market/%d", id)

			g := pbet.getRGame(gameLink, reqData, game)
			// utils.Debug(gameLink)
			if isStopped() {
				return
			}
			if g == nil {
				continue
			}

			// utils.Debug(*g)
			games <- *g
		}

	}

	if isStopped() {
		return
	}

}

func (pbet *Powerbet) getRGame(gameLink, reqData string, game Game) *Game {
	apiLink := reqData
	tw := ThreeWay{Link: gameLink, APILink: apiLink}

	req := pbet.getRequest("/subevent/bets", reqData, gameLink)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	resp.Body.Close()

	// utils.Debug(ioutil.WriteFile("pbet.json", data, 0777))
	pbet.addUsage(len(data))

	parsed := map[string]interface{}{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		// fmt.Println(string(data))
		utils.Error(err)
		return nil
	}
	// utils.Debug(string(data))
	if _, ok := parsed["bets"]; !ok {
		return nil
	}

	markets := parsed["bets"].([]interface{})

	for _, _market := range markets {
		market := _market.(map[string]interface{})
		name := market["caption"].(string)
		outcomes := market["odds"].([]interface{})
		switch name {
		case "Home/Draw/Away (FT)":
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				odd, _ := strconv.ParseFloat(outcome["value"].(string), 64)

				switch int(outcome["oddTypeId"].(float64)) {
				case 1:
					tw.Home = odd
				case 2:
					tw.Draw = odd
				case 3:
					tw.Away = odd

				}
			}

		}
	}

	game.TWAY = tw
	return &game
}
func (pbet *Powerbet) GetRugbyGame(t *http.Transport, gameLink string, apiLink string) Game {
	reqData := apiLink
	g := pbet.getRGame(gameLink, reqData, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (pbet *Powerbet) UsageReset() {
	pbet.usage = 0
	pbet.mainpage = 0
}
func (pbet *Powerbet) MainPageUsage() int {
	return pbet.mainpage
}
