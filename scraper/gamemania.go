package scraper

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"time"

	"gitlab.com/arbke/utils"
)

type Gmania struct {
	name     string
	link     string
	usage    int
	mainpage int

	lock *sync.Mutex
}

type gapimatch struct {
	AuthToken string `json:"authToken"`
	Code      int    `json:"code"`
	Message   int    `json:"msg"`
	Markets   []struct {
		MarketID int    `json:"marketId"`
		Desc     string `json:"marketDescription"`
		Spec     string `json:"specifiers"`
		Outcomes []struct {
			ID   string  `json:"id"`
			Odds float64 `json:"odds"`
		} `json:"outcomes"`
	} `json:"data"`
}
type gapidetails struct {
	AuthToken string `json:"authToken"`
	Code      int    `json:"code"`
	Message   int    `json:"msg"`
	Matches   []struct {
		GameID string `json:"gameId"`
		League string `json:"tournamentName"`
		Home   string `json:"fullHomeTeamName"`
		Away   string `json:"fullAwayTeamName"`
		Start  int64  `json:"startTime"`
	} `json:"data"`
}

func NewGMania() *Gmania {

	return &Gmania{
		name: "Gamemania",
		link: "https://api.gamemania.cc/sports/v1",
		lock: &sync.Mutex{},
	}
}

func (gm *Gmania) Usage() int {
	gm.lock.Lock()
	defer gm.lock.Unlock()
	return gm.usage
}
func (gm *Gmania) Name() string {
	return gm.name
}

func (gm *Gmania) addUsage(u int) {
	gm.lock.Lock()
	defer gm.lock.Unlock()
	gm.usage += u
}

func (gm *Gmania) doRequest(endp, body, referer string) (*http.Response, error) {

	req, _ := http.NewRequest(http.MethodPost, fmt.Sprintf("%s/%s", gm.link, endp), strings.NewReader(body))
	req.Header.Add("Referer", referer)
	req.Header.Add("Origin", "https://www.gamemania.co.ke")
	req.Header.Add("Content-Type", "application/json;charset=UTF-8")
	req.Header.Add("user-agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36")

	return http.DefaultClient.Do(req)
}
func (gm *Gmania) GetFootball(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	bodyFmt := "{\"authToken\":\"string\",\"classId\":3,\"countryName\":\"\",\"numMax\":%d,\"requestId\":\"string\",\"sportId\":\"sr:sport:1\",\"startGameId\":\"\",\"timeBegin\":%d,\"timeEnd\":%d,\"tournamentId\":\"\"}"
	today := time.Now().Unix() * 1000
	tomorrow := time.Now().Add(96*time.Hour).Unix() * 1000
	body := fmt.Sprintf(bodyFmt, 500, today, tomorrow)
	resp, err := gm.doRequest("/matches/odds", body, "https://www.gamemania.co.ke/sport/Football/UpcomingGames")
	if err != nil {
		utils.Error(err)

		return
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)

		return
	}
	// ioutil.WriteFile("out.json", data, 0777)
	gm.addUsage(len(data))
	parsed := gapidetails{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)

		return
	}
	if parsed.Code != 200 {
		utils.Error("Gmania", parsed)

		return
	}

	for _, match := range parsed.Matches {
		game := Game{}
		// game.GameID =

		game.League = match.League
		game.Home = match.Home + Allowed(game.League)
		game.Away = match.Away + Allowed(game.League)
		t := time.Unix(match.Start/1000, 0)
		game.Time = &t
		//gameLink
		gid := strings.Split(match.GameID, ":")[2]
		game.GameID = gid
		gameLink := fmt.Sprintf("https://www.gamemania.co.ke/bettingDetail/Football/%s/sport", game.GameID)
		reqBody := fmt.Sprintf("{\"authToken\":\"string\",\"matchId\":\"sr:match:%s\",\"requestId\":\"string\"}", game.GameID)

		g := gm.getGame(gameLink, reqBody, game)
		if g == nil {
			continue
		}
		if isStopped() {
			return
		}
		games <- *g
	}

	if isStopped() {
		return
	}

}

func (gm *Gmania) GetFootballGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := gm.getGame(gameLink, apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}
func (gm *Gmania) getGame(gameLink, apiLink string, game Game) *Game {
	tw := ThreeWay{Link: gameLink, APILink: apiLink}
	dnb := DrawNoBet{Link: gameLink, APILink: apiLink}
	dc := DoubleChance{Link: gameLink, APILink: apiLink}

	ou15 := OU15{Link: gameLink, APILink: apiLink}
	ou25 := OU25{Link: gameLink, APILink: apiLink}
	ou35 := OU35{Link: gameLink, APILink: apiLink}
	ou45 := OU45{Link: gameLink, APILink: apiLink}

	gng := GoalNoGoal{Link: gameLink}

	resp, err := gm.doRequest("/match/markets", apiLink, gameLink)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	// ioutil.WriteFile("out.json", data, 0777)
	gm.addUsage(len(data))
	parsed := gapimatch{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}
	if parsed.Code != 200 {
		utils.Error("Gmania", parsed.Message)
		return nil
	}
	for _, market := range parsed.Markets {

		switch market.MarketID {
		case 1:
			for _, outcome := range market.Outcomes {
				switch outcome.ID {
				case "1":
					tw.Home = outcome.Odds
				case "2":
					tw.Draw = outcome.Odds
				case "3":
					tw.Away = outcome.Odds
				}
			}

		case 29:
			for _, outcome := range market.Outcomes {
				switch outcome.ID {
				case "74":
					gng.Goal = outcome.Odds
				case "76":
					gng.NoGoal = outcome.Odds
				}
			}
		case 11:
			for _, outcome := range market.Outcomes {
				switch outcome.ID {
				case "4":
					dnb.Home = outcome.Odds
				case "5":
					dnb.Away = outcome.Odds
				}
			}
		case 10:
			for _, outcome := range market.Outcomes {
				switch outcome.ID {
				case "9":
					dc.Home = outcome.Odds
				case "10":
					dc.HomeOrAway = outcome.Odds
				case "11":
					dc.Away = outcome.Odds
				}
			}
		case 18:
			for _, outcome := range market.Outcomes {
				switch outcome.ID {
				case "12":
					switch market.Spec {
					case "total=1.5":
						ou15.Over = outcome.Odds
					case "total=2.5":
						ou25.Over = outcome.Odds
					case "total=3.5":
						ou35.Over = outcome.Odds
					case "total=4.5":
						ou45.Over = outcome.Odds

					}

				case "13":
					switch market.Spec {
					case "total=1.5":
						ou15.Under = outcome.Odds
					case "total=2.5":
						ou25.Under = outcome.Odds
					case "total=3.5":
						ou35.Under = outcome.Odds
					case "total=4.5":
						ou45.Under = outcome.Odds

					}

				}
			}

		}
	}
	game.TWAY = tw
	game.GNG = gng
	game.DNB = dnb
	game.OU15 = ou15
	game.OU25 = ou25
	game.OU35 = ou35
	game.OU45 = ou45
	game.DC = dc
	game.Site = gm.name

	return &game
}

func (gm *Gmania) GetBasketBall(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	bodyFmt := "{\"authToken\":\"string\",\"classId\":6,\"countryName\":\"\",\"numMax\":%d,\"requestId\":\"string\",\"sportId\":\"sr:sport:2\",\"startGameId\":\"\",\"timeBegin\":%d,\"timeEnd\":2067541200000,\"tournamentId\":\"\"}"
	today := time.Now().Unix() * 1000
	// tomorrow := time.Now().Add(96*time.Hour).Unix() * 1000
	body := fmt.Sprintf(bodyFmt, 500, today) //, tomorrow)
	resp, err := gm.doRequest("/matches/odds", body, "https://www.gamemania.co.ke/sport/Basketball/UpcomingGames")
	if err != nil {
		utils.Error(err)

		return
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)

		return
	}
	// ioutil.WriteFile("out.json", data, 0777)
	gm.addUsage(len(data))
	parsed := gapidetails{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)

		return
	}
	if parsed.Code != 200 {
		utils.Error("Gmania", parsed)

		return
	}

	for _, match := range parsed.Matches {
		game := Game{}
		// game.GameID =
		game.Home = match.Home
		game.Away = match.Away
		game.League = match.League
		t := time.Unix(match.Start/1000, 0)
		game.Time = &t
		//gameLink
		gid := strings.Split(match.GameID, ":")[2]
		game.GameID = gid
		gameLink := fmt.Sprintf("https://www.gamemania.co.ke/bettingDetail/Basketball/%s/sport", game.GameID)
		reqBody := fmt.Sprintf("{\"authToken\":\"string\",\"matchId\":\"sr:match:%s\",\"requestId\":\"string\"}", game.GameID)

		g := gm.getBGame(gameLink, reqBody, game)
		if g == nil {
			continue
		}
		if isStopped() {
			return
		}
		games <- *g
	}

	if isStopped() {
		return
	}

}

func (gm *Gmania) getBGame(gameLink, apiLink string, game Game) *Game {
	tw := TwoWay{Link: gameLink, APILink: apiLink}
	// dnb := DrawNoBet{Link: gameLink, APILink: apiLink}
	// dc := DoubleChance{Link: gameLink, APILink: apiLink}

	// ou15 := OU15{Link: gameLink, APILink: apiLink}
	// ou25 := OU25{Link: gameLink, APILink: apiLink}
	// ou35 := OU35{Link: gameLink, APILink: apiLink}
	// ou45 := OU45{Link: gameLink, APILink: apiLink}

	// gng := GoalNoGoal{Link: gameLink}

	resp, err := gm.doRequest("/match/markets", apiLink, gameLink)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	gm.addUsage(len(data))
	parsed := gapimatch{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}
	if parsed.Code != 200 {
		utils.Error("Gmania", parsed.Message)
		return nil
	}
	for _, market := range parsed.Markets {

		switch market.MarketID {
		case 219:
			for _, outcome := range market.Outcomes {
				switch outcome.ID {
				case "4":
					tw.Home = outcome.Odds
				// case "2":
				// 	tw.Draw = outcome.Odds
				case "5":
					tw.Away = outcome.Odds
				}
			}

		}
	}
	game.TWOWAY = tw
	game.Site = gm.name
	return &game
}

func (gm *Gmania) GetBasketBallGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := gm.getBGame(gameLink, apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (gm *Gmania) GetRugby(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	bodyFmt := "{\"authToken\":\"string\",\"classId\":12,\"countryName\":\"\",\"numMax\":%d,\"requestId\":\"string\",\"sportId\":\"sr:sport:12\",\"startGameId\":\"\",\"timeBegin\":%d,\"timeEnd\":2067541200000,\"tournamentId\":\"\"}"
	today := time.Now().Unix() * 1000
	// tomorrow := time.Now().Add(96*time.Hour).Unix() * 1000
	body := fmt.Sprintf(bodyFmt, 500, today) //, tomorrow)
	resp, err := gm.doRequest("/matches/odds", body, "https://www.gamemania.co.ke/sport/Rugby/UpcomingGames")
	if err != nil {
		utils.Error(err)

		return
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)

		return
	}
	// ioutil.WriteFile("out.json", data, 0777)
	gm.addUsage(len(data))
	parsed := gapidetails{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)

		return
	}
	if parsed.Code != 200 {
		utils.Error("Gmania", parsed)

		return
	}

	for _, match := range parsed.Matches {
		game := Game{}
		// game.GameID =
		game.Home = match.Home
		game.Away = match.Away
		game.League = match.League
		t := time.Unix(match.Start/1000, 0)
		game.Time = &t
		//gameLink
		gid := strings.Split(match.GameID, ":")[2]
		game.GameID = gid
		gameLink := fmt.Sprintf("https://www.gamemania.co.ke/bettingDetail/Rugby/%s/sport", game.GameID)
		reqBody := fmt.Sprintf("{\"authToken\":\"string\",\"matchId\":\"sr:match:%s\",\"requestId\":\"string\"}", game.GameID)

		g := gm.getRGame(gameLink, reqBody, game)
		if g == nil {
			continue
		}
		if isStopped() {
			return
		}
		games <- *g
	}

	if isStopped() {
		return
	}

}

func (gm *Gmania) GetRugbyGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := gm.getRGame(gameLink, apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}
func (gm *Gmania) getRGame(gameLink, apiLink string, game Game) *Game {
	tw := ThreeWay{Link: gameLink, APILink: apiLink}
	dnb := DrawNoBet{Link: gameLink, APILink: apiLink}
	dc := DoubleChance{Link: gameLink, APILink: apiLink}

	// ou15 := OU15{Link: gameLink, APILink: apiLink}
	// ou25 := OU25{Link: gameLink, APILink: apiLink}
	// ou35 := OU35{Link: gameLink, APILink: apiLink}
	// ou45 := OU45{Link: gameLink, APILink: apiLink}

	// gng := GoalNoGoal{Link: gameLink}

	resp, err := gm.doRequest("/match/markets", apiLink, gameLink)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	gm.addUsage(len(data))
	parsed := gapimatch{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}
	if parsed.Code != 200 {
		utils.Error("Gmania", parsed.Message)
		return nil
	}
	for _, market := range parsed.Markets {

		switch market.MarketID {
		case 1:
			for _, outcome := range market.Outcomes {
				switch outcome.ID {
				case "1":
					tw.Home = outcome.Odds
				case "2":
					tw.Draw = outcome.Odds
				case "3":
					tw.Away = outcome.Odds
				}
			}

		case 11:
			for _, outcome := range market.Outcomes {
				switch outcome.ID {
				case "4":
					dnb.Home = outcome.Odds
				case "5":
					dnb.Away = outcome.Odds
				}
			}
		case 10:
			for _, outcome := range market.Outcomes {
				switch outcome.ID {
				case "9":
					dc.Home = outcome.Odds
				case "10":
					dc.HomeOrAway = outcome.Odds
				case "11":
					dc.Away = outcome.Odds
				}
			}

		}
	}
	game.TWAY = tw
	game.DNB = dnb
	game.DC = dc

	game.Site = gm.name
	return &game
}

func (gm *Gmania) GetTennis(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	bodyFmt := "{\"authToken\":\"string\",\"classId\":8,\"countryName\":\"\",\"numMax\":%d,\"requestId\":\"string\",\"sportId\":\"sr:sport:5\",\"startGameId\":\"\",\"timeBegin\":%d,\"timeEnd\":2067541200000,\"tournamentId\":\"\"}"
	today := time.Now().Unix() * 1000
	// tomorrow := time.Now().Add(96*time.Hour).Unix() * 1000
	body := fmt.Sprintf(bodyFmt, 500, today) //, tomorrow)
	resp, err := gm.doRequest("/matches/odds", body, "https://www.gamemania.co.ke/sport/Tennis/UpcomingGames")
	if err != nil {
		utils.Error(err)

		return
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)

		return
	}
	// ioutil.WriteFile("out.json", data, 0777)
	gm.addUsage(len(data))
	parsed := gapidetails{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)

		return
	}
	if parsed.Code != 200 {
		utils.Error("Gmania", parsed)

		return
	}

	for _, match := range parsed.Matches {
		game := Game{}
		// game.GameID =
		game.Home = match.Home
		game.Away = match.Away
		game.League = match.League
		t := time.Unix(match.Start/1000, 0)
		game.Time = &t
		//gameLink
		gid := strings.Split(match.GameID, ":")[2]
		game.GameID = gid
		gameLink := fmt.Sprintf("https://www.gamemania.co.ke/bettingDetail/Tennis/%s/sport", game.GameID)
		reqBody := fmt.Sprintf("{\"authToken\":\"string\",\"matchId\":\"sr:match:%s\",\"requestId\":\"string\"}", game.GameID)

		g := gm.getTGame(gameLink, reqBody, game)
		if g == nil {
			continue
		}
		if isStopped() {
			return
		}
		games <- *g
	}

	if isStopped() {
		return
	}

}

func (gm *Gmania) getTGame(gameLink, apiLink string, game Game) *Game {
	tw := TwoWay{Link: gameLink, APILink: apiLink}
	// dnb := DrawNoBet{Link: gameLink, APILink: apiLink}
	// dc := DoubleChance{Link: gameLink, APILink: apiLink}

	// ou15 := OU15{Link: gameLink, APILink: apiLink}
	// ou25 := OU25{Link: gameLink, APILink: apiLink}
	// ou35 := OU35{Link: gameLink, APILink: apiLink}
	// ou45 := OU45{Link: gameLink, APILink: apiLink}

	// gng := GoalNoGoal{Link: gameLink}

	resp, err := gm.doRequest("/match/markets", apiLink, gameLink)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	gm.addUsage(len(data))
	parsed := gapimatch{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}
	if parsed.Code != 200 {
		utils.Error("Gmania", parsed.Message)
		return nil
	}
	for _, market := range parsed.Markets {

		switch market.MarketID {
		case 186:
			for _, outcome := range market.Outcomes {
				switch outcome.ID {
				case "4":
					tw.Home = outcome.Odds
				// case "2":
				// 	tw.Draw = outcome.Odds
				case "5":
					tw.Away = outcome.Odds
				}
			}

		}
	}
	game.TWOWAY = tw
	game.Site = gm.name

	return &game
}

func (gm *Gmania) GetTennisGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := gm.getTGame(gameLink, apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (gm *Gmania) GetHandball(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (gm *Gmania) GetHandballGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (gm *Gmania) GetIceHockey(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (gm *Gmania) GetIceHockeyGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (gm *Gmania) UsageReset() {
	gm.usage = 0
	gm.mainpage = 0
}
func (gm *Gmania) MainPageUsage() int {
	return gm.mainpage
}
