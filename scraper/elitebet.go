package scraper

import (
	"crypto/tls"
	"fmt"
	"net/http"
	"strings"
	"sync"
	"time"

	"gitlab.com/arbke/utils"

	"io/ioutil"

	"strconv"

	"github.com/PuerkitoBio/goquery"
	"github.com/headzoo/surf"
	"github.com/headzoo/surf/browser"
)

type Elitebet struct {
	name      string
	url       string
	navigator *browser.Browser
	usage     int
}

func NewElitebet() *Elitebet {
	ebet := &Elitebet{
		name:      "Elitebet",
		url:       "https://www.elitebetkenya.com",
		navigator: surf.NewBrowser(),
	}
	ebet.navigator.SetUserAgent("Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
	ebet.navigator.AddRequestHeader("accept-encoding", "gzip, deflate, br")
	return ebet
}

func (ebet *Elitebet) Name() string {
	return ebet.name
}

func (ebet *Elitebet) MainPageUsage() int {
	return 0
}

func (ebet *Elitebet) Usage() int {
	return ebet.usage
}

func (ebet *Elitebet) UsageReset() {
	ebet.usage = 0
}

func (ebet *Elitebet) getCookies() []*http.Cookie {

	// client := http.Client{}
	// data := url.Values{}
	// data.Add("p", pass)
	// data.Add("phone", phone)
	// data.Add("submitted", "TRUE")

	// req, _ := http.NewRequest(http.MethodPost, "https://www.elitebetkenya.com/login.php", strings.NewReader(data.Encode()))

	ebet.navigator.AddRequestHeader("origin", "https://www.elitebetkenya.com")
	ebet.navigator.AddRequestHeader("upgrade-insecure-requests", "1")
	ebet.navigator.AddRequestHeader("user-agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
	// ebet.navigator.AddRequestHeader("content-type", "application/x-www-form-urlencoded")
	ebet.navigator.AddRequestHeader("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
	ebet.navigator.AddRequestHeader("referer", "https://www.elitebetkenya.com")
	ebet.navigator.AddRequestHeader("accept-language", "en-US,en;q=0.8")
	ebet.navigator.AddRequestHeader("cache-control", "no-cache")

	// resp, err := client.Do(req)
	err := ebet.navigator.Open("https://www.elitebetkenya.com")
	if err != nil {
		utils.Error(err)
		return nil
	}

	utils.Info(len(ebet.navigator.Body()))
	ebet.usage += len([]byte(ebet.navigator.Body()))
	return ebet.navigator.SiteCookies()

}

func (ebet *Elitebet) GetFootball(transport *http.Transport, games chan Game, stop chan struct{}) {

	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	// ebet.navigator.SetTransport(transport)
	cookies := ebet.getCookies()
	url := "https://elitebet-sportsbook-web.exaloc.net/prelive_event/"

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("upgrade-insecure-requests", "1")
	req.Header.Add("user-agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
	req.Header.Add("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
	req.Header.Add("referer", "https://elitebet-sportsbook-web.exaloc.net/?token=&language=en")
	// req.Header.Add("accept-encoding", "gzip, deflate, sdch, br")

	for _, c := range cookies {
		req.AddCookie(c)
		// utils.Debug(c.String())
	}

	customClient := http.Client{
		Transport: &http.Transport{
			// Proxy:           http.ProxyURL(proxy.Next()),
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	}

	resp, err := customClient.Do(req)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	defer resp.Body.Close()
	data, _ := ioutil.ReadAll(resp.Body)
	ebet.usage += len(data)
	// utils.Debug(len(data))
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(string(data)))
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	leagues := []string{}
	doc.Find("#featured-games-menu").Find(".listItem").Find(".countriesList").Find(".countryItem").Each(func(i int, s *goquery.Selection) {
		s.Find(".leaguesList").Find(".leagueItem").Each(func(i int, s *goquery.Selection) {
			leagueLink, _ := s.Find("a").Attr("href")
			if strings.Contains(leagueLink, "football") {
				leagues = append(leagues, leagueLink)

			}
		})
	})
	// ioutil.WriteFile("elite.html", data, 0777)

	// return games
	nav := ebet.cloneBrowser()
	for _, league := range leagues {
		if isStopped() {
			return
		}
		if err := nav.Open(fmt.Sprintf("https://elitebet-sportsbook-web.exaloc.net%s", league)); err != nil {
			utils.Error(err)
			continue
		}
		// utils.Debug(fmt.Sprintf("https://elitebet-sportsbook-web.exaloc.net%s", league))

		nav.Find(".prelive-browse-container").Find(".prelive-list").Find(".single-event").Find("#football-wrapper").Find("#events_holder").Find(".grouped-event-list").Find("div").Each(func(i int, s *goquery.Selection) {
			if isStopped() {
				return
			}
			// utils.Debug(s.Attr("id"))
			game := Game{}
			game.Site = "Elitebet"
			game.Link = ebet.url

			_, exists := s.Attr("id")

			if !exists {
				return
			}

			market := s.Find(".single-event").Find("div").Last()
			if market == nil {
				return
			}
			marketLink := s.Find(".single-event").Find(".friendly-id").Find("a").AttrOr("href", "")
			// utils.Debug(marketLink)

			apiLink := fmt.Sprintf("https://elitebet-sportsbook-web.exaloc.net%s", marketLink)

			g := ebet.getGame(apiLink, game, cookies...)
			if isStopped() {
				return
			}
			if g == nil {
				return
			}
			// utils.Debug(*g)

			games <- *g

		})
	}

	if isStopped() {
		return
	}

}

func (ebet *Elitebet) getGame(apiLink string, game Game, cookies ...*http.Cookie) *Game {
	dc := DoubleChance{APILink: apiLink}
	tw := ThreeWay{APILink: apiLink}
	ou25 := OU25{APILink: apiLink}
	ou35 := OU35{APILink: apiLink}
	ou15 := OU15{APILink: apiLink}
	ou45 := OU45{APILink: apiLink}
	dnb := DrawNoBet{APILink: apiLink}
	gng := GoalNoGoal{APILink: apiLink}

	customClient := http.Client{
		Transport: &http.Transport{
			// Proxy:           http.ProxyURL(proxy.Next()),
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	}

	req, _ := http.NewRequest("GET", apiLink, nil)
	req.Header.Add("upgrade-insecure-requests", "1")
	req.Header.Add("user-agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
	req.Header.Add("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
	req.Header.Add("referer", "https://elitebet-sportsbook-web.exaloc.net/prelive_event/")

	for _, c := range cookies {
		req.AddCookie(c)
	}

	resp, err := customClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}

	defer resp.Body.Close()
	data, _ := ioutil.ReadAll(resp.Body)

	// ioutil.WriteFile("elite.html", data, 0777)
	ebet.usage += len(data)
	// utils.Debug(string(data))
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(string(data)))
	if err != nil {
		utils.Error(err)
		return nil
	}

	nameStr := doc.Find(".single-event").Find(".single-event-title").Find(".event-name").Text()

	// utils.Debug(nameStr)
	teams := strings.Split(nameStr, " - ")

	if len(teams) != 2 {
		return nil
	}

	game.Home = teams[0]
	game.Away = teams[1]

	gameTimestr := doc.Find(".single-event").Find(".single-event-title").Find("span").AttrOr("data-time", "")
	_time, _ := strconv.Atoi(gameTimestr)
	t := time.Unix(int64(_time), 0)
	game.Time = &t
	doc.Find(".single-event").Find(".market-block").Each(func(i int, s *goquery.Selection) {
		marketName := s.Find(".lines").AttrOr("data-market", "")
		ouSpecial := strings.Trim(s.Find(".market-name").Text(), " ")
		// utils.Debug(ouSpecial)
		if strings.Contains(ouSpecial, "Over/Under Full time") {
			marketName = "OU"
		}
		switch marketName {
		case "MRFTNA":
			s.Find(".lines").Find("div").Each(func(i int, s *goquery.Selection) {
				outcome := s.Find("input").AttrOr("data-bet-id", "")
				oddStr := s.Find("input").AttrOr("value", "")
				oddVal, _ := strconv.ParseFloat(oddStr, 64)
				switch outcome {
				case "1":
					tw.Home = oddVal
				case "0":
					tw.Draw = oddVal
				case "2":
					tw.Away = oddVal
				}
			})
		case "DCFTNA":
			s.Find(".lines").Find("div").Each(func(i int, s *goquery.Selection) {
				outcome := s.Find("input").AttrOr("data-bet-id", "")
				oddStr := s.Find("input").AttrOr("value", "")
				oddVal, _ := strconv.ParseFloat(oddStr, 64)
				switch outcome {
				case "7":
					dc.HomeOrAway = oddVal
				case "6":
					dc.Home = oddVal
				case "8":
					dc.Away = oddVal
				}
			})

		case "NDFTNA":
			s.Find(".lines").Find("div").Each(func(i int, s *goquery.Selection) {
				outcome := s.Find("input").AttrOr("data-bet-id", "")
				oddStr := s.Find("input").AttrOr("value", "")
				oddVal, _ := strconv.ParseFloat(oddStr, 64)
				switch outcome {
				case "9":
					dnb.Home = oddVal
				case "10":
					dnb.Away = oddVal
				}
			})

		case "BTSNA":
			s.Find(".lines").Find("div").Each(func(i int, s *goquery.Selection) {
				outcome := s.Find("input").AttrOr("data-bet-id", "")
				oddStr := s.Find("input").AttrOr("value", "")
				oddVal, _ := strconv.ParseFloat(oddStr, 64)
				switch outcome {
				case "16":
					gng.Goal = oddVal
				case "17":
					gng.NoGoal = oddVal
				}
			})
		case "OU":
			s.Find(".lines").Each(func(i int, s *goquery.Selection) {
				ouType := s.AttrOr("data-market", "")
				// utils.Debug(ouType)
				switch ouType {
				// case "OUFT0_5":
				// 	s.Find(".lines").Find("div").Each(func(i int, s *goquery.Selection) {
				// 		_,indicator := s.Attr("line-name","")
				// 		if indicator{
				// 			return
				// 		}
				// 		outcome := s.Find("input").AttrOr("data-bet-id", "")
				// 		oddStr := s.Find("input").AttrOr("value", "")
				// 		oddVal, _ := strconv.ParseFloat(oddStr, 64)
				// 		switch outcome {
				// 		case "100":
				// 			ou15.Over = oddVal
				// 		case "101":
				// 			ou15.Under = oddVal
				// 		}
				// 	})
				case "OUFT1_5":
					s.Find("div").Each(func(i int, s *goquery.Selection) {
						if i == 0 {
							return
						}
						outcome := s.Find("input").AttrOr("data-bet-id", "")
						oddStr := s.Find("input").AttrOr("value", "")
						oddVal, _ := strconv.ParseFloat(oddStr, 64)
						switch outcome {
						case "102":
							ou15.Over = oddVal
						case "103":
							ou15.Under = oddVal
						}
					})
				case "OUFT2_5":
					// utils.Debug("HERE")

					s.Find("div").Each(func(i int, s *goquery.Selection) {
						// utils.Debug("HERE")
						if i == 0 {
							return
						}
						outcome := s.Find(".css-checkbox").AttrOr("data-bet-id", "")
						oddStr := s.Find(".css-checkbox").AttrOr("value", "")
						oddVal, _ := strconv.ParseFloat(oddStr, 64)

						// utils.Debug(outcome, oddVal)
						switch outcome {
						case "11":
							ou25.Over = oddVal
						case "12":
							ou25.Under = oddVal
						}
					})
				case "OUFT3_5":
					s.Find("div").Each(func(i int, s *goquery.Selection) {
						if i == 0 {
							return
						}
						outcome := s.Find("input").AttrOr("data-bet-id", "")
						oddStr := s.Find("input").AttrOr("value", "")
						oddVal, _ := strconv.ParseFloat(oddStr, 64)
						switch outcome {
						case "104":
							ou35.Over = oddVal
						case "105":
							ou35.Under = oddVal
						}
					})
				case "OUFT4_5":
					s.Find("div").Each(func(i int, s *goquery.Selection) {
						if i == 0 {
							return
						}
						outcome := s.Find("input").AttrOr("data-bet-id", "")
						oddStr := s.Find("input").AttrOr("value", "")
						oddVal, _ := strconv.ParseFloat(oddStr, 64)
						switch outcome {
						case "106":
							ou45.Over = oddVal
						case "107":
							ou45.Under = oddVal
						}
					})
				}
			})
		}

	})

	game.TWAY = tw
	game.DC = dc
	game.DNB = dnb
	game.GNG = gng
	game.OU25 = ou25
	game.OU15 = ou15
	game.OU35 = ou35
	game.OU45 = ou45

	return &game
}

func (ebet *Elitebet) GetFootballGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := ebet.getGame(apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (ebet *Elitebet) GetRugby(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (ebet *Elitebet) GetRugbyGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (ebet *Elitebet) GetTennis(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (ebet *Elitebet) GetTennisGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (ebet *Elitebet) GetBasketBall(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (ebet *Elitebet) GetBasketBallGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (ebet *Elitebet) GetHandball(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (ebet *Elitebet) GetHandballGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (ebet *Elitebet) GetIceHockey(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (ebet *Elitebet) GetIceHockeyGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (ebet *Elitebet) cloneBrowser() *browser.Browser {
	nav := surf.NewBrowser()
	// nav.SetHistoryJar(jar.NewMemoryHistory())
	// nav.SetCookieJar(jar.NewMemoryCookies())
	nav.SetUserAgent("Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
	return nav
}
