package scraper

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"sync"

	"gitlab.com/arbke/utils"

	"fmt"
	"strings"
	"time"

	"github.com/headzoo/surf"
	"github.com/headzoo/surf/browser"
	"github.com/headzoo/surf/jar"
)

type Dafabet struct {
	name      string
	url       string
	navigator *browser.Browser
	usage     int

	lock *sync.Mutex
}

func NewDafabet() *Dafabet {
	dfbet := &Dafabet{
		name:      "Dafabet",
		url:       "https://www.dafabet.co.ke",
		navigator: surf.NewBrowser(),
		lock:      &sync.Mutex{},
	}
	dfbet.navigator.SetHistoryJar(jar.NewMemoryHistory())
	dfbet.navigator.SetUserAgent("Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
	dfbet.navigator.AddRequestHeader("Accept-Encoding", "gzip, deflate, br")
	return dfbet
}

func (dfbet *Dafabet) Name() string {
	return dfbet.name
}

func (dfbet *Dafabet) MainPageUsage() int {
	return 0
}

func (dfbet *Dafabet) addUsage(u int) {
	dfbet.lock.Lock()
	defer dfbet.lock.Unlock()
	dfbet.usage += u
}

func (dfbet *Dafabet) Usage() int {
	dfbet.lock.Lock()
	defer dfbet.lock.Unlock()
	return dfbet.usage
}

func (dfbet *Dafabet) UsageReset() {
	dfbet.usage = 0
}

func (dfbet *Dafabet) getAuth(client http.Client) ([]*http.Cookie, string) {
	if err := dfbet.navigator.Open("https://dafabet.co.ke/home"); err != nil {
		utils.Error(err)
		return nil, ""
	}
	dfbet.usage += len([]byte(dfbet.navigator.Body()))

	parsedData := map[string]interface{}{}

	cookies := dfbet.navigator.SiteCookies()
	req, _ := http.NewRequest(http.MethodGet, "https://dafabet.co.ke/m/acc/token?lineId=12&originId=31", nil)
	req.Header.Add("Referer", "https://dafabet.co.ke/sports-african/foot")

	resp, err := client.Do(req)
	if err != nil {
		utils.Error(err)
		return nil, ""
	}
	// utils.Debug(resp.Header.Get("Content-Encoding"))
	token, _ := ioutil.ReadAll(resp.Body)
	dfbet.usage += len([]byte(token))
	if err := json.Unmarshal(token, &parsedData); err != nil {
		utils.Debug(string(token))
		return nil, ""
	}
	// utils.Debug(string(token))

	hsToken := parsedData["hsToken"].(string)
	return cookies, hsToken
}
func (dfbet *Dafabet) makeRequest(client http.Client, url, referer, token string, cookies ...*http.Cookie) (*http.Response, error) {

	req, _ := http.NewRequest(http.MethodGet, url, nil)
	for _, c := range cookies {
		utils.Debug(c.Name, c.Value)
		req.AddCookie(c)
	}
	req.Header.Add("Accept-Language", "en-KE")
	req.Header.Add("X-LVS-PriceFormat", "EURO")
	req.Header.Add("timezone", "+0300")
	req.Header.Add("Referer", referer)
	req.Header.Add("X-LVS-HSToken", token)
	req.AddCookie(&http.Cookie{
		Name:  "i18next",
		Value: "en",
	})
	req.AddCookie(&http.Cookie{
		Name:  "pnctest",
		Value: "1",
	})

	return client.Do(req)

}

func (dfbet *Dafabet) GetFootball(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	dfbet.navigator.SetTransport(transport)

	parsedData := map[string]interface{}{}
	client := http.Client{}
	client.Transport = transport
	//get month number
	cookies, token := dfbet.getAuth(client)

	month := fmt.Sprintf("%.2d", int(time.Now().Month()))
	year := fmt.Sprintf("%d", time.Now().Year())
	day := fmt.Sprintf("%.2d", time.Now().Day())
	hour := fmt.Sprintf("%.2d", time.Now().Hour())
	minute := fmt.Sprintf("%.2d", time.Now().Minute())
	second := fmt.Sprintf("%.2d", time.Now().Second())

	tomorrow := time.Now().Add(time.Hour * 24 * 5)
	tomonth := fmt.Sprintf("%.2d", tomorrow.Month())
	toyear := fmt.Sprintf("%d", tomorrow.Year())
	today := fmt.Sprintf("%.2d", tomorrow.Day())
	tohour := fmt.Sprintf("%.2d", tomorrow.Hour())
	tominute := fmt.Sprintf("%.2d", tomorrow.Minute())
	tosecond := fmt.Sprintf("%.2d", tomorrow.Second())

	url := fmt.Sprintf("https://dafabet.co.ke/m/ff/acoupon/sFOOT/FOOT-AFRICAN?lineId=12&eventType=GAME&fromDate=%s-%s-%sT%s:%s:%s+03:00&toDate=%s-%s-%sT%s:%s:%s+03:00&african=1&originId=31", year, month, day, hour, minute, second, toyear, tomonth, today, tohour, tominute, tosecond)

	resp, err := dfbet.makeRequest(client, url, "https://dafabet.co.ke/sports-african/foot", token, cookies...)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
	}
	// ioutil.WriteFile("out.json", data, 0777)
	dfbet.addUsage(len(data))
	// utils.Debug(string(data))
	// return nil
	//////////////////////////////

	if err := json.Unmarshal(data, &parsedData); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	// utils.Debug(string(data))
	items := parsedData["items"].(map[string]interface{})
	for k, v := range items {
		game := Game{}
		holder := v.(map[string]interface{})
		// gameIdentifier := k
		//Is a game
		if strings.HasPrefix(k, "e") {
			// fmt.Println(holder)
			// p := holder["path"].(map[string]interface{})
			// p[""]
			// holder[""]
			team1 := holder["a"].(string)
			team2 := holder["b"].(string)

			startStr := holder["start"].(string)
			t := utils.DecodeTime(dfbet.name, startStr).Add(3 * time.Hour)
			game.Time = &t
			// startUnix, _ := strconv.Atoi(startUnixStr)
			// utils.Debug(startUnix)
			// game.Time = time.Unix(int64(startUnix), 0).Format(time.ANSIC)

			game.Home = team1
			game.Away = team2
			game.Site = dfbet.name
			game.Link = dfbet.url

			// utils.Debug(gameLink)

			if tmp := Allowed(holder["pdesc"].(string)); len(tmp) > 0 {
				game.Home = fmt.Sprintf("%s %s", game.Home, tmp)
				game.Away = fmt.Sprintf("%s %s", game.Away, tmp)
			}

			gameLink := fmt.Sprintf("https://dafabet.co.ke/sports/foot/%s", k)
			apiLink := fmt.Sprintf("https://dafabet.co.ke/m/ff/%s?ext=1&up=0&sortByTime=true&lineId=12&originId=31", k)

			g := dfbet.getGame(client, gameLink, apiLink, token, game, cookies...)
			if isStopped() {
				return
			}
			if g == nil {
				continue
			}
			// utils.Debug(*g)

			games <- *g
		}

	}
	if isStopped() {
		return
	}

}
func (dfbet *Dafabet) getGame(client http.Client, gameLink, apiLink, token string, game Game, cookies ...*http.Cookie) *Game {
	parsedData := map[string]interface{}{}
	resp, err := dfbet.makeRequest(client, apiLink, "https://dafabet.co.ke/sports/foot", token, cookies...)
	if err != nil {
		utils.Error(err)
		return nil
	}

	data, _ := ioutil.ReadAll(resp.Body)
	// ioutil.WriteFile("out.json", data, 0777)
	dfbet.addUsage(len(data))

	if err := json.Unmarshal(data, &parsedData); err != nil {
		utils.Error(err)
		return nil
	}
	dc := DoubleChance{Link: gameLink, APILink: apiLink}
	dnb := DrawNoBet{Link: gameLink, APILink: apiLink}
	tw := ThreeWay{Link: gameLink, APILink: apiLink}
	gng := GoalNoGoal{Link: gameLink, APILink: apiLink}
	ou25 := OU25{Link: gameLink, APILink: apiLink}

	if _, ok := parsedData["items"]; !ok {
		return nil
	}
	items := parsedData["items"].(map[string]interface{})
	//search for the available bets
	for k, v := range items {
		oddIdent := k
		holder := v.(map[string]interface{})
		if strings.HasPrefix(k, "e") {
			if len(game.Home) == 0 || len(game.Away) == 0 {
				game.Home = holder["a"].(string)
				game.Away = holder["b"].(string)
			}
		}
		if strings.HasPrefix(k, "m") {
			oddType := int(holder["pos"].(float64))
			// parent := holder["parent"].(string)

			criteria := strings.Trim(holder["desc"].(string), " ")
			period := holder["period"].(string)

			//get the odd details
			for k, v := range items {
				holder := v.(map[string]interface{})
				if strings.HasPrefix(k, "o") {
					// utils.Debug(holder)
					parent := holder["parent"].(string)
					if holder["price"] == nil {
						continue
					}
					desc := strings.Trim(holder["desc"].(string), " ")

					odd, err := strconv.ParseFloat(holder["price"].(string), 64)
					if err != nil {
						utils.Error(err)
					}
					if parent == oddIdent {

						switch oddType {
						case 2: //if DNB
							if criteria != "Head To Head" {
								break
							}
							if period != "Regulation Time" {
								break
							}
							t := int(holder["pos"].(float64))
							if t == 1 {
								dnb.Home = odd
							}
							if t == 3 {
								dnb.Away = odd
							}

						//if double chance
						case 9:
							if criteria != "DC" {
								break
							}
							t := int(holder["pos"].(float64))
							if t == 1 {
								//set 1x
								dc.Home = odd
							}
							if t == 3 {
								//set x2
								dc.Away = odd
							}
							if t == 2 {
								//set 12
								dc.HomeOrAway = odd
							}
						case 1: //1x2
							if criteria != "3-Way (1X2)" {
								break
							}
							if period != "Regulation Time" {
								break
							}
							t := int(holder["pos"].(float64))
							if t == 1 {
								// utils.Debug(desc, game.Home)
								//set 1
								tw.Home = odd
							}
							if t == 3 {
								//set 2
								tw.Away = odd
							}
							if t == 2 {
								//set x
								tw.Draw = odd
							}

						case 6:
							if criteria != "Over/Under (Total Goals) 2.5" {
								break
							}
							if period != "Regulation Time" {
								break
							}
							t := int(holder["pos"].(float64))
							if t == 2 {
								//set under 2.5
								ou25.Under = odd
							}
							if t == 1 {
								//set over2.5
								ou25.Over = odd
							}

						case 63:
							if criteria != "Goal Goal (Both Teams Score)" {
								break
							}
							if period != "Regulation Time" {
								break
							}

							if strings.Contains(desc, "No") {
								//set no goal
								gng.NoGoal = odd
							}
							if strings.Contains(desc, "Yes") {
								//set goal goal
								gng.Goal = odd
							}

						}
					}
				}
			}

		}
	}
	game.TWAY = tw
	game.DC = dc
	game.GNG = gng
	game.OU25 = ou25
	game.DNB = dnb
	return &game
}
func (dfbet *Dafabet) GetFootballGame(t *http.Transport, gameLink string, apiLink string) Game {

	client := http.Client{}
	client.Transport = t
	//get month number
	cookies, token := dfbet.getAuth(client)
	g := dfbet.getGame(client, gameLink, apiLink, token, Game{}, cookies...)
	if g == nil {
		return Game{}
	}
	return *g

}

func (dfbet *Dafabet) GetTennis(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	dfbet.navigator.SetTransport(transport)

	parsedData := map[string]interface{}{}
	client := http.Client{}
	client.Transport = transport
	//get month number
	cookies, token := dfbet.getAuth(client)
	utils.Debug(token)

	month := fmt.Sprintf("%.2d", int(time.Now().Month()))
	year := fmt.Sprintf("%d", time.Now().Year())
	day := fmt.Sprintf("%.2d", time.Now().Day())
	hour := fmt.Sprintf("%.2d", time.Now().Hour())
	minute := fmt.Sprintf("%.2d", time.Now().Minute())
	second := fmt.Sprintf("%.2d", time.Now().Second())

	tomorrow := time.Now().Add(time.Hour * 24 * 5)
	tomonth := fmt.Sprintf("%.2d", tomorrow.Month())
	toyear := fmt.Sprintf("%d", tomorrow.Year())
	today := fmt.Sprintf("%.2d", tomorrow.Day())
	tohour := fmt.Sprintf("%.2d", tomorrow.Hour())
	tominute := fmt.Sprintf("%.2d", tomorrow.Minute())
	tosecond := fmt.Sprintf("%.2d", tomorrow.Second())

	//https://dafabet.co.ke/m/ff/acoupon/sTENN/TENN-AFRICAN?eventType=GAME&fromDate=fromDate=2018-09-10T13:34:54+03:00&toDate=2018-09-15T13:34:54+03:00&lineId=12&originId=31
	//https://dafabet.co.ke/m/ff/acoupon/sTENN/TENN-AFRICAN?lineId=12&eventType=GAME&fromDate=2018-09-09T23:59:59+03:00&toDate=2018-09-11T00:00:00+03:00&originId=31

	url := fmt.Sprintf("https://dafabet.co.ke/m/ff/acoupon/sTENN/TENN-AFRICAN?eventType=GAME&fromDate=%s-%s-%sT%s:%s:%s+03:00&toDate=%s-%s-%sT%s:%s:%s+03:00&marketTypeGroups=MONEY_LINE&lineId=12&originId=31", year, month, day, hour, minute, second, toyear, tomonth, today, tohour, tominute, tosecond)

	utils.Debug(url)
	resp, err := dfbet.makeRequest(client, url, "https://dafabet.co.ke/sports/tenn", token, cookies...)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	data, _ := ioutil.ReadAll(resp.Body)
	// utils.Debug(string(data))
	// ioutil.WriteFile("out.json", data, 0777)
	dfbet.addUsage(len(data))
	// utils.Debug(string(data))
	// return nil
	//////////////////////////////

	if err := json.Unmarshal(data, &parsedData); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	// utils.Debug(string(data))
	items := parsedData["items"].(map[string]interface{})
	for k, v := range items {
		game := Game{}
		holder := v.(map[string]interface{})
		// gameIdentifier := k
		//Is a game
		if strings.HasPrefix(k, "e") {
			// fmt.Println(holder)
			// p := holder["path"].(map[string]interface{})
			// p[""]
			// holder[""]
			team1 := holder["a"].(string)
			team2 := holder["b"].(string)

			startStr := holder["start"].(string)
			t := utils.DecodeTime(dfbet.name, startStr).Add(3 * time.Hour)
			game.Time = &t
			// startUnix, _ := strconv.Atoi(startUnixStr)
			// utils.Debug(startUnix)
			// game.Time = time.Unix(int64(startUnix), 0).Format(time.ANSIC)

			game.Home = team1
			game.Away = team2
			game.Site = dfbet.name
			game.Link = dfbet.url

			// utils.Debug(gameLink)

			gameLink := fmt.Sprintf("https://dafabet.co.ke/sports/tenn/%s", k)
			apiLink := fmt.Sprintf("https://dafabet.co.ke/m/ff/%s?ext=1&up=0&sortByTime=true&lineId=12&originId=31", k)

			g := dfbet.getTGame(client, gameLink, apiLink, token, game, cookies...)
			if isStopped() {
				return
			}
			if g == nil {
				continue
			}
			// utils.Debug(*g)

			games <- *g
		}

	}
	if isStopped() {
		return
	}

}
func (dfbet *Dafabet) getTGame(client http.Client, gameLink, apiLink, token string, game Game, cookies ...*http.Cookie) *Game {
	parsedData := map[string]interface{}{}
	resp, err := dfbet.makeRequest(client, apiLink, "https://dafabet.co.ke/sports/tenn", token, cookies...)
	if err != nil {
		utils.Error(err)
		return nil
	}

	data, _ := ioutil.ReadAll(resp.Body)
	// ioutil.WriteFile("out.json", data, 0777)
	dfbet.addUsage(len(data))

	if err := json.Unmarshal(data, &parsedData); err != nil {
		utils.Error(err)
		return nil
	}
	tw := TwoWay{Link: gameLink, APILink: apiLink}

	if _, ok := parsedData["items"]; !ok {
		return nil
	}
	items := parsedData["items"].(map[string]interface{})
	//search for the available bets
	for k, v := range items {
		oddIdent := k
		holder := v.(map[string]interface{})
		if strings.HasPrefix(k, "e") {
			if len(game.Home) == 0 || len(game.Away) == 0 {
				game.Home = holder["a"].(string)
				game.Away = holder["b"].(string)
			}
		}
		if strings.HasPrefix(k, "m") {
			oddType := int(holder["pos"].(float64))
			// parent := holder["parent"].(string)

			// criteria := strings.Trim(holder["desc"].(string), " ")
			// period := holder["period"].(string)

			//get the odd details
			for k, v := range items {
				holder := v.(map[string]interface{})
				if strings.HasPrefix(k, "o") {
					// utils.Debug(holder)
					parent := holder["parent"].(string)
					if holder["price"] == nil {
						continue
					}
					// desc := strings.Trim(holder["desc"].(string), " ")

					odd, err := strconv.ParseFloat(holder["price"].(string), 64)
					if err != nil {
						utils.Error(err)
					}
					if parent == oddIdent {

						switch oddType {
						case 2: //H2H
							// if criteria != "Head To Head" {
							// 	break
							// }
							// if period != "Regulation Time" {
							// 	break
							// }
							t := int(holder["pos"].(float64))
							if t == 1 {
								tw.Home = odd
							}
							if t == 3 {
								tw.Away = odd
							}

						}
					}
				}
			}

		}
	}
	game.TWOWAY = tw

	return &game
}
func (dfbet *Dafabet) GetTennisGame(t *http.Transport, gameLink string, apiLink string) Game {

	client := http.Client{}
	client.Transport = t
	//get month number
	cookies, token := dfbet.getAuth(client)
	g := dfbet.getTGame(client, gameLink, apiLink, token, Game{}, cookies...)
	if g == nil {
		return Game{}
	}
	return *g

}

func (dfbet *Dafabet) GetBasketBall(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	dfbet.navigator.SetTransport(transport)

	parsedData := map[string]interface{}{}
	client := http.Client{}
	client.Transport = transport
	//get month number
	cookies, token := dfbet.getAuth(client)

	month := fmt.Sprintf("%.2d", int(time.Now().Month()))
	year := fmt.Sprintf("%d", time.Now().Year())
	day := fmt.Sprintf("%.2d", time.Now().Day())
	hour := fmt.Sprintf("%.2d", time.Now().Hour())
	minute := fmt.Sprintf("%.2d", time.Now().Minute())
	second := fmt.Sprintf("%.2d", time.Now().Second())

	tomorrow := time.Now().Add(time.Hour * 24 * 5)
	tomonth := fmt.Sprintf("%.2d", tomorrow.Month())
	toyear := fmt.Sprintf("%d", tomorrow.Year())
	today := fmt.Sprintf("%.2d", tomorrow.Day())
	tohour := fmt.Sprintf("%.2d", tomorrow.Hour())
	tominute := fmt.Sprintf("%.2d", tomorrow.Minute())
	tosecond := fmt.Sprintf("%.2d", tomorrow.Second())

	//https://dafabet.co.ke/m/ff/acoupon/sBASK/BASK-AFRICAN?eventType=GAME&fromDate=2018-09-10T13:45:35+03:00&toDate=2018-09-15T13:45:35+03:00&marketTypeGroups=MONEY_LINE&lineId=12&originId=31/
	//https://dafabet.co.ke/m/ff/acoupon/sBASK/BASK-AFRICAN?lineId=12&eventType=GAME&fromDate=2018-09-12T23:59:59+03:00&toDate=2018-09-14T00:00:00+03:00&originId=31
	url := fmt.Sprintf("https://dafabet.co.ke/m/ff/acoupon/sBASK/BASK-AFRICAN?eventType=GAME&fromDate=%s-%s-%sT%s:%s:%s+03:00&toDate=%s-%s-%sT%s:%s:%s+03:00&marketTypeGroups=MONEY_LINE&lineId=12&originId=31", year, month, day, hour, minute, second, toyear, tomonth, today, tohour, tominute, tosecond)
	// utils.Debug(url)
	resp, err := dfbet.makeRequest(client, url, "https://dafabet.co.ke/sports-african/bask", token, cookies...)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	data, _ := ioutil.ReadAll(resp.Body)
	// ioutil.WriteFile("out.json", data, 0777)
	dfbet.addUsage(len(data))
	// utils.Debug(string(data))
	// return nil
	//////////////////////////////

	if err := json.Unmarshal(data, &parsedData); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	// utils.Debug(string(data))
	items := parsedData["items"].(map[string]interface{})
	for k, v := range items {
		game := Game{}
		holder := v.(map[string]interface{})
		// gameIdentifier := k
		//Is a game
		if strings.HasPrefix(k, "e") {
			// fmt.Println(holder)
			// p := holder["path"].(map[string]interface{})
			// p[""]
			// holder[""]
			team1 := holder["a"].(string)
			team2 := holder["b"].(string)

			startStr := holder["start"].(string)
			t := utils.DecodeTime(dfbet.name, startStr).Add(3 * time.Hour)
			game.Time = &t
			// startUnix, _ := strconv.Atoi(startUnixStr)
			// utils.Debug(startUnix)
			// game.Time = time.Unix(int64(startUnix), 0).Format(time.ANSIC)

			game.Home = team1
			game.Away = team2
			game.Site = dfbet.name
			game.Link = dfbet.url

			// utils.Debug(gameLink)

			gameLink := fmt.Sprintf("https://dafabet.co.ke/sports/bask/%s", k)
			apiLink := fmt.Sprintf("https://dafabet.co.ke/m/ff/%s?ext=1&up=0&sortByTime=true&lineId=12&originId=31", k)

			g := dfbet.getBGame(client, gameLink, apiLink, token, game, cookies...)
			if isStopped() {
				return
			}
			if g == nil {
				continue
			}
			// utils.Debug(*g)

			games <- *g
		}

	}
	if isStopped() {
		return
	}

}
func (dfbet *Dafabet) getBGame(client http.Client, gameLink, apiLink, token string, game Game, cookies ...*http.Cookie) *Game {
	parsedData := map[string]interface{}{}
	resp, err := dfbet.makeRequest(client, apiLink, "https://dafabet.co.ke/sports/bask", token, cookies...)
	if err != nil {
		utils.Error(err)
		return nil
	}

	data, _ := ioutil.ReadAll(resp.Body)
	// ioutil.WriteFile("out.json", data, 0777)
	dfbet.addUsage(len(data))

	if err := json.Unmarshal(data, &parsedData); err != nil {
		utils.Error(err)
		return nil
	}
	tw := TwoWay{Link: gameLink, APILink: apiLink}

	if _, ok := parsedData["items"]; !ok {
		return nil
	}
	items := parsedData["items"].(map[string]interface{})
	//search for the available bets
	for k, v := range items {
		oddIdent := k
		holder := v.(map[string]interface{})
		if strings.HasPrefix(k, "e") {
			if len(game.Home) == 0 || len(game.Away) == 0 {
				game.Home = holder["a"].(string)
				game.Away = holder["b"].(string)
			}
		}
		if strings.HasPrefix(k, "m") {
			oddType := int(holder["pos"].(float64))
			// parent := holder["parent"].(string)

			criteria := strings.Trim(holder["desc"].(string), " ")
			period := holder["period"].(string)

			//get the odd details
			for k, v := range items {
				holder := v.(map[string]interface{})
				if strings.HasPrefix(k, "o") {
					// utils.Debug(holder)
					parent := holder["parent"].(string)
					if holder["price"] == nil {
						continue
					}
					// desc := strings.Trim(holder["desc"].(string), " ")

					odd, err := strconv.ParseFloat(holder["price"].(string), 64)
					if err != nil {
						utils.Error(err)
					}
					if parent == oddIdent {

						switch oddType {
						case 1: //H2H
							if criteria != "Head To Head" {
								break
							}
							if period != "Match" {
								break
							}
							t := int(holder["pos"].(float64))
							if t == 1 {
								tw.Home = odd
							}
							if t == 3 {
								tw.Away = odd
							}

						}
					}
				}
			}

		}
	}
	game.TWOWAY = tw

	return &game
}
func (dfbet *Dafabet) GetBasketBallGame(t *http.Transport, gameLink string, apiLink string) Game {

	client := http.Client{}
	client.Transport = t
	//get month number
	cookies, token := dfbet.getAuth(client)
	g := dfbet.getBGame(client, gameLink, apiLink, token, Game{}, cookies...)
	if g == nil {
		return Game{}
	}
	return *g

}

func (dfbet *Dafabet) GetRugby(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	dfbet.navigator.SetTransport(transport)

	parsedData := map[string]interface{}{}
	client := http.Client{}
	client.Transport = transport
	//get month number
	cookies, token := dfbet.getAuth(client)

	month := fmt.Sprintf("%.2d", int(time.Now().Month()))
	year := fmt.Sprintf("%d", time.Now().Year())
	day := fmt.Sprintf("%.2d", time.Now().Day())
	hour := fmt.Sprintf("%.2d", time.Now().Hour())
	minute := fmt.Sprintf("%.2d", time.Now().Minute())
	second := fmt.Sprintf("%.2d", time.Now().Second())

	tomorrow := time.Now().Add(time.Hour * 24 * 5)
	tomonth := fmt.Sprintf("%.2d", tomorrow.Month())
	toyear := fmt.Sprintf("%d", tomorrow.Year())
	today := fmt.Sprintf("%.2d", tomorrow.Day())
	tohour := fmt.Sprintf("%.2d", tomorrow.Hour())
	tominute := fmt.Sprintf("%.2d", tomorrow.Minute())
	tosecond := fmt.Sprintf("%.2d", tomorrow.Second())

	url := fmt.Sprintf("https://dafabet.co.ke/m/ff/acoupon/sRUGU/RUGU-AFRICAN?eventType=GAME&fromDate=%s-%s-%sT%s:%s:%s+03:00&toDate=%s-%s-%sT%s:%s:%s+03:00&marketTypeGroups=MONEY_LINE&lineId=12&originId=31", year, month, day, hour, minute, second, toyear, tomonth, today, tohour, tominute, tosecond)

	resp, err := dfbet.makeRequest(client, url, "https://dafabet.co.ke/sports-african/rugu", token, cookies...)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	data, _ := ioutil.ReadAll(resp.Body)
	// ioutil.WriteFile("out.json", data, 0777)
	dfbet.addUsage(len(data))
	// utils.Debug(string(data))
	// return nil
	//////////////////////////////

	if err := json.Unmarshal(data, &parsedData); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	// utils.Debug(string(data))
	items := parsedData["items"].(map[string]interface{})
	for k, v := range items {
		game := Game{}
		holder := v.(map[string]interface{})
		// gameIdentifier := k
		//Is a game
		if strings.HasPrefix(k, "e") {
			// fmt.Println(holder)
			// p := holder["path"].(map[string]interface{})
			// p[""]
			// holder[""]
			team1 := holder["a"].(string)
			team2 := holder["b"].(string)

			startStr := holder["start"].(string)
			t := utils.DecodeTime(dfbet.name, startStr).Add(3 * time.Hour)
			game.Time = &t
			// startUnix, _ := strconv.Atoi(startUnixStr)
			// utils.Debug(startUnix)
			// game.Time = time.Unix(int64(startUnix), 0).Format(time.ANSIC)

			game.Home = team1
			game.Away = team2
			game.Site = dfbet.name
			game.Link = dfbet.url

			// utils.Debug(gameLink)

			gameLink := fmt.Sprintf("https://dafabet.co.ke/sports/rugu/%s", k)
			apiLink := fmt.Sprintf("https://dafabet.co.ke/m/ff/%s?ext=1&up=0&sortByTime=true&lineId=12&originId=31", k)

			g := dfbet.getRGame(client, gameLink, apiLink, token, game, cookies...)
			if isStopped() {
				return
			}
			if g == nil {
				continue
			}
			// utils.Debug(*g)

			games <- *g
		}

	}
	if isStopped() {
		return
	}

}
func (dfbet *Dafabet) getRGame(client http.Client, gameLink, apiLink, token string, game Game, cookies ...*http.Cookie) *Game {
	parsedData := map[string]interface{}{}
	resp, err := dfbet.makeRequest(client, apiLink, "https://dafabet.co.ke/sports/rugu", token, cookies...)
	if err != nil {
		utils.Error(err)
		return nil
	}

	data, _ := ioutil.ReadAll(resp.Body)
	// ioutil.WriteFile("out.json", data, 0777)
	dfbet.addUsage(len(data))

	if err := json.Unmarshal(data, &parsedData); err != nil {
		utils.Error(err)
		return nil
	}
	tw := ThreeWay{Link: gameLink, APILink: apiLink}

	if _, ok := parsedData["items"]; !ok {
		return nil
	}
	items := parsedData["items"].(map[string]interface{})
	//search for the available bets
	for k, v := range items {
		oddIdent := k
		holder := v.(map[string]interface{})
		if strings.HasPrefix(k, "e") {
			if len(game.Home) == 0 || len(game.Away) == 0 {
				game.Home = holder["a"].(string)
				game.Away = holder["b"].(string)
			}
		}
		if strings.HasPrefix(k, "m") {
			oddType := int(holder["pos"].(float64))
			style := holder["style"].(string)
			period := holder["period"].(string)
			// parent := holder["parent"].(string)

			// criteria := strings.Trim(holder["desc"].(string), " ")
			// period := holder["period"].(string)

			//get the odd details
			for k, v := range items {
				holder := v.(map[string]interface{})
				if strings.HasPrefix(k, "o") {
					// utils.Debug(holder)
					parent := holder["parent"].(string)
					if holder["price"] == nil {
						continue
					}
					// desc := strings.Trim(holder["desc"].(string), " ")

					odd, err := strconv.ParseFloat(holder["price"].(string), 64)
					if err != nil {
						utils.Error(err)
					}
					if parent == oddIdent {

						switch oddType {
						case 1: //3way

							if style != "WIN_DRAW_WIN" {
								break
							}
							if period != "80 Mins" {
								break
							}
							t := int(holder["pos"].(float64))
							if t == 1 {
								tw.Home = odd
							}
							if t == 2 {
								tw.Draw = odd
							}
							if t == 3 {
								tw.Away = odd
							}

						}
					}
				}
			}

		}
	}
	game.TWAY = tw

	return &game
}
func (dfbet *Dafabet) GetRugbyGame(t *http.Transport, gameLink string, apiLink string) Game {

	client := http.Client{}
	client.Transport = t
	//get month number
	cookies, token := dfbet.getAuth(client)
	g := dfbet.getRGame(client, gameLink, apiLink, token, Game{}, cookies...)
	if g == nil {
		return Game{}
	}
	return *g

}

func (dfbet *Dafabet) GetIceHockey(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	dfbet.navigator.SetTransport(transport)

	parsedData := map[string]interface{}{}
	client := http.Client{}
	client.Transport = transport
	//get month number
	cookies, token := dfbet.getAuth(client)

	month := fmt.Sprintf("%.2d", int(time.Now().Month()))
	year := fmt.Sprintf("%d", time.Now().Year())
	day := fmt.Sprintf("%.2d", time.Now().Day())
	hour := fmt.Sprintf("%.2d", time.Now().Hour())
	minute := fmt.Sprintf("%.2d", time.Now().Minute())
	second := fmt.Sprintf("%.2d", time.Now().Second())

	tomorrow := time.Now().Add(time.Hour * 24 * 5)
	tomonth := fmt.Sprintf("%.2d", tomorrow.Month())
	toyear := fmt.Sprintf("%d", tomorrow.Year())
	today := fmt.Sprintf("%.2d", tomorrow.Day())
	tohour := fmt.Sprintf("%.2d", tomorrow.Hour())
	tominute := fmt.Sprintf("%.2d", tomorrow.Minute())
	tosecond := fmt.Sprintf("%.2d", tomorrow.Second())

	url := fmt.Sprintf("https://dafabet.co.ke/m/ff/sICEH?eventType=GAME&fromDate=%s-%s-%sT%s:%s:%s+03:00&toDate=%s-%s-%sT%s:%s:%s+03:00&marketTypeGroups=MONEY_LINE&lineId=12&originId=31", year, month, day, hour, minute, second, toyear, tomonth, today, tohour, tominute, tosecond)

	resp, err := dfbet.makeRequest(client, url, "https://dafabet.co.ke/sports/iceh", token, cookies...)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	data, _ := ioutil.ReadAll(resp.Body)
	// ioutil.WriteFile("out.json", data, 0777)
	dfbet.addUsage(len(data))
	// utils.Debug(string(data))
	// return nil
	//////////////////////////////

	if err := json.Unmarshal(data, &parsedData); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	// utils.Debug(string(data))
	items := parsedData["items"].(map[string]interface{})
	for k, v := range items {
		game := Game{}
		holder := v.(map[string]interface{})
		// gameIdentifier := k
		//Is a game
		if strings.HasPrefix(k, "e") {
			// fmt.Println(holder)
			// p := holder["path"].(map[string]interface{})
			// p[""]
			// holder[""]
			team1 := holder["a"].(string)
			team2 := holder["b"].(string)

			startStr := holder["start"].(string)
			t := utils.DecodeTime(dfbet.name, startStr).Add(3 * time.Hour)
			game.Time = &t
			// startUnix, _ := strconv.Atoi(startUnixStr)
			// utils.Debug(startUnix)
			// game.Time = time.Unix(int64(startUnix), 0).Format(time.ANSIC)

			game.Home = team1
			game.Away = team2
			game.Site = dfbet.name
			game.Link = dfbet.url

			// utils.Debug(gameLink)

			gameLink := fmt.Sprintf("https://dafabet.co.ke/sports/iceh/%s", k)
			apiLink := fmt.Sprintf("https://dafabet.co.ke/m/ff/%s?ext=1&up=0&sortByTime=true&lineId=12&originId=31", k)

			g := dfbet.getIHGame(client, gameLink, apiLink, token, game, cookies...)
			if isStopped() {
				return
			}
			if g == nil {
				continue
			}
			// utils.Debug(*g)

			games <- *g
		}

	}
	if isStopped() {
		return
	}

}
func (dfbet *Dafabet) getIHGame(client http.Client, gameLink, apiLink, token string, game Game, cookies ...*http.Cookie) *Game {
	parsedData := map[string]interface{}{}
	resp, err := dfbet.makeRequest(client, apiLink, "https://dafabet.co.ke/sports/iceh", token, cookies...)
	if err != nil {
		utils.Error(err)
		return nil
	}

	data, _ := ioutil.ReadAll(resp.Body)
	// ioutil.WriteFile("out.json", data, 0777)
	dfbet.addUsage(len(data))

	dc := DoubleChance{Link: gameLink, APILink: apiLink}
	dnb := DrawNoBet{Link: gameLink, APILink: apiLink}
	tw := ThreeWay{Link: gameLink, APILink: apiLink}
	// gng := GoalNoGoal{Link: gameLink, APILink: apiLink}
	// ou25 := OU25{Link: gameLink, APILink: apiLink}

	if err := json.Unmarshal(data, &parsedData); err != nil {
		utils.Error(err)
		return nil
	}

	if _, ok := parsedData["items"]; !ok {
		return nil
	}
	items := parsedData["items"].(map[string]interface{})
	//search for the available bets
	for k, v := range items {
		oddIdent := k
		holder := v.(map[string]interface{})
		if strings.HasPrefix(k, "e") {
			if len(game.Home) == 0 || len(game.Away) == 0 {
				game.Home = holder["a"].(string)
				game.Away = holder["b"].(string)
			}
		}
		if strings.HasPrefix(k, "m") {
			oddType := int(holder["pos"].(float64))
			style := holder["style"].(string)
			period := holder["period"].(string)
			// parent := holder["parent"].(string)

			// criteria := strings.Trim(holder["desc"].(string), " ")
			// period := holder["period"].(string)

			//get the odd details
			for k, v := range items {
				holder := v.(map[string]interface{})
				if strings.HasPrefix(k, "o") {
					// utils.Debug(holder)
					parent := holder["parent"].(string)
					if holder["price"] == nil {
						continue
					}
					// desc := strings.Trim(holder["desc"].(string), " ")

					odd, err := strconv.ParseFloat(holder["price"].(string), 64)
					if err != nil {
						utils.Error(err)
					}
					if parent == oddIdent {

						switch oddType {
						case 1: //3way

							if style != "WIN_DRAW_WIN" {
								break
							}
							if period != "60 Mins" {
								break
							}
							t := int(holder["pos"].(float64))
							if t == 1 {
								tw.Home = odd
							}
							if t == 2 {
								tw.Draw = odd
							}
							if t == 3 {
								tw.Away = odd
							}
						case 2: //3way

							if style != "TWO_OUTCOME_LONG" {
								break
							}
							if period != "60 Mins" {
								break
							}
							t := int(holder["pos"].(float64))
							if t == 1 {
								dnb.Home = odd
							}

							if t == 3 {
								dnb.Away = odd
							}

						case 9: //3way

							if style != "THREE_TEAMS_PRESENT" {
								break
							}
							if period != "60 Mins" {
								break
							}
							t := int(holder["pos"].(float64))
							if t == 1 {
								dc.Home = odd
							}
							if t == 2 {
								dc.HomeOrAway = odd
							}
							if t == 3 {
								dc.Away = odd
							}
						}
					}
				}
			}

		}
	}
	game.TWAY = tw
	game.DC = dc
	game.DNB = dnb
	return &game
}
func (dfbet *Dafabet) GetIceHockeyGame(t *http.Transport, gameLink string, apiLink string) Game {

	client := http.Client{}
	client.Transport = t
	//get month number
	cookies, token := dfbet.getAuth(client)
	g := dfbet.getIHGame(client, gameLink, apiLink, token, Game{}, cookies...)
	if g == nil {
		return Game{}
	}
	return *g

}

func (dfbet *Dafabet) GetHandball(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	dfbet.navigator.SetTransport(transport)

	parsedData := map[string]interface{}{}
	client := http.Client{}
	client.Transport = transport
	//get month number
	cookies, token := dfbet.getAuth(client)

	month := fmt.Sprintf("%.2d", int(time.Now().Month()))
	year := fmt.Sprintf("%d", time.Now().Year())
	day := fmt.Sprintf("%.2d", time.Now().Day())
	hour := fmt.Sprintf("%.2d", time.Now().Hour())
	minute := fmt.Sprintf("%.2d", time.Now().Minute())
	second := fmt.Sprintf("%.2d", time.Now().Second())

	tomorrow := time.Now().Add(time.Hour * 24 * 5)
	tomonth := fmt.Sprintf("%.2d", tomorrow.Month())
	toyear := fmt.Sprintf("%d", tomorrow.Year())
	today := fmt.Sprintf("%.2d", tomorrow.Day())
	tohour := fmt.Sprintf("%.2d", tomorrow.Hour())
	tominute := fmt.Sprintf("%.2d", tomorrow.Minute())
	tosecond := fmt.Sprintf("%.2d", tomorrow.Second())

	url := fmt.Sprintf("https://dafabet.co.ke/m/ff/sHAND?eventType=GAME&fromDate=%s-%s-%sT%s:%s:%s+03:00&toDate=%s-%s-%sT%s:%s:%s+03:00&marketTypeGroups=MONEY_LINE&lineId=12&originId=31", year, month, day, hour, minute, second, toyear, tomonth, today, tohour, tominute, tosecond)

	resp, err := dfbet.makeRequest(client, url, "https://dafabet.co.ke/sports/hand", token, cookies...)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	data, _ := ioutil.ReadAll(resp.Body)
	// ioutil.WriteFile("out.json", data, 0777)
	dfbet.addUsage(len(data))
	// utils.Debug(string(data))
	// return nil
	//////////////////////////////

	if err := json.Unmarshal(data, &parsedData); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	// utils.Debug(string(data))
	items := parsedData["items"].(map[string]interface{})
	for k, v := range items {
		game := Game{}
		holder := v.(map[string]interface{})
		// gameIdentifier := k
		//Is a game
		if strings.HasPrefix(k, "e") {
			// fmt.Println(holder)
			// p := holder["path"].(map[string]interface{})
			// p[""]
			// holder[""]
			team1 := holder["a"].(string)
			team2 := holder["b"].(string)

			startStr := holder["start"].(string)
			t := utils.DecodeTime(dfbet.name, startStr).Add(3 * time.Hour)
			game.Time = &t
			// startUnix, _ := strconv.Atoi(startUnixStr)
			// utils.Debug(startUnix)
			// game.Time = time.Unix(int64(startUnix), 0).Format(time.ANSIC)

			game.Home = team1
			game.Away = team2
			game.Site = dfbet.name
			game.Link = dfbet.url

			// utils.Debug(gameLink)

			gameLink := fmt.Sprintf("https://dafabet.co.ke/sports/hand/%s", k)
			apiLink := fmt.Sprintf("https://dafabet.co.ke/m/ff/%s?ext=1&up=0&sortByTime=true&lineId=12&originId=31", k)

			g := dfbet.getHGame(client, gameLink, apiLink, token, game, cookies...)
			if isStopped() {
				return
			}
			if g == nil {
				continue
			}
			// utils.Debug(*g)

			games <- *g
		}

	}
	if isStopped() {
		return
	}

}
func (dfbet *Dafabet) getHGame(client http.Client, gameLink, apiLink, token string, game Game, cookies ...*http.Cookie) *Game {
	parsedData := map[string]interface{}{}
	resp, err := dfbet.makeRequest(client, apiLink, "https://dafabet.co.ke/sports/hand", token, cookies...)
	if err != nil {
		utils.Error(err)
		return nil
	}

	data, _ := ioutil.ReadAll(resp.Body)
	// ioutil.WriteFile("out.json", data, 0777)
	dfbet.addUsage(len(data))

	tw := ThreeWay{Link: gameLink, APILink: apiLink}

	if err := json.Unmarshal(data, &parsedData); err != nil {
		utils.Error(err)
		return nil
	}

	if _, ok := parsedData["items"]; !ok {
		return nil
	}
	items := parsedData["items"].(map[string]interface{})
	//search for the available bets
	for k, v := range items {
		oddIdent := k
		holder := v.(map[string]interface{})
		if strings.HasPrefix(k, "e") {
			if len(game.Home) == 0 || len(game.Away) == 0 {
				game.Home = holder["a"].(string)
				game.Away = holder["b"].(string)
			}
		}
		if strings.HasPrefix(k, "m") {
			oddType := int(holder["pos"].(float64))
			style := holder["style"].(string)
			period := holder["period"].(string)
			// parent := holder["parent"].(string)

			// criteria := strings.Trim(holder["desc"].(string), " ")
			// period := holder["period"].(string)

			//get the odd details
			for k, v := range items {
				holder := v.(map[string]interface{})
				if strings.HasPrefix(k, "o") {
					// utils.Debug(holder)
					parent := holder["parent"].(string)
					if holder["price"] == nil {
						continue
					}
					// desc := strings.Trim(holder["desc"].(string), " ")

					odd, err := strconv.ParseFloat(holder["price"].(string), 64)
					if err != nil {
						utils.Error(err)
					}
					if parent == oddIdent {

						switch oddType {
						case 1: //3way

							if style != "WIN_DRAW_WIN" {
								break
							}
							if period != "60 Mins" {
								break
							}
							t := int(holder["pos"].(float64))
							if t == 1 {
								tw.Home = odd
							}
							if t == 2 {
								tw.Draw = odd
							}
							if t == 3 {
								tw.Away = odd
							}

						}
					}
				}
			}

		}
	}
	game.TWAY = tw

	return &game
}
func (dfbet *Dafabet) GetHandballGame(t *http.Transport, gameLink string, apiLink string) Game {

	client := http.Client{}
	client.Transport = t
	//get month number
	cookies, token := dfbet.getAuth(client)
	g := dfbet.getIHGame(client, gameLink, apiLink, token, Game{}, cookies...)
	if g == nil {
		return Game{}
	}
	return *g

}

//
//https://dafabet.co.ke/m/ff/sRUGU?eventType=GAME&fromDate=2018-09-09T00:00:00+03:00&toDate=2018-09-10T00:00:00+03:00&marketTypeGroups=MONEY_LINE&lineId=12&originId=31
//https://dafabet.co.ke/m/ff/sBASK?eventType=GAME&fromDate=2018-09-09T00:00:00+03:00&toDate=2018-09-10T00:00:00+03:00&marketTypeGroups=MONEY_LINE&lineId=12&originId=31
