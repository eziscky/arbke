package scraper

type TwoWay struct {
	APILink string  `json:"apiLink"`
	Link    string  `json:"link"`
	Home    float64 `json:"1"`
	Away    float64 `json:"2"`
}

type ThreeWay struct {
	APILink string  `json:"apiLink"`
	Link    string  `json:"link"`
	Home    float64 `json:"1"`
	Draw    float64 `json:"X"`
	Away    float64 `json:"2"`
}

type DoubleChance struct {
	APILink    string  `json:"apiLink"`
	Link       string  `json:"link"`
	Home       float64 `json:"1"`
	Away       float64 `json:"2"`
	HomeOrAway float64 `json:"12"`
}

type GoalNoGoal struct {
	APILink string  `json:"apiLink"`
	Link    string  `json:"link"`
	Goal    float64 `json:"Yes"`
	NoGoal  float64 `json:"No"`
}

type OU15 struct {
	APILink string  `json:"apiLink"`
	Link    string  `json:"link"`
	Over    float64 `json:"Over"`
	Under   float64 `json:"Under"`
}

func (ou HOU15) Over_() float64  { return ou.Over }
func (ou HOU15) Under_() float64 { return ou.Under }

type OU25 struct {
	APILink string  `json:"apiLink"`
	Link    string  `json:"link"`
	Over    float64 `json:"Over"`
	Under   float64 `json:"Under"`
}

func (ou HOU25) Over_() float64  { return ou.Over }
func (ou HOU25) Under_() float64 { return ou.Under }

type OU35 struct {
	APILink string  `json:"apiLink"`
	Link    string  `json:"link"`
	Over    float64 `json:"Over"`
	Under   float64 `json:"Under"`
}

func (ou HOU35) Over_() float64  { return ou.Over }
func (ou HOU35) Under_() float64 { return ou.Under }

type OU45 struct {
	APILink string  `json:"apiLink"`
	Link    string  `json:"link"`
	Over    float64 `json:"Over"`
	Under   float64 `json:"Under"`
}

func (ou HOU45) Over_() float64  { return ou.Over }
func (ou HOU45) Under_() float64 { return ou.Under }

type DrawNoBet struct {
	APILink string  `json:"apiLink"`
	Link    string  `json:"link"`
	Home    float64 `json:"1"`
	Away    float64 `json:"2"`
}

//Highest values

type HThreeWay struct {
	HomeSite string
	AwaySite string
	DrawSite string

	HomeSiteLink string
	DrawSiteLink string
	AwaySiteLink string

	HomeSiteAPILink string
	DrawSiteAPILink string
	AwaySiteAPILink string

	Home       float64
	Draw       float64
	Away       float64
	HDeviation float64
	DDeviation float64
	ADeviation float64
}
type HTwoWay struct {
	HomeSite string
	AwaySite string

	HomeSiteLink string

	AwaySiteLink string

	HomeSiteAPILink string

	AwaySiteAPILink string

	Home float64

	Away       float64
	HDeviation float64

	ADeviation float64
}

type HDoubleChance struct {
	HomeSite string
	Home     float64
	AwaySite string
	Away     float64

	HomeSiteLink string
	AwaySiteLink string

	HomeSiteAPILink string
	AwaySiteAPILink string

	HASite     string
	HomeOrAway float64
	HDeviation float64
	ADeviation float64
}

type HGoalNoGoal struct {
	GoalSite   string
	Goal       float64
	NoGoalSite string

	GoalSiteLink   string
	NoGoalSiteLink string

	GoalSiteAPILink   string
	NoGoalSiteAPILink string

	NoGoal      float64
	GDeviation  float64
	NGDeviation float64
}

type HOU15 struct {
	OverSite  string
	UnderSite string

	OverSiteLink  string
	UnderSiteLink string

	OverSiteAPILink  string
	UnderSiteAPILink string

	Over       float64
	Under      float64
	Deviation  float64
	ODeviation float64
	UDeviation float64
}

type HOU25 struct {
	OverSite  string
	UnderSite string

	OverSiteLink  string
	UnderSiteLink string

	OverSiteAPILink  string
	UnderSiteAPILink string

	Over       float64
	Under      float64
	Deviation  float64
	ODeviation float64
	UDeviation float64
}

type HOU35 struct {
	OverSite  string
	UnderSite string

	OverSiteLink  string
	UnderSiteLink string

	OverSiteAPILink  string
	UnderSiteAPILink string

	Over       float64
	Under      float64
	ODeviation float64
	UDeviation float64
}

type HOU45 struct {
	OverSite  string
	UnderSite string

	OverSiteLink  string
	UnderSiteLink string

	OverSiteAPILink  string
	UnderSiteAPILink string

	Over       float64
	Under      float64
	Deviation  float64
	ODeviation float64
	UDeviation float64
}

type HDrawNoBet struct {
	HomeSite string
	AwaySite string

	HomeSiteLink string
	AwaySiteLink string

	HomeSiteAPILink string
	AwaySiteAPILink string

	Home       float64
	Away       float64
	HDeviation float64
	ADeviation float64
}
