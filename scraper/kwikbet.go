package scraper

import (
	"fmt"
	"net/http"
	"strings"
	"sync"

	"gitlab.com/arbke/utils"

	"strconv"

	"github.com/PuerkitoBio/goquery"
	"github.com/headzoo/surf"
	"github.com/headzoo/surf/browser"
	"github.com/headzoo/surf/jar"
)

type Kwikbet struct {
	name      string
	url       string
	navigator *browser.Browser
	usage     int
	mainpage  int
}

func NewKwikBet() *Kwikbet {
	kbet := &Kwikbet{
		name:      "Kwikbet",
		url:       "https://www.kwikbet.co.ke",
		navigator: surf.NewBrowser(),
	}
	kbet.navigator.SetHistoryJar(jar.NewMemoryHistory())
	kbet.navigator.SetUserAgent("Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
	kbet.navigator.AddRequestHeader("Accept-Encoding", "gzip, deflate, br")
	return kbet
}

func (kbet *Kwikbet) Name() string {
	return kbet.name
}

func (kbet *Kwikbet) MainPageUsage() int {
	return kbet.mainpage
}

func (kbet *Kwikbet) Usage() int {
	return kbet.usage
}

func (kbet *Kwikbet) UsageReset() {
	kbet.usage = 0
	kbet.mainpage = 0
}

func (kbet *Kwikbet) cloneBrowser() *browser.Browser {
	nav := surf.NewBrowser()
	nav.SetHistoryJar(jar.NewMemoryHistory())
	nav.SetCookieJar(jar.NewMemoryCookies())
	nav.SetUserAgent("Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
	nav.AddRequestHeader("Accept-Encoding", "gzip, deflate, br")
	return nav
}
func (kbet *Kwikbet) GetFootball(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() {
		close(games)
	}()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	competitions := []int{166, 97, 12804, 10040, 5813, 1430, 281, 1069, 2012, 340, 87, 299, 6145, 137, 3128, 7826, 77, 10498, 5345, 1307, 12278, 136, 823, 1473, 1904, 6256, 1302, 153, 31, 17, 5666, 10, 348, 7, 23, 320, 26, 346, 236, 12732, 12735, 1277, 2135, 240, 63, 56, 11306, 115, 141, 92, 84, 6464, 3374, 12713, 12726, 12722, 12730, 12714, 12719, 12731, 12712, 12721, 12728, 12715, 12718, 12710, 12727, 12717, 12716, 3889, 3654, 3469, 3470, 3471, 3472, 4850, 5062, 5066, 12840, 5512, 152, 118, 116, 128, 1189, 1193, 478, 5931, 5112, 5111, 5942, 138, 37, 176, 2534, 3050, 2013, 99, 3851, 252, 1296, 114, 8970, 10646, 227, 1401, 1273, 233, 232, 231, 19, 4150, 246, 154, 228, 149, 6147, 7255, 219, 2006, 2975, 10855, 2010, 257, 361, 180, 38, 1608, 122, 6167, 1287, 5610}
	for _, c := range competitions {
		if isStopped() {
			return
		}
		if err := kbet.navigator.Open(fmt.Sprintf("https://kwikbet.co.ke/competition?id=%d", c)); err != nil {
			utils.Error(err)
			return
		}
		kbet.usage += len([]byte(kbet.navigator.Body()))
		kbet.mainpage = kbet.usage
		// utils.Debug(kbet.navigator.ResponseHeaders().Get("Content-Encoding"))

		kbet.navigator.Find(".panel").Find(".mobi-list").Find(".top-matches").Each(func(i int, s *goquery.Selection) {
			if isStopped() {
				return
			}
			game := Game{}
			game.Site = "Kwikbet"
			game.Link = kbet.url
			path, _ := s.Find(".side").Attr("href")
			meta := s.Find(".meta").Text()

			game.Time = utils.DecodeTime(kbet.name, meta)

			// utils.Debug(path)
			nav := kbet.cloneBrowser()

			gameLink := fmt.Sprintf("https://kwikbet.co.ke/%s", path)
			// utils.Debug(gameLink)

			tw := ThreeWay{Link: gameLink}
			dnb := DrawNoBet{Link: gameLink}
			dc := DoubleChance{Link: gameLink}
			ou25 := OU25{Link: gameLink}
			gng := GoalNoGoal{Link: gameLink}

			if err := nav.Open(gameLink); err != nil {
				utils.Error(err)
				return
			}
			kbet.usage += len([]byte(nav.Body()))

			allowed := ""
			nav.Find(".panel-header").Find("span").Each(func(i int, s *goquery.Selection) {
				allowed += Allowed(s.Text())
			})
			// utils.Debug((&nav).Body())
			nav.Find(".gz").Find(".top-matches").Find(".pick").Each(func(i int, s *goquery.Selection) {
				if isStopped() {
					return
				}
				oddType, _ := s.Attr("odd-key")
				innerType, _ := s.Attr("special-value-value")
				oddName, _ := s.Attr("oddtype")
				oddValStr, _ := s.Attr("oddvalue")
				oddVal, _ := strconv.ParseFloat(oddValStr, 64)
				team1, _ := s.Attr("hometeam")
				team2, _ := s.Attr("awayteam")
				game.Home = team1
				game.Away = team2
				if len(allowed) > 0 {
					game.Home = fmt.Sprintf("%s %s", game.Home, allowed)
					game.Away = fmt.Sprintf("%s %s", game.Away, allowed)
				}

				// utils.Debug(oddName, oddType, oddValStr)
				switch strings.ToUpper(oddType) {
				case "1":
					if strings.Trim(oddName, " ") == "3 Way" {
						tw.Home = oddVal
					}
					if strings.Trim(oddName, " ") == "Draw no bet" {
						dnb.Home = oddVal
						//TODO: DNB
					}
				case "X":
					if strings.Trim(oddName, " ") == "3 Way" {
						tw.Draw = oddVal
					}

				case "2":
					if strings.Trim(oddName, " ") == "3 Way" {
						tw.Away = oddVal
					}
					if strings.Trim(oddName, " ") == "Draw no bet" {
						dnb.Away = oddVal
						//TODO: DNB
					}
				case "1X":
					if strings.Trim(oddName, " ") == "Double Chance" {
						dc.Home = oddVal
					}

				case "X2":
					if strings.Trim(oddName, " ") == "Double Chance" {
						dc.Away = oddVal
					}
				case "12":
					if strings.Trim(oddName, " ") == "Double Chance" {
						dc.HomeOrAway = oddVal
					}
				case "OVER":
					if strings.Trim(oddName, " ") == "Totals Over Under" {
						if strings.Trim(innerType, " ") == "2.5" {
							ou25.Over = oddVal
						}
					}
				case "UNDER":
					if strings.Trim(oddName, " ") == "Totals Over Under" {
						if strings.Trim(innerType, " ") == "2.5" {
							ou25.Under = oddVal
						}
					}
				case "NO":
					if strings.Trim(oddName, " ") == "Both Teams to Score" {
						gng.NoGoal = oddVal
					}
				case "YES":
					if strings.Trim(oddName, " ") == "Both Teams to Score" {
						gng.Goal = oddVal
					}
				}
			})
			game.TWAY = tw
			game.DC = dc
			game.DNB = dnb
			game.OU25 = ou25
			game.GNG = gng

			// utils.Debug(game)
			if isStopped() {
				return
			}
			games <- game
		})
	}
	if isStopped() {
		return
	}
}

func (kbet *Kwikbet) GetFootballGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (kbet *Kwikbet) GetRugby(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (kbet *Kwikbet) GetRugbyGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (kbet *Kwikbet) GetTennis(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (kbet *Kwikbet) GetTennisGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (kbet *Kwikbet) GetBasketBall(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (kbet *Kwikbet) GetBasketBallGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (kbet *Kwikbet) GetHandball(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (kbet *Kwikbet) GetHandballGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (kbet *Kwikbet) GetIceHockey(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (kbet *Kwikbet) GetIceHockeyGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}
