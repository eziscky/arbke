package scraper

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"sync"

	"gitlab.com/arbke/utils"

	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/headzoo/surf"
	"github.com/headzoo/surf/browser"
	"github.com/headzoo/surf/jar"
)

type Betway struct {
	name      string
	url       string
	navigator *browser.Browser
	usage     int
	mainpage  int
}

func NewBetway() *Betway {
	betway := &Betway{
		name:      "Betway",
		url:       "https://www.betway.co.ke",
		navigator: surf.NewBrowser(),
	}
	betway.navigator.SetUserAgent("Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
	betway.navigator.AddRequestHeader("Accept-Encoding", "gzip, deflate, br")
	return betway
}

func (betway *Betway) Name() string {
	return betway.name
}

func (betway *Betway) MainPageUsage() int {
	return betway.mainpage
}

func (betway *Betway) Usage() int {
	return betway.usage
}
func (betway *Betway) UsageReset() {
	betway.usage = 0
	betway.mainpage = 0
}

func (betway *Betway) cloneBrowser() *browser.Browser {
	nav := surf.NewBrowser()
	nav.SetHistoryJar(jar.NewMemoryHistory())
	nav.SetCookieJar(jar.NewMemoryCookies())
	nav.SetUserAgent("Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
	nav.AddRequestHeader("Accept-Encoding", "gzip, deflate, br")
	return nav
}

func (betway *Betway) GetFootball(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	// matches := []Match{}
	if err := betway.navigator.Open(betway.url); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}
		return
	}
	betway.usage += len([]byte(betway.navigator.Body()))
	betway.mainpage = betway.usage

	// utils.Debug(betway.navigator.ResponseHeaders().Get("Content-Encoding"))

	for page := 1; page <= 30; page++ {
		if isStopped() {
			return
		}
		page++
		link := "https://www.betway.co.ke/Event/GetEventSet"
		data := url.Values{}

		data.Add("marketTypeCategoryId", "00000000-0000-0000-da7a-000000580001")
		data.Add("pageNumber", strconv.Itoa(page))

		req, _ := http.NewRequest("POST", link, strings.NewReader(data.Encode()))

		req.Header.Add("pragma", "no-cache")
		req.Header.Add("x-newrelic-id", "VQIFUFZQDhADV1ZWAQQAVg==")
		req.Header.Add("origin", "https://www.betway.co.ke")
		req.Header.Add("user-agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36")
		req.Header.Add("content-type", "application/x-www-form-urlencoded")
		req.Header.Add("accept", "*/*")
		req.Header.Add("cache-control", "no-cache")
		req.Header.Add("x-requested-with", "XMLHttpRequest")
		req.Header.Add("expires", "0")
		req.Header.Add("referer", "https://www.betway.co.ke/")
		// req.Header.Add("accept-encoding", "gzip, deflate, br")
		req.Header.Add("accept-language", "en-US,en;q=0.9")

		cookies := betway.navigator.SiteCookies()
		for _, cookie := range cookies {
			req.AddCookie(cookie)
		}

		res, err := http.DefaultClient.Do(req)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}
			return
		}
		defer res.Body.Close()
		// body, _ := ioutil.ReadAll(res.Body)
		// ioutil.WriteFile("site.html", body, 0777)
		doc, err := goquery.NewDocumentFromResponse(res)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}
			return
		}
		doc.Find(".eventRow").Each(func(i int, s *goquery.Selection) {
			// utils.Debug(s.Attr("data-eventtitle"))
			if isStopped() {
				return
			}
			game := Game{}
			game.Site = "Betway"
			game.Link = betway.url
			teamStr, _ := s.Attr("data-eventtitle")
			teams := strings.Split(teamStr, " v ")
			teamA := strings.Trim(teams[0], " ")
			teamB := strings.Trim(teams[1], " ")
			game.Home = teamA
			game.Away = teamB

			allowed := ""
			eventDetails := s.Find(".eventDetails").Find(".inplayStatusDetails")
			eventId, _ := s.Attr("data-event-id")
			marketCategory, _ := s.Attr("data-markettypecategory")

			eventDetails.Find("label").Each(func(i int, s *goquery.Selection) {
				allowed += Allowed(s.Text())

			})

			t, _ := s.Attr("data-eventdate")
			game.Time = utils.DecodeTime(betway.name, t)

			gameLink := fmt.Sprintf("https://www.betway.co.ke/Bet/EventMultiMarket?eventId=%s&marketTypeCategoryId=%s", eventId, marketCategory)
			// utils.Debug(gameLink)
			g := betway.getGame(gameLink, allowed, game)
			if isStopped() {
				return
			}
			if g == nil {
				return
			}

			// utils.Debug(*g)

			games <- *g
		})
	}
	if isStopped() {
		return
	}

}
func (betway *Betway) getGame(gameLink, allowed string, game Game) *Game {
	tw := ThreeWay{Link: gameLink, APILink: allowed}
	dc := DoubleChance{Link: gameLink, APILink: allowed}
	gng := GoalNoGoal{Link: gameLink, APILink: allowed}
	ou15 := OU15{Link: gameLink, APILink: allowed}
	ou25 := OU25{Link: gameLink, APILink: allowed}
	ou35 := OU35{Link: gameLink, APILink: allowed}
	ou45 := OU45{Link: gameLink, APILink: allowed}

	nav := betway.cloneBrowser()

	if err := nav.Open(gameLink); err != nil {
		utils.Error(err)
		return nil
	}

	betway.usage += len([]byte(nav.Body()))
	nav.Find(".row").Each(func(i int, s *goquery.Selection) {
		market, _ := s.Attr("data-markettitle")

		if market == "Match Result (1X2)" {
			s.Find(".outcomes-advancedbetting").Each(func(i int, s *goquery.Selection) {

				odd, _ := strconv.ParseFloat(strings.Trim(s.Find(".outcome-pricedecimal").Text(), " "), 64)

				switch i {
				case 0: //1
					tw.Home = odd

				case 2: //2
					tw.Away = odd
				case 1: //X

					tw.Draw = odd
				}

			})
		}
		if market == "Double Chance" {
			s.Find(".outcomes-advancedbetting").Each(func(i int, s *goquery.Selection) {

				odd, _ := strconv.ParseFloat(strings.Trim(s.Find(".outcome-pricedecimal").Text(), " "), 64)
				correctTeam := s.Find(".outcome-title").Find("span").Text()
				// utils.Debug(correctTeam)
				switch i {
				case 0: //12
					dc.HomeOrAway = odd

				case 1: //X2
					if len(allowed) > 0 {
						game.Away = fmt.Sprintf("%s %s", strings.Replace(correctTeam, "Draw", "", -1), allowed)
					} else {
						game.Away = strings.Replace(correctTeam, "Draw", "", -1)
					}
					// utils.Debug("HOME", game.Home)
					dc.Away = odd
				case 2: //1X
					if len(allowed) > 0 {
						game.Home = fmt.Sprintf("%s %s", strings.Replace(correctTeam, "Draw", "", -1), allowed)
					} else {
						game.Home = strings.Replace(correctTeam, "Draw", "", -1)
					}
					// utils.Debug("AWAY", game.Away)
					dc.Home = odd
				}

			})
		}
		if market == "Both Teams to Score" {
			s.Find(".outcomes-advancedbetting").Each(func(i int, s *goquery.Selection) {

				odd, _ := strconv.ParseFloat(strings.Trim(s.Find(".outcome-pricedecimal").Text(), " "), 64)

				switch i {
				case 0: //yes
					gng.Goal = odd
				case 1: //no
					gng.NoGoal = odd
				}

			})
		}

		if market == "Overs/Unders" {
			s.Find(".outcomes-advancedbetting").Each(func(i int, s *goquery.Selection) {
				title := s.Find(".outcome-title").Find("span").Text()
				odd, _ := strconv.ParseFloat(strings.Trim(s.Find(".outcome-pricedecimal").Text(), " "), 64)

				switch title {
				case "Over 1.5":
					ou15.Over = odd
				case "Under 1.5":
					ou15.Under = odd
				case "Over 2.5":
					ou25.Over = odd
				case "Under 2.5":
					ou25.Under = odd
				case "Over 3.5":
					ou35.Over = odd
				case "Under 3.5":
					ou35.Under = odd
				case "Over 4.5":
					ou45.Over = odd
				case "Under 4.5":
					ou45.Under = odd
				}

			})

		}

	})
	//Both Teams to Score
	// utils.Debug(game)
	game.TWAY = tw
	game.DC = dc
	game.GNG = gng
	game.OU15 = ou15
	game.OU25 = ou25
	game.OU35 = ou35
	game.OU45 = ou45
	return &game
}

//GetFootballGame apiLink used as the allowed leagues
func (betway *Betway) GetFootballGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := betway.getGame(gameLink, apiLink, Game{})
	if g == nil {
		return Game{}
	}
	// utils.Debug(*g)

	return *g
}

func (betway *Betway) GetBasketBall(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (betway *Betway) GetBasketBallGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (betway *Betway) GetRugby(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (betway *Betway) GetRugbyGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (betway *Betway) GetTennis(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (betway *Betway) GetTennisGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (betway *Betway) GetIceHockey(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (betway *Betway) GetIceHockeyGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (betway *Betway) GetHandball(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (betway *Betway) GetHandballGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}
