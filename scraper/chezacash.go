package scraper

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"sync"

	"github.com/PuerkitoBio/goquery"

	"gitlab.com/arbke/utils"

	"github.com/headzoo/surf"
	"github.com/headzoo/surf/browser"
)

type Chezacash struct {
	name      string
	url       string
	navigator *browser.Browser
	usage     int
	mainpage  int
}

func NewChezacash() *Chezacash {
	chezacash := &Chezacash{
		name:      "Chezacash",
		url:       "https://www.chezacash.com",
		navigator: surf.NewBrowser(),
	}
	chezacash.navigator.SetUserAgent("Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
	return chezacash
}

func (chezacash *Chezacash) Name() string {
	return chezacash.name
}

func (chezacash *Chezacash) MainPageUsage() int {
	return chezacash.mainpage
}

func (chezacash *Chezacash) Usage() int {
	return chezacash.usage
}

func (chezacash *Chezacash) UsageReset() {
	chezacash.usage = 0
	chezacash.mainpage = 0
}

func (chezacash *Chezacash) GetFootball(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	// completeData := []interface{}{}

	client := http.Client{}

	// client.Transport = transport

	for i := 1; i <= 20; i++ {
		if isStopped() {
			return
		}
		url := fmt.Sprintf("https://www.chezacash.com/main/?pageupcoming=%d", i)
		resp, err := client.Get(url)

		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		data, _ := ioutil.ReadAll(resp.Body)
		chezacash.usage += len(data)
		resp.Body.Close()

		doc, err := goquery.NewDocumentFromReader(bytes.NewBuffer(data))
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		doc.Find(".upcoming.tableRes").Find(".thAreTwo").Each(func(_ int, s *goquery.Selection) { //each game
			if isStopped() {
				return
			}
			game := Game{}
			game.Site = chezacash.name

			link, e := s.Find(".details").Attr("requ")
			if !e {
				return
			}

			gameLink := fmt.Sprintf("https://www.chezacash.com%s", link)
			// utils.Debug(gameLink)

			g, league := chezacash.getGame(gameLink, game)
			if isStopped() {
				return
			}
			if g == nil {
				return
			}
			g.Home = g.Home + " " + Allowed(league)
			g.Away = g.Away + " " + Allowed(league)

			// utils.Debug(*g)

			games <- *g

		})

	}

	if isStopped() {
		return
	}

	return
}

func (chezacash *Chezacash) getGame(gameLink string, game Game) (*Game, string) {
	league := ""
	client := http.Client{}
	resp, err := client.Get(gameLink)
	if err != nil {
		utils.Error(err)
		return nil, league
	}

	data, _ := ioutil.ReadAll(resp.Body)
	chezacash.usage += len(data)
	resp.Body.Close()

	inner, err := goquery.NewDocumentFromReader(bytes.NewBuffer(data))
	if err != nil {
		utils.Error(err)
		return nil, league
	}
	inner.Length()

	dateTimeStr := inner.Find(".betdetails").Find(".bDetailsInfo").Find(".innerbetDiv").Find("h6").First().Text()
	inner.Find(".betdetails").Find(".bDetailsInfo").Find(".innerbetDiv").Find("h5").Find("span").Each(func(i int, s *goquery.Selection) {
		switch i {
		case 0:
			game.Home = strings.Trim(s.Text(), " ")

		case 2:
			game.Away = strings.Trim(s.Text(), " ")
		}
	})
	if len(dateTimeStr) == 0 {
		return nil, league
	}
	dateTime := strings.Split(dateTimeStr, " - ")
	// utils.Debug(dateTime, len(dateTime))
	if len(dateTime) < 2 {
		return nil, league
	}

	dateTimeArr := strings.Split(dateTime[1], " ")
	dateStr := strings.Trim(dateTimeArr[0], " ")
	timeStr := strings.Trim(dateTimeArr[1], " ")

	// utils.Debug(dateStr, timeStr)
	if len(dateStr) == 0 || len(timeStr) == 0 {
		utils.Debug("SKIPPING")
		// continue
		return nil, league
	}
	game.Time = utils.DecodeTime(chezacash.name, fmt.Sprintf("%s %s", dateStr, timeStr))
	inner.Find(".betdetails").Find(".bDetailsInfo").Find(".innerbetDiv").Find("p").Each(func(i int, s *goquery.Selection) {
		if strings.Contains(s.Text(), "vs.") {
			teams := strings.Split(s.Text(), "vs.")
			game.Home = strings.Trim(teams[0], " ")
			game.Away = strings.Trim(teams[1], " ")
		}
		if strings.HasPrefix(s.Text(), "League:") {
			league = strings.Trim(strings.Split(s.Text(), ":")[1], " ")
			game.League = league
		}
	})

	tw := ThreeWay{Link: gameLink, APILink: gameLink}
	gng := GoalNoGoal{Link: gameLink, APILink: gameLink}
	dnb := DrawNoBet{Link: gameLink, APILink: gameLink}
	ou15 := OU15{Link: gameLink, APILink: gameLink}
	ou25 := OU25{Link: gameLink, APILink: gameLink}
	ou35 := OU35{Link: gameLink, APILink: gameLink}
	ou45 := OU45{Link: gameLink, APILink: gameLink}
	inner.Find(".betdetails").Find(".type-panel").Each(func(i int, s *goquery.Selection) {
		market := s.Find(".panel-heading").Text()

		s.Find("div").Each(func(i int, s *goquery.Selection) {
			// if s.AttrOr("data-type", "") != "bet-details" {
			// 	return
			// }
			outcome := s.AttrOr("title", "")
			oddStr := s.AttrOr("data-odds", "")
			odd, err := strconv.ParseFloat(oddStr, 64)
			// utils.Debug(market, outcome, oddStr)
			if err != nil {
				// utils.Error(err)
				return
			}

			switch market {
			case "3 Way":
				switch strings.Trim(outcome, " ") {
				case game.Home:
					tw.Home = odd
				case game.Away:
					tw.Away = odd
				case "Draw":
					tw.Draw = odd
				}
			case "Total 2.5":
				switch strings.Trim(outcome, " ") {
				case "over 2.5":
					ou25.Over = odd
				case "under 2.5":
					ou25.Under = odd
				}
			case "Both teams to score":
				switch strings.Trim(outcome, " ") {
				case "yes":
					gng.Goal = odd
				case "no":
					gng.NoGoal = odd
				}

			case "Draw no bet":
				switch strings.Trim(outcome, " ") {
				case game.Home:
					dnb.Home = odd
				case game.Away:
					dnb.Away = odd

				}

			case "Total 1.5":
				switch strings.Trim(outcome, " ") {
				case "over 1.5":
					ou15.Over = odd
				case "under 1.5":
					ou15.Under = odd
				}
			case "Total 3.5":
				switch strings.Trim(outcome, " ") {
				case "over 3.5":
					ou35.Over = odd
				case "under 3.5":
					ou35.Under = odd
				}
			case "Total 4.5":
				switch strings.Trim(outcome, " ") {
				case "over 4.5":
					ou45.Over = odd
				case "under 4.5":
					ou45.Under = odd
				}
			}

		})

	})

	game.TWAY = tw
	game.DNB = dnb
	game.GNG = gng
	game.OU15 = ou15
	game.OU25 = ou25
	game.OU35 = ou35
	game.OU45 = ou45

	return &game, league
}

func (chezacash *Chezacash) GetFootballGame(t *http.Transport, gameLink string, apiLink string) Game {
	g, _ := chezacash.getGame(apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (chezacash *Chezacash) GetRugby(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (chezacash *Chezacash) GetRugbyGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (chezacash *Chezacash) GetTennis(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (chezacash *Chezacash) GetTennisGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (chezacash *Chezacash) GetBasketBall(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (chezacash *Chezacash) GetBasketBallGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (chezacash *Chezacash) GetHandball(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (chezacash *Chezacash) GetHandballGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (chezacash *Chezacash) GetIceHockey(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (chezacash *Chezacash) GetIceHockeyGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}
