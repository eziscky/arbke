package scraper

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"sync"

	"github.com/PuerkitoBio/goquery"

	"gitlab.com/arbke/utils"

	"github.com/headzoo/surf"
	"github.com/headzoo/surf/browser"
)

type Odibet struct {
	name      string
	url       string
	navigator *browser.Browser
	usage     int
	mainpage  int
}

func NewOdibet() *Odibet {
	odibet := &Odibet{
		name:      "Odibet",
		url:       "https://www.odibet.com",
		navigator: surf.NewBrowser(),
	}
	odibet.navigator.SetUserAgent("Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
	return odibet
}

func (odibet *Odibet) Name() string {
	return odibet.name
}

func (odibet *Odibet) MainPageUsage() int {
	return odibet.mainpage
}

func (odibet *Odibet) Usage() int {
	return odibet.usage
}

func (odibet *Odibet) UsageReset() {
	odibet.usage = 0
	odibet.mainpage = 0
}

func (odibet *Odibet) GetFootball(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	// completeData := []interface{}{}

	client := http.Client{}

	// client.Transport = transport

	for i := 1; i < 6; i++ {
		if isStopped() {
			return
		}
		url := fmt.Sprintf("https://www.odibets.com/sport?id=1&p=%d", i)
		resp, err := client.Get(url)

		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		data, _ := ioutil.ReadAll(resp.Body)
		odibet.usage += len(data)
		resp.Body.Close()

		doc, err := goquery.NewDocumentFromReader(bytes.NewBuffer(data))
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		doc.Find(".l-events").Find(".l-events-games").Each(func(_ int, s *goquery.Selection) { //each game
			if isStopped() {
				return
			}
			//leagues

			leagueName := ""
			s.Find(".l-events-games-title").Find(".title").Find(".title-left").Find("a").Each(func(_ int, s *goquery.Selection) {
				leagueName += s.Text() + ", "
			})
			s.Find(".l-events-games-matches").Find(".event").Each(func(_ int, s *goquery.Selection) {
				game := Game{}
				game.Site = odibet.name

				teams := s.Find(".event-teams")
				gameLink := fmt.Sprintf("https://odibets.com/%s", teams.AttrOr("href", ""))
				teams.Find(".team").Each(func(i int, s *goquery.Selection) {
					switch i {
					case 0:
						game.Home = s.Text()
					case 1:
						game.Away = s.Text()
					}
				})
				timeStr := ""
				teams.Find(".meta").Find("span").Each(func(i int, s *goquery.Selection) {
					if strings.Contains(s.Text(), "#") || strings.Contains(s.Text(), "+") {
						return
					}
					timeStr = s.Text()
				})
				game.Time = utils.DecodeTime(odibet.name, timeStr)

				g := odibet.getGame(gameLink, game)
				if isStopped() {
					return
				}
				if g == nil {
					return
				}
				g.Home = g.Home + " " + Allowed(leagueName)
				g.Away = g.Away + " " + Allowed(leagueName)

				// utils.Debug(*g)

				games <- *g

			})

		})

	}

	if isStopped() {
		return
	}

	return
}

func (odibet *Odibet) getGame(gameLink string, game Game) *Game {

	client := http.Client{}
	resp, err := client.Get(gameLink)
	if err != nil {
		utils.Error(err)
		return nil
	}

	data, _ := ioutil.ReadAll(resp.Body)
	odibet.usage += len(data)
	resp.Body.Close()

	inner, err := goquery.NewDocumentFromReader(bytes.NewBuffer(data))
	if err != nil {
		utils.Error(err)
		return nil
	}
	inner.Length()

	tw := ThreeWay{Link: gameLink, APILink: gameLink}
	dc := DoubleChance{Link: gameLink, APILink: gameLink}
	gng := GoalNoGoal{Link: gameLink, APILink: gameLink}
	dnb := DrawNoBet{Link: gameLink, APILink: gameLink}
	ou15 := OU15{Link: gameLink, APILink: gameLink}
	ou25 := OU25{Link: gameLink, APILink: gameLink}
	ou35 := OU35{Link: gameLink, APILink: gameLink}
	ou45 := OU45{Link: gameLink, APILink: gameLink}
	inner.Find(".l-markets").Find(".market").Each(func(i int, s *goquery.Selection) {
		market := s.Find(".market-title").Text()
		s.Find(".market-odds").Find("button").Each(func(i int, s *goquery.Selection) {

			outcome := s.AttrOr("outcomename", "")
			oddStr := s.AttrOr("oddvalue", "")
			odd, err := strconv.ParseFloat(oddStr, 64)
			// utils.Debug(market, outcome, oddStr)
			if err != nil {
				utils.Error(err)
				return
			}

			switch market {
			case "3 Way":
				switch strings.Trim(outcome, " ") {
				case "1":
					tw.Home = odd
				case "2":
					tw.Away = odd
				case "X":
					tw.Draw = odd
				}
			case "Double chance":
				switch strings.Trim(outcome, " ") {
				case "1 or X":
					dc.Home = odd
				case "X or 2":
					dc.Away = odd
				case "1 or 2":
					dc.HomeOrAway = odd
				}
			case "Over/Under":
				switch strings.Trim(outcome, " ") {
				case "over  2.5":
					ou25.Over = odd
				case "under  2.5":
					ou25.Under = odd
				case "over  1.5":
					ou15.Over = odd
				case "under  1.5":
					ou15.Under = odd
				case "over  3.5":
					ou35.Over = odd
				case "under  3.5":
					ou35.Under = odd
				case "over  4.5":
					ou45.Over = odd
				case "under  4.5":
					ou45.Under = odd
				}
			case "Both teams to score":
				switch strings.Trim(outcome, " ") {
				case "yes":
					gng.Goal = odd
				case "no":
					gng.NoGoal = odd
				}

			case "Draw no bet":
				switch strings.Trim(outcome, " ") {
				case "1":
					dnb.Home = odd
				case "2":
					dnb.Away = odd

				}

			}

		})

	})

	game.TWAY = tw
	game.DC = dc
	game.DNB = dnb
	game.GNG = gng
	game.OU15 = ou15
	game.OU25 = ou25
	game.OU35 = ou35
	game.OU45 = ou45

	return &game
}

func (odibet *Odibet) GetFootballGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := odibet.getGame(apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (odibet *Odibet) GetRugby(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	// completeData := []interface{}{}

	client := http.Client{}

	// client.Transport = transport

	for i := 1; i < 6; i++ {
		if isStopped() {
			return
		}
		url := fmt.Sprintf("https://www.odibets.com/sport?id=12&p=%d", i)
		resp, err := client.Get(url)

		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		data, _ := ioutil.ReadAll(resp.Body)
		odibet.usage += len(data)
		resp.Body.Close()

		doc, err := goquery.NewDocumentFromReader(bytes.NewBuffer(data))
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		doc.Find(".l-events").Find(".l-events-games").Each(func(_ int, s *goquery.Selection) { //each game
			if isStopped() {
				return
			}
			//leagues

			leagueName := ""
			s.Find(".l-events-games-title").Find(".title").Find(".title-left").Find("a").Each(func(_ int, s *goquery.Selection) {
				leagueName += s.Text() + ", "
			})
			s.Find(".l-events-games-matches").Find(".event").Each(func(_ int, s *goquery.Selection) {
				game := Game{}
				game.Site = odibet.name

				teams := s.Find(".event-teams")
				gameLink := fmt.Sprintf("https://odibets.com/%s", teams.AttrOr("href", ""))
				teams.Find(".team").Each(func(i int, s *goquery.Selection) {
					switch i {
					case 0:
						game.Home = s.Text()
					case 1:
						game.Away = s.Text()
					}
				})
				timeStr := ""
				teams.Find(".meta").Find("span").Each(func(i int, s *goquery.Selection) {
					if strings.Contains(s.Text(), "#") || strings.Contains(s.Text(), "+") {
						return
					}
					timeStr = s.Text()
				})
				game.Time = utils.DecodeTime(odibet.name, timeStr)

				g := odibet.getRGame(gameLink, game)
				if isStopped() {
					return
				}
				if g == nil {
					return
				}
				g.Home = g.Home + " " + Allowed(leagueName)
				g.Away = g.Away + " " + Allowed(leagueName)

				// utils.Debug(*g)

				games <- *g

			})

		})

	}

	if isStopped() {
		return
	}

	return
}
func (odibet *Odibet) GetRugbyGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := odibet.getRGame(apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}
func (odibet *Odibet) getRGame(gameLink string, game Game) *Game {

	client := http.Client{}
	resp, err := client.Get(gameLink)
	if err != nil {
		utils.Error(err)
		return nil
	}

	data, _ := ioutil.ReadAll(resp.Body)
	odibet.usage += len(data)
	resp.Body.Close()

	inner, err := goquery.NewDocumentFromReader(bytes.NewBuffer(data))
	if err != nil {
		utils.Error(err)
		return nil
	}
	inner.Length()

	tw := ThreeWay{Link: gameLink, APILink: gameLink}
	// dc := DoubleChance{Link: gameLink, APILink: gameLink}
	// gng := GoalNoGoal{Link: gameLink, APILink: gameLink}
	// dnb := DrawNoBet{Link: gameLink, APILink: gameLink}
	// ou15 := OU15{Link: gameLink, APILink: gameLink}
	// ou25 := OU25{Link: gameLink, APILink: gameLink}
	// ou35 := OU35{Link: gameLink, APILink: gameLink}
	// ou45 := OU45{Link: gameLink, APILink: gameLink}
	inner.Find(".l-markets").Find(".market").Each(func(i int, s *goquery.Selection) {
		market := s.Find(".market-title").Text()
		s.Find(".market-odds").Find("button").Each(func(i int, s *goquery.Selection) {

			outcome := s.AttrOr("outcomename", "")
			oddStr := s.AttrOr("oddvalue", "")
			odd, err := strconv.ParseFloat(oddStr, 64)
			// utils.Debug(market, outcome, oddStr)
			if err != nil {
				utils.Error(err)
				return
			}

			switch market {
			case "3 Way":
				switch strings.Trim(outcome, " ") {
				case "1":
					tw.Home = odd
				case "2":
					tw.Away = odd
				case "X":
					tw.Draw = odd
				}
			}

		})

	})

	game.TWAY = tw
	// game.DC = dc
	// game.DNB = dnb
	// game.GNG = gng
	// game.OU15 = ou15
	// game.OU25 = ou25
	// game.OU35 = ou35
	// game.OU45 = ou45

	return &game
}

func (odibet *Odibet) GetTennis(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	// completeData := []interface{}{}

	client := http.Client{}

	// client.Transport = transport

	for i := 1; i < 6; i++ {
		if isStopped() {
			return
		}
		url := fmt.Sprintf("https://www.odibets.com/sport?id=5&p=%d", i)
		resp, err := client.Get(url)

		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		data, _ := ioutil.ReadAll(resp.Body)
		odibet.usage += len(data)
		resp.Body.Close()

		doc, err := goquery.NewDocumentFromReader(bytes.NewBuffer(data))
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		doc.Find(".l-events").Find(".l-events-games").Each(func(_ int, s *goquery.Selection) { //each game
			if isStopped() {
				return
			}
			//leagues

			leagueName := ""
			s.Find(".l-events-games-title").Find(".title").Find(".title-left").Find("a").Each(func(_ int, s *goquery.Selection) {
				leagueName += s.Text() + ", "
			})
			s.Find(".l-events-games-matches").Find(".event").Each(func(_ int, s *goquery.Selection) {
				game := Game{}
				game.Site = odibet.name

				teams := s.Find(".event-teams")
				gameLink := fmt.Sprintf("https://odibets.com/%s", teams.AttrOr("href", ""))
				teams.Find(".team").Each(func(i int, s *goquery.Selection) {
					switch i {
					case 0:
						game.Home = s.Text()
					case 1:
						game.Away = s.Text()
					}
				})
				timeStr := ""
				teams.Find(".meta").Find("span").Each(func(i int, s *goquery.Selection) {
					if strings.Contains(s.Text(), "#") || strings.Contains(s.Text(), "+") {
						return
					}
					timeStr = s.Text()
				})
				if len(timeStr) == 0 {
					return
				}
				game.Time = utils.DecodeTime(odibet.name, timeStr)

				g := odibet.getTGame(gameLink, game)
				if isStopped() {
					return
				}
				if g == nil {
					return
				}
				g.Home = g.Home + " " + Allowed(leagueName)
				g.Away = g.Away + " " + Allowed(leagueName)

				// utils.Debug(*g)

				games <- *g

			})

		})

	}

	if isStopped() {
		return
	}

	return
}
func (odibet *Odibet) GetTennisGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := odibet.getTGame(apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}
func (odibet *Odibet) getTGame(gameLink string, game Game) *Game {

	client := http.Client{}
	resp, err := client.Get(gameLink)
	if err != nil {
		utils.Error(err)
		return nil
	}

	data, _ := ioutil.ReadAll(resp.Body)
	odibet.usage += len(data)
	resp.Body.Close()

	inner, err := goquery.NewDocumentFromReader(bytes.NewBuffer(data))
	if err != nil {
		utils.Error(err)
		return nil
	}
	inner.Length()

	twow := TwoWay{Link: gameLink, APILink: gameLink}
	// dc := DoubleChance{Link: gameLink, APILink: gameLink}
	// gng := GoalNoGoal{Link: gameLink, APILink: gameLink}
	// dnb := DrawNoBet{Link: gameLink, APILink: gameLink}
	// ou15 := OU15{Link: gameLink, APILink: gameLink}
	// ou25 := OU25{Link: gameLink, APILink: gameLink}
	// ou35 := OU35{Link: gameLink, APILink: gameLink}
	// ou45 := OU45{Link: gameLink, APILink: gameLink}
	inner.Find(".l-markets").Find(".market").Each(func(i int, s *goquery.Selection) {
		market := s.Find(".market-title").Text()
		s.Find(".market-odds").Find("button").Each(func(i int, s *goquery.Selection) {

			outcome := s.AttrOr("outcomename", "")
			oddStr := s.AttrOr("oddvalue", "")
			odd, err := strconv.ParseFloat(oddStr, 64)
			// utils.Debug(market, outcome, oddStr)
			if err != nil {
				utils.Error(err)
				return
			}

			switch market {
			case "Winner":
				switch strings.Trim(outcome, " ") {
				case "1":
					twow.Home = odd
				case "2":
					twow.Away = odd
				}
			}

		})

	})

	game.TWOWAY = twow
	// game.DC = dc
	// game.DNB = dnb
	// game.GNG = gng
	// game.OU15 = ou15
	// game.OU25 = ou25
	// game.OU35 = ou35
	// game.OU45 = ou45

	return &game
}

func (odibet *Odibet) GetBasketBall(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	// completeData := []interface{}{}

	client := http.Client{}

	// client.Transport = transport

	for i := 1; i < 6; i++ {
		if isStopped() {
			return
		}
		url := fmt.Sprintf("https://www.odibets.com/sport?id=2&p=%d", i)
		resp, err := client.Get(url)

		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		data, _ := ioutil.ReadAll(resp.Body)
		odibet.usage += len(data)
		resp.Body.Close()

		doc, err := goquery.NewDocumentFromReader(bytes.NewBuffer(data))
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		doc.Find(".l-events").Find(".l-events-games").Each(func(_ int, s *goquery.Selection) { //each game
			if isStopped() {
				return
			}
			//leagues

			leagueName := ""
			s.Find(".l-events-games-title").Find(".title").Find(".title-left").Find("a").Each(func(_ int, s *goquery.Selection) {
				leagueName += s.Text() + ", "
			})
			s.Find(".l-events-games-matches").Find(".event").Each(func(_ int, s *goquery.Selection) {
				game := Game{}
				game.Site = odibet.name

				teams := s.Find(".event-teams")
				gameLink := fmt.Sprintf("https://odibets.com/%s", teams.AttrOr("href", ""))
				teams.Find(".team").Each(func(i int, s *goquery.Selection) {
					switch i {
					case 0:
						game.Home = s.Text()
					case 1:
						game.Away = s.Text()
					}
				})
				timeStr := ""
				teams.Find(".meta").Find("span").Each(func(i int, s *goquery.Selection) {
					if strings.Contains(s.Text(), "#") || strings.Contains(s.Text(), "+") {
						return
					}
					timeStr = s.Text()
				})
				if len(timeStr) == 0 {
					return
				}
				game.Time = utils.DecodeTime(odibet.name, timeStr)

				g := odibet.getBGame(gameLink, game)
				if isStopped() {
					return
				}
				if g == nil {
					return
				}
				g.Home = g.Home + " " + Allowed(leagueName)
				g.Away = g.Away + " " + Allowed(leagueName)

				// utils.Debug(*g)

				games <- *g

			})

		})

	}

	if isStopped() {
		return
	}

	return
}
func (odibet *Odibet) GetBasketBallGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := odibet.getBGame(apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}
func (odibet *Odibet) getBGame(gameLink string, game Game) *Game {

	client := http.Client{}
	resp, err := client.Get(gameLink)
	if err != nil {
		utils.Error(err)
		return nil
	}

	data, _ := ioutil.ReadAll(resp.Body)
	odibet.usage += len(data)
	resp.Body.Close()

	inner, err := goquery.NewDocumentFromReader(bytes.NewBuffer(data))
	if err != nil {
		utils.Error(err)
		return nil
	}
	inner.Length()

	tw := ThreeWay{Link: gameLink, APILink: gameLink}
	twow := TwoWay{Link: gameLink, APILink: gameLink}
	// dc := DoubleChance{Link: gameLink, APILink: gameLink}
	// gng := GoalNoGoal{Link: gameLink, APILink: gameLink}
	// dnb := DrawNoBet{Link: gameLink, APILink: gameLink}
	// ou15 := OU15{Link: gameLink, APILink: gameLink}
	// ou25 := OU25{Link: gameLink, APILink: gameLink}
	// ou35 := OU35{Link: gameLink, APILink: gameLink}
	// ou45 := OU45{Link: gameLink, APILink: gameLink}
	inner.Find(".l-markets").Find(".market").Each(func(i int, s *goquery.Selection) {
		market := s.Find(".market-title").Text()
		s.Find(".market-odds").Find("button").Each(func(i int, s *goquery.Selection) {

			outcome := s.AttrOr("outcomename", "")
			oddStr := s.AttrOr("oddvalue", "")
			odd, err := strconv.ParseFloat(oddStr, 64)
			// utils.Debug(market, outcome, oddStr)
			if err != nil {
				utils.Error(err)
				return
			}

			switch market {
			case "Draw no bet":
				switch strings.Trim(outcome, " ") {
				case "1":
					twow.Home = odd
				case "2":
					twow.Away = odd
				}
			case "3 Way":
				switch strings.Trim(outcome, " ") {
				case "1":
					tw.Home = odd
				case "2":
					tw.Away = odd
				case "X":
					tw.Draw = odd
				}
			}

		})

	})

	game.TWAY = tw
	game.TWOWAY = twow
	// game.DC = dc
	// game.DNB = dnb
	// game.GNG = gng
	// game.OU15 = ou15
	// game.OU25 = ou25
	// game.OU35 = ou35
	// game.OU45 = ou45

	return &game
}

func (odibet *Odibet) GetHandball(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	// completeData := []interface{}{}

	client := http.Client{}

	// client.Transport = transport

	for i := 1; i < 6; i++ {
		if isStopped() {
			return
		}
		url := fmt.Sprintf("https://www.odibets.com/sport?id=6&p=%d", i)
		resp, err := client.Get(url)

		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		data, _ := ioutil.ReadAll(resp.Body)
		odibet.usage += len(data)
		resp.Body.Close()

		doc, err := goquery.NewDocumentFromReader(bytes.NewBuffer(data))
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		doc.Find(".l-events").Find(".l-events-games").Each(func(_ int, s *goquery.Selection) { //each game
			if isStopped() {
				return
			}
			//leagues

			leagueName := ""
			s.Find(".l-events-games-title").Find(".title").Find(".title-left").Find("a").Each(func(_ int, s *goquery.Selection) {
				leagueName += s.Text() + ", "
			})
			s.Find(".l-events-games-matches").Find(".event").Each(func(_ int, s *goquery.Selection) {
				game := Game{}
				game.Site = odibet.name

				teams := s.Find(".event-teams")
				gameLink := fmt.Sprintf("https://odibets.com/%s", teams.AttrOr("href", ""))
				teams.Find(".team").Each(func(i int, s *goquery.Selection) {
					switch i {
					case 0:
						game.Home = s.Text()
					case 1:
						game.Away = s.Text()
					}
				})
				timeStr := ""
				teams.Find(".meta").Find("span").Each(func(i int, s *goquery.Selection) {
					if strings.Contains(s.Text(), "#") || strings.Contains(s.Text(), "+") {
						return
					}
					timeStr = s.Text()
				})
				if len(timeStr) == 0 {
					return
				}
				game.Time = utils.DecodeTime(odibet.name, timeStr)

				g := odibet.getHGame(gameLink, game)
				if isStopped() {
					return
				}
				if g == nil {
					return
				}
				g.Home = g.Home + " " + Allowed(leagueName)
				g.Away = g.Away + " " + Allowed(leagueName)

				// utils.Debug(*g)

				games <- *g

			})

		})

	}

	if isStopped() {
		return
	}

	return
}
func (odibet *Odibet) GetHandballGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := odibet.getHGame(apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}
func (odibet *Odibet) getHGame(gameLink string, game Game) *Game {

	client := http.Client{}
	resp, err := client.Get(gameLink)
	if err != nil {
		utils.Error(err)
		return nil
	}

	data, _ := ioutil.ReadAll(resp.Body)
	odibet.usage += len(data)
	resp.Body.Close()

	inner, err := goquery.NewDocumentFromReader(bytes.NewBuffer(data))
	if err != nil {
		utils.Error(err)
		return nil
	}
	inner.Length()

	tw := ThreeWay{Link: gameLink, APILink: gameLink}
	dc := DoubleChance{Link: gameLink, APILink: gameLink}
	// gng := GoalNoGoal{Link: gameLink, APILink: gameLink}
	dnb := DrawNoBet{Link: gameLink, APILink: gameLink}
	// ou15 := OU15{Link: gameLink, APILink: gameLink}
	// ou25 := OU25{Link: gameLink, APILink: gameLink}
	// ou35 := OU35{Link: gameLink, APILink: gameLink}
	// ou45 := OU45{Link: gameLink, APILink: gameLink}
	inner.Find(".l-markets").Find(".market").Each(func(i int, s *goquery.Selection) {
		market := s.Find(".market-title").Text()
		s.Find(".market-odds").Find("button").Each(func(i int, s *goquery.Selection) {

			outcome := s.AttrOr("outcomename", "")
			oddStr := s.AttrOr("oddvalue", "")
			odd, err := strconv.ParseFloat(oddStr, 64)
			// utils.Debug(market, outcome, oddStr)
			if err != nil {
				utils.Error(err)
				return
			}

			switch market {
			case "3 Way":
				switch strings.Trim(outcome, " ") {
				case "1":
					tw.Home = odd
				case "2":
					tw.Away = odd
				case "X":
					tw.Draw = odd
				}
			case "Double chance":
				switch strings.Trim(outcome, " ") {
				case "1 or X":
					dc.Home = odd
				case "X or 2":
					dc.Away = odd
				case "1 or 2":
					dc.HomeOrAway = odd
				}

			case "Draw no bet":
				switch strings.Trim(outcome, " ") {
				case "1":
					dnb.Home = odd
				case "2":
					dnb.Away = odd

				}

			}

		})

	})

	game.TWAY = tw
	game.DC = dc
	game.DNB = dnb
	// game.GNG = gng
	// game.OU15 = ou15
	// game.OU25 = ou25
	// game.OU35 = ou35
	// game.OU45 = ou45

	return &game
}

func (odibet *Odibet) GetIceHockey(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	// completeData := []interface{}{}

	client := http.Client{}

	// client.Transport = transport

	for i := 1; i < 6; i++ {
		if isStopped() {
			return
		}
		url := fmt.Sprintf("https://www.odibets.com/sport?id=4&p=%d", i)
		resp, err := client.Get(url)

		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		data, _ := ioutil.ReadAll(resp.Body)
		odibet.usage += len(data)
		resp.Body.Close()

		doc, err := goquery.NewDocumentFromReader(bytes.NewBuffer(data))
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		doc.Find(".l-events").Find(".l-events-games").Each(func(_ int, s *goquery.Selection) { //each game
			if isStopped() {
				return
			}
			//leagues

			leagueName := ""
			s.Find(".l-events-games-title").Find(".title").Find(".title-left").Find("a").Each(func(_ int, s *goquery.Selection) {
				leagueName += s.Text() + ", "
			})
			s.Find(".l-events-games-matches").Find(".event").Each(func(_ int, s *goquery.Selection) {
				game := Game{}
				game.Site = odibet.name

				teams := s.Find(".event-teams")
				gameLink := fmt.Sprintf("https://odibets.com/%s", teams.AttrOr("href", ""))
				teams.Find(".team").Each(func(i int, s *goquery.Selection) {
					switch i {
					case 0:
						game.Home = s.Text()
					case 1:
						game.Away = s.Text()
					}
				})
				timeStr := ""
				teams.Find(".meta").Find("span").Each(func(i int, s *goquery.Selection) {
					if strings.Contains(s.Text(), "#") || strings.Contains(s.Text(), "+") {
						return
					}
					timeStr = s.Text()
				})
				if len(timeStr) == 0 {
					return
				}
				game.Time = utils.DecodeTime(odibet.name, timeStr)

				g := odibet.getIHGame(gameLink, game)
				if isStopped() {
					return
				}
				if g == nil {
					return
				}
				g.Home = g.Home + " " + Allowed(leagueName)
				g.Away = g.Away + " " + Allowed(leagueName)

				// utils.Debug(*g)

				games <- *g

			})

		})

	}

	if isStopped() {
		return
	}

	return
}
func (odibet *Odibet) GetIceHockeyGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := odibet.getIHGame(apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}
func (odibet *Odibet) getIHGame(gameLink string, game Game) *Game {

	client := http.Client{}
	resp, err := client.Get(gameLink)
	if err != nil {
		utils.Error(err)
		return nil
	}

	data, _ := ioutil.ReadAll(resp.Body)
	odibet.usage += len(data)
	resp.Body.Close()

	inner, err := goquery.NewDocumentFromReader(bytes.NewBuffer(data))
	if err != nil {
		utils.Error(err)
		return nil
	}
	inner.Length()

	tw := ThreeWay{Link: gameLink, APILink: gameLink}
	dc := DoubleChance{Link: gameLink, APILink: gameLink}
	gng := GoalNoGoal{Link: gameLink, APILink: gameLink}
	dnb := DrawNoBet{Link: gameLink, APILink: gameLink}
	// ou15 := OU15{Link: gameLink, APILink: gameLink}
	// ou25 := OU25{Link: gameLink, APILink: gameLink}
	// ou35 := OU35{Link: gameLink, APILink: gameLink}
	// ou45 := OU45{Link: gameLink, APILink: gameLink}
	inner.Find(".l-markets").Find(".market").Each(func(i int, s *goquery.Selection) {
		market := s.Find(".market-title").Text()
		s.Find(".market-odds").Find("button").Each(func(i int, s *goquery.Selection) {

			outcome := s.AttrOr("outcomename", "")
			oddStr := s.AttrOr("oddvalue", "")
			odd, err := strconv.ParseFloat(oddStr, 64)
			// utils.Debug(market, outcome, oddStr)
			if err != nil {
				utils.Error(err)
				return
			}

			switch market {
			case "3 Way":
				switch strings.Trim(outcome, " ") {
				case "1":
					tw.Home = odd
				case "2":
					tw.Away = odd
				case "X":
					tw.Draw = odd
				}
			case "Double chance":
				switch strings.Trim(outcome, " ") {
				case "1 or X":
					dc.Home = odd
				case "X or 2":
					dc.Away = odd
				case "1 or 2":
					dc.HomeOrAway = odd
				}

			case "Both teams to score":
				switch strings.Trim(outcome, " ") {
				case "yes":
					gng.Goal = odd
				case "no":
					gng.NoGoal = odd
				}

			case "Draw no bet":
				switch strings.Trim(outcome, " ") {
				case "1":
					dnb.Home = odd
				case "2":
					dnb.Away = odd

				}

			}

		})

	})

	game.TWAY = tw
	game.DC = dc
	game.DNB = dnb
	game.GNG = gng
	// game.OU15 = ou15
	// game.OU25 = ou25
	// game.OU35 = ou35
	// game.OU45 = ou45

	return &game
}
