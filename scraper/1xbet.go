package scraper

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/headzoo/surf"
	"github.com/headzoo/surf/browser"
	"github.com/headzoo/surf/jar"
	"gitlab.com/arbke/utils"
)

type Xbet struct {
	Site string
	Link string
	nav  *browser.Browser

	count    int
	usage    int
	mainpage int

	lock *sync.Mutex
}

func NewXbet() *Xbet {
	xbet := &Xbet{
		Site: "1XBet",
		Link: "https://1xbet.co.ke/en/line/BasketBall/",
		nav:  surf.NewBrowser(),
		lock: &sync.Mutex{},
	}
	xbet.nav.SetHistoryJar(jar.NewMemoryHistory())
	xbet.nav.SetUserAgent("Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
	return xbet
}

func (xbet *Xbet) MainPageUsage() int {
	return xbet.mainpage
}

func (xbet *Xbet) Usage() int {
	xbet.lock.Lock()
	defer xbet.lock.Unlock()
	return xbet.usage
}

func (xbet *Xbet) addUsage(u int) {
	xbet.lock.Lock()
	defer xbet.lock.Unlock()
	xbet.usage += u
}

func (xbet *Xbet) UsageReset() {
	xbet.usage = 0
	xbet.mainpage = 0
}

func (xbet *Xbet) Name() string {
	return xbet.Site
}
func (xbet *Xbet) GetFootball(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() {
		close(games)
	}()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	req, _ := http.NewRequest(http.MethodGet, "https://1xbet.co.ke/LineFeed/Get1x2_Zip2?sports=1&count=500&lng=en&tf=707&mode=4&country=87&partner=61&getEmpty=true", nil)
	// req.Header.Add("Accept-Encoding", "gzip, deflate, br")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	data, _ := ioutil.ReadAll(resp.Body)
	// utils.Debug(string(data))
	xbet.addUsage(len(data))
	xbet.mainpage += len(data)

	parsed := map[string]interface{}{}
	json.Unmarshal(data, &parsed)

	form := func(name string) (string, string) {
		lname := strings.Replace(name, ".", "", -1)
		lname = strings.Trim(lname, " ")
		lname = strings.Replace(lname, " ", "-", -1)
		lname = strings.Replace(lname, "(", "", -1)
		lname = strings.Replace(lname, ")", "", -1)
		lname = strings.Trim(lname, " ")
		lname = strings.Replace(lname, "&", "", -1)
		lname = strings.Replace(lname, " ", "-", -1)
		return lname, name
	}
	allowedLeague := func(name string) bool {
		name = strings.ToUpper(name)
		prohib := []string{"SPECIALS", "STATISTICS", "SPECIAL", "DUEL"}
		for _, p := range prohib {
			if strings.Contains(name, p) {
				return false
			}
		}
		return true
	}

	if _, ok := parsed["Value"]; !ok {
		utils.Error(string(data))
		if isStopped() {
			return
		}

		return
	}
	fmt.Printf("1XTYPE %T", parsed["Value"])
	values := parsed["Value"].([]interface{})
	for _, _v := range values {
		v := _v.(map[string]interface{})
		league, _ := form(v["L"].(string))

		if !allowedLeague(league) {
			continue
		}
		if _, ok := v["O1"]; !ok {
			utils.Error(v)
			continue
		}
		if _, ok := v["O2"]; !ok {
			utils.Error(v)
			continue
		}

		fullPath := fmt.Sprintf("https://1xbet.co.ke/LineFeed/GetGameZip?id=%d&lng=en&cfview=0&isSubGames=true&GroupEvents=true&allEventsGroupSubGames=true&countevents=250&partner=61", int(v["CI"].(float64)))

		gameTime := time.Unix(int64(v["S"].(float64)), 0)

		game := Game{}
		game.Site = xbet.Name()
		game.League = league
		// game.GameID = gameId
		game.Home = v["O1"].(string) + Allowed(game.League)
		game.Away = v["O2"].(string) + Allowed(game.League)
		game.TimeStr = gameTime.Format(time.ANSIC)
		game.Time = &gameTime

		h, _ := form(game.Home)
		a, _ := form(game.Away)

		gameLink := fmt.Sprintf("https://1xbet.co.ke/en/line/Football/%d-%s/%d-%s-%s/", int(v["LI"].(float64)), league, int(v["CI"].(float64)), h, a)

		g := xbet.getGame(gameLink, fullPath, game)
		if isStopped() {
			return
		}
		// utils.Debug(*g)
		if g == nil {
			continue
		}

		xbet.count++
		games <- *g

	}

	if isStopped() {
		return
	}

	return
}

func (xbet *Xbet) getGame(gameLink, apiLink string, game Game) *Game {
	tw := ThreeWay{Link: gameLink, APILink: apiLink}
	dc := DoubleChance{Link: gameLink, APILink: apiLink}
	gng := GoalNoGoal{Link: gameLink, APILink: apiLink}
	ou15 := OU15{Link: gameLink, APILink: apiLink}
	ou25 := OU25{Link: gameLink, APILink: apiLink}
	ou35 := OU35{Link: gameLink, APILink: apiLink}
	ou45 := OU45{Link: gameLink, APILink: apiLink}

	req, _ := http.NewRequest(http.MethodGet, apiLink, nil)
	// req.Header.Add("Accept-Encoding", "gzip, deflate, br")
	resp, err := http.DefaultClient.Do(req)

	if err != nil {
		utils.Error(err)
		return nil
	}

	if resp.Body == nil {
		d, _ := ioutil.ReadAll(resp.Body)
		utils.Debug(string(d))
		return nil
	}
	data, _ := ioutil.ReadAll(resp.Body)
	xbet.addUsage(len(data))
	parsed := map[string]interface{}{}
	json.Unmarshal(data, &parsed)

	// utils.Debug(string(data))
	if _, ok := parsed["Value"]; !ok {
		return nil
	}
	if parsed["Value"] == nil {
		return nil
	}
	markets := parsed["Value"].(map[string]interface{})["GE"].([]interface{})

	for _, _market := range markets {
		market := _market.(map[string]interface{})
		odds := market["E"].([]interface{})
		switch int(market["G"].(float64)) {
		//three way
		case 1:
			for _, _odds := range odds {
				odd := _odds.([]interface{})[0].(map[string]interface{})
				oddVal := odd["C"].(float64)
				switch int(odd["T"].(float64)) {
				case 1:
					tw.Home = oddVal
				case 2:
					tw.Draw = oddVal
				case 3:
					tw.Away = oddVal
				}
			}
		case 8:
			for _, _odds := range odds {
				odd := _odds.([]interface{})[0].(map[string]interface{})
				oddVal := odd["C"].(float64)
				switch int(odd["T"].(float64)) {
				case 4:
					dc.Home = oddVal
				case 5:
					dc.HomeOrAway = oddVal
				case 6:
					dc.Away = oddVal
				}
			}

		case 17:
			for _, __odds := range odds {
				_odds := __odds.([]interface{})
				for _, _odd := range _odds {
					odd := _odd.(map[string]interface{})
					oddVal := odd["C"].(float64)
					_type := odd["P"].(float64)
					switch int(odd["T"].(float64)) {
					case 9: //over
						switch _type {
						case 1.5:
							ou15.Over = oddVal
						case 2.5:
							ou25.Over = oddVal
						case 3.5:
							ou35.Over = oddVal
						case 4.5:
							ou45.Over = oddVal
						}
					case 10: //under
						switch _type {
						case 1.5:
							ou15.Under = oddVal
						case 2.5:
							ou25.Under = oddVal
						case 3.5:
							ou35.Under = oddVal
						case 4.5:
							ou45.Under = oddVal
						}

					}
				}
			}
		case 19:
			for _, _odds := range odds {
				odd := _odds.([]interface{})[0].(map[string]interface{})
				oddVal := odd["C"].(float64)
				switch int(odd["T"].(float64)) {
				case 180:
					gng.Goal = oddVal
				case 181:
					gng.NoGoal = oddVal
				}
			}
		}
	}

	game.TWAY = tw
	game.DC = dc
	game.GNG = gng
	game.OU15 = ou15
	game.OU25 = ou25
	game.OU35 = ou35
	game.OU45 = ou45

	// utils.Debug(game)
	return &game
}

func (xbet *Xbet) GetFootballGame(transport *http.Transport, gameLink, apiLink string) Game {
	g := xbet.getGame(gameLink, apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (xbet *Xbet) GetBasketBall(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() {
		close(games)
	}()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	//https://1xbet.co.ke/LineFeed/Get1x2_VZip?sports=3%2C4&count=50&lng=en&tf=2200000&tz=3&mode=4&country=87&partner=61&getEmpty=true
	req, _ := http.NewRequest(http.MethodGet, "https://1xbet.co.ke/LineFeed/Get1x2_VZip?sports=3&count=500&lng=en&tf=2200000&mode=4&country=87&partner=61&getEmpty=true", nil)
	// req.Header.Add("Accept-Encoding", "gzip, deflate, br")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	data, _ := ioutil.ReadAll(resp.Body)
	// utils.Debug(string(data))
	xbet.addUsage(len(data))
	// xbet.mainpage += len(data)

	parsed := map[string]interface{}{}
	json.Unmarshal(data, &parsed)

	form := func(name string) (string, string) {
		lname := strings.Replace(name, ".", "", -1)
		lname = strings.Trim(lname, " ")
		lname = strings.Replace(lname, " ", "-", -1)
		lname = strings.Replace(lname, "(", "", -1)
		lname = strings.Replace(lname, ")", "", -1)
		lname = strings.Trim(lname, " ")
		lname = strings.Replace(lname, "&", "", -1)
		lname = strings.Replace(lname, " ", "-", -1)
		return lname, name
	}
	allowedLeague := func(name string) bool {
		name = strings.ToUpper(name)
		prohib := []string{"SPECIALS", "STATISTICS", "SPECIAL", "DUEL"}
		for _, p := range prohib {
			if strings.Contains(name, p) {
				return false
			}
		}
		return true
	}

	if _, ok := parsed["Value"]; !ok {
		utils.Error(string(data))
		if isStopped() {
			return
		}

		return
	}
	fmt.Printf("1XTYPE %T", parsed["Value"])
	values := parsed["Value"].([]interface{})
	for _, _v := range values {
		v := _v.(map[string]interface{})
		league, _ := form(v["L"].(string))

		if !allowedLeague(league) {
			continue
		}
		if _, ok := v["O1"]; !ok {
			utils.Error(v)
			continue
		}
		if _, ok := v["O2"]; !ok {
			utils.Error(v)
			continue
		}

		fullPath := fmt.Sprintf("https://1xbet.co.ke/LineFeed/GetGameZip?id=%d&lng=en&cfview=0&isSubGames=true&GroupEvents=true&allEventsGroupSubGames=true&countevents=250&partner=61", int(v["CI"].(float64)))

		gameTime := time.Unix(int64(v["S"].(float64)), 0)

		game := Game{}
		game.Site = xbet.Name()
		game.League = league
		// game.GameID = gameId
		game.Home = v["O1"].(string)
		game.Away = v["O2"].(string)
		game.TimeStr = gameTime.Format(time.ANSIC)
		game.Time = &gameTime

		h, _ := form(game.Home)
		a, _ := form(game.Away)

		gameLink := fmt.Sprintf("https://1xbet.co.ke/en/line/Basketball/%d-%s/%d-%s-%s/", int(v["LI"].(float64)), league, int(v["CI"].(float64)), h, a)

		g := xbet.getBGame(gameLink, fullPath, game)
		if isStopped() {
			return
		}
		// utils.Debug(*g)
		if g == nil {
			continue
		}

		xbet.count++
		games <- *g

	}

	if isStopped() {
		return
	}

	return
}

func (xbet *Xbet) getBGame(gameLink, apiLink string, game Game) *Game {
	tw := ThreeWay{Link: gameLink, APILink: apiLink}

	req, _ := http.NewRequest(http.MethodGet, apiLink, nil)
	// req.Header.Add("Accept-Encoding", "gzip, deflate, br")
	resp, err := http.DefaultClient.Do(req)

	if err != nil {
		utils.Error(err)
		return nil
	}

	if resp.Body == nil {
		d, _ := ioutil.ReadAll(resp.Body)
		utils.Debug(string(d))
		return nil
	}
	data, _ := ioutil.ReadAll(resp.Body)
	// ioutil.WriteFile("out.json", data, 0777)
	xbet.addUsage(len(data))
	parsed := map[string]interface{}{}
	json.Unmarshal(data, &parsed)

	// utils.Debug(string(data))
	if _, ok := parsed["Value"]; !ok {
		return nil
	}
	if parsed["Value"] == nil {
		return nil
	}
	markets := parsed["Value"].(map[string]interface{})["GE"].([]interface{})

	for _, _market := range markets {
		market := _market.(map[string]interface{})
		odds := market["E"].([]interface{})
		switch int(market["G"].(float64)) {
		//three way
		case 1:
			for _, _odds := range odds {
				odd := _odds.([]interface{})[0].(map[string]interface{})
				oddVal := odd["C"].(float64)
				switch int(odd["T"].(float64)) {
				case 1:
					tw.Home = oddVal
				case 2:
					tw.Draw = oddVal
				case 3:
					tw.Away = oddVal
				}
			}

		}
	}

	game.TWAY = tw

	// utils.Debug(game)
	return &game
}

func (xbet *Xbet) GetBasketBallGame(transport *http.Transport, gameLink, apiLink string) Game {
	g := xbet.getBGame(gameLink, apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (xbet *Xbet) GetTennis(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() {
		close(games)
	}()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	//https://1xbet.co.ke/LineFeed/Get1x2_VZip?sports=3%2C4&count=50&lng=en&tf=2200000&tz=3&mode=4&country=87&partner=61&getEmpty=true
	req, _ := http.NewRequest(http.MethodGet, "https://1xbet.co.ke/LineFeed/Get1x2_VZip?sports=4&count=500&lng=en&tf=2200000&mode=4&country=87&partner=61&getEmpty=true", nil)
	// req.Header.Add("Accept-Encoding", "gzip, deflate, br")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	data, _ := ioutil.ReadAll(resp.Body)
	// utils.Debug(string(data))
	xbet.addUsage(len(data))
	// xbet.mainpage += len(data)

	parsed := map[string]interface{}{}
	json.Unmarshal(data, &parsed)

	form := func(name string) (string, string) {
		lname := strings.Replace(name, ".", "", -1)
		lname = strings.Trim(lname, " ")
		lname = strings.Replace(lname, " ", "-", -1)
		lname = strings.Replace(lname, "(", "", -1)
		lname = strings.Replace(lname, ")", "", -1)
		lname = strings.Trim(lname, " ")
		lname = strings.Replace(lname, "&", "", -1)
		lname = strings.Replace(lname, " ", "-", -1)
		return lname, name
	}
	allowedLeague := func(name string) bool {
		name = strings.ToUpper(name)
		prohib := []string{"SPECIALS", "STATISTICS", "SPECIAL", "DUEL"}
		for _, p := range prohib {
			if strings.Contains(name, p) {
				return false
			}
		}
		return true
	}

	if _, ok := parsed["Value"]; !ok {
		utils.Error(string(data))
		if isStopped() {
			return
		}

		return
	}
	fmt.Printf("1XTYPE %T", parsed["Value"])
	values := parsed["Value"].([]interface{})
	for _, _v := range values {
		v := _v.(map[string]interface{})
		league, _ := form(v["L"].(string))

		if !allowedLeague(league) {
			continue
		}
		if _, ok := v["O1"]; !ok {
			utils.Error(v)
			continue
		}
		if _, ok := v["O2"]; !ok {
			utils.Error(v)
			continue
		}

		fullPath := fmt.Sprintf("https://1xbet.co.ke/LineFeed/GetGameZip?id=%d&lng=en&cfview=0&isSubGames=true&GroupEvents=true&allEventsGroupSubGames=true&countevents=250&partner=61", int(v["CI"].(float64)))

		gameTime := time.Unix(int64(v["S"].(float64)), 0)

		game := Game{}
		game.Site = xbet.Name()
		game.League = league
		// game.GameID = gameId
		game.Home = v["O1"].(string)
		game.Away = v["O2"].(string)
		game.TimeStr = gameTime.Format(time.ANSIC)
		game.Time = &gameTime

		h, _ := form(game.Home)
		a, _ := form(game.Away)

		gameLink := fmt.Sprintf("https://1xbet.co.ke/en/line/Tennis/%d-%s/%d-%s-%s/", int(v["LI"].(float64)), league, int(v["CI"].(float64)), h, a)

		g := xbet.getTGame(gameLink, fullPath, game)
		if isStopped() {
			return
		}
		// utils.Debug(*g)
		if g == nil {
			continue
		}

		xbet.count++
		games <- *g

	}

	if isStopped() {
		return
	}

	return
}

func (xbet *Xbet) getTGame(gameLink, apiLink string, game Game) *Game {
	tw := TwoWay{Link: gameLink, APILink: apiLink}

	req, _ := http.NewRequest(http.MethodGet, apiLink, nil)
	// req.Header.Add("Accept-Encoding", "gzip, deflate, br")
	resp, err := http.DefaultClient.Do(req)

	if err != nil {
		utils.Error(err)
		return nil
	}

	if resp.Body == nil {
		d, _ := ioutil.ReadAll(resp.Body)
		utils.Debug(string(d))
		return nil
	}
	data, _ := ioutil.ReadAll(resp.Body)
	xbet.addUsage(len(data))
	parsed := map[string]interface{}{}
	json.Unmarshal(data, &parsed)

	// utils.Debug(string(data))
	if _, ok := parsed["Value"]; !ok {
		return nil
	}
	if parsed["Value"] == nil {
		return nil
	}
	markets := parsed["Value"].(map[string]interface{})["GE"].([]interface{})

	for _, _market := range markets {
		market := _market.(map[string]interface{})
		odds := market["E"].([]interface{})
		switch int(market["G"].(float64)) {
		//three way
		case 1:
			for _, _odds := range odds {
				odd := _odds.([]interface{})[0].(map[string]interface{})
				oddVal := odd["C"].(float64)
				switch int(odd["T"].(float64)) {
				case 1:
					tw.Home = oddVal
				// case 2:
				// 	tw.Draw = oddVal
				case 3:
					tw.Away = oddVal
				}
			}

		}
	}

	game.TWOWAY = tw

	// utils.Debug(game)
	return &game
}

func (xbet *Xbet) GetTennisGame(transport *http.Transport, gameLink, apiLink string) Game {
	g := xbet.getTGame(gameLink, apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (xbet *Xbet) GetRugby(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() {
		close(games)
	}()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	//https://1xbet.co.ke/LineFeed/Get1x2_VZip?sports=3%2C4&count=50&lng=en&tf=2200000&tz=3&mode=4&country=87&partner=61&getEmpty=true
	req, _ := http.NewRequest(http.MethodGet, "https://1xbet.co.ke/LineFeed/Get1x2_VZip?sports=7&count=500&lng=en&tf=2200000&mode=4&country=87&partner=61&getEmpty=true", nil)
	// req.Header.Add("Accept-Encoding", "gzip, deflate, br")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	data, _ := ioutil.ReadAll(resp.Body)
	// utils.Debug(string(data))
	xbet.addUsage(len(data))
	// xbet.mainpage += len(data)

	parsed := map[string]interface{}{}
	json.Unmarshal(data, &parsed)

	form := func(name string) (string, string) {
		lname := strings.Replace(name, ".", "", -1)
		lname = strings.Trim(lname, " ")
		lname = strings.Replace(lname, " ", "-", -1)
		lname = strings.Replace(lname, "(", "", -1)
		lname = strings.Replace(lname, ")", "", -1)
		lname = strings.Trim(lname, " ")
		lname = strings.Replace(lname, "&", "", -1)
		lname = strings.Replace(lname, " ", "-", -1)
		return lname, name
	}
	allowedLeague := func(name string) bool {
		name = strings.ToUpper(name)
		prohib := []string{"SPECIALS", "STATISTICS", "SPECIAL", "DUEL"}
		for _, p := range prohib {
			if strings.Contains(name, p) {
				return false
			}
		}
		return true
	}

	if _, ok := parsed["Value"]; !ok {
		utils.Error(string(data))
		if isStopped() {
			return
		}

		return
	}
	fmt.Printf("1XTYPE %T", parsed["Value"])
	values := parsed["Value"].([]interface{})
	for _, _v := range values {
		v := _v.(map[string]interface{})
		league, _ := form(v["L"].(string))

		if !allowedLeague(league) {
			continue
		}
		if _, ok := v["O1"]; !ok {
			utils.Error(v)
			continue
		}
		if _, ok := v["O2"]; !ok {
			utils.Error(v)
			continue
		}

		fullPath := fmt.Sprintf("https://1xbet.co.ke/LineFeed/GetGameZip?id=%d&lng=en&cfview=0&isSubGames=true&GroupEvents=true&allEventsGroupSubGames=true&countevents=250&partner=61", int(v["CI"].(float64)))

		gameTime := time.Unix(int64(v["S"].(float64)), 0)

		game := Game{}
		game.Site = xbet.Name()
		game.League = league
		// game.GameID = gameId
		game.Home = v["O1"].(string)
		game.Away = v["O2"].(string)
		game.TimeStr = gameTime.Format(time.ANSIC)
		game.Time = &gameTime

		h, _ := form(game.Home)
		a, _ := form(game.Away)

		gameLink := fmt.Sprintf("https://1xbet.co.ke/en/line/Rugby/%d-%s/%d-%s-%s/", int(v["LI"].(float64)), league, int(v["CI"].(float64)), h, a)

		g := xbet.getRGame(gameLink, fullPath, game)
		if isStopped() {
			return
		}
		// utils.Debug(*g)
		if g == nil {
			continue
		}

		xbet.count++
		games <- *g

	}

	if isStopped() {
		return
	}

	return
}

func (xbet *Xbet) getRGame(gameLink, apiLink string, game Game) *Game {
	tw := ThreeWay{Link: gameLink, APILink: apiLink}

	req, _ := http.NewRequest(http.MethodGet, apiLink, nil)
	// req.Header.Add("Accept-Encoding", "gzip, deflate, br")
	resp, err := http.DefaultClient.Do(req)

	if err != nil {
		utils.Error(err)
		return nil
	}

	if resp.Body == nil {
		d, _ := ioutil.ReadAll(resp.Body)
		utils.Debug(string(d))
		return nil
	}
	data, _ := ioutil.ReadAll(resp.Body)
	xbet.addUsage(len(data))
	parsed := map[string]interface{}{}
	json.Unmarshal(data, &parsed)

	// utils.Debug(string(data))
	if _, ok := parsed["Value"]; !ok {
		return nil
	}
	if parsed["Value"] == nil {
		return nil
	}
	markets := parsed["Value"].(map[string]interface{})["GE"].([]interface{})

	for _, _market := range markets {
		market := _market.(map[string]interface{})
		odds := market["E"].([]interface{})
		switch int(market["G"].(float64)) {
		//three way
		case 1:
			for _, _odds := range odds {
				odd := _odds.([]interface{})[0].(map[string]interface{})
				oddVal := odd["C"].(float64)
				switch int(odd["T"].(float64)) {
				case 1:
					tw.Home = oddVal
				case 2:
					tw.Draw = oddVal
				case 3:
					tw.Away = oddVal
				}
			}

		}
	}

	game.TWAY = tw

	// utils.Debug(game)
	return &game
}

func (xbet *Xbet) GetRugbyGame(transport *http.Transport, gameLink, apiLink string) Game {
	g := xbet.getRGame(gameLink, apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (xbet *Xbet) GetIceHockey(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() {
		close(games)
	}()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	//https://1xbet.co.ke/LineFeed/Get1x2_VZip?sports=3%2C4&count=50&lng=en&tf=2200000&tz=3&mode=4&country=87&partner=61&getEmpty=true
	req, _ := http.NewRequest(http.MethodGet, "https://1xbet.co.ke/LineFeed/Get1x2_VZip?sports=2&count=500&lng=en&tf=2200000&mode=4&country=87&partner=61&getEmpty=true", nil)
	// req.Header.Add("Accept-Encoding", "gzip, deflate, br")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	data, _ := ioutil.ReadAll(resp.Body)
	// utils.Debug(string(data))
	xbet.addUsage(len(data))
	// xbet.mainpage += len(data)

	parsed := map[string]interface{}{}
	json.Unmarshal(data, &parsed)

	form := func(name string) (string, string) {
		lname := strings.Replace(name, ".", "", -1)
		lname = strings.Trim(lname, " ")
		lname = strings.Replace(lname, " ", "-", -1)
		lname = strings.Replace(lname, "(", "", -1)
		lname = strings.Replace(lname, ")", "", -1)
		lname = strings.Trim(lname, " ")
		lname = strings.Replace(lname, "&", "", -1)
		lname = strings.Replace(lname, " ", "-", -1)
		return lname, name
	}
	allowedLeague := func(name string) bool {
		name = strings.ToUpper(name)
		prohib := []string{"SPECIALS", "STATISTICS", "SPECIAL", "DUEL"}
		for _, p := range prohib {
			if strings.Contains(name, p) {
				return false
			}
		}
		return true
	}

	if _, ok := parsed["Value"]; !ok {
		utils.Error(string(data))
		if isStopped() {
			return
		}

		return
	}
	fmt.Printf("1XTYPE %T", parsed["Value"])
	values := parsed["Value"].([]interface{})
	for _, _v := range values {
		v := _v.(map[string]interface{})
		league, _ := form(v["L"].(string))

		if !allowedLeague(league) {
			continue
		}
		if _, ok := v["O1"]; !ok {
			utils.Error(v)
			continue
		}
		if _, ok := v["O2"]; !ok {
			utils.Error(v)
			continue
		}

		fullPath := fmt.Sprintf("https://1xbet.co.ke/LineFeed/GetGameZip?id=%d&lng=en&cfview=0&isSubGames=true&GroupEvents=true&allEventsGroupSubGames=true&countevents=250&partner=61", int(v["CI"].(float64)))

		gameTime := time.Unix(int64(v["S"].(float64)), 0)

		game := Game{}
		game.Site = xbet.Name()
		game.League = league
		// game.GameID = gameId
		game.Home = v["O1"].(string)
		game.Away = v["O2"].(string)
		game.TimeStr = gameTime.Format(time.ANSIC)
		game.Time = &gameTime

		h, _ := form(game.Home)
		a, _ := form(game.Away)

		gameLink := fmt.Sprintf("https://1xbet.co.ke/en/line/Ice-Hockey/%d-%s/%d-%s-%s/", int(v["LI"].(float64)), league, int(v["CI"].(float64)), h, a)

		g := xbet.getIHGame(gameLink, fullPath, game)
		if isStopped() {
			return
		}
		// utils.Debug(*g)
		if g == nil {
			continue
		}

		xbet.count++
		games <- *g

	}

	if isStopped() {
		return
	}

	return
}

func (xbet *Xbet) getIHGame(gameLink, apiLink string, game Game) *Game {
	tw := ThreeWay{Link: gameLink, APILink: apiLink}
	dc := DoubleChance{Link: gameLink, APILink: apiLink}
	gng := GoalNoGoal{Link: gameLink, APILink: apiLink}

	req, _ := http.NewRequest(http.MethodGet, apiLink, nil)
	// req.Header.Add("Accept-Encoding", "gzip, deflate, br")
	resp, err := http.DefaultClient.Do(req)

	if err != nil {
		utils.Error(err)
		return nil
	}

	if resp.Body == nil {
		d, _ := ioutil.ReadAll(resp.Body)
		utils.Debug(string(d))
		return nil
	}
	data, _ := ioutil.ReadAll(resp.Body)
	xbet.addUsage(len(data))
	parsed := map[string]interface{}{}
	json.Unmarshal(data, &parsed)

	// utils.Debug(string(data))
	if _, ok := parsed["Value"]; !ok {
		return nil
	}
	if parsed["Value"] == nil {
		return nil
	}
	markets := parsed["Value"].(map[string]interface{})["GE"].([]interface{})

	for _, _market := range markets {
		market := _market.(map[string]interface{})
		odds := market["E"].([]interface{})
		switch int(market["G"].(float64)) {
		//three way
		case 1:
			for _, _odds := range odds {
				odd := _odds.([]interface{})[0].(map[string]interface{})
				oddVal := odd["C"].(float64)
				switch int(odd["T"].(float64)) {
				case 1:
					tw.Home = oddVal
				case 2:
					tw.Draw = oddVal
				case 3:
					tw.Away = oddVal
				}
			}

		case 8:
			for _, _odds := range odds {
				odd := _odds.([]interface{})[0].(map[string]interface{})
				oddVal := odd["C"].(float64)
				switch int(odd["T"].(float64)) {
				case 4:
					dc.Home = oddVal
				case 5:
					dc.HomeOrAway = oddVal
				case 6:
					dc.Away = oddVal
				}
			}

		case 19:
			for _, _odds := range odds {
				odd := _odds.([]interface{})[0].(map[string]interface{})
				oddVal := odd["C"].(float64)
				switch int(odd["T"].(float64)) {
				case 180:
					gng.Goal = oddVal
				case 181:
					gng.NoGoal = oddVal
				}
			}
		}
	}

	game.TWAY = tw
	game.GNG = gng
	game.DC = dc

	// utils.Debug(game)
	return &game
}

func (xbet *Xbet) GetIceHockeyGame(transport *http.Transport, gameLink, apiLink string) Game {
	g := xbet.getIHGame(gameLink, apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (xbet *Xbet) GetHandball(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() {
		close(games)
	}()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	//https://1xbet.co.ke/LineFeed/Get1x2_VZip?sports=3%2C4&count=50&lng=en&tf=2200000&tz=3&mode=4&country=87&partner=61&getEmpty=true
	req, _ := http.NewRequest(http.MethodGet, "https://1xbet.co.ke/LineFeed/Get1x2_VZip?sports=8&count=500&lng=en&tf=2200000&mode=4&country=87&partner=61&getEmpty=true", nil)
	// req.Header.Add("Accept-Encoding", "gzip, deflate, br")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	data, _ := ioutil.ReadAll(resp.Body)
	// utils.Debug(string(data))
	xbet.addUsage(len(data))
	// xbet.mainpage += len(data)

	parsed := map[string]interface{}{}
	json.Unmarshal(data, &parsed)

	form := func(name string) (string, string) {
		lname := strings.Replace(name, ".", "", -1)
		lname = strings.Trim(lname, " ")
		lname = strings.Replace(lname, " ", "-", -1)
		lname = strings.Replace(lname, "(", "", -1)
		lname = strings.Replace(lname, ")", "", -1)
		lname = strings.Trim(lname, " ")
		lname = strings.Replace(lname, "&", "", -1)
		lname = strings.Replace(lname, " ", "-", -1)
		return lname, name
	}
	allowedLeague := func(name string) bool {
		name = strings.ToUpper(name)
		prohib := []string{"SPECIALS", "STATISTICS", "SPECIAL", "DUEL"}
		for _, p := range prohib {
			if strings.Contains(name, p) {
				return false
			}
		}
		return true
	}

	if _, ok := parsed["Value"]; !ok {
		utils.Error(string(data))
		if isStopped() {
			return
		}

		return
	}
	fmt.Printf("1XTYPE %T", parsed["Value"])
	values := parsed["Value"].([]interface{})
	for _, _v := range values {
		v := _v.(map[string]interface{})
		league, _ := form(v["L"].(string))

		if !allowedLeague(league) {
			continue
		}
		if _, ok := v["O1"]; !ok {
			utils.Error(v)
			continue
		}
		if _, ok := v["O2"]; !ok {
			utils.Error(v)
			continue
		}

		fullPath := fmt.Sprintf("https://1xbet.co.ke/LineFeed/GetGameZip?id=%d&lng=en&cfview=0&isSubGames=true&GroupEvents=true&allEventsGroupSubGames=true&countevents=250&partner=61", int(v["CI"].(float64)))

		gameTime := time.Unix(int64(v["S"].(float64)), 0)

		game := Game{}
		game.Site = xbet.Name()
		game.League = league
		// game.GameID = gameId
		game.Home = v["O1"].(string)
		game.Away = v["O2"].(string)
		game.TimeStr = gameTime.Format(time.ANSIC)
		game.Time = &gameTime

		h, _ := form(game.Home)
		a, _ := form(game.Away)

		gameLink := fmt.Sprintf("https://1xbet.co.ke/en/line/Handball/%d-%s/%d-%s-%s/", int(v["LI"].(float64)), league, int(v["CI"].(float64)), h, a)

		g := xbet.getHGame(gameLink, fullPath, game)
		if isStopped() {
			return
		}
		// utils.Debug(*g)
		if g == nil {
			continue
		}

		xbet.count++
		games <- *g

	}

	if isStopped() {
		return
	}

	return
}

func (xbet *Xbet) getHGame(gameLink, apiLink string, game Game) *Game {
	tw := ThreeWay{Link: gameLink, APILink: apiLink}
	dc := DoubleChance{Link: gameLink, APILink: apiLink}

	req, _ := http.NewRequest(http.MethodGet, apiLink, nil)
	// req.Header.Add("Accept-Encoding", "gzip, deflate, br")
	resp, err := http.DefaultClient.Do(req)

	if err != nil {
		utils.Error(err)
		return nil
	}

	if resp.Body == nil {
		d, _ := ioutil.ReadAll(resp.Body)
		utils.Debug(string(d))
		return nil
	}
	data, _ := ioutil.ReadAll(resp.Body)
	xbet.addUsage(len(data))
	parsed := map[string]interface{}{}
	json.Unmarshal(data, &parsed)

	// utils.Debug(string(data))
	if _, ok := parsed["Value"]; !ok {
		return nil
	}
	if parsed["Value"] == nil {
		return nil
	}
	markets := parsed["Value"].(map[string]interface{})["GE"].([]interface{})

	for _, _market := range markets {
		market := _market.(map[string]interface{})
		odds := market["E"].([]interface{})
		switch int(market["G"].(float64)) {
		//three way
		case 1:
			for _, _odds := range odds {
				odd := _odds.([]interface{})[0].(map[string]interface{})
				oddVal := odd["C"].(float64)
				switch int(odd["T"].(float64)) {
				case 1:
					tw.Home = oddVal
				case 2:
					tw.Draw = oddVal
				case 3:
					tw.Away = oddVal
				}
			}

		case 8:
			for _, _odds := range odds {
				odd := _odds.([]interface{})[0].(map[string]interface{})
				oddVal := odd["C"].(float64)
				switch int(odd["T"].(float64)) {
				case 4:
					dc.Home = oddVal
				case 5:
					dc.HomeOrAway = oddVal
				case 6:
					dc.Away = oddVal
				}
			}

		}
	}

	game.TWAY = tw
	game.DC = dc

	// utils.Debug(game)
	return &game
}

func (xbet *Xbet) GetHandballGame(transport *http.Transport, gameLink, apiLink string) Game {
	g := xbet.getHGame(gameLink, apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (xbet *Xbet) cloneBrowser() *browser.Browser {
	nav := surf.NewBrowser()
	nav.SetHistoryJar(jar.NewMemoryHistory())
	nav.SetCookieJar(jar.NewMemoryCookies())
	nav.SetUserAgent("Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
	return nav
}
