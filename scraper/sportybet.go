package scraper

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"sync"
	"time"

	"gitlab.com/arbke/utils"
)

type Sportybet struct {
	name     string
	link     string
	usage    int
	mainpage int
	lock     *sync.Mutex
}

func NewSportybet() *Sportybet {
	//1,18,10,29,11,26,36,14
	// marketIds := "1,18,10,29,11,26,36,14"
	// pagesize := 500 //num games
	// spid := "sr:sport:1"

	return &Sportybet{
		name: "Sportybet",
		//set game nos with pageSize
		link: "https://www.sportybet.com/api/ke/factsCenter/pcUpcomingEvents?sportId=sr%3Asport%3A1&marketId=1%2C18%2C10%2C29%2C11%2C26%2C36%2C14&pageSize=100&pageNum=1&todayGames=true",
		lock: &sync.Mutex{},
	}
}

func (sbet *Sportybet) addUsage(u int) {
	sbet.lock.Lock()
	defer sbet.lock.Unlock()
	sbet.usage += u
}
func (sbet *Sportybet) Usage() int {
	sbet.lock.Lock()
	defer sbet.lock.Unlock()
	return sbet.usage
}
func (sbet *Sportybet) Name() string {
	return sbet.name
}
func (sbet *Sportybet) GetFootball(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	tournaments := []interface{}{}

	// today := "https://www.sportybet.com/api/ke/factsCenter/pcUpcomingEvents?sportId=sr%3Asport%3A1&marketId=1%2C18%2C10%2C29%2C11%2C26%2C36%2C14&pageSize=100&pageNum=1&todayGames=true"
	upcoming := "https://www.sportybet.com/api/ke/factsCenter/pcUpcomingEvents?sportId=sr%%3Asport%%3A1&marketId=1%%2C18%%2C10%%2C29%%2C11%%2C26%%2C36%%2C14&pageSize=100&pageNum=%d"

	for page := 1; page <= 5; page++ {
		link := ""

		link = fmt.Sprintf(upcoming, page)

		req, _ := http.NewRequest(http.MethodGet, link, nil)

		req.Header.Add("clientid", "web")
		req.Header.Add("platform", "web")
		req.Header.Add("operid", "1")
		req.Header.Add("user-agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
		req.Header.Add("content-type", "application/x-www-form-urlencoded; charset=UTF-8")
		req.Header.Add("accept", "*/*")
		req.Header.Add("referer", "https://www.sportybet.com/ke/sport/football/upcoming?time=0")
		req.Header.Add("accept-language", "en-US,en;q=0.9")

		resp, err := http.DefaultClient.Do(req)

		if err != nil {
			utils.Error(err)
			return
		}

		defer resp.Body.Close()

		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		// ioutil.WriteFile(fmt.Sprintf("out%d.json", page), data, 0777)
		sbet.addUsage(len(data))
		_parsed := map[string]interface{}{}

		if err := json.Unmarshal(data, &_parsed); err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		if _, ok := _parsed["data"]; !ok {
			utils.Error("Sportybet", string(data))
			if isStopped() {
				return
			}

			return
		}
		parsed := _parsed["data"].(map[string]interface{})
		if _, ok := parsed["tournaments"]; !ok {
			utils.Error(_parsed)
			if isStopped() {
				return
			}

			return
		}

		tournaments = append(tournaments, parsed["tournaments"].([]interface{})...)
	}
	for _, _league := range tournaments {
		league := _league.(map[string]interface{})
		name := league["name"].(string)
		for _, _event := range league["events"].([]interface{}) {

			event := _event.(map[string]interface{})
			game := Game{}
			game.Site = sbet.name
			game.League = name
			game.Home = event["homeTeamName"].(string) + Allowed(name)
			game.Away = event["awayTeamName"].(string) + Allowed(name)

			catId := event["sport"].(map[string]interface{})["category"].(map[string]interface{})["id"].(string)
			tournId := event["sport"].(map[string]interface{})["category"].(map[string]interface{})["tournament"].(map[string]interface{})["id"].(string)

			gameLink := fmt.Sprintf("https://www.sportybet.com/ke/sport/football/%s/%s/%s", catId, tournId, event["eventId"].(string))
			// utils.Debug(gameLink)

			t := time.Unix(int64(event["estimateStartTime"].(float64))/1000, 0)
			game.Time = &t

			tw := ThreeWay{Link: gameLink, APILink: gameLink}
			dnb := DrawNoBet{Link: gameLink, APILink: gameLink}
			dc := DoubleChance{Link: gameLink, APILink: gameLink}

			ou15 := OU15{Link: gameLink, APILink: gameLink}
			ou25 := OU25{Link: gameLink, APILink: gameLink}
			ou35 := OU35{Link: gameLink, APILink: gameLink}
			ou45 := OU45{Link: gameLink, APILink: gameLink}

			gng := GoalNoGoal{Link: gameLink}
			if _, ok := event["markets"]; !ok {
				continue
			}
			for _, _market := range event["markets"].([]interface{}) {
				market := _market.(map[string]interface{})
				outcomes := market["outcomes"].([]interface{})
				status := int(market["status"].(float64))
				// utils.Debug(status)
				if status != 0 {
					// utils.Debug("Closed Market", market["name"].(string), game.Home, game.Away)
					continue
				}
				switch market["id"].(string) {
				case "1": //1x2
					for _, _outcome := range outcomes {
						outcome := _outcome.(map[string]interface{})
						switch outcome["id"].(string) {
						case "1":
							tw.Home, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
						case "3":
							tw.Away, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
						case "2":
							tw.Draw, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
						}
					}

				case "10": //DC

					for _, _outcome := range outcomes {
						outcome := _outcome.(map[string]interface{})
						switch outcome["id"].(string) {
						case "9":
							dc.Home, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
						case "11":
							dc.Away, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
						case "10":
							dc.HomeOrAway, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
						}
					}

				case "11": //DNB
					for _, _outcome := range outcomes {
						outcome := _outcome.(map[string]interface{})
						switch outcome["id"].(string) {
						case "4":
							dnb.Home, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
						case "5":
							dnb.Away, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
						}
					}

				case "29": //GNG
					for _, _outcome := range outcomes {
						outcome := _outcome.(map[string]interface{})
						switch outcome["id"].(string) {
						case "74":
							gng.Goal, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
						case "76":
							gng.NoGoal, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
						}
					}

				case "18": //O/U
					spec := market["specifier"].(string)
					for _, _outcome := range outcomes {
						outcome := _outcome.(map[string]interface{})
						switch outcome["id"].(string) {
						case "12":
							switch spec {
							case "total=1.5":
								ou15.Over, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
							case "total=2.5":
								ou25.Over, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
							case "total=3.5":
								ou35.Over, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
							case "total=4.5":
								ou45.Over, _ = strconv.ParseFloat(outcome["odds"].(string), 64)

							}

						case "13":
							switch spec {
							case "total=1.5":
								// utils.Debug(outcome["odds"].(string))
								ou15.Under, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
							case "total=2.5":
								ou25.Under, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
							case "total=3.5":
								ou35.Under, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
							case "total=4.5":
								ou45.Under, _ = strconv.ParseFloat(outcome["odds"].(string), 64)

							}

						}
					}

				}

			}
			game.TWAY = tw
			game.DC = dc
			game.DNB = dnb
			game.GNG = gng
			game.OU15 = ou15
			game.OU25 = ou25
			game.OU35 = ou35
			game.OU45 = ou45

			if isStopped() {
				return
			}
			games <- game
		}
	}

	if isStopped() {
		return
	}

}

func (sbet *Sportybet) GetFootballGame(t *http.Transport, gameLink string, apiLink string) Game {
	games := make(chan Game)
	stop := make(chan struct{})
	go func() {
		sbet.GetFootball(t, games, stop)
	}()

	for g := range games {
		if g.TWAY.APILink == apiLink {
			stop <- struct{}{}
			return g
		}
	}
	if !ChannelClosed(games) {

	}

	return Game{}
}

//https://www.sportybet.com/api/ke/factsCenter/pcUpcomingEvents?sportId=sr%3Asport%3A2&marketId=219%2C18%2C16%2C1%2C14%2C11&pageSize=100&pageNum=1&_t=1536482138115
func (sbet *Sportybet) GetBasketBall(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	tournaments := []interface{}{}

	// today := "https://www.sportybet.com/api/ke/factsCenter/pcUpcomingEvents?sportId=sr%3Asport%3A1&marketId=1%2C18%2C10%2C29%2C11%2C26%2C36%2C14&pageSize=100&pageNum=1&todayGames=true"
	link := "https://www.sportybet.com/api/ke/factsCenter/pcUpcomingEvents?sportId=sr%3Asport%3A2&marketId=219%2C18%2C16%2C1%2C14%2C11&pageSize=100&pageNum=1"

	req, _ := http.NewRequest(http.MethodGet, link, nil)

	req.Header.Add("clientid", "web")
	req.Header.Add("platform", "web")
	req.Header.Add("operid", "1")
	req.Header.Add("user-agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
	req.Header.Add("content-type", "application/x-www-form-urlencoded; charset=UTF-8")
	req.Header.Add("accept", "*/*")
	req.Header.Add("referer", "https://www.sportybet.com/ke/sport/basketball/upcoming?time=0")
	req.Header.Add("accept-language", "en-US,en;q=0.9")

	resp, err := http.DefaultClient.Do(req)

	if err != nil {
		utils.Error(err)
		return
	}

	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	// ioutil.WriteFile(fmt.Sprintf("out%d.json", page), data, 0777)
	sbet.addUsage(len(data))
	_parsed := map[string]interface{}{}

	if err := json.Unmarshal(data, &_parsed); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	if _, ok := _parsed["data"]; !ok {
		utils.Error("Sportybet", string(data))
		if isStopped() {
			return
		}

		return
	}
	parsed := _parsed["data"].(map[string]interface{})
	if _, ok := parsed["tournaments"]; !ok {
		utils.Error(_parsed)
		if isStopped() {
			return
		}

		return
	}

	tournaments = append(tournaments, parsed["tournaments"].([]interface{})...)

	for _, _league := range tournaments {
		league := _league.(map[string]interface{})
		name := league["name"].(string)
		for _, _event := range league["events"].([]interface{}) {

			event := _event.(map[string]interface{})
			game := Game{}
			game.Site = sbet.name
			game.League = name
			game.Home = event["homeTeamName"].(string)
			game.Away = event["awayTeamName"].(string)

			catId := event["sport"].(map[string]interface{})["category"].(map[string]interface{})["id"].(string)
			tournId := event["sport"].(map[string]interface{})["category"].(map[string]interface{})["tournament"].(map[string]interface{})["id"].(string)

			gameLink := fmt.Sprintf("https://www.sportybet.com/ke/sport/basketball/%s/%s/%s", catId, tournId, event["eventId"].(string))
			// utils.Debug(gameLink)

			t := time.Unix(int64(event["estimateStartTime"].(float64))/1000, 0)
			game.Time = &t

			tw := TwoWay{Link: gameLink, APILink: gameLink}
			if _, ok := event["markets"]; !ok {
				continue
			}
			for _, _market := range event["markets"].([]interface{}) {
				market := _market.(map[string]interface{})
				outcomes := market["outcomes"].([]interface{})
				status := int(market["status"].(float64))
				// utils.Debug(status)
				if status != 0 {
					// utils.Debug("Closed Market", market["name"].(string), game.Home, game.Away)
					continue
				}
				switch market["id"].(string) {
				case "219": //1-2
					for _, _outcome := range outcomes {
						outcome := _outcome.(map[string]interface{})
						switch outcome["id"].(string) {
						case "4":
							tw.Home, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
						// case "3":
						// 	tw.Away, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
						case "5":
							tw.Away, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
						}
					}

				}

			}
			game.TWOWAY = tw

			if isStopped() {
				return
			}
			games <- game
		}
	}

	if isStopped() {
		return
	}

}

func (sbet *Sportybet) GetBasketBallGame(t *http.Transport, gameLink string, apiLink string) Game {
	games := make(chan Game)
	stop := make(chan struct{})
	go func() {
		sbet.GetBasketBall(t, games, stop)
	}()

	for g := range games {
		if g.TWAY.APILink == apiLink {
			stop <- struct{}{}
			return g
		}
	}

	return Game{}
}

func (sbet *Sportybet) GetTennis(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	tournaments := []interface{}{}

	// today := "https://www.sportybet.com/api/ke/factsCenter/pcUpcomingEvents?sportId=sr%3Asport%3A1&marketId=1%2C18%2C10%2C29%2C11%2C26%2C36%2C14&pageSize=100&pageNum=1&todayGames=true"
	link := "https://www.sportybet.com/api/ke/factsCenter/pcUpcomingEvents?sportId=sr%3Asport%3A5&marketId=186%2C189%2C202%2C188%2C187&pageSize=100&pageNum=1"

	req, _ := http.NewRequest(http.MethodGet, link, nil)

	req.Header.Add("clientid", "web")
	req.Header.Add("platform", "web")
	req.Header.Add("operid", "1")
	req.Header.Add("user-agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
	req.Header.Add("content-type", "application/x-www-form-urlencoded; charset=UTF-8")
	req.Header.Add("accept", "*/*")
	req.Header.Add("referer", "https://www.sportybet.com/ke/sport/tennis/upcoming?time=0")
	req.Header.Add("accept-language", "en-US,en;q=0.9")

	resp, err := http.DefaultClient.Do(req)

	if err != nil {
		utils.Error(err)
		return
	}

	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	// ioutil.WriteFile(fmt.Sprintf("out%d.json", page), data, 0777)
	sbet.addUsage(len(data))
	_parsed := map[string]interface{}{}

	if err := json.Unmarshal(data, &_parsed); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	if _, ok := _parsed["data"]; !ok {
		utils.Error("Sportybet", string(data))
		if isStopped() {
			return
		}

		return
	}
	parsed := _parsed["data"].(map[string]interface{})
	if _, ok := parsed["tournaments"]; !ok {
		utils.Error(_parsed)
		if isStopped() {
			return
		}

		return
	}

	tournaments = append(tournaments, parsed["tournaments"].([]interface{})...)

	for _, _league := range tournaments {
		league := _league.(map[string]interface{})
		name := league["name"].(string)
		for _, _event := range league["events"].([]interface{}) {

			event := _event.(map[string]interface{})
			game := Game{}
			game.Site = sbet.name
			game.League = name
			game.Home = event["homeTeamName"].(string)
			game.Away = event["awayTeamName"].(string)

			catId := event["sport"].(map[string]interface{})["category"].(map[string]interface{})["id"].(string)
			tournId := event["sport"].(map[string]interface{})["category"].(map[string]interface{})["tournament"].(map[string]interface{})["id"].(string)

			gameLink := fmt.Sprintf("https://www.sportybet.com/ke/sport/tennis/%s/%s/%s", catId, tournId, event["eventId"].(string))
			// utils.Debug(gameLink)

			t := time.Unix(int64(event["estimateStartTime"].(float64))/1000, 0)
			game.Time = &t

			tw := TwoWay{Link: gameLink, APILink: gameLink}
			if _, ok := event["markets"]; !ok {
				continue
			}
			for _, _market := range event["markets"].([]interface{}) {
				market := _market.(map[string]interface{})
				outcomes := market["outcomes"].([]interface{})
				status := int(market["status"].(float64))
				// utils.Debug(status)
				if status != 0 {
					// utils.Debug("Closed Market", market["name"].(string), game.Home, game.Away)
					continue
				}
				switch market["id"].(string) {
				case "186": //1-2
					for _, _outcome := range outcomes {
						outcome := _outcome.(map[string]interface{})
						switch outcome["id"].(string) {
						case "4":
							tw.Home, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
						// case "3":
						// 	tw.Away, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
						case "5":
							tw.Away, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
						}
					}

				}

			}
			game.TWOWAY = tw

			if isStopped() {
				return
			}
			games <- game
		}
	}

	if isStopped() {
		return
	}

}

func (sbet *Sportybet) GetTennisGame(t *http.Transport, gameLink string, apiLink string) Game {
	games := make(chan Game)
	stop := make(chan struct{})
	go func() {
		sbet.GetTennis(t, games, stop)
	}()

	for g := range games {
		if g.TWAY.APILink == apiLink {
			stop <- struct{}{}
			return g
		}
	}

	return Game{}
}

func (sbet *Sportybet) GetRugby(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	tournaments := []interface{}{}

	// today := "https://www.sportybet.com/api/ke/factsCenter/pcUpcomingEvents?sportId=sr%3Asport%3A1&marketId=1%2C18%2C10%2C29%2C11%2C26%2C36%2C14&pageSize=100&pageNum=1&todayGames=true"
	link := "https://www.sportybet.com/api/ke/factsCenter/pcUpcomingEvents?sportId=sr%3Asport%3A12&marketId=1%2C18%2C14%2C16%2C487&pageSize=100&pageNum=1"

	req, _ := http.NewRequest(http.MethodGet, link, nil)

	req.Header.Add("clientid", "web")
	req.Header.Add("platform", "web")
	req.Header.Add("operid", "1")
	req.Header.Add("user-agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36")
	req.Header.Add("content-type", "application/x-www-form-urlencoded; charset=UTF-8")
	req.Header.Add("accept", "*/*")
	req.Header.Add("referer", "https://www.sportybet.com/ke/sport/tennis/upcoming?time=0")
	req.Header.Add("accept-language", "en-US,en;q=0.9")

	resp, err := http.DefaultClient.Do(req)

	if err != nil {
		utils.Error(err)
		return
	}

	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	// ioutil.WriteFile(fmt.Sprintf("out%d.json", page), data, 0777)
	sbet.addUsage(len(data))
	_parsed := map[string]interface{}{}

	if err := json.Unmarshal(data, &_parsed); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	if _, ok := _parsed["data"]; !ok {
		utils.Error("Sportybet", string(data))
		if isStopped() {
			return
		}

		return
	}
	parsed := _parsed["data"].(map[string]interface{})
	if _, ok := parsed["tournaments"]; !ok {
		utils.Error(_parsed)
		if isStopped() {
			return
		}

		return
	}

	tournaments = append(tournaments, parsed["tournaments"].([]interface{})...)

	for _, _league := range tournaments {
		league := _league.(map[string]interface{})
		name := league["name"].(string)
		for _, _event := range league["events"].([]interface{}) {

			event := _event.(map[string]interface{})
			game := Game{}
			game.Site = sbet.name
			game.League = name
			game.Home = event["homeTeamName"].(string)
			game.Away = event["awayTeamName"].(string)

			catId := event["sport"].(map[string]interface{})["category"].(map[string]interface{})["id"].(string)
			tournId := event["sport"].(map[string]interface{})["category"].(map[string]interface{})["tournament"].(map[string]interface{})["id"].(string)

			gameLink := fmt.Sprintf("https://www.sportybet.com/ke/sport/rugby/%s/%s/%s", catId, tournId, event["eventId"].(string))
			// utils.Debug(gameLink)

			t := time.Unix(int64(event["estimateStartTime"].(float64))/1000, 0)
			game.Time = &t

			tw := ThreeWay{Link: gameLink, APILink: gameLink}
			if _, ok := event["markets"]; !ok {
				continue
			}
			for _, _market := range event["markets"].([]interface{}) {
				market := _market.(map[string]interface{})
				outcomes := market["outcomes"].([]interface{})
				status := int(market["status"].(float64))
				// utils.Debug(status)
				if status != 0 {
					// utils.Debug("Closed Market", market["name"].(string), game.Home, game.Away)
					continue
				}
				switch market["id"].(string) {
				case "1": //1-2
					for _, _outcome := range outcomes {
						outcome := _outcome.(map[string]interface{})
						switch outcome["id"].(string) {
						case "1":
							tw.Home, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
						case "3":
							tw.Away, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
						case "2":
							tw.Draw, _ = strconv.ParseFloat(outcome["odds"].(string), 64)
						}
					}

				}

			}
			game.TWAY = tw

			if isStopped() {
				return
			}
			games <- game
		}
	}

	if isStopped() {
		return
	}

}

func (sbet *Sportybet) GetRugbyGame(t *http.Transport, gameLink string, apiLink string) Game {
	games := make(chan Game)
	stop := make(chan struct{})
	go func() {
		sbet.GetRugby(t, games, stop)
	}()

	for g := range games {
		if g.TWAY.APILink == apiLink {
			stop <- struct{}{}
			return g
		}
	}

	return Game{}
}

func (sbet *Sportybet) GetHandball(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (sbet *Sportybet) GetHandballGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (sbet *Sportybet) GetIceHockey(transport *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}
func (sbet *Sportybet) GetIceHockeyGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (sbet *Sportybet) UsageReset() {
	sbet.usage = 0
	sbet.mainpage = 0
}
func (sbet *Sportybet) MainPageUsage() int {
	return sbet.mainpage
}
