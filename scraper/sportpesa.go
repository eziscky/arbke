package scraper

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"sync"
	"time"

	"gitlab.com/arbke/utils"
)

type Sportpesa struct {
	name     string
	link     string
	usage    int
	mainpage int

	lock *sync.Mutex
}

type apimatch struct {
	Id          int `json:"id"`
	Competition struct {
		Name string `json:"name"`
	} `json:"competition"`
	Competitors []struct {
		Name string `json:"name"`
	} `json:"competitors"`
	DateTimestamp int64 `json:"dateTimestamp"`
}

type apidetails struct {
	Name     string  `json:"name"`
	Num      int     `json:"columns"`
	Spec     float64 `json:"specValue"`
	Outcomes []struct {
		Name string `json:"shortName"`
		Odd  string `json:"odds"`
	} `json:"selections"`
}

func NewSpesa() *Sportpesa {
	return &Sportpesa{
		name: "Sportpesa",
		link: "https://www.sportpesa.co.ke/api",
		lock: &sync.Mutex{},
	}
}

func (sbet *Sportpesa) addUsage(u int) {
	sbet.lock.Lock()
	defer sbet.lock.Unlock()
	sbet.usage += u
}

func (sbet *Sportpesa) Usage() int {
	sbet.lock.Lock()
	defer sbet.lock.Unlock()
	return sbet.usage
}
func (sbet *Sportpesa) Name() string {
	return sbet.name
}

func (spesa *Sportpesa) getRequest(endpoint string, referer string) *http.Request {
	req, _ := http.NewRequest(http.MethodGet, endpoint, nil)
	req.Header.Add("accept-language", "en-US,en;q=0.5")
	req.Header.Add("connection", "keep-alive")
	req.Header.Add("x-requested-with", "XMLHttpRequest")
	req.Header.Add("x-app-timezone", "Africa/Nairobi")
	req.Header.Add("content-type", "application/x-www-form-urlencoded; charset=UTF-8")
	req.Header.Add("Referer", referer)
	req.Header.Add("user-agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0")
	return req
}

func (spesa *Sportpesa) GetFootball(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	//https://www.sportpesa.co.ke/api/
	//get all upcoming games
	endpoint := fmt.Sprintf("%s/upcoming/games?type=prematch&sportId=1&section=upcoming&o=leagues&pag_count=%d&pag_min=1", spesa.link, 500)
	req := spesa.getRequest(endpoint, "https://www.sportpesa.co.ke/?sportId=1&section=upcoming")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	resp.Body.Close()
	// utils.Debug(string(data))
	spesa.addUsage(len(data))
	parsed := []apimatch{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	for _, match := range parsed {
		game := Game{}
		game.League = match.Competition.Name
		t := time.Unix(match.DateTimestamp/1000, 0)
		game.Time = &t
		game.Home = match.Competitors[0].Name + Allowed(game.League)
		game.Away = match.Competitors[1].Name + Allowed(game.League)

		gameLink := fmt.Sprintf("https://www.sportpesa.co.ke/games/%d/markets?sportId=1&section=upcoming", match.Id)
		// utils.Debug(gameLink)

		game.Site = spesa.name

		apiLink := fmt.Sprintf("%s/games/markets?games=%d&markets=all", spesa.link, match.Id)

		g := spesa.getGame(gameLink, apiLink, game)
		if isStopped() {
			return
		}
		if g == nil {
			continue
		}
		// utils.Debug(*g)

		games <- *g

	}

	if isStopped() {
		return
	}

}
func (spesa *Sportpesa) getGame(gameLink, apiLink string, game Game) *Game {

	tw := ThreeWay{Link: gameLink, APILink: apiLink}
	dnb := DrawNoBet{Link: gameLink, APILink: apiLink}
	dc := DoubleChance{Link: gameLink, APILink: apiLink}

	ou15 := OU15{Link: gameLink, APILink: apiLink}
	ou25 := OU25{Link: gameLink, APILink: apiLink}
	ou35 := OU35{Link: gameLink, APILink: apiLink}
	ou45 := OU45{Link: gameLink, APILink: apiLink}

	gng := GoalNoGoal{Link: gameLink}
	referer := gameLink
	req := spesa.getRequest(apiLink, referer)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	resp.Body.Close()

	spesa.addUsage(len(data))
	parsed := map[string][]apidetails{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}

	for _, markets := range parsed {

		for _, market := range markets {
			// utils.Debug(market)
			switch market.Name {

			case "3 Way": //TWAY

				for _, outcome := range market.Outcomes {

					odd, err := strconv.ParseFloat(outcome.Odd, 64)
					if err != nil {
						continue
					}

					switch outcome.Name {
					case "1":
						tw.Home = odd
					case "X":
						tw.Draw = odd
					case "2":
						tw.Away = odd
					}
				}

			case "Double Chance": //DC

				for _, outcome := range market.Outcomes {

					odd, err := strconv.ParseFloat(outcome.Odd, 64)
					if err != nil {
						continue
					}

					switch outcome.Name {
					case "1X":
						dc.Home = odd
					case "12":
						dc.HomeOrAway = odd
					case "X2":
						dc.Away = odd
					}
				}
			case "Draw no bet - Full Time": //DNB
				for _, outcome := range market.Outcomes {

					odd, err := strconv.ParseFloat(outcome.Odd, 64)
					if err != nil {
						continue
					}
					switch outcome.Name {
					case "1":
						dnb.Home = odd
					case "2":
						dnb.Away = odd
					}
				}

			case "Both Teams To Score": //GNG
				for _, outcome := range market.Outcomes {

					odd, err := strconv.ParseFloat(outcome.Odd, 64)
					if err != nil {
						continue
					}
					switch outcome.Name {
					case "GG":
						gng.Goal = odd
					case "NG":
						gng.NoGoal = odd
					}
				}

			case "Over/Under":
				switch market.Spec {
				case 1.5:
					for _, outcome := range market.Outcomes {

						odd, err := strconv.ParseFloat(outcome.Odd, 64)
						if err != nil {
							continue
						}
						switch outcome.Name {
						case "OV":
							ou15.Over = odd
						case "UN":
							ou15.Under = odd
						}
					}

				case 2.5:
					for _, outcome := range market.Outcomes {

						odd, err := strconv.ParseFloat(outcome.Odd, 64)
						if err != nil {
							continue
						}
						switch outcome.Name {
						case "OV":
							ou25.Over = odd
						case "UN":
							ou25.Under = odd
						}
					}
				case 3.5:
					for _, outcome := range market.Outcomes {

						odd, err := strconv.ParseFloat(outcome.Odd, 64)
						if err != nil {
							continue
						}
						switch outcome.Name {
						case "OV":
							ou35.Over = odd
						case "UN":
							ou35.Under = odd
						}
					}

				case 4.5:
					for _, outcome := range market.Outcomes {

						odd, err := strconv.ParseFloat(outcome.Odd, 64)
						if err != nil {
							continue
						}
						switch outcome.Name {
						case "OV":
							ou45.Over = odd
						case "UN":
							ou45.Under = odd
						}
					}
				}
			}

		}

	}
	game.TWAY = tw
	game.DC = dc
	game.DNB = dnb
	game.GNG = gng
	game.OU15 = ou15
	game.OU25 = ou25
	game.OU35 = ou35
	game.OU45 = ou45

	return &game
}

func (spesa *Sportpesa) GetFootballGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := spesa.getGame(gameLink, apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (spesa *Sportpesa) GetBasketBall(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	//https://www.sportpesa.co.ke/api/upcoming/games?type=prematch&sportId=2&section=upcoming&o=leagues&pag_count=15&pag_min=1
	//https://www.sportpesa.co.ke/api/
	//get all upcoming games
	endpoint := fmt.Sprintf("%s/upcoming/games?type=prematch&sportId=2&section=upcoming&o=leagues&pag_count=%d&pag_min=1", spesa.link, 500)
	req := spesa.getRequest(endpoint, "https://www.sportpesa.co.ke/?sportId=2&section=upcoming")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	resp.Body.Close()
	// utils.Debug(string(data))
	spesa.addUsage(len(data))
	parsed := []apimatch{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	for _, match := range parsed {
		game := Game{}
		game.League = match.Competition.Name
		t := time.Unix(match.DateTimestamp/1000, 0)
		game.Time = &t
		game.Home = match.Competitors[0].Name
		game.Away = match.Competitors[1].Name

		gameLink := fmt.Sprintf("https://www.sportpesa.co.ke/games/%d/markets?sportId=2&section=upcoming", match.Id)
		// utils.Debug(gameLink)

		game.Site = spesa.name

		apiLink := fmt.Sprintf("%s/games/markets?games=%d&markets=all", spesa.link, match.Id)

		g := spesa.getBGame(gameLink, apiLink, game)
		if isStopped() {
			return
		}
		if g == nil {
			continue
		}
		// utils.Debug(*g)

		games <- *g

	}

	if isStopped() {
		return
	}

}

func (spesa *Sportpesa) getBGame(gameLink, apiLink string, game Game) *Game {

	tw := TwoWay{Link: gameLink, APILink: apiLink}

	referer := gameLink
	req := spesa.getRequest(apiLink, referer)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	resp.Body.Close()

	spesa.addUsage(len(data))
	parsed := map[string][]apidetails{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}

	for _, markets := range parsed {

		for _, market := range markets {
			// utils.Debug(market)
			switch market.Name {

			case "2 Way - OT incl.": //TWAY

				for _, outcome := range market.Outcomes {

					odd, err := strconv.ParseFloat(outcome.Odd, 64)
					if err != nil {
						continue
					}

					switch outcome.Name {
					case "1":
						tw.Home = odd
					// case "X":
					// 	tw.Draw = odd
					case "2":
						tw.Away = odd
					}
				}

			}

		}

	}
	game.TWOWAY = tw

	return &game
}
func (spesa *Sportpesa) GetBasketBallGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := spesa.getBGame(gameLink, apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (spesa *Sportpesa) GetTennis(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	//https://www.sportpesa.co.ke/api/upcoming/games?type=prematch&sportId=2&section=upcoming&o=leagues&pag_count=15&pag_min=1
	//https://www.sportpesa.co.ke/api/
	//get all upcoming games
	endpoint := fmt.Sprintf("%s/upcoming/games?type=prematch&sportId=5&section=upcoming&o=leagues&pag_count=%d&pag_min=1", spesa.link, 500)
	req := spesa.getRequest(endpoint, "https://www.sportpesa.co.ke/?sportId=5&section=upcoming")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	resp.Body.Close()
	// utils.Debug(string(data))
	spesa.addUsage(len(data))
	parsed := []apimatch{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	for _, match := range parsed {
		game := Game{}
		game.League = match.Competition.Name
		t := time.Unix(match.DateTimestamp/1000, 0)
		game.Time = &t
		game.Home = match.Competitors[0].Name
		game.Away = match.Competitors[1].Name

		gameLink := fmt.Sprintf("https://www.sportpesa.co.ke/games/%d/markets?sportId=5&section=upcoming", match.Id)
		// utils.Debug(gameLink)

		game.Site = spesa.name

		apiLink := fmt.Sprintf("%s/games/markets?games=%d&markets=all", spesa.link, match.Id)

		g := spesa.getTGame(gameLink, apiLink, game)
		if isStopped() {
			return
		}
		if g == nil {
			continue
		}
		// utils.Debug(*g)

		games <- *g

	}

	if isStopped() {
		return
	}

}
func (spesa *Sportpesa) getTGame(gameLink, apiLink string, game Game) *Game {

	tw := TwoWay{Link: gameLink, APILink: apiLink}

	referer := gameLink
	req := spesa.getRequest(apiLink, referer)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	resp.Body.Close()

	spesa.addUsage(len(data))
	parsed := map[string][]apidetails{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}

	for _, markets := range parsed {

		for _, market := range markets {
			// utils.Debug(market)
			switch market.Name {

			case "2 Way - Who will win?": //TWAY

				for _, outcome := range market.Outcomes {

					odd, err := strconv.ParseFloat(outcome.Odd, 64)
					if err != nil {
						continue
					}

					switch outcome.Name {
					case "1":
						tw.Home = odd
					// case "X":
					// 	tw.Draw = odd
					case "2":
						tw.Away = odd
					}
				}

			}

		}

	}
	game.TWOWAY = tw

	return &game
}
func (spesa *Sportpesa) GetTennisGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := spesa.getTGame(gameLink, apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (spesa *Sportpesa) GetRugby(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	//https://www.sportpesa.co.ke/api/upcoming/games?type=prematch&sportId=2&section=upcoming&o=leagues&pag_count=15&pag_min=1
	//https://www.sportpesa.co.ke/api/
	//get all upcoming games
	endpoint := fmt.Sprintf("%s/upcoming/games?type=prematch&sportId=12&section=upcoming&o=leagues&pag_count=%d&pag_min=1", spesa.link, 500)
	req := spesa.getRequest(endpoint, "https://www.sportpesa.co.ke/?sportId=12&section=upcoming")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	resp.Body.Close()
	// utils.Debug(string(data))
	spesa.addUsage(len(data))
	parsed := []apimatch{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	for _, match := range parsed {
		game := Game{}
		game.League = match.Competition.Name
		t := time.Unix(match.DateTimestamp/1000, 0)
		game.Time = &t
		game.Home = match.Competitors[0].Name
		game.Away = match.Competitors[1].Name

		gameLink := fmt.Sprintf("https://www.sportpesa.co.ke/games/%d/markets?sportId=12&section=upcoming", match.Id)
		// utils.Debug(gameLink)

		game.Site = spesa.name

		apiLink := fmt.Sprintf("%s/games/markets?games=%d&markets=all", spesa.link, match.Id)

		g := spesa.getRGame(gameLink, apiLink, game)
		if isStopped() {
			return
		}
		if g == nil {
			continue
		}
		// utils.Debug(*g)

		games <- *g

	}

	if isStopped() {
		return
	}

}
func (spesa *Sportpesa) getRGame(gameLink, apiLink string, game Game) *Game {

	tw := ThreeWay{Link: gameLink, APILink: apiLink}

	referer := gameLink
	req := spesa.getRequest(apiLink, referer)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	resp.Body.Close()

	spesa.addUsage(len(data))
	parsed := map[string][]apidetails{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}

	for _, markets := range parsed {

		for _, market := range markets {
			// utils.Debug(market)
			switch market.Name {

			case "3 Way - Full Time": //TWAY

				for _, outcome := range market.Outcomes {

					odd, err := strconv.ParseFloat(outcome.Odd, 64)
					if err != nil {
						continue
					}

					switch outcome.Name {
					case "1":
						tw.Home = odd
					case "X":
						tw.Draw = odd
					case "2":
						tw.Away = odd
					}
				}

			}

		}

	}
	game.TWAY = tw

	return &game
}
func (spesa *Sportpesa) GetRugbyGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := spesa.getRGame(gameLink, apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (spesa *Sportpesa) GetHandball(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	//https://www.sportpesa.co.ke/api/upcoming/games?type=prematch&sportId=2&section=upcoming&o=leagues&pag_count=15&pag_min=1
	//https://www.sportpesa.co.ke/api/
	//get all upcoming games
	endpoint := fmt.Sprintf("%s/upcoming/games?type=prematch&sportId=6&section=upcoming&o=leagues&pag_count=%d&pag_min=1", spesa.link, 500)
	req := spesa.getRequest(endpoint, "https://www.sportpesa.co.ke/?sportId=6&section=upcoming")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	resp.Body.Close()
	// utils.Debug(string(data))
	spesa.addUsage(len(data))
	parsed := []apimatch{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	for _, match := range parsed {
		game := Game{}
		game.League = match.Competition.Name
		t := time.Unix(match.DateTimestamp/1000, 0)
		game.Time = &t
		game.Home = match.Competitors[0].Name
		game.Away = match.Competitors[1].Name

		gameLink := fmt.Sprintf("https://www.sportpesa.co.ke/games/%d/markets?sportId=6&section=upcoming", match.Id)
		// utils.Debug(gameLink)

		game.Site = spesa.name

		apiLink := fmt.Sprintf("%s/games/markets?games=%d&markets=all", spesa.link, match.Id)

		g := spesa.getHGame(gameLink, apiLink, game)
		if isStopped() {
			return
		}
		if g == nil {
			continue
		}
		// utils.Debug(*g)

		games <- *g

	}

	if isStopped() {
		return
	}

}
func (spesa *Sportpesa) getHGame(gameLink, apiLink string, game Game) *Game {

	tw := ThreeWay{Link: gameLink, APILink: apiLink}

	referer := gameLink
	req := spesa.getRequest(apiLink, referer)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	resp.Body.Close()

	spesa.addUsage(len(data))
	parsed := map[string][]apidetails{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}

	for _, markets := range parsed {

		for _, market := range markets {
			// utils.Debug(market)
			switch market.Name {

			case "3 Way Winner": //TWAY

				for _, outcome := range market.Outcomes {

					odd, err := strconv.ParseFloat(outcome.Odd, 64)
					if err != nil {
						continue
					}

					switch outcome.Name {
					case "1":
						tw.Home = odd
					case "X":
						tw.Draw = odd
					case "2":
						tw.Away = odd
					}
				}

			}

		}

	}
	game.TWAY = tw

	return &game
}
func (spesa *Sportpesa) GetHandballGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := spesa.getHGame(gameLink, apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (spesa *Sportpesa) GetIceHockey(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	//https://www.sportpesa.co.ke/api/upcoming/games?type=prematch&sportId=2&section=upcoming&o=leagues&pag_count=15&pag_min=1
	//https://www.sportpesa.co.ke/api/
	//get all upcoming games
	endpoint := fmt.Sprintf("%s/upcoming/games?type=prematch&sportId=4&section=upcoming&o=leagues&pag_count=%d&pag_min=1", spesa.link, 500)
	req := spesa.getRequest(endpoint, "https://www.sportpesa.co.ke/?sportId=4&section=upcoming")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	resp.Body.Close()
	// utils.Debug(string(data))
	spesa.addUsage(len(data))
	parsed := []apimatch{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	for _, match := range parsed {
		game := Game{}
		game.League = match.Competition.Name
		t := time.Unix(match.DateTimestamp/1000, 0)
		game.Time = &t
		game.Home = match.Competitors[0].Name
		game.Away = match.Competitors[1].Name

		gameLink := fmt.Sprintf("https://www.sportpesa.co.ke/games/%d/markets?sportId=4&section=upcoming", match.Id)
		// utils.Debug(gameLink)

		game.Site = spesa.name

		apiLink := fmt.Sprintf("%s/games/markets?games=%d&markets=all", spesa.link, match.Id)

		g := spesa.getRGame(gameLink, apiLink, game)
		if isStopped() {
			return
		}
		if g == nil {
			continue
		}
		// utils.Debug(*g)

		games <- *g

	}

	if isStopped() {
		return
	}

}
func (spesa *Sportpesa) getIHGame(gameLink, apiLink string, game Game) *Game {

	tw := ThreeWay{Link: gameLink, APILink: apiLink}
	dnb := DrawNoBet{Link: gameLink, APILink: apiLink}
	referer := gameLink
	req := spesa.getRequest(apiLink, referer)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	resp.Body.Close()

	spesa.addUsage(len(data))
	parsed := map[string][]apidetails{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}

	for _, markets := range parsed {

		for _, market := range markets {
			// utils.Debug(market)
			switch market.Name {

			case "3 Way - Full Time": //TWAY

				for _, outcome := range market.Outcomes {

					odd, err := strconv.ParseFloat(outcome.Odd, 64)
					if err != nil {
						continue
					}

					switch outcome.Name {
					case "1":
						tw.Home = odd
					case "X":
						tw.Draw = odd
					case "2":
						tw.Away = odd
					}
				}
			case "2 Way Winner OT-incl.": //TWAY

				for _, outcome := range market.Outcomes {

					odd, err := strconv.ParseFloat(outcome.Odd, 64)
					if err != nil {
						continue
					}

					switch outcome.Name {
					case "1":
						dnb.Home = odd

					case "2":
						dnb.Away = odd
					}
				}

			}

		}

	}
	game.TWAY = tw
	game.DNB = dnb
	return &game
}
func (spesa *Sportpesa) GetIceHockeyGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := spesa.getIHGame(gameLink, apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (spesa *Sportpesa) UsageReset() {
	spesa.usage = 0
	spesa.mainpage = 0
}
func (spesa *Sportpesa) MainPageUsage() int {
	return spesa.mainpage
}
