package scraper

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"sync"

	"gitlab.com/arbke/utils"

	"github.com/headzoo/surf"
	"github.com/headzoo/surf/browser"
	"github.com/headzoo/surf/jar"
)

func GetSpecLeague(team string) string {
	name := strings.ToUpper(team)
	s := []string{"YOUTH", "U21", "U19", "UNDER 19", "UNDER 21", "U20", "UNDER 20", "AMATEUR", "CAMPIONATO", "WOMEN", "FEMININA"}
	for _, i := range s {
		if strings.Contains(name, i) {
			if i == "FEMININA" {
				return "WOMEN"
			}
			return i
		}
	}
	return ""
}
func Allowed(name string) string {
	comp := strings.ToUpper(name)
	s := []string{"YOUTH", "U21", "U19", "UNDER 19", "UNDER 21", "U20", "UNDER 20", "AMATEUR", "CAMPIONATO", "WOMEN", "FEMININA"}
	for _, i := range s {
		if strings.Contains(comp, i) {
			if i == "FEMININA" {
				return "WOMEN"
			}
			return i
		}
	}
	return ""
}

type Betika struct {
	name      string
	url       string
	navigator *browser.Browser
	usage     int
	mainpage  int

	lock *sync.Mutex
}

func NewBetika() *Betika {
	betika := &Betika{
		name:      "Betika",
		url:       "https://www.betika.com",
		navigator: surf.NewBrowser(),
		lock:      &sync.Mutex{},
	}
	// betika.navigator.SetTimeout(30 * time.Second)
	betika.navigator.SetUserAgent("Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
	return betika
}

func (betika *Betika) Name() string {
	return betika.name
}
func (betika *Betika) MainPageUsage() int {
	return betika.mainpage
}

func (betika *Betika) addUsage(u int) {
	betika.lock.Lock()
	defer betika.lock.Unlock()
	betika.usage += u
}
func (betika *Betika) Usage() int {
	betika.lock.Lock()
	defer betika.lock.Unlock()
	return betika.usage
}

func (betika *Betika) UsageReset() {
	betika.usage = 0
	betika.mainpage = 0
}

func (betika *Betika) cloneBrowser() *browser.Browser {
	nav := surf.NewBrowser()
	// nav.SetTimeout(30 * time.Second)
	nav.SetHistoryJar(jar.NewMemoryHistory())
	nav.SetCookieJar(jar.NewMemoryCookies())
	nav.SetUserAgent("Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
	nav.AddRequestHeader("Accept-Encoding", "gzip, deflate, br")
	return nav
}

func (betika *Betika) GetFootball(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	req, err := http.NewRequest("GET", "https://api.betika.com/v1/matches?page=1&limit=500&keyword=&tab=upcoming", nil)
	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}
	req.Header.Add("accept", "application/json, text/plain, */*")
	req.Header.Add("user-agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
	req.Header.Add("content-type", "application/json;charset=UTF-8")
	req.Header.Add("accept-encoding", "gzip, deflate, br")
	req.Header.Add("accept-language", "en-US,en;q=0.8")
	req.Header.Add("cache-control", "no-cache")

	resp, err := http.DefaultClient.Do(req)

	if err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	data, _ := ioutil.ReadAll(resp.Body)
	betika.addUsage(len(data))
	betika.mainpage += len(data)
	// ioutil.WriteFile("out2.json", data, 0777)

	parsed := map[string]interface{}{}
	json.Unmarshal(data, &parsed)
	utils.Debug(string(data))
	if _, ok := parsed["data"]; !ok {
		return
	}
	matches := parsed["data"].([]interface{})

	for _, _content := range matches {
		content := _content.(map[string]interface{})
		game := Game{}
		game.Site = betika.Name()
		game.GameID = content["match_id"].(string)
		game.Time = utils.DecodeTime(betika.name, content["start_time"].(string))
		game.Away = content["away_team"].(string)
		game.Home = content["home_team"].(string)

		league := fmt.Sprintf("%s %s", strings.ToUpper(content["category"].(string)), strings.ToUpper(content["competition_name"].(string)))
		game.League = league
		// utils.Debug(league, game.Home)
		allowed := Allowed(league)
		if len(allowed) > 0 {
			// utils.Debug(league)
			game.Home = fmt.Sprintf("%s %s", game.Home, allowed)
			game.Away = fmt.Sprintf("%s %s", game.Away, allowed)
		}

		apiLink := fmt.Sprintf("https://api.betika.com/v1/match?id=%s", game.GameID)

		g := betika.getGame(apiLink, game)
		if isStopped() {
			return
		}
		if g == nil {
			continue
		}
		// utils.Debug(*g)

		games <- *g
	}
	if isStopped() {
		return
	}

	return
}

func (betika *Betika) getGame(apiLink string, game Game) *Game {
	tw := ThreeWay{APILink: apiLink}
	dc := DoubleChance{APILink: apiLink}
	dnb := DrawNoBet{APILink: apiLink}
	gng := GoalNoGoal{APILink: apiLink}
	ou15 := OU15{APILink: apiLink}
	ou25 := OU25{APILink: apiLink}
	ou35 := OU35{APILink: apiLink}

	// content["competition_name"]
	req, err := http.NewRequest("GET", apiLink, nil)
	if err != nil {
		utils.Error(err)
		return nil
	}
	req.Header.Add("accept-encoding", "gzip, deflate, br")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}
	// utils.Debug(resp.Header.Get("Content-Encoding"))
	data, _ := ioutil.ReadAll(utils.DecodeGZIP(resp.Body))
	betika.addUsage(len(data))
	// ioutil.WriteFile("out.json", data, 0777)

	parsed := map[string]interface{}{}
	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}

	if _, ok := parsed["data"]; !ok {
		utils.Error(string(data))
		return nil
	}
	if parsed["data"] == nil {
		utils.Debug(string(data))
		return nil
	}
	markets := parsed["data"].([]interface{})

	for _, _market := range markets {
		market := _market.(map[string]interface{})
		odds := market["odds"].([]interface{})
		switch market["sub_type_id"].(string) {
		case "10": //three way

			for _, _odd := range odds {
				odd := _odd.(map[string]interface{})
				switch odd["odd_key"].(string) {
				case "1":
					tw.Home, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				case "X":
					tw.Draw, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				case "2":
					tw.Away, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				}
			}
		case "46":

			for _, _odd := range odds {
				odd := _odd.(map[string]interface{})
				switch odd["odd_key"].(string) {
				case "1X":
					dc.Home, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				case "X2":
					dc.Away, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				case "12":
					dc.HomeOrAway, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				}
			}

		case "47":

			for _, _odd := range odds {
				odd := _odd.(map[string]interface{})
				switch odd["odd_key"].(string) {
				case "1":
					dnb.Home, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				case "2":
					dnb.Away, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)

				}
			}
		case "43":

			for _, _odd := range odds {
				odd := _odd.(map[string]interface{})
				switch odd["odd_key"].(string) {
				case "Yes":
					gng.Goal, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				case "No":
					gng.NoGoal, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)

				}
			}

		case "56":

			for _, _odd := range odds {
				//over 1.5
				odd := _odd.(map[string]interface{})
				switch odd["odd_key"].(string) {
				case "Over":
					val, _ := strconv.ParseFloat(odd["odd_value"].(string), 64)
					switch odd["special_bet_value"].(string) {
					case "1.5":
						ou15.Over = val
					case "2.5":
						ou25.Over = val
					case "3.5":
						ou35.Over = val
					}

				case "Under":
					val, _ := strconv.ParseFloat(odd["odd_value"].(string), 64)
					switch odd["special_bet_value"].(string) {
					case "1.5":
						ou15.Under = val
					case "2.5":
						ou25.Under = val
					case "3.5":
						ou35.Under = val
					}
				}
			}
		}

	}

	game.TWAY = tw
	game.DC = dc
	game.DNB = dnb
	game.GNG = gng
	game.OU15 = ou15
	game.OU25 = ou25
	game.OU35 = ou35
	return &game
}
func (betika *Betika) GetFootballGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := betika.getGame(apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (betika *Betika) getCategories(sportID int) []int {
	switch sportID {
	case 28:
		return []int{19212, 19216, 8988, 9002}
	case 30:
		return []int{9005, 9007, 9008, 9009, 9015, 18921, 18923, 18929, 18924, 18925, 8899}
	case 41:
		return []int{8946, 805, 14457, 15059, 8935}
	case 33:
		return []int{16520, 15867, 15891, 16343}
	case 29:
		return []int{8882, 8890, 8885, 8931, 15717, 9016, 8895}
	}
	ids := []int{}
	link := fmt.Sprintf("https://api.betika.com/v1/sport?id=%d", sportID)

	req, _ := http.NewRequest("GET", link, nil)
	req.Header.Add("accept", "application/json, text/plain, */*")
	req.Header.Add("user-agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
	req.Header.Add("content-type", "application/json;charset=UTF-8")
	req.Header.Add("accept-encoding", "gzip, deflate, br")
	req.Header.Add("accept-language", "en-US,en;q=0.8")
	req.Header.Add("cache-control", "no-cache")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
	}

	data, _ := ioutil.ReadAll(utils.DecodeGZIP(resp.Body))

	parsed := map[string]interface{}{}
	// ioutil.WriteFile("out.json", data, 0777)
	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return ids
	}

	if _, ok := parsed["data"]; !ok {
		return ids
	}

	d := parsed["data"].([]interface{})
	for _, c := range d {
		category := c.(map[string]interface{})
		// utils.Debug(category)
		for _, _comp := range category["competitions"].([]interface{}) {
			comp := _comp.(map[string]interface{})
			if id, ok := comp["competition_id"]; ok {
				_id, _ := strconv.Atoi(id.(string))
				ids = append(ids, _id)
			}
		}
	}
	utils.Debug(ids)
	return ids
}

func (betika *Betika) GetTennis(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	categories := betika.getCategories(28)
	for _, cat := range categories {
		link := fmt.Sprintf("https://api.betika.com/v1/competition?id=%d", cat)

		req, err := http.NewRequest("GET", link, nil)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		req.Header.Add("accept", "application/json, text/plain, */*")
		req.Header.Add("user-agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
		req.Header.Add("content-type", "application/json;charset=UTF-8")
		req.Header.Add("accept-encoding", "gzip, deflate, br")
		req.Header.Add("accept-language", "en-US,en;q=0.8")
		req.Header.Add("cache-control", "no-cache")

		resp, err := http.DefaultClient.Do(req)

		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		data, _ := ioutil.ReadAll(utils.DecodeGZIP(resp.Body))
		betika.addUsage(len(data))
		betika.mainpage += len(data)
		// ioutil.WriteFile("out2.json", data, 0777)

		parsed := map[string]interface{}{}
		json.Unmarshal(data, &parsed)
		matches := parsed["data"].([]interface{})

		for _, _content := range matches {
			content := _content.(map[string]interface{})
			game := Game{}
			game.Site = betika.Name()
			game.GameID = content["match_id"].(string)
			game.Time = utils.DecodeTime(betika.name, content["start_time"].(string))
			game.Away = content["away_team"].(string)
			game.Home = content["home_team"].(string)

			league := fmt.Sprintf("%s", strings.ToUpper(content["competition_name"].(string)))
			game.League = league
			// utils.Debug(league, game.Home)

			apiLink := fmt.Sprintf("https://api.betika.com/v1/match?id=%s", game.GameID)

			g := betika.getTGame(apiLink, game)
			if isStopped() {
				return
			}
			if g == nil {
				continue
			}
			// utils.Debug(*g)

			games <- *g
		}
	}
	if isStopped() {
		return
	}

	return
}

func (betika *Betika) getTGame(apiLink string, game Game) *Game {
	tw := TwoWay{APILink: apiLink}
	// content["competition_name"]
	req, err := http.NewRequest("GET", apiLink, nil)
	if err != nil {
		utils.Error(err)
		return nil
	}
	req.Header.Add("accept-encoding", "gzip, deflate, br")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}
	// utils.Debug(resp.Header.Get("Content-Encoding"))
	data, _ := ioutil.ReadAll(utils.DecodeGZIP(resp.Body))
	betika.addUsage(len(data))
	// ioutil.WriteFile("out.json", data, 0777)

	parsed := map[string]interface{}{}
	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}

	if _, ok := parsed["data"]; !ok {
		utils.Error(string(data))
		return nil
	}
	if parsed["data"] == nil {
		utils.Debug(string(data))
		return nil
	}
	markets := parsed["data"].([]interface{})

	for _, _market := range markets {
		market := _market.(map[string]interface{})
		odds := market["odds"].([]interface{})
		switch market["sub_type_id"].(string) {
		case "20": //three way

			for _, _odd := range odds {
				odd := _odd.(map[string]interface{})
				switch odd["odd_key"].(string) {
				case "1":
					tw.Home, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				// case "X":
				// 	tw.Draw, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				case "2":
					tw.Away, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				}
			}

		}

	}

	game.TWOWAY = tw

	return &game
}
func (betika *Betika) GetTennisGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := betika.getTGame(apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (betika *Betika) GetBasketBall(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	categories := betika.getCategories(30)
	// utils.Debug(categories)
	for _, cat := range categories {
		link := fmt.Sprintf("https://api.betika.com/v1/competition?id=%d", cat)

		req, err := http.NewRequest("GET", link, nil)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		req.Header.Add("accept", "application/json, text/plain, */*")
		req.Header.Add("user-agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
		req.Header.Add("content-type", "application/json;charset=UTF-8")
		req.Header.Add("accept-encoding", "gzip, deflate, br")
		req.Header.Add("accept-language", "en-US,en;q=0.8")
		req.Header.Add("cache-control", "no-cache")

		resp, err := http.DefaultClient.Do(req)

		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		data, _ := ioutil.ReadAll(utils.DecodeGZIP(resp.Body))
		betika.addUsage(len(data))
		betika.mainpage += len(data)
		// ioutil.WriteFile("out2.json", data, 0777)

		parsed := map[string]interface{}{}
		json.Unmarshal(data, &parsed)
		matches := parsed["data"].([]interface{})

		for _, _content := range matches {
			content := _content.(map[string]interface{})
			game := Game{}
			game.Site = betika.Name()
			game.GameID = content["match_id"].(string)
			game.Time = utils.DecodeTime(betika.name, content["start_time"].(string))
			game.Away = content["away_team"].(string)
			game.Home = content["home_team"].(string)

			league := fmt.Sprintf("%s", strings.ToUpper(content["competition_name"].(string)))
			game.League = league
			// utils.Debug(league, game.Home)

			apiLink := fmt.Sprintf("https://api.betika.com/v1/match?id=%s", game.GameID)

			g := betika.getBGame(apiLink, game)
			if isStopped() {
				return
			}
			if g == nil {
				continue
			}
			// utils.Debug(*g)

			games <- *g
		}
	}
	if isStopped() {
		return
	}

	return
}

func (betika *Betika) getBGame(apiLink string, game Game) *Game {
	tw := TwoWay{APILink: apiLink}
	// content["competition_name"]
	req, err := http.NewRequest("GET", apiLink, nil)
	if err != nil {
		utils.Error(err)
		return nil
	}
	req.Header.Add("accept-encoding", "gzip, deflate, br")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}
	// utils.Debug(resp.Header.Get("Content-Encoding"))
	data, _ := ioutil.ReadAll(utils.DecodeGZIP(resp.Body))
	betika.addUsage(len(data))
	// ioutil.WriteFile("out.json", data, 0777)

	parsed := map[string]interface{}{}
	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}

	if _, ok := parsed["data"]; !ok {
		utils.Error(string(data))
		return nil
	}
	if parsed["data"] == nil {
		utils.Debug(string(data))
		return nil
	}
	markets := parsed["data"].([]interface{})

	for _, _market := range markets {
		market := _market.(map[string]interface{})
		odds := market["odds"].([]interface{})
		switch market["sub_type_id"].(string) {
		case "20": //three way

			for _, _odd := range odds {
				odd := _odd.(map[string]interface{})
				switch odd["odd_key"].(string) {
				case "1":
					tw.Home, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				// case "X":
				// 	tw.Draw, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				case "2":
					tw.Away, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				}
			}

		}

	}

	game.TWOWAY = tw

	return &game
}
func (betika *Betika) GetBasketBallGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := betika.getBGame(apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (betika *Betika) GetRugby(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	categories := betika.getCategories(41)
	for _, cat := range categories {
		link := fmt.Sprintf("https://api.betika.com/v1/competition?id=%d", cat)

		req, err := http.NewRequest("GET", link, nil)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		req.Header.Add("accept", "application/json, text/plain, */*")
		req.Header.Add("user-agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
		req.Header.Add("content-type", "application/json;charset=UTF-8")
		// req.Header.Add("accept-encoding", "gzip, deflate, br")
		req.Header.Add("accept-language", "en-US,en;q=0.8")
		req.Header.Add("cache-control", "no-cache")

		resp, err := http.DefaultClient.Do(req)

		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		data, _ := ioutil.ReadAll(resp.Body)
		betika.addUsage(len(data))
		betika.mainpage += len(data)
		// ioutil.WriteFile("out2.json", data, 0777)

		parsed := map[string]interface{}{}
		json.Unmarshal(data, &parsed)
		matches := parsed["data"].([]interface{})

		for _, _content := range matches {
			content := _content.(map[string]interface{})
			game := Game{}
			game.Site = betika.Name()
			game.GameID = content["match_id"].(string)
			game.Time = utils.DecodeTime(betika.name, content["start_time"].(string))
			game.Away = content["away_team"].(string)
			game.Home = content["home_team"].(string)

			league := fmt.Sprintf("%s", strings.ToUpper(content["competition_name"].(string)))
			game.League = league
			// utils.Debug(league, game.Home)

			apiLink := fmt.Sprintf("https://api.betika.com/v1/match?id=%s", game.GameID)

			g := betika.getRGame(apiLink, game)
			if isStopped() {
				return
			}
			if g == nil {
				continue
			}
			// utils.Debug(*g)

			games <- *g
		}
	}
	if isStopped() {
		return
	}

	return
}

func (betika *Betika) getRGame(apiLink string, game Game) *Game {
	tw := ThreeWay{APILink: apiLink}
	// content["competition_name"]
	req, err := http.NewRequest("GET", apiLink, nil)
	if err != nil {
		utils.Error(err)
		return nil
	}
	req.Header.Add("accept-encoding", "gzip, deflate, br")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}
	// utils.Debug(resp.Header.Get("Content-Encoding"))
	data, _ := ioutil.ReadAll(utils.DecodeGZIP(resp.Body))
	betika.addUsage(len(data))
	// ioutil.WriteFile("out.json", data, 0777)

	parsed := map[string]interface{}{}
	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}

	if _, ok := parsed["data"]; !ok {
		utils.Error(string(data))
		return nil
	}
	if parsed["data"] == nil {
		utils.Debug(string(data))
		return nil
	}
	markets := parsed["data"].([]interface{})

	for _, _market := range markets {
		market := _market.(map[string]interface{})
		odds := market["odds"].([]interface{})
		switch market["sub_type_id"].(string) {
		case "10": //three way

			for _, _odd := range odds {
				odd := _odd.(map[string]interface{})
				switch odd["odd_key"].(string) {
				case "1":
					tw.Home, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				case "X":
					tw.Draw, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				case "2":
					tw.Away, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				}
			}

		}

	}

	game.TWAY = tw

	return &game
}
func (betika *Betika) GetRugbyGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := betika.getRGame(apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (betika *Betika) GetIceHockey(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	categories := betika.getCategories(29)
	for _, cat := range categories {
		link := fmt.Sprintf("https://api.betika.com/v1/competition?id=%d", cat)

		req, err := http.NewRequest("GET", link, nil)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		req.Header.Add("accept", "application/json, text/plain, */*")
		req.Header.Add("user-agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
		req.Header.Add("content-type", "application/json;charset=UTF-8")
		req.Header.Add("accept-encoding", "gzip, deflate, br")
		req.Header.Add("accept-language", "en-US,en;q=0.8")
		req.Header.Add("cache-control", "no-cache")

		resp, err := http.DefaultClient.Do(req)

		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		data, _ := ioutil.ReadAll(utils.DecodeGZIP(resp.Body))
		betika.addUsage(len(data))
		betika.mainpage += len(data)
		// ioutil.WriteFile("out2.json", data, 0777)

		parsed := map[string]interface{}{}
		json.Unmarshal(data, &parsed)
		matches := parsed["data"].([]interface{})

		for _, _content := range matches {
			content := _content.(map[string]interface{})
			game := Game{}
			game.Site = betika.Name()
			game.GameID = content["match_id"].(string)
			game.Time = utils.DecodeTime(betika.name, content["start_time"].(string))
			game.Away = content["away_team"].(string)
			game.Home = content["home_team"].(string)

			league := fmt.Sprintf("%s", strings.ToUpper(content["competition_name"].(string)))
			game.League = league
			// utils.Debug(league, game.Home)

			apiLink := fmt.Sprintf("https://api.betika.com/v1/match?id=%s", game.GameID)

			g := betika.getIHGame(apiLink, game)
			if isStopped() {
				return
			}
			if g == nil {
				continue
			}
			// utils.Debug(*g)

			games <- *g
		}
	}
	if isStopped() {
		return
	}

	return
}

func (betika *Betika) getIHGame(apiLink string, game Game) *Game {
	tw := ThreeWay{APILink: apiLink}
	dc := DoubleChance{APILink: apiLink}
	dnb := DrawNoBet{APILink: apiLink}
	gng := GoalNoGoal{APILink: apiLink}
	ou15 := OU15{APILink: apiLink}
	ou25 := OU25{APILink: apiLink}
	ou35 := OU35{APILink: apiLink}

	// content["competition_name"]
	req, err := http.NewRequest("GET", apiLink, nil)
	if err != nil {
		utils.Error(err)
		return nil
	}
	req.Header.Add("accept-encoding", "gzip, deflate, br")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}
	// utils.Debug(resp.Header.Get("Content-Encoding"))

	tmp := utils.DecodeGZIP(resp.Body)
	if tmp == nil {
		return nil
	}
	data, _ := ioutil.ReadAll(tmp)
	betika.addUsage(len(data))
	// ioutil.WriteFile("out.json", data, 0777)

	parsed := map[string]interface{}{}
	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}

	if _, ok := parsed["data"]; !ok {
		utils.Error(string(data))
		return nil
	}
	if parsed["data"] == nil {
		utils.Debug(string(data))
		return nil
	}
	markets := parsed["data"].([]interface{})

	for _, _market := range markets {
		market := _market.(map[string]interface{})
		odds := market["odds"].([]interface{})
		switch market["sub_type_id"].(string) {
		case "10": //three way

			for _, _odd := range odds {
				odd := _odd.(map[string]interface{})
				switch odd["odd_key"].(string) {
				case "1":
					tw.Home, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				case "X":
					tw.Draw, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				case "2":
					tw.Away, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				}
			}
		case "46":

			for _, _odd := range odds {
				odd := _odd.(map[string]interface{})
				switch odd["odd_key"].(string) {
				case "1X":
					dc.Home, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				case "X2":
					dc.Away, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				case "12":
					dc.HomeOrAway, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				}
			}

		case "47":

			for _, _odd := range odds {
				odd := _odd.(map[string]interface{})
				switch odd["odd_key"].(string) {
				case "1":
					dnb.Home, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				case "2":
					dnb.Away, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)

				}
			}
		case "43":

			for _, _odd := range odds {
				odd := _odd.(map[string]interface{})
				switch odd["odd_key"].(string) {
				case "Yes":
					gng.Goal, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				case "No":
					gng.NoGoal, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)

				}
			}

		}

	}

	game.TWAY = tw
	game.DC = dc
	game.DNB = dnb
	game.GNG = gng
	game.OU15 = ou15
	game.OU25 = ou25
	game.OU35 = ou35

	return &game
}
func (betika *Betika) GetIceHockeyGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := betika.getIHGame(apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (betika *Betika) GetHandball(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	categories := betika.getCategories(33)
	for _, cat := range categories {
		link := fmt.Sprintf("https://api.betika.com/v1/competition?id=%d", cat)

		req, err := http.NewRequest("GET", link, nil)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		req.Header.Add("accept", "application/json, text/plain, */*")
		req.Header.Add("user-agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
		req.Header.Add("content-type", "application/json;charset=UTF-8")
		req.Header.Add("accept-encoding", "gzip, deflate, br")
		req.Header.Add("accept-language", "en-US,en;q=0.8")
		req.Header.Add("cache-control", "no-cache")

		resp, err := http.DefaultClient.Do(req)

		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		data, _ := ioutil.ReadAll(utils.DecodeGZIP(resp.Body))
		betika.addUsage(len(data))
		betika.mainpage += len(data)
		// ioutil.WriteFile("out2.json", data, 0777)

		parsed := map[string]interface{}{}
		json.Unmarshal(data, &parsed)
		matches := parsed["data"].([]interface{})

		for _, _content := range matches {
			content := _content.(map[string]interface{})
			game := Game{}
			game.Site = betika.Name()
			game.GameID = content["match_id"].(string)
			game.Time = utils.DecodeTime(betika.name, content["start_time"].(string))
			game.Away = content["away_team"].(string)
			game.Home = content["home_team"].(string)

			league := fmt.Sprintf("%s", strings.ToUpper(content["competition_name"].(string)))
			game.League = league
			// utils.Debug(league, game.Home)

			apiLink := fmt.Sprintf("https://api.betika.com/v1/match?id=%s", game.GameID)

			g := betika.getHGame(apiLink, game)
			if isStopped() {
				return
			}
			if g == nil {
				continue
			}
			// utils.Debug(*g)

			games <- *g
		}
	}
	if isStopped() {
		return
	}

	return
}

func (betika *Betika) getHGame(apiLink string, game Game) *Game {
	tw := ThreeWay{APILink: apiLink}
	dc := DoubleChance{APILink: apiLink}

	// content["competition_name"]
	req, err := http.NewRequest("GET", apiLink, nil)
	if err != nil {
		utils.Error(err)
		return nil
	}
	req.Header.Add("accept-encoding", "gzip, deflate, br")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}
	// utils.Debug(resp.Header.Get("Content-Encoding"))
	data, _ := ioutil.ReadAll(utils.DecodeGZIP(resp.Body))
	betika.addUsage(len(data))
	// ioutil.WriteFile("out.json", data, 0777)

	parsed := map[string]interface{}{}
	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}

	if _, ok := parsed["data"]; !ok {
		utils.Error(string(data))
		return nil
	}
	if parsed["data"] == nil {
		utils.Debug(string(data))
		return nil
	}
	markets := parsed["data"].([]interface{})

	for _, _market := range markets {
		market := _market.(map[string]interface{})
		odds := market["odds"].([]interface{})
		switch market["sub_type_id"].(string) {
		case "10": //three way

			for _, _odd := range odds {
				odd := _odd.(map[string]interface{})
				switch odd["odd_key"].(string) {
				case "1":
					tw.Home, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				case "X":
					tw.Draw, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				case "2":
					tw.Away, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				}
			}
		case "46":

			for _, _odd := range odds {
				odd := _odd.(map[string]interface{})
				switch odd["odd_key"].(string) {
				case "1X":
					dc.Home, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				case "X2":
					dc.Away, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				case "12":
					dc.HomeOrAway, _ = strconv.ParseFloat(odd["odd_value"].(string), 64)
				}
			}

		}

	}

	game.TWAY = tw
	game.DC = dc

	return &game
}
func (betika *Betika) GetHandballGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := betika.getHGame(apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}
