package scraper

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"sync"

	"net/http"
	"strconv"
	"strings"

	"gitlab.com/arbke/utils"

	"github.com/headzoo/surf"
	"github.com/headzoo/surf/browser"
)

type Betpawa struct {
	name      string
	url       string
	navigator *browser.Browser
	usage     int
	mainpage  int
}

func NewBetpawa() *Betpawa {
	betpawa := &Betpawa{
		name:      "Betpawa",
		url:       "https://www.betpawa.com",
		navigator: surf.NewBrowser(),
	}
	betpawa.navigator.SetUserAgent("Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
	betpawa.navigator.AddRequestHeader("accept-encoding", "gzip, deflate, br")
	return betpawa
}

func (betpawa *Betpawa) Name() string {
	return betpawa.name
}

func (betpawa *Betpawa) MainPageUsage() int {
	return betpawa.mainpage
}

func (betpawa *Betpawa) Usage() int {
	return betpawa.usage
}

func (betpawa *Betpawa) UsageReset() {
	betpawa.usage = 0
	betpawa.mainpage = 0
}
func (betpawa *Betpawa) GetFootball(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	if err := betpawa.navigator.Open("http://www.betpawa.co.ke"); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	cookies := betpawa.navigator.SiteCookies()
	eventIds := []int{}
	respData := map[string]interface{}{}
	events := []interface{}{}

	//generate enough games
	categories := []int{2}
	tmp := []map[string]int{}
	for _, category := range categories {
		tmp = append(tmp, map[string]int{
			"Id":          category,
			"ItemsToShow": 500,
		})

	}
	payload := map[string]interface{}{
		"Max":                  500,
		"Hours":                24 * 5,
		"SportId":              2,
		"IncludeMatchOfTheDay": false,
		"EventCategories":      tmp,
	}

	resp := betpawa.makeRequest("https://www.betpawa.co.ke/ws/public/pricing/getUpcomingEventsWithPrices", "http://www.betpawa.co.ke/", payload, eventIds, cookies...)
	if resp == nil {
		if isStopped() {
			return
		}

		return
	}
	debug, _ := ioutil.ReadAll(resp)
	// utils.Debug(string(debug))
	betpawa.usage += len(debug)
	betpawa.mainpage += len(debug)
	if err := json.Unmarshal(debug, &respData); err != nil {
		utils.Error(err)
		if isStopped() {
			return
		}

		return
	}

	events = append(events, respData["Data"].(map[string]interface{})["Events"].([]interface{})...)
	for _, _event := range events {
		event := _event.(map[string]interface{})
		eventIds = append(eventIds, int(event["Id"].(float64)))
	}

	//

	utils.Info("EVENTS", len(events))
	for _, _event := range events {
		event := _event.(map[string]interface{})
		game := Game{}
		game.Site = "Betpawa"
		game.Link = betpawa.url
		teams := strings.Split(event["Name"].(string), " - ")
		game.Home = strings.Trim(teams[0], " ")
		game.Away = strings.Trim(teams[1], " ")
		game.Time = utils.DecodeTime(betpawa.name, event["StartsRaw"].(string))

		h := strings.Replace(strings.Trim(strings.ToLower(game.Home), " "), " ", "-", -1)
		h = strings.Replace(h, "(", "", -1)
		h = strings.Replace(h, ")", "", -1)
		h = strings.Replace(h, ".", "", -1)
		a := strings.Replace(strings.Trim(strings.ToLower(game.Away), " "), " ", "-", -1)
		a = strings.Replace(a, "(", "", -1)
		a = strings.Replace(a, ")", "", -1)
		h = strings.Replace(h, ".", "", -1)

		h = strings.Replace(strings.Trim(h, " "), " ", "-", -1)
		a = strings.Replace(strings.Trim(a, " "), " ", "-", -1)

		gameLink := fmt.Sprintf("http://www.betpawa.co.ke/event-%d-%s-%s", int(event["Id"].(float64)), h, a)
		// utils.Debug(gameLink)

		compt := event["League"].(string)

		game.League = compt
		allowed := Allowed(compt)
		if len(allowed) > 0 {
			game.Home = fmt.Sprintf("%s %s", game.Home, allowed)
			game.Away = fmt.Sprintf("%s %s", game.Away, allowed)
		}

		game.GameID = strconv.Itoa(int(event["Id"].(float64)))

		g := betpawa.getGame(gameLink, game)
		if isStopped() {
			return
		}
		// utils.Debug(*g)
		if g == nil {
			continue
		}

		games <- *g
	}

	if isStopped() {
		return
	}

}

func (betpawa *Betpawa) getGame(gameLink string, game Game) *Game {
	resp := temp(game.GameID, gameLink)
	if resp == nil {
		return nil
	}
	tw := ThreeWay{Link: gameLink, APILink: game.GameID}
	dc := DoubleChance{Link: gameLink, APILink: game.GameID}
	dnb := DrawNoBet{Link: gameLink, APILink: game.GameID}
	ou15 := OU15{Link: gameLink, APILink: game.GameID}
	ou25 := OU25{Link: gameLink, APILink: game.GameID}
	ou35 := OU35{Link: gameLink, APILink: game.GameID}
	ou45 := OU45{Link: gameLink, APILink: game.GameID}
	gng := GoalNoGoal{Link: gameLink, APILink: game.GameID}

	respData := map[string]interface{}{}
	_data, _ := ioutil.ReadAll(resp)
	betpawa.usage += len(_data)
	// if strings.Contains(game.Home, "Stadl") {
	// ioutil.WriteFile("out.json", _data, 0777)
	// return nil
	// }
	// utils.Debug(">>>>>", _i)
	// utils.Debug(string(_data))
	if err := json.Unmarshal(_data, &respData); err != nil {
		utils.Error(err)
		return nil
	}

	if _, ok := respData["Data"]; !ok {
		utils.Error(respData)
		return nil
	}
	data := respData["Data"].(map[string]interface{})

	for _, _market := range data["Markets"].([]interface{}) {

		market := _market.(map[string]interface{})

		// utils.Debug(market["Name"].(string))
		prices := market["Prices"].([]interface{})

		switch market["Name"].(string) {
		case "1X2 - FT":
			for _, _price := range prices {
				price := _price.(map[string]interface{})

				switch strings.ToUpper(price["Name"].(string)) {
				case "1":
					tw.Home = price["PriceRaw"].(float64)
				case "X":
					tw.Draw = price["PriceRaw"].(float64)
				case "2":
					tw.Away = price["PriceRaw"].(float64)
				}
			}
		case "Double Chance - FT":
			for _, _price := range prices {
				price := _price.(map[string]interface{})

				switch strings.ToUpper(price["Name"].(string)) {
				case "1X":
					dc.Home = price["PriceRaw"].(float64)
				case "X2":
					dc.Away = price["PriceRaw"].(float64)
				case "12":
					dc.HomeOrAway = price["PriceRaw"].(float64)
				}
			}
		case "Draw No Bet - FT":
			for _, _price := range prices {
				price := _price.(map[string]interface{})
				switch strings.ToUpper(price["Name"].(string)) {
				case "1":
					dnb.Home = price["PriceRaw"].(float64)
				case "2":
					dnb.Away = price["PriceRaw"].(float64)

				}
			}
		case "Both Teams To Score - FT":
			for _, _price := range prices {
				price := _price.(map[string]interface{})
				switch strings.ToUpper(price["Name"].(string)) {
				case "YES":
					gng.Goal = price["PriceRaw"].(float64)
				case "NO":
					gng.NoGoal = price["PriceRaw"].(float64)

				}
			}

		case "Total Score Over/Under - FT":
			for _, _price := range prices {
				price := _price.(map[string]interface{})

				switch strings.ToUpper(price["Name"].(string)) {
				case "OVER":
					switch price["Hcp"].(string) {
					case "1.5":
						ou15.Over = price["PriceRaw"].(float64)
					case "2.5":
						ou25.Over = price["PriceRaw"].(float64)
					case "3.5":
						ou35.Over = price["PriceRaw"].(float64)
					case "4.5":
						ou45.Over = price["PriceRaw"].(float64)
					}

				case "UNDER":
					switch price["Hcp"].(string) {
					case "1.5":
						ou15.Under = price["PriceRaw"].(float64)
					case "2.5":
						ou25.Under = price["PriceRaw"].(float64)
					case "3.5":
						ou35.Under = price["PriceRaw"].(float64)
					case "4.5":
						ou45.Under = price["PriceRaw"].(float64)
					}

				}
			}
		}

	}
	// utils.Debug(game)

	game.TWAY = tw
	game.DC = dc
	game.DNB = dnb
	game.GNG = gng
	game.OU15 = ou15
	game.OU25 = ou25
	game.OU35 = ou35
	game.OU45 = ou45

	return &game
}

//apiLink used for gameID
func (betpawa *Betpawa) GetFootballGame(t *http.Transport, gameLink, apiLink string) Game {
	g := betpawa.getGame(gameLink, Game{GameID: apiLink})
	if g == nil {
		return Game{}
	}
	return *g
}

func (betpawa *Betpawa) GetBasketBall(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
}
func (betpawa *Betpawa) GetBasketBallGame(t *http.Transport, gameLink, apiLink string) Game {
	return Game{}
}

func (betpawa *Betpawa) GetTennis(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
}
func (betpawa *Betpawa) GetTennisGame(t *http.Transport, gameLink, apiLink string) Game {
	return Game{}
}

func (betpawa *Betpawa) GetRugby(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
}
func (betpawa *Betpawa) GetRugbyGame(t *http.Transport, gameLink, apiLink string) Game {
	return Game{}
}

func (betpawa *Betpawa) GetIceHockey(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
}
func (betpawa *Betpawa) GetIceHockeyGame(t *http.Transport, gameLink, apiLink string) Game {
	return Game{}
}

func (betpawa *Betpawa) GetHandball(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
}
func (betpawa *Betpawa) GetHandballGame(t *http.Transport, gameLink, apiLink string) Game {
	return Game{}
}

func temp(id, referer string) io.Reader {
	url := "https://www.betpawa.co.ke/ws/public/pricing/getPricesForEvent"

	payload := strings.NewReader("{\"EventId\":" + id + "}")

	req, _ := http.NewRequest("POST", url, payload)

	req.Header.Add("accept", "application/json, text/javascript, */*; q=0.01")
	req.Header.Add("origin", "https://www.betpawa.co.ke")
	req.Header.Add("x-requested-with", "XMLHttpRequest")
	req.Header.Add("user-agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36")
	req.Header.Add("content-type", "application/json; charset=UTF-8")
	req.Header.Add("referer", "https://www.betpawa.co.ke/"+referer)
	// req.Header.Add("accept-encoding", "gzip, deflate, br")
	req.Header.Add("accept-language", "en-US,en;q=0.9")
	req.Header.Add("cookie", "_gat=1; sport-selector=2; _ga=GA1.3.451612228.1513698357; _gid=GA1.3.691290680.1513698357; #other-country-switch=1; sport-selector=2; _ga=GA1.3.451612228.1513698357; _gid=GA1.3.691290680.1513698357")
	req.Header.Add("cache-control", "no-cache")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}
	// defer res.Body.Close()
	// body, _ := ioutil.ReadAll(res.Body)

	return res.Body
}
func (betpawa *Betpawa) makeRequest(url, referer string, data map[string]interface{}, eventIds []int, cookies ...*http.Cookie) io.Reader {
	dataStr, _ := json.Marshal(data)

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(dataStr))
	if err != nil {
		utils.Error(err)
		return nil
	}
	req.Header.Add("accept", "application/json, text/javascript, */*; q=0.01")
	req.Header.Add("origin", "http://www.betpawa.co.ke")
	req.Header.Add("x-requested-with", "XMLHttpRequest")
	req.Header.Add("user-agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
	req.Header.Add("content-type", "application/json; charset=UTF-8")
	req.Header.Add("referer", referer)
	// req.Header.Add("accept-encoding", "gzip, deflate,br")
	req.Header.Add("accept-language", "en-US,en;q=0.8")
	req.Header.Add("cookie", "sport-selector=2")
	req.Header.Add("cache-control", "no-cache")
	for _, cookie := range cookies {
		utils.Info(cookie.String())
		req.AddCookie(cookie)
	}

	// utils.Debug(req.Header.Get("Cookie"))
	client := http.Client{}
	resp, err := client.Do(req)
	if resp == nil {
		return nil
	}
	// return utils.DecodeGZIP(resp.Body)
	return resp.Body

}
