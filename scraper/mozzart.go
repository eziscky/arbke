package scraper

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitlab.com/arbke/utils"
)

type Mozzart struct {
	name     string
	link     string
	usage    int
	mainpage int

	lock *sync.Mutex
}

type mzapimatch struct {
	Match struct {
		Markets map[string]struct {
			Outcomes map[string]struct {
				ID   int    `json:"id"`
				Name string `json:"name"`
				Odds string `json:"value"`
			} `json:"subgames"`
		} `json:"odds"`
	} `json:"match"`
}
type mzmatch struct {
	GameID  int    `json:"matchNumber"`
	League  string `json:"tournamentName"`
	Home    string `json:"home"`
	Away    string `json:"visitor"`
	Start   int64  `json:"timestamp"`
	DateStr string `json:"date"`
}
type mzapidetails struct {
	Events []struct {
		ID           int `json:"sportId"`
		Count        int `json:"count"`
		Competitions []struct {
			Matches []mzmatch `json:"matches"`
		} `json:"competitions"`
	} `json:"regular_events_by_sport"`
}

func NewMozzart() *Mozzart {

	return &Mozzart{
		name: "Mozzart",
		link: "https://www.mozzartbet.co.ke",
		lock: &sync.Mutex{},
	}
}

func (mz *Mozzart) Usage() int {
	mz.lock.Lock()
	defer mz.lock.Unlock()
	return mz.usage
}
func (mz *Mozzart) Name() string {
	return mz.name
}

func (mz *Mozzart) addUsage(u int) {
	mz.lock.Lock()
	defer mz.lock.Unlock()
	mz.usage += u
}

func (mz *Mozzart) doRequest(endp, body, referer string) (*http.Response, error) {
	// utils.Debug(fmt.Sprintf("%s%s", mz.link, endp))
	req, _ := http.NewRequest(http.MethodPost, fmt.Sprintf("%s%s", mz.link, endp), strings.NewReader(body))
	req.Header.Add("Referer", referer)
	req.Header.Add("Origin", "https://www.mozzartbet.co.ke")
	req.Header.Add("X-Requested-With", "XMLHttpRequest")
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
	req.Header.Add("user-agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36")

	transport := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := http.Client{
		Transport: transport,
	}
	return client.Do(req)
}

func (mz *Mozzart) getMatches(id int) []mzmatch {
	req, err := http.NewRequest(http.MethodGet, "https://www.mozzartbet.co.ke/getGroupedMatchesBySportAndCompetition", nil)
	req.Header.Add("X-Requested-With", "XMLHttpRequest")
	transport := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := http.Client{
		Transport: transport,
	}
	resp, err := client.Do(req)

	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}

	ioutil.WriteFile("out.json", data, 0777)
	parsed := mzapidetails{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)

		return nil
	}
	matches := []mzmatch{}
	for _, sport := range parsed.Events {
		if sport.ID == id {
			utils.Debug(sport.Count)
			for _, comp := range sport.Competitions {
				matches = append(matches, comp.Matches...)
			}
		}
	}
	return matches

}
func (mz *Mozzart) GetFootball(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	matches := mz.getMatches(1)

	for _, match := range matches {

		game := Game{}
		// game.GameID =
		game.Home = match.Home
		game.Away = match.Away
		// game.League = match.League
		t := time.Unix(match.Start/1000, 0)
		game.Time = &t
		game.GameID = strconv.Itoa(match.GameID)
		home := strings.Replace(match.Home, " ", "+", -1)
		away := strings.Replace(match.Away, " ", "+", -1)
		gameLink := fmt.Sprintf("https://www.mozzartbet.co.ke/en/betting/odds/%s-%s/%s/%d", home, away, match.DateStr, match.GameID)

		vals := url.Values{}
		vals.Add("date", match.DateStr)
		vals.Add("match_number", game.GameID)

		g := mz.getGame(gameLink, vals.Encode(), game)
		if g == nil {
			continue
		}
		if isStopped() {
			return
		}
		games <- *g
	}

	if isStopped() {
		return
	}

}

func (mz *Mozzart) GetFootballGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := mz.getGame(gameLink, apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}
func (mz *Mozzart) getGame(gameLink, apiLink string, game Game) *Game {
	tw := ThreeWay{Link: gameLink, APILink: apiLink}
	dnb := DrawNoBet{Link: gameLink, APILink: apiLink}
	dc := DoubleChance{Link: gameLink, APILink: apiLink}

	ou15 := OU15{Link: gameLink, APILink: apiLink}
	ou25 := OU25{Link: gameLink, APILink: apiLink}
	ou35 := OU35{Link: gameLink, APILink: apiLink}
	ou45 := OU45{Link: gameLink, APILink: apiLink}

	gng := GoalNoGoal{Link: gameLink}

	resp, err := mz.doRequest("/getBettingMatch", apiLink, gameLink)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	// ioutil.WriteFile("out2.json", data, 0777)
	mz.addUsage(len(data))
	parsed := mzapimatch{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}

	for id, market := range parsed.Match.Markets {

		switch id {
		case "1":
			for _, outcome := range market.Outcomes {
				odds, _ := strconv.ParseFloat(strings.Replace(outcome.Odds, ",", ".", -1), 64)
				switch outcome.Name {
				case "1":
					tw.Home = odds
				case "X":
					tw.Draw = odds
				case "2":
					tw.Away = odds
				}
			}

		case "130":

			for _, outcome := range market.Outcomes {
				odds, _ := strconv.ParseFloat(strings.Replace(outcome.Odds, ",", ".", -1), 64)
				switch outcome.Name {
				case "gg":
					gng.Goal = odds
				case "ng":
					gng.NoGoal = odds
				}
			}
		case "26":
			for _, outcome := range market.Outcomes {
				odds, _ := strconv.ParseFloat(strings.Replace(outcome.Odds, ",", ".", -1), 64)
				switch outcome.Name {
				case "1":
					dnb.Home = odds
				case "2":
					dnb.Away = odds
				}
			}
		case "2":
			for _, outcome := range market.Outcomes {
				odds, _ := strconv.ParseFloat(strings.Replace(outcome.Odds, ",", ".", -1), 64)
				switch outcome.Name {
				case "1X":
					dc.Home = odds
				case "12":
					dc.HomeOrAway = odds
				case "X2":
					dc.Away = odds
				}
			}
		case "3":
			for _, outcome := range market.Outcomes {
				odds, _ := strconv.ParseFloat(strings.Replace(outcome.Odds, ",", ".", -1), 64)
				switch outcome.Name {
				case "2+":
					ou15.Over = odds
				case "3+":
					ou25.Over = odds
				case "4+":
					ou35.Over = odds
				case "5+":
					ou45.Over = odds

				}
			}

		}

	}

	game.TWAY = tw
	game.GNG = gng
	game.DNB = dnb
	game.OU15 = ou15
	game.OU25 = ou25
	game.OU35 = ou35
	game.OU45 = ou45
	game.DC = dc
	game.Site = mz.name

	return &game
}

func (mz *Mozzart) GetBasketBall(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	matches := mz.getMatches(2)

	for _, match := range matches {

		game := Game{}
		// game.GameID =
		game.Home = match.Home
		game.Away = match.Away
		// game.League = match.League
		t := time.Unix(match.Start/1000, 0)
		game.Time = &t
		game.GameID = strconv.Itoa(match.GameID)
		home := strings.Replace(match.Home, " ", "+", -1)
		away := strings.Replace(match.Away, " ", "+", -1)
		gameLink := fmt.Sprintf("https://www.mozzartbet.co.ke/en/betting/odds/%s-%s/%s/%d", home, away, match.DateStr, match.GameID)

		vals := url.Values{}
		vals.Add("date", match.DateStr)
		vals.Add("match_number", game.GameID)

		g := mz.getBGame(gameLink, vals.Encode(), game)
		if g == nil {
			continue
		}
		if isStopped() {
			return
		}
		games <- *g
	}

	if isStopped() {
		return
	}

}

func (mz *Mozzart) getBGame(gameLink, apiLink string, game Game) *Game {
	tw := ThreeWay{Link: gameLink, APILink: apiLink}
	twow := TwoWay{Link: gameLink, APILink: apiLink}
	dc := DoubleChance{Link: gameLink, APILink: apiLink}

	resp, err := mz.doRequest("/getBettingMatch", apiLink, gameLink)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	// ioutil.WriteFile("out2.json", data, 0777)
	mz.addUsage(len(data))
	parsed := mzapimatch{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}

	for id, market := range parsed.Match.Markets {

		switch id {
		case "17":
			for _, outcome := range market.Outcomes {
				odds, _ := strconv.ParseFloat(strings.Replace(outcome.Odds, ",", ".", -1), 64)
				switch outcome.Name {
				case "1":
					tw.Home = odds
				case "X":
					tw.Draw = odds
				case "2":
					tw.Away = odds
				}
			}

		case "196":
			for _, outcome := range market.Outcomes {
				odds, _ := strconv.ParseFloat(strings.Replace(outcome.Odds, ",", ".", -1), 64)
				switch outcome.Name {
				case "1":
					twow.Home = odds
				case "2":
					twow.Away = odds
				}
			}
		case "2":
			for _, outcome := range market.Outcomes {
				odds, _ := strconv.ParseFloat(strings.Replace(outcome.Odds, ",", ".", -1), 64)
				switch outcome.Name {
				case "1X":
					dc.Home = odds
				case "12":
					dc.HomeOrAway = odds
				case "X2":
					dc.Away = odds
				}
			}

		}

	}

	game.TWAY = tw
	game.TWOWAY = twow
	game.DC = dc
	game.Site = mz.name
	return &game
}

func (mz *Mozzart) GetBasketBallGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := mz.getBGame(gameLink, apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (mz *Mozzart) GetRugby(t *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}

func (mz *Mozzart) GetRugbyGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}
func (mz *Mozzart) getRGame(gameLink, apiLink string, game Game) *Game {
	return nil
}
func (mz *Mozzart) GetTennis(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	matches := mz.getMatches(5)

	for _, match := range matches {

		game := Game{}
		// game.GameID =
		game.Home = match.Home
		game.Away = match.Away
		// game.League = match.League
		t := time.Unix(match.Start/1000, 0)
		game.Time = &t
		game.GameID = strconv.Itoa(match.GameID)
		home := strings.Replace(match.Home, " ", "+", -1)
		away := strings.Replace(match.Away, " ", "+", -1)
		gameLink := fmt.Sprintf("https://www.mozzartbet.co.ke/en/betting/odds/%s-%s/%s/%d", home, away, match.DateStr, match.GameID)

		vals := url.Values{}
		vals.Add("date", match.DateStr)
		vals.Add("match_number", game.GameID)

		g := mz.getTGame(gameLink, vals.Encode(), game)
		if g == nil {
			continue
		}
		if isStopped() {
			return
		}
		games <- *g
	}

	if isStopped() {
		return
	}

}

func (mz *Mozzart) getTGame(gameLink, apiLink string, game Game) *Game {
	twow := TwoWay{Link: gameLink, APILink: apiLink}

	resp, err := mz.doRequest("/getBettingMatch", apiLink, gameLink)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	// ioutil.WriteFile("out2.json", data, 0777)
	mz.addUsage(len(data))
	parsed := mzapimatch{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}

	for id, market := range parsed.Match.Markets {

		switch id {
		case "17":
			for _, outcome := range market.Outcomes {
				odds, _ := strconv.ParseFloat(strings.Replace(outcome.Odds, ",", ".", -1), 64)
				switch outcome.Name {
				case "1":
					twow.Home = odds
				case "2":
					twow.Away = odds
				}
			}

		}

	}

	game.TWOWAY = twow
	game.Site = mz.name
	return &game
}
func (mz *Mozzart) GetTennisGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := mz.getTGame(gameLink, apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (mz *Mozzart) GetIceHockey(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	matches := mz.getMatches(4)

	for _, match := range matches {

		game := Game{}
		// game.GameID =
		game.Home = match.Home
		game.Away = match.Away
		// game.League = match.League
		t := time.Unix(match.Start/1000, 0)
		game.Time = &t
		game.GameID = strconv.Itoa(match.GameID)
		home := strings.Replace(match.Home, " ", "+", -1)
		away := strings.Replace(match.Away, " ", "+", -1)
		gameLink := fmt.Sprintf("https://www.mozzartbet.co.ke/en/betting/odds/%s-%s/%s/%d", home, away, match.DateStr, match.GameID)

		vals := url.Values{}
		vals.Add("date", match.DateStr)
		vals.Add("match_number", game.GameID)

		g := mz.getIHGame(gameLink, vals.Encode(), game)
		if g == nil {
			continue
		}
		if isStopped() {
			return
		}
		games <- *g
	}

	if isStopped() {
		return
	}

}

func (mz *Mozzart) getIHGame(gameLink, apiLink string, game Game) *Game {
	tw := ThreeWay{Link: gameLink, APILink: apiLink}
	dnb := DrawNoBet{Link: gameLink, APILink: apiLink}
	dc := DoubleChance{Link: gameLink, APILink: apiLink}

	ou15 := OU15{Link: gameLink, APILink: apiLink}
	ou25 := OU25{Link: gameLink, APILink: apiLink}
	ou35 := OU35{Link: gameLink, APILink: apiLink}
	ou45 := OU45{Link: gameLink, APILink: apiLink}

	resp, err := mz.doRequest("/getBettingMatch", apiLink, gameLink)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	// ioutil.WriteFile("out2.json", data, 0777)
	mz.addUsage(len(data))
	parsed := mzapimatch{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}

	for id, market := range parsed.Match.Markets {

		switch id {
		case "17":
			for _, outcome := range market.Outcomes {
				odds, _ := strconv.ParseFloat(strings.Replace(outcome.Odds, ",", ".", -1), 64)
				switch outcome.Name {
				case "1":
					tw.Home = odds
				case "X":
					tw.Draw = odds
				case "2":
					tw.Away = odds
				}
			}

		case "26":
			for _, outcome := range market.Outcomes {
				odds, _ := strconv.ParseFloat(strings.Replace(outcome.Odds, ",", ".", -1), 64)
				switch outcome.Name {
				case "1":
					dnb.Home = odds
				case "2":
					dnb.Away = odds
				}
			}
		case "2":
			for _, outcome := range market.Outcomes {
				odds, _ := strconv.ParseFloat(strings.Replace(outcome.Odds, ",", ".", -1), 64)
				switch outcome.Name {
				case "1X":
					dc.Home = odds
				case "12":
					dc.HomeOrAway = odds
				case "X2":
					dc.Away = odds
				}
			}
		case "3":
			for _, outcome := range market.Outcomes {
				odds, _ := strconv.ParseFloat(strings.Replace(outcome.Odds, ",", ".", -1), 64)
				switch outcome.Name {
				case "2+":
					ou15.Over = odds
				case "3+":
					ou25.Over = odds
				case "4+":
					ou35.Over = odds
				case "5+":
					ou45.Over = odds

				}
			}

		}

	}

	game.TWAY = tw
	game.DNB = dnb
	game.OU15 = ou15
	game.OU25 = ou25
	game.OU35 = ou35
	game.OU45 = ou45
	game.DC = dc
	game.Site = mz.name

	return &game
}
func (mz *Mozzart) GetIceHockeyGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := mz.getIHGame(gameLink, apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (mz *Mozzart) GetHandball(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	matches := mz.getMatches(7)

	for _, match := range matches {

		game := Game{}
		// game.GameID =
		game.Home = match.Home
		game.Away = match.Away
		// game.League = match.League
		t := time.Unix(match.Start/1000, 0)
		game.Time = &t
		game.GameID = strconv.Itoa(match.GameID)
		home := strings.Replace(match.Home, " ", "+", -1)
		away := strings.Replace(match.Away, " ", "+", -1)
		gameLink := fmt.Sprintf("https://www.mozzartbet.co.ke/en/betting/odds/%s-%s/%s/%d", home, away, match.DateStr, match.GameID)

		vals := url.Values{}
		vals.Add("date", match.DateStr)
		vals.Add("match_number", game.GameID)

		g := mz.getHGame(gameLink, vals.Encode(), game)
		if g == nil {
			continue
		}
		if isStopped() {
			return
		}
		games <- *g
	}

	if isStopped() {
		return
	}

}

func (mz *Mozzart) getHGame(gameLink, apiLink string, game Game) *Game {
	tw := ThreeWay{Link: gameLink, APILink: apiLink}

	resp, err := mz.doRequest("/getBettingMatch", apiLink, gameLink)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	// ioutil.WriteFile("out2.json", data, 0777)
	mz.addUsage(len(data))
	parsed := mzapimatch{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}

	for id, market := range parsed.Match.Markets {

		switch id {
		case "17":
			for _, outcome := range market.Outcomes {
				odds, err := strconv.ParseFloat(strings.Replace(outcome.Odds, ",", ".", -1), 64)
				if err != nil {
					utils.Error(err)
				}
				switch outcome.Name {
				case "1":
					tw.Home = odds
				case "X":
					tw.Draw = odds
				case "2":
					tw.Away = odds
				}
			}

		}

	}

	game.TWAY = tw
	game.Site = mz.name
	return &game
}
func (mz *Mozzart) GetHandballGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := mz.getHGame(gameLink, apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (mz *Mozzart) UsageReset() {
	mz.usage = 0
	mz.mainpage = 0
}
func (mz *Mozzart) MainPageUsage() int {
	return mz.mainpage
}
