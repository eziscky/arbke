package scraper

import (
	"net/http"
	"time"

	"github.com/montanaflynn/stats"
)

type Scraper interface {
	Name() string
	GetFootball(*http.Transport, chan Game, chan struct{})
	GetFootballGame(t *http.Transport, gameLink string, apiLink string) Game
	GetTennis(*http.Transport, chan Game, chan struct{})
	GetTennisGame(t *http.Transport, gameLink string, apiLink string) Game
	GetRugby(*http.Transport, chan Game, chan struct{})
	GetRugbyGame(t *http.Transport, gameLink string, apiLink string) Game
	GetBasketBall(*http.Transport, chan Game, chan struct{})
	GetBasketBallGame(t *http.Transport, gameLink string, apiLink string) Game
	GetHandball(*http.Transport, chan Game, chan struct{})
	GetHandballGame(t *http.Transport, gameLink string, apiLink string) Game
	GetIceHockey(*http.Transport, chan Game, chan struct{})
	GetIceHockeyGame(t *http.Transport, gameLink string, apiLink string) Game
	Usage() int
	UsageReset()
	MainPageUsage() int
}

type OU interface {
	Over_() float64
	Under_() float64
}

type Game struct {
	GameID  string       `json:""`
	Site    string       `json:"Site"`
	Sport   string       `json:"sport"`
	Link    string       `json:"Link"`
	Time    *time.Time   `json:"-"`
	TimeStr string       `json:"Time"`
	Home    string       `json:"Home"`
	Away    string       `json:"Away"`
	League  string       `json:"League"`
	TWAY    ThreeWay     `json:"ThreeWay"`
	TWOWAY  TwoWay       `json:"TwoWay"`
	DC      DoubleChance `json:"DC"`
	DNB     DrawNoBet    `json:"DNB"`
	GNG     GoalNoGoal   `json:"GNG"`
	OU15    OU15         `json:"1.5"`
	OU25    OU25         `json:"2.5"`
	OU35    OU35         `json:"3.5"`
	OU45    OU45         `json:"4.5"`
}

// type Tennis struct {
// 	GameID  string     `json:""`
// 	Site    string     `json:"Site"`
// 	Link    string     `json:"Link"`
// 	Time    *time.Time `json:"-"`
// 	TimeStr string     `json:"Time"`
// 	Home    string     `json:"Home"`
// 	Away    string     `json:"Away"`
// 	League  string     `json:"League"`
// 	TWAY    TwoWay     `json:"TwoWay"`
// }

// type BasketBall struct {
// 	GameID  string     `json:""`
// 	Site    string     `json:"Site"`
// 	Link    string     `json:"Link"`
// 	Time    *time.Time `json:"-"`
// 	TimeStr string     `json:"Time"`
// 	Home    string     `json:"Home"`
// 	Away    string     `json:"Away"`
// 	League  string     `json:"League"`
// 	TWAY    TwoWay     `json:"TwoWay"`
// }

// type Rugby struct {
// 	GameID  string     `json:""`
// 	Site    string     `json:"Site"`
// 	Link    string     `json:"Link"`
// 	Time    *time.Time `json:"-"`
// 	TimeStr string     `json:"Time"`
// 	Home    string     `json:"Home"`
// 	Away    string     `json:"Away"`
// 	League  string     `json:"League"`
// 	TWAY    ThreeWay   `json:"ThreeWay"`
// }

type Aggregate struct {
	Hash     string
	HashRecs []Game
	Home     string
	Away     string
	League   string
	Sport    string
	Time     *time.Time
	TWAY     map[string]ThreeWay
	TWOWAY   map[string]TwoWay
	DC       map[string]DoubleChance
	DNB      map[string]DrawNoBet
	GNG      map[string]GoalNoGoal
	OU15     map[string]OU15
	OU25     map[string]OU25
	OU35     map[string]OU35
	OU45     map[string]OU45
}

//HighestTWAY returns a HTWAY object with the largest of Each Outcome with the respective site
func (agg Aggregate) HighestTWAY(exclude ...string) []HThreeWay {
	ordered := []HThreeWay{}
	init := map[string]ThreeWay{}

	for k, v := range agg.TWAY {
		init[k] = v
	}
	for _, ex := range exclude {
		delete(init, ex)
	}

	for {
		if len(init) == 0 {
			break
		}
		lHome, lkHome, homeLink, homeAPI := 0.0, "", "", ""
		lDraw, lkDraw, drawLink, drawAPI := 0.0, "", "", ""
		lAway, lkAway, awayLink, awayAPI := 0.0, "", "", ""

		ht, dt, at := []float64{}, []float64{}, []float64{}
		for k, v := range init {

			ht = append(ht, v.Home)
			at = append(at, v.Away)
			dt = append(at, v.Draw)
			if v.Home >= lHome {
				lHome = v.Home
				lkHome = k
				homeLink = v.Link
				homeAPI = v.APILink
			}
			if v.Draw >= lDraw {
				lDraw = v.Draw
				lkDraw = k
				drawLink = v.Link
				drawAPI = v.APILink
			}
			if v.Away >= lAway {
				lAway = v.Away
				lkAway = k
				awayLink = v.Link
				awayAPI = v.APILink
			}

		}

		// for k, v := range init {
		// 	if v.Home == lHome {
		// 		if !strings.Contains(lkHome, k) {
		// 			lkHome += fmt.Sprintf("/%s", k)
		// 		}
		// 	}

		// 	if v.Draw == lDraw {
		// 		if !strings.Contains(lkDraw, k) {
		// 			lkDraw += fmt.Sprintf("/%s", k)
		// 		}
		// 	}

		// 	if v.Away == lAway {
		// 		if !strings.Contains(lkAway, k) {
		// 			lkAway += fmt.Sprintf("/%s", k)
		// 		}
		// 	}

		// }
		hm, _ := stats.HarmonicMean(ht)
		am, _ := stats.HarmonicMean(at)
		dm, _ := stats.HarmonicMean(dt)

		ordered = append(ordered, HThreeWay{
			HomeSite:     lkHome,
			DrawSite:     lkDraw,
			AwaySite:     lkAway,
			HomeSiteLink: homeLink,
			DrawSiteLink: drawLink,
			AwaySiteLink: awayLink,

			HomeSiteAPILink: homeAPI,
			DrawSiteAPILink: drawAPI,
			AwaySiteAPILink: awayAPI,

			Home:       lHome,
			Draw:       lDraw,
			Away:       lAway,
			HDeviation: lHome - hm,
			DDeviation: lDraw - dm,
			ADeviation: lAway - am,
		})
		// utils.Debug(init)
		// utils.Debug(lkHome, lkDraw, lkAway)
		delete(init, lkHome)
		delete(init, lkDraw)
		delete(init, lkAway)
		// utils.Debug(init)

	}
	return ordered
}

//HighestTWOWAY returns a HTWAY object with the largest of Each Outcome with the respective site
func (agg Aggregate) HighestTWOWAY(exclude ...string) []HTwoWay {
	ordered := []HTwoWay{}
	init := map[string]TwoWay{}

	for k, v := range agg.TWOWAY {
		init[k] = v
	}

	for _, ex := range exclude {
		delete(init, ex)
	}

	for {
		if len(init) == 0 {
			break
		}

		lHome, lkHome, homeLink, homeAPI := 0.0, "", "", ""
		lAway, lkAway, awayLink, awayAPI := 0.0, "", "", ""

		ht, at := []float64{}, []float64{}
		for k, v := range init {

			ht = append(ht, v.Home)
			at = append(at, v.Away)
			if v.Home >= lHome {
				lHome = v.Home
				lkHome = k
				homeLink = v.Link
				homeAPI = v.APILink
			}
			if v.Away >= lAway {
				lAway = v.Away
				lkAway = k
				awayLink = v.Link
				awayAPI = v.APILink
			}

		}
		// utils.Debug(lkHome, lHome, lkAway, lAway)
		// for k, v := range init {
		// 	if v.Home == lHome {
		// 		if !strings.Contains(lkHome, k) {
		// 			lkHome += fmt.Sprintf("/%s", k)
		// 		}
		// 	}

		// 	if v.Away == lAway {
		// 		if !strings.Contains(lkAway, k) {
		// 			lkAway += fmt.Sprintf("/%s", k)
		// 		}
		// 	}

		// }
		hm, _ := stats.HarmonicMean(ht)
		am, _ := stats.HarmonicMean(at)

		ordered = append(ordered, HTwoWay{
			HomeSite: lkHome,

			AwaySite:     lkAway,
			HomeSiteLink: homeLink,

			AwaySiteLink: awayLink,

			HomeSiteAPILink: homeAPI,

			AwaySiteAPILink: awayAPI,

			Home: lHome,

			Away:       lAway,
			HDeviation: lHome - hm,

			ADeviation: lAway - am,
		})
		delete(init, lkHome)
		delete(init, lkAway)
	}

	return ordered
}

func (agg Aggregate) HighestDC(exclude ...string) []HDoubleChance {
	ordered := []HDoubleChance{}
	init := map[string]DoubleChance{}

	for k, v := range agg.DC {
		init[k] = v
	}
	for _, ex := range exclude {
		delete(init, ex)
	}

	for {
		if len(init) == 0 {
			break
		}

		lHome, lkHome, homeLink, homeAPI := 0.0, "", "", ""
		// lHA, lkHA .0, ""
		lAway, lkAway, awayLink, awayAPI := 0.0, "", "", ""

		ht, at := []float64{}, []float64{}
		for k, v := range init {

			ht = append(ht, v.Home)
			at = append(at, v.Away)
			if v.Home >= lHome {
				lHome = v.Home
				lkHome = k
				homeLink = v.Link
				homeAPI = v.APILink
			}
			// if v.HomeOrAway > lHA {
			// 	lHA = v.HomeOrAway
			// 	lkHA = k
			// }
			if v.Away >= lAway {
				lAway = v.Away
				lkAway = k
				awayLink = v.Link
				awayAPI = v.APILink
			}

		}

		// for k, v := range init {
		// 	if v.Home == lHome {
		// 		if !strings.Contains(lkHome, k) {
		// 			lkHome += fmt.Sprintf("/%s", k)
		// 		}
		// 	}
		// 	if v.Away == lAway {
		// 		if !strings.Contains(lkAway, k) {
		// 			lkAway += fmt.Sprintf("/%s", k)
		// 		}
		// 	}
		// }
		hm, _ := stats.HarmonicMean(ht)
		am, _ := stats.HarmonicMean(at)
		ordered = append(ordered, HDoubleChance{
			HomeSite: lkHome,
			// HASite:     lkHA,
			AwaySite: lkAway,
			Home:     lHome,
			// HomeOrAway: lHA,
			HomeSiteLink: homeLink,
			AwaySiteLink: awayLink,

			HomeSiteAPILink: homeAPI,
			AwaySiteAPILink: awayAPI,

			Away:       lAway,
			HDeviation: lHome - hm,
			ADeviation: lAway - am,
		})

		delete(init, lkHome)
		delete(init, lkAway)
	}

	return ordered
}

func (agg Aggregate) HighestDNB(exclude ...string) []HDrawNoBet {
	ordered := []HDrawNoBet{}
	init := map[string]DrawNoBet{}

	for k, v := range agg.DNB {
		init[k] = v
	}
	for _, ex := range exclude {
		delete(init, ex)
	}
	for {
		if len(init) == 0 {
			break
		}

		lHome, lkHome, homeLink, homeAPI := 0.0, "", "", ""
		lAway, lkAway, awayLink, awayAPI := 0.0, "", "", ""
		ht, at := []float64{}, []float64{}
		for k, v := range init {

			ht = append(ht, v.Home)
			at = append(at, v.Away)
			if v.Home >= lHome {
				lHome = v.Home
				lkHome = k
				homeLink = v.Link
				homeAPI = v.APILink
			}
			if v.Away >= lAway {
				lAway = v.Away
				lkAway = k
				awayLink = v.Link
				awayAPI = v.APILink
			}

		}

		// for k, v := range init {
		// 	if v.Home == lHome {
		// 		if !strings.Contains(lkHome, k) {
		// 			lkHome += fmt.Sprintf("/%s", k)
		// 		}
		// 	}
		// 	if v.Away == lAway {
		// 		if !strings.Contains(lkAway, k) {
		// 			lkAway += fmt.Sprintf("/%s", k)
		// 		}
		// 	}
		// }

		hm, _ := stats.HarmonicMean(ht)
		am, _ := stats.HarmonicMean(at)

		// utils.Debug(agg.Home, agg.DNB, lHome, lAway)
		ordered = append(ordered, HDrawNoBet{
			HomeSite:        lkHome,
			AwaySite:        lkAway,
			HomeSiteLink:    homeLink,
			AwaySiteLink:    awayLink,
			HomeSiteAPILink: homeAPI,
			AwaySiteAPILink: awayAPI,
			Home:            lHome,
			Away:            lAway,
			HDeviation:      lHome - hm,
			ADeviation:      lAway - am,
		})

		delete(init, lkHome)
		delete(init, lkAway)
	}
	return ordered
}

func (agg Aggregate) HighestGNG(exclude ...string) []HGoalNoGoal {
	ordered := []HGoalNoGoal{}
	init := map[string]GoalNoGoal{}

	for k, v := range agg.GNG {
		init[k] = v
	}
	for _, ex := range exclude {
		delete(init, ex)
	}
	for {
		if len(init) == 0 {
			break
		}

		lGoal, lkGoal, goalSite, goalAPI := 0.0, "", "", ""
		lNGoal, lkNGoal, nGoalSite, nGoalAPI := 0.0, "", "", ""

		gt, ngt := []float64{}, []float64{}

		for k, v := range init {

			gt = append(gt, v.Goal)
			ngt = append(ngt, v.NoGoal)
			if v.Goal >= lGoal {
				lGoal = v.Goal
				lkGoal = k
				goalSite = v.Link
				goalAPI = v.APILink
			}
			if v.NoGoal >= lNGoal {
				lNGoal = v.NoGoal
				lkNGoal = k
				nGoalSite = v.Link
				nGoalAPI = v.APILink
			}

		}
		// for k, v := range init {
		// 	if v.Goal == lGoal {
		// 		if !strings.Contains(lkGoal, k) {
		// 			lkGoal += fmt.Sprintf("/%s", k)
		// 		}
		// 	}
		// 	if v.NoGoal == lNGoal {
		// 		if !strings.Contains(lkNGoal, k) {
		// 			lkNGoal += fmt.Sprintf("/%s", k)
		// 		}
		// 	}
		// }
		gm, _ := stats.HarmonicMean(gt)
		ngm, _ := stats.HarmonicMean(ngt)

		ordered = append(ordered, HGoalNoGoal{
			GoalSite:          lkGoal,
			NoGoalSite:        lkNGoal,
			GoalSiteLink:      goalSite,
			NoGoalSiteLink:    nGoalSite,
			GoalSiteAPILink:   goalAPI,
			NoGoalSiteAPILink: nGoalAPI,
			Goal:              lGoal,
			NoGoal:            lNGoal,
			GDeviation:        lGoal - gm,
			NGDeviation:       lNGoal - ngm,
		})
		delete(init, lkGoal)
		delete(init, lkNGoal)
	}
	return ordered
}

func (agg Aggregate) HighestOU15(exclude ...string) []HOU15 {
	ordered := []HOU15{}
	init := map[string]OU15{}

	for k, v := range agg.OU15 {
		init[k] = v
	}
	for _, ex := range exclude {
		delete(init, ex)
	}
	for {
		if len(init) == 0 {
			break
		}

		lOver, lkOver, overSite, overAPI := 0.0, "", "", ""
		lUnder, lkUnder, underSite, underAPI := 0.0, "", "", ""

		ot, ut := []float64{}, []float64{}

		for k, v := range init {

			ot = append(ot, v.Over)
			ut = append(ut, v.Under)
			if v.Over >= lOver {
				lOver = v.Over
				lkOver = k
				overSite = v.Link
				overAPI = v.APILink
			}
			if v.Under >= lUnder {
				lUnder = v.Under
				lkUnder = k
				underSite = v.Link
				underAPI = v.APILink
			}

		}
		// for k, v := range init {
		// 	if v.Over == lOver {
		// 		if !strings.Contains(lkOver, k) {
		// 			lkOver += fmt.Sprintf("/%s", k)
		// 		}
		// 	}
		// 	if v.Under == lUnder {
		// 		if !strings.Contains(lkUnder, k) {
		// 			lkUnder += fmt.Sprintf("/%s", k)
		// 		}
		// 	}
		// }
		om, _ := stats.HarmonicMean(ot)
		um, _ := stats.HarmonicMean(ut)

		ordered = append(ordered, HOU15{
			OverSite:         lkOver,
			UnderSite:        lkUnder,
			OverSiteLink:     overSite,
			UnderSiteLink:    underSite,
			OverSiteAPILink:  overAPI,
			UnderSiteAPILink: underAPI,
			Over:             lOver,
			Under:            lUnder,
			ODeviation:       lOver - om,
			UDeviation:       lUnder - um,
		})
		delete(init, lkOver)
		delete(init, lkUnder)
	}

	return ordered
}

func (agg Aggregate) HighestOU25(exclude ...string) []HOU25 {
	ordered := []HOU25{}
	init := map[string]OU25{}

	for k, v := range agg.OU25 {
		init[k] = v
	}
	for _, ex := range exclude {
		delete(init, ex)
	}
	for {
		if len(init) == 0 {
			break
		}

		lOver, lkOver, overSite, overAPI := 0.0, "", "", ""
		lUnder, lkUnder, underSite, underAPI := 0.0, "", "", ""

		ot, ut := []float64{}, []float64{}

		for k, v := range init {

			ot = append(ot, v.Over)
			ut = append(ut, v.Under)
			if v.Over >= lOver {
				lOver = v.Over
				lkOver = k
				overSite = v.Link
				overAPI = v.APILink
			}
			if v.Under >= lUnder {
				lUnder = v.Under
				lkUnder = k
				underSite = v.Link
				underAPI = v.APILink
			}

		}
		// for k, v := range init {
		// 	if v.Over == lOver {
		// 		if !strings.Contains(lkOver, k) {
		// 			lkOver += fmt.Sprintf("/%s", k)
		// 		}
		// 	}
		// 	if v.Under == lUnder {
		// 		if !strings.Contains(lkUnder, k) {
		// 			lkUnder += fmt.Sprintf("/%s", k)
		// 		}
		// 	}
		// }
		om, _ := stats.HarmonicMean(ot)
		um, _ := stats.HarmonicMean(ut)

		ordered = append(ordered, HOU25{
			OverSite:         lkOver,
			UnderSite:        lkUnder,
			OverSiteLink:     overSite,
			UnderSiteLink:    underSite,
			OverSiteAPILink:  overAPI,
			UnderSiteAPILink: underAPI,
			Over:             lOver,
			Under:            lUnder,
			ODeviation:       lOver - om,
			UDeviation:       lUnder - um,
		})
		delete(init, lkOver)
		delete(init, lkUnder)
	}

	return ordered
}

func (agg Aggregate) HighestOU35(exclude ...string) []HOU35 {
	ordered := []HOU35{}
	init := map[string]OU35{}

	for k, v := range agg.OU35 {
		init[k] = v
	}
	for _, ex := range exclude {
		delete(init, ex)
	}
	for {
		if len(init) == 0 {
			break
		}

		lOver, lkOver, overSite, overAPI := 0.0, "", "", ""
		lUnder, lkUnder, underSite, underAPI := 0.0, "", "", ""

		ot, ut := []float64{}, []float64{}

		for k, v := range init {

			ot = append(ot, v.Over)
			ut = append(ut, v.Under)
			if v.Over >= lOver {
				lOver = v.Over
				lkOver = k
				overSite = v.Link
				overAPI = v.APILink
			}
			if v.Under >= lUnder {
				lUnder = v.Under
				lkUnder = k
				underSite = v.Link
				underAPI = v.APILink
			}

		}
		// for k, v := range init {
		// 	if v.Over == lOver {
		// 		if !strings.Contains(lkOver, k) {
		// 			lkOver += fmt.Sprintf("/%s", k)
		// 		}
		// 	}
		// 	if v.Under == lUnder {
		// 		if !strings.Contains(lkUnder, k) {
		// 			lkUnder += fmt.Sprintf("/%s", k)
		// 		}
		// 	}
		// }
		om, _ := stats.HarmonicMean(ot)
		um, _ := stats.HarmonicMean(ut)

		ordered = append(ordered, HOU35{
			OverSite:         lkOver,
			UnderSite:        lkUnder,
			OverSiteLink:     overSite,
			UnderSiteLink:    underSite,
			OverSiteAPILink:  overAPI,
			UnderSiteAPILink: underAPI,
			Over:             lOver,
			Under:            lUnder,
			ODeviation:       lOver - om,
			UDeviation:       lUnder - um,
		})
		delete(init, lkOver)
		delete(init, lkUnder)
	}

	return ordered
}

func (agg Aggregate) HighestOU45(exclude ...string) []HOU45 {
	ordered := []HOU45{}
	init := map[string]OU45{}

	for k, v := range agg.OU45 {
		init[k] = v
	}
	for _, ex := range exclude {
		delete(init, ex)
	}
	for {
		if len(init) == 0 {
			break
		}

		lOver, lkOver, overSite, overAPI := 0.0, "", "", ""
		lUnder, lkUnder, underSite, underAPI := 0.0, "", "", ""

		ot, ut := []float64{}, []float64{}

		for k, v := range init {

			ot = append(ot, v.Over)
			ut = append(ut, v.Under)
			if v.Over >= lOver {
				lOver = v.Over
				lkOver = k
				overSite = v.Link
				overAPI = v.APILink
			}
			if v.Under >= lUnder {
				lUnder = v.Under
				lkUnder = k
				underSite = v.Link
				underAPI = v.APILink
			}

		}
		// for k, v := range init {
		// 	if v.Over == lOver {
		// 		if !strings.Contains(lkOver, k) {
		// 			lkOver += fmt.Sprintf("/%s", k)
		// 		}
		// 	}
		// 	if v.Under == lUnder {
		// 		if !strings.Contains(lkUnder, k) {
		// 			lkUnder += fmt.Sprintf("/%s", k)
		// 		}
		// 	}
		// }
		om, _ := stats.HarmonicMean(ot)
		um, _ := stats.HarmonicMean(ut)

		ordered = append(ordered, HOU45{
			OverSite:         lkOver,
			UnderSite:        lkUnder,
			OverSiteLink:     overSite,
			UnderSiteLink:    underSite,
			OverSiteAPILink:  overAPI,
			UnderSiteAPILink: underAPI,
			Over:             lOver,
			Under:            lUnder,
			ODeviation:       lOver - om,
			UDeviation:       lUnder - um,
		})
		delete(init, lkOver)
		delete(init, lkUnder)
	}

	return ordered
}

func excludeSite(name string, list ...string) bool {
	for _, site := range list {
		if site == name {
			return true
		}
	}
	return false
}

func ShortForms() []string {
	return []string{
		"FC",
		"CF",
		"IF",
		"SD",
		"RCD",
		"SC",
		"PS",
		"FK",
		"PFK",
		"BK",
		"BV",
		"US",
		"SV",
		"AS",
		"CFC",
		"AFC",
		"ACF",
		"SSC",
		"VFL",
		"RC",
		"OGC",
		"SMB",
		"R.",
		"KAA",
		"KRC",
		"08",
		"04",
		"05",
		"SV",
		"TUS",
		"R",
		"SPORTS",
		"CA",
		"BA",
		"FF",
		"GFC",
		"REP.",
		"IGF",
		"IK",
		"AB",
		"CSD",
		"MC",
		"BP",
	}
}

func ChannelClosed(c <-chan Game) bool {
	_, closed := <-c
	return !closed
}
