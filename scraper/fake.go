package scraper

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"sync"
	"time"

	"gitlab.com/arbke/utils"
)

func generateGameJSON(name string) []byte {
	data, _ := ioutil.ReadFile(name)

	return data
}

type Fake1 struct {
}

func NewFake() *Fake1 {
	return &Fake1{}
}

func (f *Fake1) Name() string {
	return "Fake1"
}

func (f *Fake1) GetFootball(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() {
		close(games)
	}()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	gs := []Game{
		Game{
			Site: "Fake1",
			Home: "Home",
			Away: "Away",
		},
		Game{
			Site: "Fake1",
			Home: "Home1",
			Away: "Away1",
		},
		Game{
			Site: "Fake1",
			Home: "Home2",
			Away: "Away2",
		},
	}

	for _, game := range gs {
		if isStopped() {
			utils.Debug("CHANNEL CLOSED NOW")
			return
		}
		utils.Debug("SENDING GAME")
		games <- game
		// games <- game
		time.Sleep(30 * time.Second)
	}

	if isStopped() {
		utils.Debug("CHANNEL ALREADY CLOSED")
	}

}
func (f *Fake1) GetFootballGame(t *http.Transport, gameLink string, apiLink string) Game {

	games := make(chan Game)
	stop := make(chan struct{})
	go func() {
		f.GetFootball(t, games, stop)
	}()

	for g := range games {
		if g.TWAY.APILink == apiLink {
			stop <- struct{}{}
			return g
		}
	}
	if !ChannelClosed(games) {
		close(games)
	}

	return Game{}
}

func (f *Fake1) GetBasketBall(t *http.Transport, games chan Game, stop chan struct{}) {
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	gs := []Game{
		Game{
			Site: "Fake1",
			Home: "Home",
			Away: "Away",
		},
		Game{
			Site: "Fake1",
			Home: "Home1",
			Away: "Away1",
		},
		Game{
			Site: "Fake1",
			Home: "Home2",
			Away: "Away2",
		},
	}

	for _, game := range gs {
		if isStopped() {
			utils.Debug("CHANNEL CLOSED NOW")
			return
		}
		utils.Debug("SENDING GAME")
		games <- game
		// games <- game
		time.Sleep(60 * time.Second)
	}

	if isStopped() {
		utils.Debug("CHANNEL ALREADY CLOSED")
		return
	}
	close(games)

	return
}
func (f *Fake1) GetBasketBallGame(t *http.Transport, gameLink string, apiLink string) Game {

	games := make(chan Game)
	stop := make(chan struct{})
	go func() {
		f.GetFootball(t, games, stop)
	}()

	for g := range games {
		if g.TWAY.APILink == apiLink {
			stop <- struct{}{}
			return g
		}
	}
	if !ChannelClosed(games) {
		close(games)
	}

	return Game{}
}

func (f *Fake1) GetHandball(t *http.Transport, games chan Game, stop chan struct{}) {
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	gs := []Game{
		Game{
			Site: "Fake1",
			Home: "Home",
			Away: "Away",
		},
		Game{
			Site: "Fake1",
			Home: "Home1",
			Away: "Away1",
		},
		Game{
			Site: "Fake1",
			Home: "Home2",
			Away: "Away2",
		},
	}

	for _, game := range gs {
		if isStopped() {
			utils.Debug("CHANNEL CLOSED NOW")
			return
		}
		utils.Debug("SENDING GAME")
		games <- game
		// games <- game
		time.Sleep(60 * time.Second)
	}

	if isStopped() {
		utils.Debug("CHANNEL ALREADY CLOSED")
		return
	}
	close(games)

	return
}
func (f *Fake1) GetHandballGame(t *http.Transport, gameLink string, apiLink string) Game {

	games := make(chan Game)
	stop := make(chan struct{})
	go func() {
		f.GetFootball(t, games, stop)
	}()

	for g := range games {
		if g.TWAY.APILink == apiLink {
			stop <- struct{}{}
			return g
		}
	}
	if !ChannelClosed(games) {
		close(games)
	}

	return Game{}
}

func (f *Fake1) GetIceHockey(t *http.Transport, games chan Game, stop chan struct{}) {
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	gs := []Game{
		Game{
			Site: "Fake1",
			Home: "Home",
			Away: "Away",
		},
		Game{
			Site: "Fake1",
			Home: "Home1",
			Away: "Away1",
		},
		Game{
			Site: "Fake1",
			Home: "Home2",
			Away: "Away2",
		},
	}

	for _, game := range gs {
		if isStopped() {
			utils.Debug("CHANNEL CLOSED NOW")
			return
		}
		utils.Debug("SENDING GAME")
		games <- game
		// games <- game
		time.Sleep(60 * time.Second)
	}

	if isStopped() {
		utils.Debug("CHANNEL ALREADY CLOSED")
		return
	}
	close(games)

	return
}
func (f *Fake1) GetIceHockeyGame(t *http.Transport, gameLink string, apiLink string) Game {

	games := make(chan Game)
	stop := make(chan struct{})
	go func() {
		f.GetFootball(t, games, stop)
	}()

	for g := range games {
		if g.TWAY.APILink == apiLink {
			stop <- struct{}{}
			return g
		}
	}
	if !ChannelClosed(games) {
		close(games)
	}

	return Game{}
}

func (f *Fake1) GetRugby(t *http.Transport, games chan Game, stop chan struct{}) {
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	gs := []Game{
		Game{
			Site: "Fake1",
			Home: "Home",
			Away: "Away",
		},
		Game{
			Site: "Fake1",
			Home: "Home1",
			Away: "Away1",
		},
		Game{
			Site: "Fake1",
			Home: "Home2",
			Away: "Away2",
		},
	}

	for _, game := range gs {
		if isStopped() {
			utils.Debug("CHANNEL CLOSED NOW")
			return
		}
		utils.Debug("SENDING GAME")
		games <- game
		// games <- game
		time.Sleep(60 * time.Second)
	}

	if isStopped() {
		utils.Debug("CHANNEL ALREADY CLOSED")
		return
	}
	close(games)

	return
}
func (f *Fake1) GetRugbyGame(t *http.Transport, gameLink string, apiLink string) Game {

	games := make(chan Game)
	stop := make(chan struct{})
	go func() {
		f.GetFootball(t, games, stop)
	}()

	for g := range games {
		if g.TWAY.APILink == apiLink {
			stop <- struct{}{}
			return g
		}
	}
	if !ChannelClosed(games) {
		close(games)
	}

	return Game{}
}

func (f *Fake1) GetTennis(t *http.Transport, games chan Game, stop chan struct{}) {
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	gs := []Game{
		Game{
			Site: "Fake1",
			Home: "Home",
			Away: "Away",
		},
		Game{
			Site: "Fake1",
			Home: "Home1",
			Away: "Away1",
		},
		Game{
			Site: "Fake1",
			Home: "Home2",
			Away: "Away2",
		},
	}

	for _, game := range gs {
		if isStopped() {
			utils.Debug("CHANNEL CLOSED NOW")
			return
		}
		utils.Debug("SENDING GAME")
		games <- game
		// games <- game
		time.Sleep(60 * time.Second)
	}

	if isStopped() {
		utils.Debug("CHANNEL ALREADY CLOSED")
		return
	}
	close(games)

	return
}
func (f *Fake1) GetTennisGame(t *http.Transport, gameLink string, apiLink string) Game {

	games := make(chan Game)
	stop := make(chan struct{})
	go func() {
		f.GetFootball(t, games, stop)
	}()

	for g := range games {
		if g.TWAY.APILink == apiLink {
			stop <- struct{}{}
			return g
		}
	}
	if !ChannelClosed(games) {
		close(games)
	}

	return Game{}
}

func (f *Fake1) MainPageUsage() int {
	return 0
}
func (f *Fake1) Usage() int {
	return 0
}
func (f *Fake1) UsageReset() {

}

type Fake2 struct {
}

func NewFake2() *Fake2 {
	return &Fake2{}
}

func (f *Fake2) Name() string {
	return "Fake2"
}

func (f *Fake2) GetFootball(t *http.Transport, games chan Game, stop chan struct{}) {
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()

	data := generateGameJSON("out2.json")
	gs := []Game{}
	json.Unmarshal(data, &gs)
	utils.Debug(gs)
	for _, game := range gs {
		if isStopped() {
			utils.Debug("CHANNEL CLOSED NOW")
			return
		}
		utils.Debug("SENDING GAME")
		games <- game
		// games <- game
		time.Sleep(1 * time.Second)
	}

	if isStopped() {
		utils.Debug("CHANNEL ALREADY CLOSED")
		return
	}
	close(games)

	return
}
func (f *Fake2) MainPageUsage() int {
	return 0
}
func (f *Fake2) Usage() int {
	return 0
}
func (f *Fake2) UsageReset() {

}
func (f *Fake2) GetFootballGame(t *http.Transport, gameLink string, apiLink string) Game {

	games := make(chan Game)
	stop := make(chan struct{})
	go func() {
		f.GetFootball(t, games, stop)
	}()

	for g := range games {
		if g.TWAY.APILink == apiLink {
			stop <- struct{}{}
			return g
		}
	}
	if !ChannelClosed(games) {
		close(games)
	}

	return Game{}
}
