package scraper

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"

	"gitlab.com/arbke/utils"
)

type WSBet struct {
	name     string
	link     string
	usage    int
	mainpage int
	lock     *sync.Mutex
}

func NewWSBet() *WSBet {
	return &WSBet{
		name: "wsbetting",
		link: "https://wsbetting.co.ke/petition.php",
		lock: &sync.Mutex{},
	}
}

func (sbet *WSBet) addUsage(u int) {
	sbet.lock.Lock()
	defer sbet.lock.Unlock()
	sbet.usage += u
}
func (sbet *WSBet) Usage() int {
	sbet.lock.Lock()
	defer sbet.lock.Unlock()
	return sbet.usage
}
func (sbet *WSBet) Name() string {
	return sbet.name
}

func (wsbet *WSBet) getRequest(data url.Values) *http.Request {
	req, _ := http.NewRequest(http.MethodPost, wsbet.link, strings.NewReader(data.Encode()))
	req.Header.Add("accept-language", "en-US,en;q=0.5")
	req.Header.Add("connection", "keep-alive")
	req.Header.Add("content-type", "application/x-www-form-urlencoded; charset=UTF-8")
	req.Header.Add("origin", "https://wsbetting.co.ke")
	req.Header.Add("referer", "https://wsbetting.co.ke/")
	req.Header.Add("user-agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0")

	return req
}

func (wsbet *WSBet) GetFootball(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	//get all upcoming games
	matches := []interface{}{}
	pages := []int{0, 1, 2}
	for _, page := range pages {
		d := url.Values{}
		d.Add("action", "get_daily_events")
		d.Add("canita_key", fmt.Sprintf("EVENTS_Sport_Football_%d", page))
		d.Add("sport_id", "Sport_Football")
		req := wsbet.getRequest(d)

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		resp.Body.Close()

		wsbet.addUsage(len(data))
		parsed := map[string]interface{}{}

		if err := json.Unmarshal(data, &parsed); err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		_matches := parsed["content"].([]interface{})
		matches = append(matches, _matches...)
	}
	for _, _match := range matches {
		match := _match.(map[string]interface{})
		game := Game{}
		game.League = match["tag_group_name"].(string) + " " + match["tag_competition_name"].(string)
		t := time.Unix(int64(match["off_datetime"].(float64))/1000, 0)
		eventId := match["event_id"].(float64)
		game.Time = &t

		game.Home = match["tag_home_team_name"].(string)
		game.Away = match["tag_away_team_name"].(string)

		game.Site = wsbet.name
		g := wsbet.getGame(fmt.Sprintf("%d", int(eventId)), game)
		if isStopped() {
			return
		}
		if g == nil {
			continue
		}
		// utils.Debug(*g)

		games <- *g

	}

	if isStopped() {
		return
	}

}

func (wsbet *WSBet) getGame(eventId string, game Game) *Game {
	gameLink := fmt.Sprintf("https://wsbetting.co.ke/#view=%s", eventId)
	tw := ThreeWay{APILink: eventId, Link: gameLink}
	dnb := DrawNoBet{APILink: eventId, Link: gameLink}
	dc := DoubleChance{APILink: eventId, Link: gameLink}

	ou15 := OU15{APILink: eventId, Link: gameLink}
	ou25 := OU25{APILink: eventId, Link: gameLink}
	ou35 := OU35{APILink: eventId, Link: gameLink}
	ou45 := OU45{APILink: eventId, Link: gameLink}

	gng := GoalNoGoal{APILink: eventId, Link: gameLink}

	d := url.Values{}
	d.Add("action", "get_event_by_id")
	d.Add("event_id[]", eventId)

	req := wsbet.getRequest(d)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	resp.Body.Close()

	wsbet.addUsage(len(data))
	parsed := map[string]interface{}{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}

	content := parsed["content"].(map[string]interface{})

	if _, ok := content["event"]; !ok {
		return nil
	}

	event := content["event"].([]interface{})
	if len(event) == 0 {
		return nil
	}

	marketData := event[0].(map[string]interface{})

	if marketData["status"].(string) != "OPEN" {
		return nil
	}
	for _, _market := range marketData["childrenList"].([]interface{}) {
		market := _market.(map[string]interface{})

		marketID := market["market_key"].(string)
		outcomes := market["childrenList"].([]interface{})
		status := market["status"].(string)
		line := ""

		switch market["line_1"].(type) {
		case string:
			line = market["line_1"].(string)
		}
		// utils.Debug(market["line_1"], line, marketID)
		// utils.Debug(marketID, status, len(outcomes))

		if status != "OPEN" {
			continue
		}
		switch marketID {

		case "DOUBLE_CHANCE": //DC
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				name := outcome["code_name"].(string)
				odd := outcome["odds"].(float64)

				switch name {
				case "12":
					dc.HomeOrAway = odd
				case "X2":
					dc.Away = odd
				case "1X":
					dc.Home = odd
				}
			}

		case "MATCH_BETTING": //THREE WAY
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				name := outcome["code_name"].(string)
				odd := outcome["odds"].(float64)

				switch name {
				case "1":
					tw.Home = odd
				case "X":
					tw.Draw = odd
				case "2":
					tw.Away = odd
				}
			}

		case "DRAW_NO_BET": //DNB
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				name := outcome["code_name"].(string)
				odd := outcome["odds"].(float64)

				switch name {
				case "1":
					dnb.Home = odd
				case "2":
					dnb.Away = odd
				}
			}

		case "BOTH_TEAMS_TO_SCORE": //GNG
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				name := outcome["code_name"].(string)
				odd := outcome["odds"].(float64)

				switch name {
				case "Y":
					gng.Goal = odd
				case "N":
					gng.NoGoal = odd
				}
			}

		case "TOTAL_GOALS_(NO_QUARTERS)":

			switch line {
			case "1.50":

				for _, _outcome := range outcomes {
					outcome := _outcome.(map[string]interface{})
					name := outcome["code_name"].(string)
					odd := outcome["odds"].(float64)

					switch name {
					case "+":
						ou15.Over = odd
					case "-":
						ou15.Under = odd
					}
				}

			case "2.50":
				for _, _outcome := range outcomes {
					outcome := _outcome.(map[string]interface{})
					name := outcome["code_name"].(string)
					odd := outcome["odds"].(float64)

					switch name {
					case "+":
						ou25.Over = odd
					case "-":
						ou25.Under = odd
					}
				}
			case "3.50":
				for _, _outcome := range outcomes {
					outcome := _outcome.(map[string]interface{})
					name := outcome["code_name"].(string)
					odd := outcome["odds"].(float64)

					switch name {
					case "+":
						ou35.Over = odd
					case "-":
						ou35.Under = odd
					}
				}

			case "4.50":
				for _, _outcome := range outcomes {
					outcome := _outcome.(map[string]interface{})
					name := outcome["code_name"].(string)
					odd := outcome["odds"].(float64)

					switch name {
					case "+":
						ou45.Over = odd
					case "-":
						ou45.Under = odd
					}
				}
			}

		}

	}

	game.TWAY = tw
	game.DC = dc
	game.DNB = dnb
	game.GNG = gng
	game.OU15 = ou15
	game.OU25 = ou25
	game.OU35 = ou35
	game.OU45 = ou45

	return &game

}
func (wsbet *WSBet) GetFootballGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := wsbet.getGame(apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (wsbet *WSBet) GetBasketBall(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	//get all upcoming games
	matches := []interface{}{}
	pages := []int{0}
	for range pages {
		d := url.Values{}
		d.Add("action", "get_all_events_from_sport")
		d.Add("canita_key", "EVENTS_Sport_Basketball_ALL")
		d.Add("sport_id", "Sport_Basketball")
		req := wsbet.getRequest(d)

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		resp.Body.Close()

		wsbet.addUsage(len(data))
		parsed := map[string]interface{}{}

		if err := json.Unmarshal(data, &parsed); err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		_matches := parsed["content"].([]interface{})
		matches = append(matches, _matches...)
	}
	for _, _match := range matches {
		match := _match.(map[string]interface{})
		game := Game{}
		game.League = match["tag_competition_name"].(string)
		t := time.Unix(int64(match["off_datetime"].(float64))/1000, 0)
		eventId := match["event_id"].(float64)
		game.Time = &t

		game.Home = match["tag_home_team_name"].(string)
		game.Away = match["tag_away_team_name"].(string)

		game.Site = wsbet.name
		g := wsbet.getBGame(fmt.Sprintf("%d", int(eventId)), game)
		if isStopped() {
			return
		}
		if g == nil {
			continue
		}
		// utils.Debug(*g)

		games <- *g

	}

	if isStopped() {
		return
	}

}

func (wsbet *WSBet) getBGame(eventId string, game Game) *Game {
	gameLink := fmt.Sprintf("https://wsbetting.co.ke/#view=%s", eventId)
	tw := TwoWay{APILink: eventId, Link: gameLink}

	d := url.Values{}
	d.Add("action", "get_event_by_id")
	d.Add("event_id[]", eventId)

	req := wsbet.getRequest(d)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	resp.Body.Close()

	wsbet.addUsage(len(data))
	parsed := map[string]interface{}{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}

	content := parsed["content"].(map[string]interface{})

	if _, ok := content["event"]; !ok {
		return nil
	}

	event := content["event"].([]interface{})
	if len(event) == 0 {
		return nil
	}

	marketData := event[0].(map[string]interface{})

	if marketData["status"].(string) != "OPEN" {
		return nil
	}
	for _, _market := range marketData["childrenList"].([]interface{}) {
		market := _market.(map[string]interface{})

		marketID := market["market_key"].(string)
		outcomes := market["childrenList"].([]interface{})
		status := market["status"].(string)
		// line := ""

		// switch market["line_1"].(type) {
		// case string:
		// 	line = market["line_1"].(string)
		// }
		// utils.Debug(market["line_1"], line, marketID)
		// utils.Debug(marketID, status, len(outcomes))

		if status != "OPEN" {
			continue
		}
		switch marketID {

		case "MONEY_LINE": //MONEY LINE
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				name := outcome["code_name"].(string)
				odd := outcome["odds"].(float64)

				switch name {
				case "1":
					tw.Home = odd
				case "2":
					tw.Away = odd
				}
			}

		}

	}

	game.TWOWAY = tw

	return &game

}
func (wsbet *WSBet) GetBasketBallGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := wsbet.getBGame(apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (wsbet *WSBet) GetTennis(t *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {
		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	//get all upcoming games
	matches := []interface{}{}
	pages := []int{0}
	for range pages {
		d := url.Values{}
		d.Add("action", "get_all_events_from_sport")
		d.Add("canita_key", "EVENTS_Sport_Tennis_ALL")
		d.Add("sport_id", "Sport_Tennis")
		req := wsbet.getRequest(d)

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		resp.Body.Close()

		wsbet.addUsage(len(data))
		parsed := map[string]interface{}{}

		if err := json.Unmarshal(data, &parsed); err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		_matches := parsed["content"].([]interface{})
		matches = append(matches, _matches...)
	}
	for _, _match := range matches {
		match := _match.(map[string]interface{})
		game := Game{}
		game.League = match["tag_competition_name"].(string)
		t := time.Unix(int64(match["off_datetime"].(float64))/1000, 0)
		eventId := match["event_id"].(float64)
		game.Time = &t

		game.Home = match["tag_home_team_name"].(string)
		game.Away = match["tag_away_team_name"].(string)

		game.Site = wsbet.name
		g := wsbet.getTGame(fmt.Sprintf("%d", int(eventId)), game)
		if isStopped() {
			return
		}
		if g == nil {
			continue
		}
		// utils.Debug(*g)

		games <- *g

	}

	if isStopped() {
		return
	}

}

func (wsbet *WSBet) getTGame(eventId string, game Game) *Game {
	gameLink := fmt.Sprintf("https://wsbetting.co.ke/#view=%s", eventId)
	tw := TwoWay{APILink: eventId, Link: gameLink}

	d := url.Values{}
	d.Add("action", "get_event_by_id")
	d.Add("event_id[]", eventId)

	req := wsbet.getRequest(d)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return nil
	}
	resp.Body.Close()

	wsbet.addUsage(len(data))
	parsed := map[string]interface{}{}

	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		return nil
	}

	content := parsed["content"].(map[string]interface{})

	if _, ok := content["event"]; !ok {
		return nil
	}

	event := content["event"].([]interface{})
	if len(event) == 0 {
		return nil
	}

	marketData := event[0].(map[string]interface{})

	if marketData["status"].(string) != "OPEN" {
		return nil
	}
	for _, _market := range marketData["childrenList"].([]interface{}) {
		market := _market.(map[string]interface{})

		marketID := market["market_key"].(string)
		outcomes := market["childrenList"].([]interface{})
		status := market["status"].(string)
		// line := ""

		// switch market["line_1"].(type) {
		// case string:
		// 	line = market["line_1"].(string)
		// }
		// utils.Debug(market["line_1"], line, marketID)
		// utils.Debug(marketID, status, len(outcomes))

		if status != "OPEN" {
			continue
		}
		switch marketID {

		case "MATCH_BETTING": //MONEY LINE
			for _, _outcome := range outcomes {
				outcome := _outcome.(map[string]interface{})
				name := outcome["code_name"].(string)
				odd := outcome["odds"].(float64)

				switch name {
				case "1":
					tw.Home = odd
				case "2":
					tw.Away = odd
				}
			}

		}

	}

	game.TWOWAY = tw

	return &game

}
func (wsbet *WSBet) GetTennisGame(t *http.Transport, gameLink string, apiLink string) Game {
	g := wsbet.getTGame(apiLink, Game{})
	if g == nil {
		return Game{}
	}
	return *g
}

func (wsbet *WSBet) GetRugby(t *http.Transport, games chan Game, stop chan struct{}) {
	close(games)
}

func (wsbet *WSBet) GetRugbyGame(t *http.Transport, gameLink string, apiLink string) Game {
	return Game{}
}

func (wsbet *WSBet) UsageReset() {
	wsbet.usage = 0
	wsbet.mainpage = 0
}
func (wsbet *WSBet) MainPageUsage() int {
	return wsbet.mainpage
}
