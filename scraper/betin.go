package scraper

import (
	"io/ioutil"
	"net/http"
	"strings"
	"sync"

	"encoding/json"

	"bytes"

	"gitlab.com/arbke/utils"

	"fmt"

	"github.com/headzoo/surf"
	"github.com/headzoo/surf/browser"
)

type BetIn struct {
	name      string
	url       string
	navigator *browser.Browser
	usage     int
	mainpage  int

	lock *sync.Mutex
}

func NewBetIn() *BetIn {
	betin := &BetIn{
		name:      "Betin",
		url:       "https://web.betin.co.ke",
		navigator: surf.NewBrowser(),
		lock:      &sync.Mutex{},
	}
	betin.navigator.SetUserAgent("Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
	return betin
}

func (betin *BetIn) Name() string {
	return betin.name
}

func (betin *BetIn) MainPageUsage() int {
	return betin.mainpage
}

func (betin *BetIn) addUsage(u int) {
	betin.lock.Lock()
	defer betin.lock.Unlock()
	betin.usage += u
}

func (betin *BetIn) Usage() int {
	betin.lock.Lock()
	defer betin.lock.Unlock()
	return betin.usage
}

func (betin *BetIn) UsageReset() {
	betin.usage = 0
	betin.mainpage = 0
}

func (betin *BetIn) GetFootball(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {

		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	// matches := []Match{}
	if err := betin.navigator.Open("https://web.betin.co.ke/Sport/OddsToday.aspx?IDSport=20"); err != nil {
		utils.Error("Betin error:", err.Error())
		if isStopped() {
			return
		}

		return
	}
	cookies := betin.navigator.SiteCookies()

	urls := map[string]interface{}{
		//tw,dc,ou25
		"tw-https://web.betin.co.ke/Controls/ControlsWS.asmx/OddsTodayFullEvent": map[string]int{
			"IDSport":       20,
			"IDGruppoQuota": -1,
		},
		//gg/ng
		"gg-https://web.betin.co.ke/Controls/ControlsWS.asmx/OddsTodayDetails": map[string]int{
			"IDSport":       20,
			"IDGruppoQuota": 10,
		},
		//dnb
		"dnb-https://web.betin.co.ke/Controls/ControlsWS.asmx/OddsTodayDetails": map[string]int{
			"IDSport":       20,
			"IDGruppoQuota": 14,
		},
		//ou15
		"ou15-https://web.betin.co.ke/Controls/ControlsWS.asmx/OddsTodayDetails": map[string]int{
			"IDSport":       20,
			"IDGruppoQuota": 50,
		},
		//ou35
		"ou35-https://web.betin.co.ke/Controls/ControlsWS.asmx/OddsTodayDetails": map[string]int{
			"IDSport":       20,
			"IDGruppoQuota": 52,
		},
	}
	events := []map[string]interface{}{}
	for addr, payload := range urls {
		dataStr, _ := json.Marshal(payload)
		endpoint := strings.Split(addr, "-")[1]
		req, err := http.NewRequest("POST", endpoint, bytes.NewBuffer(dataStr))
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		req.Header.Add("accept", "application/json, text/plain, */*")
		req.Header.Add("origin", "https://web.betin.co.ke")
		req.Header.Add("user-agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
		req.Header.Add("content-type", "application/json;charset=UTF-8")
		req.Header.Add("referer", "https://web.betin.co.ke/Sport/OddsToday.aspx?IDSport=20")
		// req.Header.Add("accept-encoding", "gzip, deflate, br")
		req.Header.Add("accept-language", "en-US,en;q=0.8")
		req.Header.Add("cache-control", "no-cache")

		for _, cookie := range cookies {
			req.AddCookie(cookie)
		}

		// fmt.Println(req.Header.Get("Cookie"))
		client := http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			utils.Error(err)
			continue
		}
		respData := map[string]interface{}{}

		data, _ := ioutil.ReadAll(resp.Body)
		betin.addUsage(len(data))
		// ioutil.WriteFile("out.json", data, 0777)
		// return nil
		if err := json.Unmarshal(data, &respData); err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		parsedData := respData["d"]

		switch d := parsedData.(type) {
		case []interface{}:
			tmp := map[string]interface{}{}
			for _, _i := range d {
				i := _i.(map[string]interface{})
				if int(i["IDTipoSport"].(float64)) == 1 {
					tmp = i
					break
				}

			}
			if _, ok := tmp["Detail"]; ok {

				for _, t := range tmp["Detail"].(map[string]interface{})["SottoEventiList"].([]interface{}) {
					events = append(events, t.(map[string]interface{}))
				}
			}
		case map[string]interface{}:

			for _, tmp := range d["SottoEventiList"].([]interface{}) {
				events = append(events, tmp.(map[string]interface{}))
			}

		}

	}

	// _d, _ := json.Marshal(events)
	// ioutil.WriteFile("out.json", _d, 0777)
	// fmt.Println(events)
	utils.Debug("LOGGED")

	tmpGames := map[int]*Game{}
	for _, event := range events {
		gameID := int(event["IDSottoEvento"].(float64))
		league := event["Evento"].(string)

		teams := strings.Split(event["SottoEvento"].(string), " - ")
		// if strings.Contains(event["SottoEvento"].(string), "Stadl") {
		// 	utils.Debug(event["SottoEvento"].(string), teams)
		// 	return nil
		// }
		if _, ok := tmpGames[gameID]; !ok {
			game := Game{}
			game.League = league
			game.Site = betin.Name()
			game.GameID = fmt.Sprintf("%d", gameID)
			gameLink := fmt.Sprintf("https://web.betin.co.ke/Sport/SubEventOdds.aspx?SubEventID=%d", gameID)
			// utils.Debug(gameLink)
			if len(teams) < 2 {
				utils.Info(event["SottoEvento"].(string))
				continue
			}
			game.Home = strings.Trim(teams[0], " ")
			game.Away = strings.Trim(teams[1], " ")
			if tmp := Allowed(league); len(tmp) > 0 {
				game.Home = fmt.Sprintf("%s %s", game.Home, tmp)
				game.Away = fmt.Sprintf("%s %s", game.Away, tmp)
			}
			game.Time = utils.DecodeTime(betin.name, fmt.Sprintf("%s %s", event["StrDataInizioBreve"].(string), event["StrOraInizio"].(string)))
			// utils.Debug(game.Time)
			game.TWAY = ThreeWay{Link: gameLink, APILink: gameLink}
			game.DC = DoubleChance{Link: gameLink, APILink: gameLink}
			game.DNB = DrawNoBet{Link: gameLink, APILink: gameLink}
			game.GNG = GoalNoGoal{Link: gameLink, APILink: gameLink}
			game.OU15 = OU15{Link: gameLink, APILink: gameLink}
			game.OU25 = OU25{Link: gameLink, APILink: gameLink}
			game.OU35 = OU35{Link: gameLink, APILink: gameLink}
			game.OU45 = OU45{Link: gameLink, APILink: gameLink}
			tmpGames[gameID] = &game
		}
		markets := event["Quote"].([]interface{})
		for _, _market := range markets {
			market := _market.(map[string]interface{})
			odd := market["QuotaValore"].(float64)
			spec := market["StrHND"].(string)
			switch market["TipoQuotaBreve"].(string) {
			case "1":
				tmpGames[gameID].TWAY.Home = odd
			case "X":
				tmpGames[gameID].TWAY.Draw = odd
			case "2":
				tmpGames[gameID].TWAY.Away = odd
			case "12":
				tmpGames[gameID].DC.HomeOrAway = odd
			case "1X":
				tmpGames[gameID].DC.Home = odd
			case "X2":
				tmpGames[gameID].DC.Away = odd
			case "Over":
				switch spec {
				case "1.5":
					tmpGames[gameID].OU15.Over = odd
				case "2.5":
					tmpGames[gameID].OU25.Over = odd
				case "3.5":
					tmpGames[gameID].OU35.Over = odd
				case "4.5":
					tmpGames[gameID].OU45.Over = odd
				}
			case "Under":
				switch spec {
				case "1.5":
					tmpGames[gameID].OU15.Under = odd
				case "2.5":
					tmpGames[gameID].OU25.Under = odd
				case "3.5":
					tmpGames[gameID].OU35.Under = odd
				case "4.5":
					tmpGames[gameID].OU45.Under = odd
				}
			case "GG":
				tmpGames[gameID].GNG.Goal = odd
			case "NG":
				tmpGames[gameID].GNG.NoGoal = odd
			case "1 DNB":
				tmpGames[gameID].DNB.Home = odd
			case "2 DNB":
				tmpGames[gameID].DNB.Away = odd
			}
		}

	}

	for _, v := range tmpGames {
		if isStopped() {
			return
		}

		games <- *v
	}
	if isStopped() {
		return
	}

}

func (betin *BetIn) GetFootballGame(t *http.Transport, gameLink string, apiLink string) Game {
	games := make(chan Game)
	stop := make(chan struct{})
	go func() {
		betin.GetFootball(t, games, stop)
	}()

	for game := range games {
		if game.TWAY.APILink == apiLink {
			stop <- struct{}{}
			return game
		}
	}
	if !ChannelClosed(games) {

	}
	return Game{}
}

func (betin *BetIn) GetTennis(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {

		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	// matches := []Match{}
	if err := betin.navigator.Open("https://web.betin.co.ke/Sport/OddsToday.aspx?IDSport=21"); err != nil {
		utils.Error("Betin error:", err.Error())
		if isStopped() {
			return
		}

		return
	}
	cookies := betin.navigator.SiteCookies()

	urls := map[string]interface{}{
		//tw,dc,ou25
		"tw-https://web.betin.co.ke/Controls/ControlsWS.asmx/OddsTodayFullEvent": map[string]int{
			"IDSport":       21,
			"IDGruppoQuota": -1,
		},
	}
	events := []map[string]interface{}{}
	for addr, payload := range urls {
		dataStr, _ := json.Marshal(payload)
		endpoint := strings.Split(addr, "-")[1]
		req, err := http.NewRequest("POST", endpoint, bytes.NewBuffer(dataStr))
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		req.Header.Add("accept", "application/json, text/plain, */*")
		req.Header.Add("origin", "https://web.betin.co.ke")
		req.Header.Add("user-agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
		req.Header.Add("content-type", "application/json;charset=UTF-8")
		req.Header.Add("referer", "https://web.betin.co.ke/Sport/OddsToday.aspx?IDSport=21")
		// req.Header.Add("accept-encoding", "gzip, deflate, br")
		req.Header.Add("accept-language", "en-US,en;q=0.8")
		req.Header.Add("cache-control", "no-cache")

		for _, cookie := range cookies {
			req.AddCookie(cookie)
		}

		// fmt.Println(req.Header.Get("Cookie"))
		client := http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			utils.Error(err)
			continue
		}
		respData := map[string]interface{}{}

		data, _ := ioutil.ReadAll(resp.Body)
		betin.addUsage(len(data))
		// ioutil.WriteFile("out.json", data, 0777)
		// return nil
		if err := json.Unmarshal(data, &respData); err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		parsedData := respData["d"]

		switch d := parsedData.(type) {
		case []interface{}:
			tmp := map[string]interface{}{}
			for _, _i := range d {
				i := _i.(map[string]interface{})
				if int(i["IDTipoSport"].(float64)) == 4 {
					tmp = i
					break
				}

			}
			if _, ok := tmp["Detail"]; ok {

				for _, t := range tmp["Detail"].(map[string]interface{})["SottoEventiList"].([]interface{}) {
					events = append(events, t.(map[string]interface{}))
				}
			}
		case map[string]interface{}:

			for _, tmp := range d["SottoEventiList"].([]interface{}) {
				events = append(events, tmp.(map[string]interface{}))
			}

		}

	}

	tmpGames := map[int]*Game{}
	for _, event := range events {
		gameID := int(event["IDSottoEvento"].(float64))
		league := event["Evento"].(string)

		teams := strings.Split(event["SottoEvento"].(string), " - ")
		// if strings.Contains(event["SottoEvento"].(string), "Stadl") {
		// 	utils.Debug(event["SottoEvento"].(string), teams)
		// 	return nil
		// }

		if _, ok := tmpGames[gameID]; !ok {
			game := Game{}
			game.League = league
			game.Site = betin.Name()
			game.GameID = fmt.Sprintf("%d", gameID)
			gameLink := fmt.Sprintf("https://web.betin.co.ke/Sport/SubEventOdds.aspx?SubEventID=%d", gameID)
			// utils.Debug(gameLink)
			if len(teams) < 2 {
				utils.Info(event["SottoEvento"].(string))
				continue
			}
			game.Home = strings.Trim(teams[0], " ")
			game.Away = strings.Trim(teams[1], " ")

			game.Time = utils.DecodeTime(betin.name, fmt.Sprintf("%s %s", event["StrDataInizioBreve"].(string), event["StrOraInizio"].(string)))
			// utils.Debug(game.Time)
			game.TWOWAY = TwoWay{Link: gameLink, APILink: game.GameID}

			tmpGames[gameID] = &game
		}
		markets := event["Quote"].([]interface{})
		for _, _market := range markets {
			market := _market.(map[string]interface{})
			odd := market["QuotaValore"].(float64)
			// spec := market["StrHND"].(string)
			switch market["TipoQuotaBreve"].(string) {
			case "1":
				tmpGames[gameID].TWOWAY.Home = odd
			case "2":
				tmpGames[gameID].TWOWAY.Away = odd

			}
		}

	}
	for _, v := range tmpGames {
		if isStopped() {
			return
		}

		games <- *v
	}
	if isStopped() {
		return
	}

}
func (betin *BetIn) GetTennisGame(t *http.Transport, gameLink string, apiLink string) Game {
	games := make(chan Game)
	stop := make(chan struct{})
	go func() {
		betin.GetTennis(t, games, stop)
	}()

	for game := range games {
		if game.TWAY.APILink == apiLink {
			stop <- struct{}{}
			return game
		}
	}

	return Game{}
}

func (betin *BetIn) GetBasketBall(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {

		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	// matches := []Match{}
	if err := betin.navigator.Open("https://web.betin.co.ke/Sport/OddsToday.aspx?IDSport=22"); err != nil {
		utils.Error("Betin error:", err.Error())
		if isStopped() {
			return
		}

		return
	}
	cookies := betin.navigator.SiteCookies()

	urls := map[string]interface{}{
		//tw,dc,ou25
		"tww-https://web.betin.co.ke/Controls/ControlsWS.asmx/OddsTodayFullEvent": map[string]int{
			"IDSport":       22,
			"IDGruppoQuota": -1,
		},

		"tw-https://web.betin.co.ke/Controls/ControlsWS.asmx/OddsTodayFullEvent": map[string]int{
			"IDSport":       22,
			"IDGruppoQuota": 1027,
		},
	}
	events := []map[string]interface{}{}
	for addr, payload := range urls {
		dataStr, _ := json.Marshal(payload)
		endpoint := strings.Split(addr, "-")[1]
		req, err := http.NewRequest("POST", endpoint, bytes.NewBuffer(dataStr))
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		req.Header.Add("accept", "application/json, text/plain, */*")
		req.Header.Add("origin", "https://web.betin.co.ke")
		req.Header.Add("user-agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
		req.Header.Add("content-type", "application/json;charset=UTF-8")
		req.Header.Add("referer", "https://web.betin.co.ke/Sport/OddsToday.aspx?IDSport=22")
		// req.Header.Add("accept-encoding", "gzip, deflate, br")
		req.Header.Add("accept-language", "en-US,en;q=0.8")
		req.Header.Add("cache-control", "no-cache")

		for _, cookie := range cookies {
			req.AddCookie(cookie)
		}

		// fmt.Println(req.Header.Get("Cookie"))
		client := http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			utils.Error(err)
			continue
		}
		respData := map[string]interface{}{}

		data, _ := ioutil.ReadAll(resp.Body)
		betin.addUsage(len(data))
		// ioutil.WriteFile("out.json", data, 0777)
		// return nil
		if err := json.Unmarshal(data, &respData); err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		parsedData := respData["d"]

		switch d := parsedData.(type) {
		case []interface{}:
			tmp := map[string]interface{}{}
			for _, _i := range d {
				i := _i.(map[string]interface{})
				if int(i["IDTipoSport"].(float64)) == 2 {
					tmp = i
					break
				}

			}
			if _, ok := tmp["Detail"]; ok {
				if tmp["Detail"] != nil {
					for _, t := range tmp["Detail"].(map[string]interface{})["SottoEventiList"].([]interface{}) {
						events = append(events, t.(map[string]interface{}))
					}
				}
			}
		case map[string]interface{}:

			for _, tmp := range d["SottoEventiList"].([]interface{}) {
				events = append(events, tmp.(map[string]interface{}))
			}

		}

	}

	tmpGames := map[int]*Game{}
	for _, event := range events {
		gameID := int(event["IDSottoEvento"].(float64))
		league := event["Evento"].(string)

		teams := strings.Split(event["SottoEvento"].(string), " - ")
		// if strings.Contains(event["SottoEvento"].(string), "Stadl") {
		// 	utils.Debug(event["SottoEvento"].(string), teams)
		// 	return nil
		// }

		if _, ok := tmpGames[gameID]; !ok {
			game := Game{}
			game.League = league
			game.Site = betin.Name()
			game.GameID = fmt.Sprintf("%d", gameID)
			gameLink := fmt.Sprintf("https://web.betin.co.ke/Sport/SubEventOdds.aspx?SubEventID=%d", gameID)
			// utils.Debug(gameLink)
			if len(teams) < 2 {
				utils.Info(event["SottoEvento"].(string))
				continue
			}
			game.Home = strings.Trim(teams[0], " ")
			game.Away = strings.Trim(teams[1], " ")

			game.Time = utils.DecodeTime(betin.name, fmt.Sprintf("%s %s", event["StrDataInizioBreve"].(string), event["StrOraInizio"].(string)))
			// utils.Debug(game.Time)
			game.TWOWAY = TwoWay{Link: gameLink, APILink: game.GameID}
			game.TWAY = ThreeWay{Link: gameLink, APILink: game.GameID}

			tmpGames[gameID] = &game
		}
		markets := event["Quote"].([]interface{})
		for _, _market := range markets {
			market := _market.(map[string]interface{})
			odd := market["QuotaValore"].(float64)
			// spec := market["StrHND"].(string)
			switch market["TipoQuota"].(string) {
			case "1HH":
				tmpGames[gameID].TWOWAY.Home = odd
			case "2HH":
				tmpGames[gameID].TWOWAY.Away = odd
			case "1":
				tmpGames[gameID].TWAY.Home = odd
			case "X":
				tmpGames[gameID].TWAY.Draw = odd
			case "2":
				tmpGames[gameID].TWAY.Away = odd
			}
		}

	}
	for _, v := range tmpGames {
		if isStopped() {
			return
		}

		games <- *v
	}
	if isStopped() {
		return
	}

}
func (betin *BetIn) GetBasketBallGame(t *http.Transport, gameLink string, apiLink string) Game {
	games := make(chan Game)
	stop := make(chan struct{})
	go func() {
		betin.GetBasketBall(t, games, stop)
	}()

	for game := range games {
		if game.TWAY.APILink == apiLink {
			stop <- struct{}{}
			return game
		}
	}

	return Game{}
}

func (betin *BetIn) GetRugby(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {

		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	// matches := []Match{}
	if err := betin.navigator.Open("https://web.betin.co.ke/Sport/OddsToday.aspx?IDSport=27"); err != nil {
		utils.Error("Betin error:", err.Error())
		if isStopped() {
			return
		}

		return
	}
	cookies := betin.navigator.SiteCookies()

	urls := map[string]interface{}{
		//tw,dc,ou25
		"tw-https://web.betin.co.ke/Controls/ControlsWS.asmx/OddsTodayFullEvent": map[string]int{
			"IDSport":       27,
			"IDGruppoQuota": -1,
		},
	}
	events := []map[string]interface{}{}
	for addr, payload := range urls {
		dataStr, _ := json.Marshal(payload)
		endpoint := strings.Split(addr, "-")[1]
		req, err := http.NewRequest("POST", endpoint, bytes.NewBuffer(dataStr))
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		req.Header.Add("accept", "application/json, text/plain, */*")
		req.Header.Add("origin", "https://web.betin.co.ke")
		req.Header.Add("user-agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
		req.Header.Add("content-type", "application/json;charset=UTF-8")
		req.Header.Add("referer", "https://web.betin.co.ke/Sport/OddsToday.aspx?IDSport=22")
		// req.Header.Add("accept-encoding", "gzip, deflate, br")
		req.Header.Add("accept-language", "en-US,en;q=0.8")
		req.Header.Add("cache-control", "no-cache")

		for _, cookie := range cookies {
			req.AddCookie(cookie)
		}

		// fmt.Println(req.Header.Get("Cookie"))
		client := http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			utils.Error(err)
			continue
		}
		respData := map[string]interface{}{}

		data, _ := ioutil.ReadAll(resp.Body)
		betin.addUsage(len(data))
		// ioutil.WriteFile("out.json", data, 0777)
		// return nil
		if err := json.Unmarshal(data, &respData); err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		parsedData := respData["d"]

		switch d := parsedData.(type) {
		case []interface{}:
			tmp := map[string]interface{}{}
			for _, _i := range d {
				i := _i.(map[string]interface{})
				if int(i["IDSport"].(float64)) == 27 {
					tmp = i
					break
				}

			}
			if _, ok := tmp["Detail"]; ok {
				if tmp["Detail"] != nil {
					for _, t := range tmp["Detail"].(map[string]interface{})["SottoEventiList"].([]interface{}) {
						events = append(events, t.(map[string]interface{}))
					}
				}
			}
		case map[string]interface{}:

			for _, tmp := range d["SottoEventiList"].([]interface{}) {
				events = append(events, tmp.(map[string]interface{}))
			}

		}

	}

	tmpGames := map[int]*Game{}
	for _, event := range events {
		gameID := int(event["IDSottoEvento"].(float64))
		league := event["Evento"].(string)

		teams := strings.Split(event["SottoEvento"].(string), " - ")
		// if strings.Contains(event["SottoEvento"].(string), "Stadl") {
		// 	utils.Debug(event["SottoEvento"].(string), teams)
		// 	return nil
		// }

		if _, ok := tmpGames[gameID]; !ok {
			game := Game{}
			game.League = league
			game.Site = betin.Name()
			game.GameID = fmt.Sprintf("%d", gameID)
			gameLink := fmt.Sprintf("https://web.betin.co.ke/Sport/SubEventOdds.aspx?SubEventID=%d", gameID)
			// utils.Debug(gameLink)
			if len(teams) < 2 {
				utils.Info(event["SottoEvento"].(string))
				continue
			}
			game.Home = strings.Trim(teams[0], " ")
			game.Away = strings.Trim(teams[1], " ")

			game.Time = utils.DecodeTime(betin.name, fmt.Sprintf("%s %s", event["StrDataInizioBreve"].(string), event["StrOraInizio"].(string)))
			// utils.Debug(game.Time)
			game.TWAY = ThreeWay{Link: gameLink, APILink: game.GameID}

			tmpGames[gameID] = &game
		}
		markets := event["Quote"].([]interface{})
		for _, _market := range markets {
			market := _market.(map[string]interface{})
			odd := market["QuotaValore"].(float64)
			// spec := market["StrHND"].(string)
			switch market["TipoQuotaBreve"].(string) {
			case "1":
				tmpGames[gameID].TWAY.Home = odd
			case "X":
				tmpGames[gameID].TWAY.Draw = odd
			case "2":
				tmpGames[gameID].TWAY.Away = odd

			}
		}

	}
	for _, v := range tmpGames {
		if isStopped() {
			return
		}

		games <- *v
	}
	if isStopped() {
		return
	}

}
func (betin *BetIn) GetRugbyGame(t *http.Transport, gameLink string, apiLink string) Game {
	games := make(chan Game)
	stop := make(chan struct{})
	go func() {
		betin.GetRugby(t, games, stop)
	}()

	for game := range games {
		if game.TWAY.APILink == apiLink {
			stop <- struct{}{}
			return game
		}
	}

	return Game{}
}

func (betin *BetIn) GetIceHockey(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {

		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	// matches := []Match{}
	if err := betin.navigator.Open("https://web.betin.co.ke/Sport/OddsToday.aspx?IDSport=27"); err != nil {
		utils.Error("Betin error:", err.Error())
		if isStopped() {
			return
		}

		return
	}
	cookies := betin.navigator.SiteCookies()

	urls := map[string]interface{}{
		//tw,dc,ou25
		"tw-https://web.betin.co.ke/Controls/ControlsWS.asmx/OddsTodayFullEvent": map[string]int{
			"IDSport":       23,
			"IDGruppoQuota": -1,
		},
		"gg-https://web.betin.co.ke/Controls/ControlsWS.asmx/OddsTodayDetails": map[string]int{
			"IDSport":       23,
			"IDGruppoQuota": 1042,
		},
		//dnb
		"dnb-https://web.betin.co.ke/Controls/ControlsWS.asmx/OddsTodayDetails": map[string]int{
			"IDSport":       23,
			"IDGruppoQuota": 1006,
		},
	}
	events := []map[string]interface{}{}
	for addr, payload := range urls {
		dataStr, _ := json.Marshal(payload)
		endpoint := strings.Split(addr, "-")[1]
		req, err := http.NewRequest("POST", endpoint, bytes.NewBuffer(dataStr))
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		req.Header.Add("accept", "application/json, text/plain, */*")
		req.Header.Add("origin", "https://web.betin.co.ke")
		req.Header.Add("user-agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
		req.Header.Add("content-type", "application/json;charset=UTF-8")
		req.Header.Add("referer", "https://web.betin.co.ke/Sport/OddsToday.aspx?IDSport=22")
		// req.Header.Add("accept-encoding", "gzip, deflate, br")
		req.Header.Add("accept-language", "en-US,en;q=0.8")
		req.Header.Add("cache-control", "no-cache")

		for _, cookie := range cookies {
			req.AddCookie(cookie)
		}

		// fmt.Println(req.Header.Get("Cookie"))
		client := http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			utils.Error(err)
			continue
		}
		respData := map[string]interface{}{}

		data, _ := ioutil.ReadAll(resp.Body)
		betin.addUsage(len(data))
		// ioutil.WriteFile("out.json", data, 0777)
		// return nil
		if err := json.Unmarshal(data, &respData); err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		parsedData := respData["d"]

		switch d := parsedData.(type) {
		case []interface{}:
			tmp := map[string]interface{}{}
			for _, _i := range d {
				i := _i.(map[string]interface{})
				if int(i["IDSport"].(float64)) == 23 {
					tmp = i
					break
				}

			}
			if _, ok := tmp["Detail"]; ok {
				if tmp["Detail"] != nil {
					for _, t := range tmp["Detail"].(map[string]interface{})["SottoEventiList"].([]interface{}) {
						events = append(events, t.(map[string]interface{}))
					}
				}
			}
		case map[string]interface{}:

			for _, tmp := range d["SottoEventiList"].([]interface{}) {
				events = append(events, tmp.(map[string]interface{}))
			}

		}

	}

	tmpGames := map[int]*Game{}
	for _, event := range events {
		gameID := int(event["IDSottoEvento"].(float64))
		league := event["Evento"].(string)

		teams := strings.Split(event["SottoEvento"].(string), " - ")
		// if strings.Contains(event["SottoEvento"].(string), "Stadl") {
		// 	utils.Debug(event["SottoEvento"].(string), teams)
		// 	return nil
		// }

		if _, ok := tmpGames[gameID]; !ok {
			game := Game{}
			game.League = league
			game.Site = betin.Name()
			game.GameID = fmt.Sprintf("%d", gameID)
			gameLink := fmt.Sprintf("https://web.betin.co.ke/Sport/SubEventOdds.aspx?SubEventID=%d", gameID)
			// utils.Debug(gameLink)
			if len(teams) < 2 {
				utils.Info(event["SottoEvento"].(string))
				continue
			}
			game.Home = strings.Trim(teams[0], " ")
			game.Away = strings.Trim(teams[1], " ")

			game.Time = utils.DecodeTime(betin.name, fmt.Sprintf("%s %s", event["StrDataInizioBreve"].(string), event["StrOraInizio"].(string)))
			// utils.Debug(game.Time)
			game.TWAY = ThreeWay{Link: gameLink, APILink: game.GameID}

			tmpGames[gameID] = &game
		}
		markets := event["Quote"].([]interface{})
		for _, _market := range markets {
			market := _market.(map[string]interface{})
			odd := market["QuotaValore"].(float64)
			// spec := market["StrHND"].(string)
			switch market["TipoQuotaBreve"].(string) {
			case "1":
				tmpGames[gameID].TWAY.Home = odd
			case "X":
				tmpGames[gameID].TWAY.Draw = odd
			case "2":
				tmpGames[gameID].TWAY.Away = odd
			case "1X DC":
				tmpGames[gameID].DC.Home = odd
			case "12 DC":
				tmpGames[gameID].DC.HomeOrAway = odd
			case "X2 DC":
				tmpGames[gameID].DC.Away = odd
			case "1 DNB":
				tmpGames[gameID].DNB.Home = odd
			case "2 DNB":
				tmpGames[gameID].DNB.Away = odd
			}
		}

	}
	for _, v := range tmpGames {
		if isStopped() {
			return
		}

		games <- *v
	}
	if isStopped() {
		return
	}

}
func (betin *BetIn) GetIceHockeyGame(t *http.Transport, gameLink string, apiLink string) Game {
	games := make(chan Game)
	stop := make(chan struct{})
	go func() {
		betin.GetIceHockey(t, games, stop)
	}()

	for game := range games {
		if game.TWAY.APILink == apiLink {
			stop <- struct{}{}
			return game
		}
	}

	return Game{}
}

func (betin *BetIn) GetHandball(transport *http.Transport, games chan Game, stop chan struct{}) {
	defer func() { close(games) }()
	var stopped bool
	lock := sync.Mutex{}
	isStopped := func() bool {

		lock.Lock()
		defer lock.Unlock()
		return stopped
	}
	_stop := func() {
		lock.Lock()
		defer lock.Unlock()
		stopped = true
	}

	go func() {
		select {
		case <-stop:
			_stop()
		}
	}()
	// matches := []Match{}
	if err := betin.navigator.Open("https://web.betin.co.ke/Sport/OddsToday.aspx?IDSport=27"); err != nil {
		utils.Error("Betin error:", err.Error())
		if isStopped() {
			return
		}

		return
	}
	cookies := betin.navigator.SiteCookies()

	urls := map[string]interface{}{
		//tw,dc,ou25
		"tw-https://web.betin.co.ke/Controls/ControlsWS.asmx/OddsTodayFullEvent": map[string]int{
			"IDSport":       24,
			"IDGruppoQuota": -1,
		},
		"dnb-https://web.betin.co.ke/Controls/ControlsWS.asmx/OddsTodayDetails": map[string]int{
			"IDSport":       24,
			"IDGruppoQuota": 1042,
		},
		//dnb
		"dc-https://web.betin.co.ke/Controls/ControlsWS.asmx/OddsTodayDetails": map[string]int{
			"IDSport":       24,
			"IDGruppoQuota": 1006,
		},
	}
	events := []map[string]interface{}{}
	for addr, payload := range urls {
		dataStr, _ := json.Marshal(payload)
		endpoint := strings.Split(addr, "-")[1]
		req, err := http.NewRequest("POST", endpoint, bytes.NewBuffer(dataStr))
		if err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}
		req.Header.Add("accept", "application/json, text/plain, */*")
		req.Header.Add("origin", "https://web.betin.co.ke")
		req.Header.Add("user-agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
		req.Header.Add("content-type", "application/json;charset=UTF-8")
		req.Header.Add("referer", "https://web.betin.co.ke/Sport/OddsToday.aspx?IDSport=22")
		// req.Header.Add("accept-encoding", "gzip, deflate, br")
		req.Header.Add("accept-language", "en-US,en;q=0.8")
		req.Header.Add("cache-control", "no-cache")

		for _, cookie := range cookies {
			req.AddCookie(cookie)
		}

		// fmt.Println(req.Header.Get("Cookie"))
		client := http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			utils.Error(err)
			continue
		}
		respData := map[string]interface{}{}

		data, _ := ioutil.ReadAll(resp.Body)
		betin.addUsage(len(data))
		// ioutil.WriteFile("out.json", data, 0777)
		// return nil
		if err := json.Unmarshal(data, &respData); err != nil {
			utils.Error(err)
			if isStopped() {
				return
			}

			return
		}

		parsedData := respData["d"]

		switch d := parsedData.(type) {
		case []interface{}:
			tmp := map[string]interface{}{}
			for _, _i := range d {
				i := _i.(map[string]interface{})
				if int(i["IDSport"].(float64)) == 24 {
					tmp = i
					break
				}

			}
			if _, ok := tmp["Detail"]; ok {
				if tmp["Detail"] != nil {
					for _, t := range tmp["Detail"].(map[string]interface{})["SottoEventiList"].([]interface{}) {
						events = append(events, t.(map[string]interface{}))
					}
				}
			}
		case map[string]interface{}:

			for _, tmp := range d["SottoEventiList"].([]interface{}) {
				events = append(events, tmp.(map[string]interface{}))
			}

		}

	}

	tmpGames := map[int]*Game{}
	for _, event := range events {
		gameID := int(event["IDSottoEvento"].(float64))
		league := event["Evento"].(string)

		teams := strings.Split(event["SottoEvento"].(string), " - ")
		// if strings.Contains(event["SottoEvento"].(string), "Stadl") {
		// 	utils.Debug(event["SottoEvento"].(string), teams)
		// 	return nil
		// }

		if _, ok := tmpGames[gameID]; !ok {
			game := Game{}
			game.League = league
			game.Site = betin.Name()
			game.GameID = fmt.Sprintf("%d", gameID)
			gameLink := fmt.Sprintf("https://web.betin.co.ke/Sport/SubEventOdds.aspx?SubEventID=%d", gameID)
			// utils.Debug(gameLink)
			if len(teams) < 2 {
				utils.Info(event["SottoEvento"].(string))
				continue
			}
			game.Home = strings.Trim(teams[0], " ")
			game.Away = strings.Trim(teams[1], " ")

			game.Time = utils.DecodeTime(betin.name, fmt.Sprintf("%s %s", event["StrDataInizioBreve"].(string), event["StrOraInizio"].(string)))
			// utils.Debug(game.Time)
			game.TWAY = ThreeWay{Link: gameLink, APILink: game.GameID}

			tmpGames[gameID] = &game
		}
		markets := event["Quote"].([]interface{})
		for _, _market := range markets {
			market := _market.(map[string]interface{})
			odd := market["QuotaValore"].(float64)
			// spec := market["StrHND"].(string)
			switch market["TipoQuotaBreve"].(string) {
			case "1":
				tmpGames[gameID].TWAY.Home = odd
			case "X":
				tmpGames[gameID].TWAY.Draw = odd
			case "2":
				tmpGames[gameID].TWAY.Away = odd
			case "1X DC":
				tmpGames[gameID].DC.Home = odd
			case "12 DC":
				tmpGames[gameID].DC.HomeOrAway = odd
			case "X2 DC":
				tmpGames[gameID].DC.Away = odd
			case "1 DNB":
				tmpGames[gameID].DNB.Home = odd
			case "2 DNB":
				tmpGames[gameID].DNB.Away = odd
			}
		}

	}
	for _, v := range tmpGames {
		if isStopped() {
			return
		}

		games <- *v
	}
	if isStopped() {
		return
	}

}
func (betin *BetIn) GetHandballGame(t *http.Transport, gameLink string, apiLink string) Game {
	games := make(chan Game)
	stop := make(chan struct{})
	go func() {
		betin.GetHandball(t, games, stop)
	}()

	for game := range games {
		if game.TWAY.APILink == apiLink {
			stop <- struct{}{}
			return game
		}
	}

	return Game{}
}
