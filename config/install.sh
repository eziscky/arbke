#!/usr/bin/bash

sudo cp $GOPATH/src/gitlab.com/arbke/config/arb.service /etc/systemd/system
sudo cp $GOPATH/src/gitlab.com/arbke/config/arb_prem.service /etc/systemd/system

sudo cp $GOPATH/src/gitlab.com/arbke/config/prem_build.sh $GOPATH/bin
sudo cp $GOPATH/src/gitlab.com/arbke/config/arbprem_start.sh $GOPATH/bin
sudo cp $GOPATH/src/gitlab.com/arbke/config/arb_build.sh $GOPATH/bin
sudo cp $GOPATH/src/gitlab.com/arbke/config/arb_start.sh $GOPATH/bin

sudo chmod +x $GOPATH/src/gitlab.com/arbke/config/prem_build.sh
sudo chmod +x $GOPATH/src/gitlab.com/arbke/config/arbprem_start.sh
sudo chmod +x $GOPATH/src/gitlab.com/arbke/config/arb_build.sh
sudo chmod +x $GOPATH/src/gitlab.com/arbke/config/arb_start.sh

sudo systemctl daemon-reload
sudo systemctl enable arb
sudo systemctl enable arb_prem

sudo systemctl start arb
sudo systemctl start arb_prem


