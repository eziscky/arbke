package models

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/arbke/utils"
	r "gopkg.in/gorethink/gorethink.v3"
)

var (
	DB_NAME = "arb_premium"
)

type DB struct {
	*r.Session
}

type User struct {
	PhoneNumber string `gorethink:"phone_number"`
	ChatID      int64  `gorethink:"chat_id"`
	GroupID     int    `gorethink:"group_id"`

	ActiveGeneral bool `gorethink:"active_general"`
	ActivePremium bool `gorethink:"active_premium"`
	ActiveBonus   bool `gorethink:"active_bonus"`
	ActiveOneOff  bool `gorethink:"active_oneoff"`

	BonusTime   int64 `gorethink:"bonus_time"`   //unix secs
	PremiumTime int64 `gorethink:"premium_time"` //unix secs
	GeneralTime int64 `gorethink:"general_time"` //unix secs
	JoinTime    int64 `gorethink:"join_time"`    //unix secs

	FName   string  `gorethink:"fname"`
	LName   string  `gorethink:"lname"`
	Balance float64 `gorethink:"balance"`

	Fixed        bool `gorethink:"fixed"`
	FixedPremium bool `gorethink:"fixed_premium"`

	Stake        float64 `gorethink:"stake"`
	HighestStake float64 `gorethink:"highest_stake"`
	NumStakes    float64 `gorethink:"num_stakes"`
}

func (user *User) ToInfo() string {
	msg := "NAME: %s\nPHONE: %s\nPAID: %s\nEXPIRY: %s\nFORMAT: %s\nGROUP: %d"
	paid := "NO"
	expiry := ""
	format := "ARBS"

	if user.ActiveGeneral {
		paid = "YES"
		expiry = time.Unix(user.GeneralTime, 0).Format(time.ANSIC)
	}
	if user.ActivePremium {
		if (user.PremiumTime - time.Now().Unix()) <= (24 * 60 * 60) {
			paid = "BONUS"
		} else {
			paid = "YES"
			expiry = time.Unix(user.PremiumTime, 0).Format(time.ANSIC)
		}
	}

	if user.Fixed {
		format = "SURETIP"
	}
	if user.FixedPremium {
		format = "BOTH"
	}
	return fmt.Sprintf(msg, user.FName, user.PhoneNumber, paid, expiry, format, user.GroupID)
}

func AddUser(chatId int64, phoneNumber, fname string, query *Query) (bool, *User, error) {
	// return nil
	user := User{
		PhoneNumber: strings.Replace(phoneNumber, "+", "", -1),
		ChatID:      chatId,
		FName:       fname,
	}

	if u := GetUser(phoneNumber, query); u != nil {
		u.ChatID = chatId
		u.FName = fname
		return false, u, UpdateUser(u, query)
	}

	return true, &user, query.Insert("users", user)
}

func GetUsersGroup(groupID int, query *Query) []User {
	users := []User{}
	query.GetMany("users", map[string]interface{}{
		"group_id": groupID,
	}, &users)
	return users
}

//GetUserChatID
func GetUserChatID(query *Query, chatID int64) *User {
	user := User{}
	if err := query.GetOne("users", map[string]interface{}{
		"chat_id": chatID,
	}, &user); err != nil {
		utils.Error(err)
		return nil
	}

	return &user
}
func GetUsersFilter(query *Query, filter map[string]interface{}) ([]User, error) {
	users := []User{}
	if err := query.GetMany("users", filter, &users); err != nil {
		utils.Error(err)
		return users, err

	}
	return users, nil
}
func GetUser(phoneNumber string, query *Query) *User {
	user := User{}
	if err := query.GetOne("users", map[string]interface{}{
		"phone_number": phoneNumber,
	}, &user); err != nil {
		utils.Info(err)
		return nil
	}

	return &user
}

func UpdateUser(user *User, query *Query) error {
	return query.Update("users", map[string]interface{}{
		"phone_number": user.PhoneNumber,
	}, user)
}

type Transaction struct {
	PhoneNumber   string  `gorethink:"phone_number"`
	TransactionID string  `gorethink:"transaction_id"`
	ProviderTime  string  `gorethink:"saf_time"`
	Provider      string  `gorethink:"provider"`
	MpesaCode     string  `gorethink:"mpesa_code"`
	Time          string  `gorethink:"time"`
	Method        string  `gorethink:"method"`
	Amount        float64 `gorethink:"amount"`
	Confirmed     bool    `gorethink:"confirmed"`
}

func AddTransaction(trans Transaction, query *Query) error {
	return query.Insert("transactions", trans)
}
func GetTransaction(id string, query *Query) *Transaction {
	trans := Transaction{}
	if err := query.GetOne("transactions", map[string]interface{}{
		"transaction_id": id,
	}, &trans); err != nil {
		utils.Error(err)
		return nil
	}
	return &trans
}

func UpdateTransaction(trans *Transaction, query *Query) error {
	return query.Update("transactions", map[string]interface{}{
		"transaction_id": trans.TransactionID,
	}, trans)
}
