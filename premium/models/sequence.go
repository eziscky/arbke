package models

import (
	"gitlab.com/arbke/utils"
	"gopkg.in/mgo.v2/bson"
)

var (
	SEQUENCE = "arb_group_sequence"
)

type Sequence struct {
	Name         string `gorethink:"name"`
	Number       int    `gorethink:"number"`
	dbCollection *DB
	query        *Query
}

func (sequence *Sequence) InitiateTransaction(db *DB) {
	sequence.query = NewQuery(db)

}

func (sequence *Sequence) Create(name string) error {
	tmp := &Sequence{
		Name:   name,
		Number: -1,
	}

	return sequence.query.Insert(SEQUENCE, *tmp)
}

func (sequence *Sequence) Exists(name string) bool {
	var tmp Sequence
	if err := sequence.query.GetOne(SEQUENCE, bson.M{"name": name}, &tmp); err != nil {
		return false
	}
	return true
}
func (sequence *Sequence) Get(name string) (int, error) {
	var tmp Sequence
	if err := sequence.query.GetOne(SEQUENCE, bson.M{"name": name}, &tmp); err != nil {
		return -1, err
	}
	return sequence.Number, nil
}
func (sequence *Sequence) GetAndIncreament(name string) (int, error) {
	var tmp Sequence
	if err := sequence.query.GetOne(SEQUENCE, bson.M{"name": name}, &tmp); err != nil {
		return -1, err
	}
	tmpdb := sequence.dbCollection
	tmpquery := sequence.query

	sequence = &tmp
	sequence.dbCollection = tmpdb
	sequence.query = tmpquery
	sequence.Number = tmp.Number + 1
	return sequence.Number, sequence.query.Update(SEQUENCE, bson.M{"name": name}, sequence)
}
func (sequence *Sequence) Set(name string, num int) (int, error) {
	var tmp Sequence
	if err := sequence.query.GetOne(SEQUENCE, bson.M{"name": name}, &tmp); err != nil {
		return -1, err
	}
	tmpdb := sequence.dbCollection
	tmpquery := sequence.query

	sequence = &tmp
	sequence.dbCollection = tmpdb
	sequence.query = tmpquery
	sequence.Number = num
	return sequence.Number, sequence.query.Update(SEQUENCE, bson.M{"name": name}, sequence)
}
func generateSequence(name string, dbSession *DB) int {
	seq := new(Sequence)
	seq.InitiateTransaction(dbSession)
	if !seq.Exists(name) {
		utils.Debug("Creating sequence")
		if err := seq.Create(name); err != nil {
			utils.Error(err)
		}
	}
	num, err := seq.GetAndIncreament(name)
	if err != nil {
		utils.Error(err)
	}
	return num
}

func setSequence(name string, num int, dbSession *DB) int {
	seq := new(Sequence)
	seq.InitiateTransaction(dbSession)
	if !seq.Exists(name) {
		utils.Debug("Creating sequence")
		if err := seq.Create(name); err != nil {
			utils.Error(err)
		}
	}
	num, err := seq.Set(name, num)
	if err != nil {
		utils.Error(err)
	}
	return num
}

func getSequence(name string, dbSession *DB) int {
	seq := new(Sequence)
	seq.InitiateTransaction(dbSession)
	num, err := seq.Get(name)
	if err != nil {
		utils.Error(err)
	}
	return num
}
