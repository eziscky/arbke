package models

import "gitlab.com/arbke/utils"

type GroupStats struct {
	GroupID    int   `gorethink:"id"`
	NumArbs    int   `gorethink:"num_arbs"`
	LastArb    int64 `gorethink:"last_arb"`
	ArbCyc     int64 `gorethink:"arb_cycles"`
	ArbCycT    int64 `gorethink:"arb_cycle_totals"`
	ArbLatency int64 `gorethink:"arb_latency"`
}

type GroupSelect struct {
	GroupID int     `gorethink:"id"`
	Total   float64 `gorethink:"total"`
}

func registerGSel(gs GroupSelect, query *Query) error {
	return query.Insert("group_select", gs)
}
func UpdateGSel(gs GroupSelect, query *Query) error {
	_gs := getGSel(gs.GroupID, query)
	if _gs == nil {
		err := registerGSel(gs, query)
		if err != nil {
			utils.Error(err)
		}
	}
	return query.Update("group_select", map[string]interface{}{
		"id": gs.GroupID,
	}, gs)
}
func GetAGSel(query *Query) map[int]float64 {
	gs := []GroupSelect{}
	query.GetMany("group_select", map[string]interface{}{}, &gs)
	data := map[int]float64{}
	for _, g := range gs {
		data[g.GroupID] = g.Total
	}
	return data
}

func getGSel(id int, query *Query) *GroupSelect {
	gs := &GroupSelect{}
	if err := query.GetOne("group_select", map[string]interface{}{
		"id": id,
	}, gs); err != nil {
		utils.Error(err)
		gs = nil
	}
	return gs
}
func RegisterGroupStats(gs GroupStats, query *Query) error {
	_gs := GetGroupStats(gs.GroupID, query)
	if _gs != nil {
		return UpdateGroupStats(*_gs, query)
	}
	return query.Insert("group_stats", gs)
}

func GetGroupStats(id int, query *Query) *GroupStats {
	gs := &GroupStats{}
	if err := query.GetOne("group_stats", map[string]interface{}{
		"id": id,
	}, gs); err != nil {
		gs = nil
	}
	return gs
}
func UpdateGroupStats(gs GroupStats, query *Query) error {
	return query.Update("group_stats", map[string]interface{}{
		"id": gs.GroupID,
	}, gs)
}
