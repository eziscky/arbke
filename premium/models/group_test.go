package models

import (
	"log"
	"os"
	"testing"

	"gitlab.com/arbke/utils"
	r "gopkg.in/gorethink/gorethink.v3"
)

func connectDB() *DB {

	addr := os.Getenv("arb_db")
	if len(addr) == 0 {
		addr = "127.0.0.1:28015"
	}
	session, err := r.Connect(r.ConnectOpts{
		Address:  addr,
		Username: "admin",
		Password: "nitrade.nitride",
	})
	if err != nil {
		log.Panic(err)
	}
	c, err := r.DBList().Run(session)
	if err != nil {
		log.Fatal(err)
	}
	defer c.Close()
	var db string
	var create int
	for c.Next(&db) {
		utils.Info(db)
		if db == DB_NAME {
			create = -1
		}
	}
	if create >= 0 {
		utils.Info("Creating")
		_, err = r.DBCreate(DB_NAME).RunWrite(session)
	}
	utils.Debug("DB:", addr)
	return &DB{Session: session}
}
func TestGS(t *testing.T) {

	db := connectDB()
	q := NewQuery(db)

	Setup(db)
	utils.Error(UpdateGSel(GroupSelect{
		Total:   10,
		GroupID: 1,
	}, q))

	utils.Error(RegisterGroupStats(GroupStats{
		GroupID: 2,
	}, q))
}
