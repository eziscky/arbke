package bot

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	"crypto/tls"
	"strconv"

	"gitlab.com/arbke/premium/models"
	"gitlab.com/arbke/utils"
)

/*
Paybill: 961700
Account: 13544
*/

//Payment provider utilizing the lipisha API

var (
	LIPISHA_KEY      = "f67fd16d52d77cda29fbbf8e0b8fdcdc"
	LIPISHA_SIG      = "SA8aXXl1VJa/WSmOxXlRHLk/lGltdRHClYItPtiCoSB3o6qr2OehrSGPfS9vN1DtJvFlG2VBPjNvH8QEg/6/rZ6EHLlwKhIKPWRcbEUj2w+500H5Mvr0VrKd5H7qOnVFbZe+BKKXrWwc7bLMpY6Zu0Cg9zA/HE+dcOxoG+KGB8w="
	LIPISHA_ENDPOINT = "https://api.lipisha.com/v2/"

	LIPISHA_TEST_KEY      = "0975482db78b49bb45899f7a5636bcce"
	LIPISHA_TEST_SIG      = "cud067UVo1kLIFmnCCF2ocemr4Eewn/OwjxH4lhKubINXkneQHatKYO+af5A/W/s1a8hJsXcrR5K2JNPv/6wmpP4Hx6Rpqyj4+uWX4yA8hFoqRTyODxvaJsfl9Ek38DiRKtN/xHDEwIcv7hsM0D9LexbMxmdG2LCWg71crDUoYQ="
	LIPISHA_TEST_ENDPOINT = "http://developer.lipisha.com/index.php/v2/api/"
)

type Lipisha struct {
	query     *models.Query
	apiKey    string
	apiSig    string
	endpoint  string
	accountNo string
}

type lipishaResponse map[string]interface{}

func NewLipisha(db *models.DB) *Lipisha {
	return &Lipisha{
		query:     models.NewQuery(db),
		apiKey:    LIPISHA_KEY,
		apiSig:    LIPISHA_SIG,
		endpoint:  LIPISHA_ENDPOINT,
		accountNo: "13544",
	}
}
func NewTestLipisha(db *models.DB) *Lipisha {
	return &Lipisha{
		query:     models.NewQuery(db),
		apiKey:    LIPISHA_TEST_KEY,
		apiSig:    LIPISHA_TEST_SIG,
		endpoint:  LIPISHA_TEST_ENDPOINT,
		accountNo: "05419",
	}
}

func (l *Lipisha) Bill(phoneNumber, ref string, amount float64) error {
	data := map[string]string{
		"api_key":        l.apiKey,
		"api_signature":  l.apiSig,
		"account_number": l.accountNo,
		"mobile_number":  phoneNumber,
		"method":         "Paybill (M-Pesa)",
		"amount":         "10.0",
		"currency":       "KES",
		"reference":      ref,
	}

	resp, err := l.makeRequest("request_money", data)
	if err != nil {
		utils.Error(err)
		return err
	}

	d, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return err
	}
	utils.Debug(string(d))
	return l.processResponse(d)
}

func (l *Lipisha) HandleInitiate(r *http.Request) []byte {
	validAmount := func(f float64) bool {
		i := int(f)
		valid := []int{250, 500, 1000}
		for _, v := range valid {
			if v == i {
				return true
			}
		}
		return false
	}
	r.ParseForm()
	for _, v := range r.Form {
		utils.Debug(v)
	}

	amount, _ := strconv.ParseFloat(r.FormValue("transaction_amount"), 64)

	user := models.GetUser(r.FormValue("transaction_mobile"), l.query)

	msg := ""
	status := "SUCCESS"
	desc := "Transaction received successfully."
	action := "ACCEPT"
	code := "001"
	reason := "VALID_TRANSACTION"

	utils.Debug(amount, user)
	resp := map[string]interface{}{}
	if user == nil || !validAmount(amount) { //reject payment
		// status = "FAIL"
		// desc = "Transaction rejected"
		// action = "REJECT"
		// code = "002"
		// reason = "INVALID_TRANSACTION"

		resp = map[string]interface{}{
			"api_key":                        l.apiKey,
			"api_signature":                  l.apiSig,
			"api_version":                    "2.0.0",
			"api_type":                       "Receipt",
			"transaction_reference":          r.FormValue("transaction_reference"),
			"transaction_status_code":        code,
			"transaction_status":             status,
			"transaction_status_description": desc,
			"transaction_status_action":      action,
			"transaction_status_reason":      reason,
			"transaction_custom_sms":         desc,
		}

	} else {
		user.FName = r.FormValue("transaction_name")
		models.UpdateUser(user, l.query)
		// models.UpdateTransaction(transaction, l.query)
		msg = fmt.Sprintf("Dear %s, your payment of KES %.2f via telegram was received.", user.FName, amount)
		utils.Debug(msg)

		if err := models.AddTransaction(models.Transaction{
			PhoneNumber:   user.PhoneNumber,
			TransactionID: r.FormValue("transaction_reference"),
			ProviderTime:  r.FormValue("transaction_date"),
			Provider:      "Lipisha",
			MpesaCode:     "",
			Time:          time.Now().Format(time.ANSIC),
			Method:        "LIPISHA",
			Amount:        amount,
		}, l.query); err != nil {
			utils.Error(err)
		}

		resp = map[string]interface{}{
			"api_key":                        l.apiKey,
			"api_signature":                  l.apiSig,
			"api_version":                    "2.0.0",
			"api_type":                       "Receipt",
			"transaction_reference":          r.FormValue("transaction_reference"),
			"transaction_status_code":        code,
			"transaction_status":             status,
			"transaction_status_description": desc,
			"transaction_status_action":      action,
			"transaction_status_reason":      reason,
			"transaction_custom_sms":         msg,
		}

	}

	payload, _ := json.Marshal(resp)
	return payload
}

func (l *Lipisha) HandleACK(r *http.Request) (bool, string, *models.User) {
	utils.Debug(*r)
	status := r.FormValue("transaction_status")
	ref := r.FormValue("transaction_reference")

	transaction := models.GetTransaction(ref, l.query)
	success := false
	switch status {
	case "Success":
		success = true

	default:
		utils.Error(status)
	}

	user := &models.User{}
	msg := ""
	if success {
		if transaction != nil {
			transaction.Confirmed = true
			utils.Error(models.UpdateTransaction(transaction, l.query))
		}
		user = models.GetUser(transaction.PhoneNumber, l.query)
		switch transaction.Amount {
		case 250.0:
			user.ActiveGeneral = true
			user.GeneralTime = time.Now().Add(168 * time.Hour).Unix()
			msg = "Welcome to the money team,you have succesfully paid for 7 days of non-stop upto 50% arbs.Watch your notifications!"
		case 500.0:
			user.ActivePremium = true
			user.PremiumTime = time.Now().Add(168 * time.Hour).Unix()
			msg = "Welcome to the money team,you have succesfully paid for 7 days of non-stop upto 150% arbs.Watch your notifications."
		case 1000.0:
			user.ActivePremium = true
			user.PremiumTime = time.Now().Add(720 * time.Hour).Unix()
			msg = "Welcome to the money team,you have succesfully paid for 30 days of non-stop unlimited arbs.Watch your notifications."
		default:
			utils.Error("Invalid payment")
			return false, status, nil
		}
	}
	utils.Debug(user)
	if err := models.UpdateUser(user, l.query); err != nil {
		utils.Error(err)
	}
	return success, msg, user
}

func (l *Lipisha) Send(ref, PhoneNumber string, amount string) error {
	data := map[string]string{
		"api_key":        l.apiKey,
		"api_signature":  l.apiSig,
		"account_number": l.accountNo,
		"mobile_number":  PhoneNumber,
		"method":         "Paybill (M-Pesa)",
		"amount":         amount,
		"currency":       "KES",
		"reference":      ref,
	}

	resp, err := l.makeRequest("send_money", data)
	if err != nil {
		utils.Error(err)
		return err
	}
	d, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.Error(err)
		return err
	}
	return l.processResponse(d)
}

func (l *Lipisha) makeRequest(route string, data map[string]string) (*http.Response, error) {
	transport := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := http.Client{Transport: transport}
	vals, _ := l.getPostVars(data)

	return client.PostForm(fmt.Sprintf("%s%s", l.endpoint, route), *vals)
}

//check http://developer.lipisha.com/index.php/app/launch/api_how_it_works
func (l *Lipisha) processResponse(r []byte) error {
	resp := lipishaResponse{}
	var err error

	err = json.Unmarshal(r, &resp)
	if err != nil {
		utils.Error(string(r), err)
		return err
	}
	utils.Debug(resp)

	var status int

	rstatus := resp["status"].(map[string]interface{})
	//due to API  inconsistency
	switch rstatus["status_code"].(type) {
	case float64:
		status = int(rstatus["status_code"].(float64))
	case string:
		status, err = strconv.Atoi(rstatus["status_code"].(string))
	}
	if err != nil {
		utils.Error(err)
		return err
	}

	if status > 0 { //not a success
		err = errors.New(rstatus["status_description"].(string))
	}

	if err != nil {
		utils.Error(err)
	}
	return err
}

func (l *Lipisha) getPostVars(data map[string]string) (*url.Values, *bytes.Buffer) {
	form := &url.Values{}
	for k, v := range data {
		form.Add(k, v)
	}
	return form, bytes.NewBufferString(form.Encode())
}
