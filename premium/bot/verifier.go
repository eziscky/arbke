package bot

import (
	"context"
	"crypto/tls"
	"net/http"
	"strings"
	"time"

	b "gitlab.com/arbke/bot"
	"gitlab.com/arbke/scraper"
	"gitlab.com/arbke/utils"
)

//verifier service gets games
type Verifier struct {
	botInstance   *Bot
	proxy         *b.Proxy
	verifierDelay time.Duration
	arbRegistry   map[string]*b.Arb
}

func NewVerifier(botInstance *Bot, verifierDelay int) *Verifier {

	return &Verifier{
		arbRegistry:   map[string]*b.Arb{},
		botInstance:   botInstance,
		proxy:         b.NewProxy(),
		verifierDelay: time.Duration(verifierDelay) * time.Second,
	}
}

func (v *Verifier) scraperResolver(site string) scraper.Scraper {
	name := site
	if strings.Contains(site, "/") {
		name = strings.Split(site, "/")[0]
	}
	switch name {
	case "1XBet":
		return scraper.NewXbet()
	case "Betin":
		return scraper.NewBetIn()
	case "Betpawa":
		return scraper.NewBetpawa()
	case "Chezacash":
		return scraper.NewChezacash()
	case "Dafabet":
		return scraper.NewDafabet()
	case "Kwikbet":
		return scraper.NewKwikBet()
	case "Betika":
		return scraper.NewBetika()
	case "Betway":
		return scraper.NewBetway()
	case "Elitebet":
		return scraper.NewElitebet()
	case "Lollybet":
		return scraper.NewLollyybet()
	case "Powerbet":
		return scraper.NewPowerbet()
	case "Sportpesa":
		return scraper.NewSpesa()
	case "Sportybet":
		return scraper.NewSportybet()
	// case "wsbetting":
	// 	return scraper.NewWSBet()
	case "Gamemania":
		return scraper.NewGMania()
	case "Mozzart":
		return scraper.NewMozzart()
	case "Odibet":
		return scraper.NewOdibet()
		////

	// case "Fake1":
	// 	return scraper.NewFake()
	// case "Fake2":
	// 	return scraper.NewFake2()
	default:
		utils.Error(name)
		return nil
	}
}

func (v *Verifier) Start(stop chan struct{}) {

	for {

		arbs := v.botInstance.GetPremiumArbsFCORE()
		utils.Debug("EXECUTING VERIFIER", len(arbs))
		aggregates := []scraper.Aggregate{}
		scraped := map[string]scraper.Game{}
		proxyAddr := ""
		for _, arb := range arbs {
			proxyAddr = arb.Proxy
			agg := scraper.Aggregate{}
			agg.Home = arb.Home
			agg.Away = arb.Away
			agg.Sport = arb.Sport
			agg.League = arb.League
			agg.Time = arb.TimeObj
			agg.TWAY = map[string]scraper.ThreeWay{}
			agg.TWOWAY = map[string]scraper.TwoWay{}
			agg.DC = map[string]scraper.DoubleChance{}
			agg.DNB = map[string]scraper.DrawNoBet{}
			agg.GNG = map[string]scraper.GoalNoGoal{}
			agg.OU15 = map[string]scraper.OU15{}
			agg.OU25 = map[string]scraper.OU25{}
			agg.OU35 = map[string]scraper.OU35{}
			agg.OU45 = map[string]scraper.OU45{}
			for _, c := range arb.Combinations {
				siteI := ""
				siteN := ""
				if val, ok := c["*Site"]; ok {
					siteI = "⚡️Site"
					siteN = val.(string)
				} else {
					siteI = "Site"
					siteN = c["Site"].(string)

				}
				siteScraper := v.scraperResolver(siteN)
				g := scraper.Game{}
				if game, ok := scraped[c["api"].(string)]; ok {
					g = game
				} else {
					g = v.scrape(siteScraper, c["link"].(string), c["api"].(string), proxyAddr, arb.Sport)
					scraped[c["api"].(string)] = g
				}

				agg.TWAY[siteN] = g.TWAY
				agg.TWOWAY[siteN] = g.TWOWAY
				agg.DC[siteN] = g.DC
				agg.DNB[siteN] = g.DNB
				agg.GNG[siteN] = g.GNG
				agg.OU15[siteN] = g.OU15
				agg.OU25[siteN] = g.OU25
				agg.OU35[siteN] = g.OU35
				agg.OU45[siteN] = g.OU45
				// i
				aggregates = append(aggregates, agg)
				utils.Info(siteI, siteN, siteScraper.Name())
				// utils.Debug(agg)
			}
		}

		b.CurrentProxy = proxyAddr

		// b.CombineMiddles(aggregates)
		newArbs, _, _ := b.Combine(aggregates, "", true)

		v.arbRegistry = b.CurrentArbs

		v.botInstance.grouper.Register(newArbs...)
		groupedArbs, ugroupedArbs := v.botInstance.grouper.GroupedArbs()
		utils.Debug(groupedArbs)

		highest := 0.0
		highestArb := b.Arb{}

		for group, arbs := range groupedArbs {
			_arbs := []b.Arb{}
			for _, arb := range arbs {
				if val, ok := v.arbRegistry[arb]; ok {
					_arbs = append(_arbs, *val)
				} else {
					utils.Error("MISSING", arb)
				}
			}
			gen, prem, promo := v.botInstance.classify(_arbs)
			if promo.Percentage > highest {
				highest = promo.Percentage
				highestArb = promo
			}
			v.botInstance.Notify(gen, prem, group)
		}

		v.botInstance.NotifyBonus(ugroupedArbs, v.arbRegistry)

		v.botInstance.NotifyValueBets()
		utils.Info(highestArb)
		// v.botInstance.PromoNotify(highestArb)
		time.Sleep(v.verifierDelay)
	}

}

func (v *Verifier) scrape(siteScraper scraper.Scraper, gameLink, apiLink, proxyAddr, sport string) scraper.Game {
	transport := &http.Transport{}

	var _p *b.ProxyServer
	if siteScraper.Name() == "Dafabet" {
		p := v.proxy.GetServer(proxyAddr)
		utils.Debug("Setting Proxy:", p.Addr.String(), "Latency(ms):", p.ResponseTime, " For: ", siteScraper.Name())
		transport = &http.Transport{
			Proxy:           http.ProxyURL(p.Addr),
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		_p = p
	}
	data := scraper.Game{}
	ctx, cf := context.WithTimeout(context.Background(), (30 * time.Second))
	ch := make(chan scraper.Game)
	// usage := 0
	name := siteScraper.Name()
	start := time.Now().UnixNano()
	failed := false
	go func() {
		switch sport {
		case "football":
			ch <- siteScraper.GetFootballGame(transport, gameLink, apiLink)
		case "basketball":
			ch <- siteScraper.GetBasketBallGame(transport, gameLink, apiLink)
		case "rugby":
			ch <- siteScraper.GetRugbyGame(transport, gameLink, apiLink)
		case "tennis":
			ch <- siteScraper.GetTennisGame(transport, gameLink, apiLink)
		case "icehockey":
			ch <- siteScraper.GetIceHockeyGame(transport, gameLink, apiLink)
		case "handball":
			ch <- siteScraper.GetHandballGame(transport, gameLink, apiLink)
		}

		// usage = site.Usage()
	}()
	select {
	case tmp := <-ch:
		data = tmp
		// num = len(data)
		if (data.TWAY.Home == 0.0) && (data.TWOWAY.Home == 0.0) {
			utils.Debug("No data on", name)
		}
		break
	case <-ctx.Done():
		utils.Error("Timeout:", name, ctx.Err().Error())
		failed = true
		// cf()
		break
	}
	cf()
	stop := time.Now().UnixNano()
	utils.Debug(name, "Latency(ms):", (stop-start)/1000000)
	if _p != nil {
		_p.ResponseTime = (stop - start)
		_p.Up = !failed
	}

	return data
}
