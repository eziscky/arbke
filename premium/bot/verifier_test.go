package bot

import (
	"testing"
	"time"

	b "gitlab.com/arbke/bot"
	"gitlab.com/arbke/premium/models"
	"gitlab.com/arbke/utils"
)

type id struct {
	current int
}

func (i *id) next() int {
	i.current++

	if i.current > 10 {
		i.current = 1
	}
	return i.current
}

func TestAssignGroups(t *testing.T) {
	utils.LogDebug, utils.LogError = true, true
	db := connectDB()
	query := models.NewQuery(db)

	models.Setup(db)
	data := []models.User{}
	criteria := []map[string]interface{}{
		map[string]interface{}{
			"active_premium": true,
		},
		map[string]interface{}{
			"active_general": true,
		},
	}
	for _, cr := range criteria {
		users, err := models.GetUsersFilter(query, cr)
		if err != nil {
			t.Fail()
			return
		}

		data = append(data, users...)
	}

	ids := &id{}
	for _, user := range data {
		if user.GroupID == 0 {
			gid := ids.next()
			utils.Debug(gid, user.ChatID)
			user.GroupID = gid
			if user.ActivePremium {
				user.PremiumTime += 2592000
			}
			if user.ActiveGeneral {
				user.GeneralTime += 604800
			}
			models.UpdateUser(&user, query)
		}
	}
}

func testFreeExtension(t *testing.T) {
	utils.LogDebug, utils.LogError = true, true
	db := connectDB()
	query := models.NewQuery(db)

	now := time.Now().Unix()
	models.Setup(db)
	extra := []int64{519777256}
	data := []models.User{}
	criteria := []map[string]interface{}{
		map[string]interface{}{
			"active_premium": true,
		},
		map[string]interface{}{
			"active_general": true,
		},
	}
	for _, cr := range criteria {
		users, err := models.GetUsersFilter(query, cr)
		if err != nil {
			t.Fail()
			return
		}
		for _, user := range users {
			if ((user.PremiumTime - now) >= 172800) || ((user.GeneralTime - now) >= 172800) {
				data = append(data, user)
			}
		}

	}
	for _, u := range extra {
		user := models.GetUserChatID(query, u)
		if user != nil {
			data = append(data, *user)
		}
	}

	// ids := &id{}
	for i, user := range data {
		utils.Debug("EXTENDING", i)
		// gid := ids.next()
		if user.ActivePremium {
			user.PremiumTime -= 950400
		}
		if user.ActiveGeneral {
			user.GeneralTime -= 604800
		}
		models.UpdateUser(&user, query)
	}
}

func testVerifier(t *testing.T) {
	bot := NewBot(nil, nil, 3)
	stop := make(chan int)
	go bot.Start()
	bot.premiumArbs = []b.Arb{
		b.Arb{
			ID:         "id1",
			Name:       "Home1vAway1",
			Home:       "Home1",
			Away:       "Away1",
			Time:       "Today1",
			Percentage: 24.2,
			Combinations: []b.Container{
				b.Container{"Site": "Fake1", "link": "link1", "api": "link1", "Outcome": "1", "Odd": 1.9, "Stake": 0.2444},
				b.Container{"Site": "Fake2", "link": "link2", "api": "link2", "Outcome": "2", "Odd": 9.9, "Stake": 0.7666},
			},
		},
	}

	utils.Debug(bot.premiumArbs)

	<-stop

}
