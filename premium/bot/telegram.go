package bot

import (
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"

	tbot "gopkg.in/tucnak/telebot.v2"

	// "github.com/yanzay/tbot"
	b "gitlab.com/arbke/bot"
	"gitlab.com/arbke/premium/models"
	"gitlab.com/arbke/utils"
)

var (
	arbServer     *tbot.Bot
	spotPesaCount int
	GEN_SUPPORT   int64 = -1001393615292
	PREM_SUPPORT  int64 = -1001133857130
	ACK_CHAN      int64 = -1001279482982
)

func NewTBOTServer(key string) *tbot.Bot {
	// tbot.New
	tb, err := tbot.NewBot(tbot.Settings{
		Token:  key,
		Poller: &tbot.LongPoller{Timeout: 10 * time.Second},
	})

	if err != nil {
		utils.Error(err)
		return nil
	}
	return tb
}

func getSpotPesaMessage() string {
	msgs := []string{advc, advc1, advc2, advc3, advc4, advc5}
	return msgs[rand.Int63n(6)]
}

func GenEncodedMessage(arbs []b.Arb) string {
	finalMsg := ""
	for _, arb := range arbs {
		sport := ""
		switch arb.Sport {
		case "football":
			sport = "⚽️"
		case "basketball":
			sport = "🏀"
		case "rugby":
			sport = "🏈"
		case "tennis":
			sport = "🎾"
		}
		msg := fmt.Sprintf("%s Game: XXXX V XXXX\n💰 Profit: %.2f%%\n Market: %s\n📈 Reliability: HIGH\n🕐 Time:%s\nCombination:\n\t", sport, arb.Percentage, arb.Market, arb.Time)
		// arb.Time
		for _, c := range arb.Combinations {
			site := ""
			// v := ""
			if _, ok := c["*Site"]; ok {
				site = "⚡️Site"
				// v = val.(string)
			} else {
				// v = c["Site"].(string)
				site = "Site"
			}
			msg += fmt.Sprintf("%s: XXXX Outcome: %s Odd: X.XX StakeSplit: 0.XXXX\n\t", site, c["Outcome"].(string))
		}
		msg += "\n\n"
		finalMsg += msg
	}

	//debug arb
	return finalMsg
}

func GenValueBetMessage(vbs []b.ValueBet) string {
	finalMsg := ""
	for _, valueBet := range vbs {
		sport := valueBet.Sport
		switch valueBet.Sport {
		case "football":
			sport = "⚽️"
		case "basketball":
			sport = "🏀"
		case "rugby":
			sport = "🏈"
		case "tennis":
			sport = "🎾"
		case "icehockey":
			sport = "🏒"
		case "handball":
			sport = "🏐"
		}
		msg := fmt.Sprintf("%s Game: %s\nMarket: %s\n🕐 Time:%s\n⬆️ Value:%.2f%%\nOutcome: %s", sport, valueBet.Name, valueBet.Market, valueBet.Time, valueBet.Percentage, valueBet.Outcome)
		msg += "\n\n"
		finalMsg += msg

	}
	return finalMsg
}
func GenSurebetMessage(arbs []b.Arb, bot *Bot) string {
	finalMsg := ""
	markets := []string{"Three", "Draw"}
	allowedMarket := func(name string) bool {
		for _, market := range markets {
			if strings.Contains(name, market) {
				return true
			}
		}
		return false
	}
	for _, arb := range arbs {
		sport := ""

		switch arb.Sport {
		case "football":
			sport = "⚽️"
		case "basketball":
			sport = "🏀"
		case "rugby":
			sport = "🏈"
		case "tennis":
			sport = "🎾"
		}
		msg := fmt.Sprintf("%s Game: %s\nMarket: %s\n🕐 Time:%s\n", sport, arb.Name, arb.Market, arb.Time)
		leastOdd := 0.0
		highestOdd := 0.0
		leastMsg := ""
		for i, c := range arb.Combinations {
			site := ""
			v := ""
			if val, ok := c["*Site"]; ok {
				site = "⚡️Site"
				v = val.(string)
			} else {
				v = c["Site"].(string)
				site = "Site"
			}
			if i == 0 {
				leastOdd = c["Odd"].(float64)
				highestOdd = c["Odd"].(float64)
				leastMsg = fmt.Sprintf("%s: %s Outcome: %s Odd: %.2f \n\t", site, v, c["Outcome"].(string), c["Odd"].(float64))
				continue
			}
			if c["Odd"].(float64) < leastOdd {
				leastOdd = c["Odd"].(float64)
				leastMsg = fmt.Sprintf(" %s: %s Outcome: %s Odd: %.2f \n\t", site, v, c["Outcome"].(string), c["Odd"].(float64))
			}

			if c["Odd"].(float64) > highestOdd {
				highestOdd = c["Odd"].(float64)
			}

		}

		//ADD LOGIC FOR O/U
		if (highestOdd - leastOdd) < 0.8 {
			utils.Debug("Skipping risky sure bet", highestOdd, leastOdd)
			continue
		}

		if !allowedMarket(arb.Market) {
			utils.Debug("Skipping SBF ", arb.Market)
			continue
		}

		if bot.Match(arb.Name, leastOdd, leastMsg) {
			continue
		}

		msg += leastMsg
		msg += "\n\n"
		finalMsg += msg
	}

	//debug arb
	return finalMsg
}

func GenMessage(arbs []b.Arb, zero bool, stake float64, groups ...int) (string, string, string) {
	finalMsg := ""
	finalMsg2 := ""
	finalMsg3 := ""

	groupStr := ""
	for _, group := range groups {
		comma := ","
		if len(groupStr) == 0 {
			comma = ""
		}
		groupStr += comma + strconv.Itoa(group)
	}

	for i, arb := range arbs {
		sport := ""
		switch arb.Sport {
		case "football":
			sport = "⚽️"
		case "basketball":
			sport = "🏀"
		case "rugby":
			sport = "🏈"
		case "tennis":
			sport = "🎾"
		case "icehockey":
			sport = "🏒"
		case "handball":
			sport = "🏐"
		}
		profit := arb.Percentage
		tpl := "%s Game %s\n💰 Profit: %.2f%%\n🏆 League: %s\nMarket: %s\n📈 Reliability: %s\n🕐 Time:%s\nCombination:\n\t"
		if stake > 0.0 {
			profit = ((profit / 100) * stake)
			tpl = "%s %s\n💰 Profit: %.2f\n🏆 League: %s\nMarket: %s\n📈 Reliability: %s\n🕐 Time:%s\nCombination:\n\t"
		}

		msg := fmt.Sprintf(tpl, sport, arb.Name, profit, arb.League, arb.Market, arb.Reliability, arb.Time)
		// arb.Time
		for _, c := range arb.Combinations {
			s := c["Stake"].(float64)
			tpl := "%s: [%s](%s) Outcome: %s Odd: %.2f StakeSplit: %.2f\n\t"
			if stake > 0.0 {
				s = s * stake
				tpl = "%s: [%s](%s) Outcome: %s Odd: %.2f Amount: %.2f\n\t"
			}
			site := ""
			v := ""
			if val, ok := c["*Site"]; ok {
				site = "⚡️Site"
				v = val.(string)
			} else {
				v = c["Site"].(string)
				site = "Site"
			}
			msg += fmt.Sprintf(tpl, site, v, c["link"].(string), c["Outcome"].(string), c["Odd"].(float64), s)
		}
		if len(groupStr) > 0 {
			msg += "\nGROUP: " + groupStr
		}
		msg += "\n\n"

		if i > 10 && i < 20 {
			finalMsg2 += msg
			continue
		}
		if i > 20 {
			finalMsg3 += msg
			continue
		}
		finalMsg += msg
	}
	if zero {
		finalMsg = "Previous arbs now obsolete!"
	}

	//debug arb
	return finalMsg, finalMsg2, finalMsg3
}

func GetMessageServer(arb *tbot.Bot, db *models.DB, feedback chan map[string]interface{}, stakeNotif chan *models.User) *tbot.Bot {
	arb.Handle("/join", func(msg *tbot.Message) {
		//TODO: check if group
		// arb.Send(msg.Sender,pricingMsg)

		replyBtn := tbot.ReplyButton{Text: "🌕 Join Now", Contact: true}
		replyKeys := [][]tbot.ReplyButton{
			[]tbot.ReplyButton{replyBtn},
			// ...
		}

		arb.Send(msg.Sender, "Click the button to join:", &tbot.ReplyMarkup{
			ReplyKeyboard: replyKeys,
		})

		// msg.RequestContactButton("Text me /help for assistance.", "Join Now", tbot.OneTimeKeyboard)
	})

	arb.Handle("/start", func(msg *tbot.Message) {

		//TODO: check if group
		arb.Send(msg.Sender, arbIntroMsg)
		time.Sleep(time.Second * 5)
		arb.Send(msg.Sender, arbIntroMsg1)
		time.Sleep(time.Second * 5)

		replyBtn := tbot.ReplyButton{Text: "Join Now", Contact: true}
		replyKeys := [][]tbot.ReplyButton{
			[]tbot.ReplyButton{replyBtn},
			// ...
		}

		arb.Send(msg.Sender, "Click the button to join:", &tbot.ReplyMarkup{
			ReplyKeyboard:       replyKeys,
			ResizeReplyKeyboard: true,
		})

		//send gif

		// arb.Send(msg.Sender,)
		// arb.Send(msg.Sender,arbIntroMsg2)
		// msg.RequestContactButton("Text me /help for assistance.\n /join to join", "Join Now", tbot.OneTimeKeyboard)

	})

	arb.Handle(tbot.OnContact, func(msg *tbot.Message) {

		query := models.NewQuery(db)
		_new, user, err := models.AddUser(int64(msg.Sender.ID), msg.Contact.PhoneNumber, msg.Contact.FirstName, query)
		if err != nil {
			utils.Error(err)
			return
		}
		if _new {

			regID := models.NextID(query, "arb_registry")
			utils.Debug("FP REGISTRATION", regID)
			if regID > 10 {
				utils.Debug("FP REGISTRATION RESETTING", regID)
				models.SetID(query, "arb_registry", 0)
				regID = 1
			}
			//***** FREE PREMIUM *****////
			user.ActivePremium = true
			user.GroupID = regID
			user.PremiumTime = time.Now().Add(time.Hour * 24).Unix()
			//////////////////////////////

			models.UpdateUser(user, query)
			arb.Send(msg.Sender, "https://media.giphy.com/media/xT3i0MGfzfEo54bh60/giphy.gif")
			arb.Send(msg.Sender, "Welcome to the money team! I'll be sending you arbs! \n Create accounts in: Sportpesa,Betin,Betway,Betika,Betpawa,Dafabet,1XBet,Elitebet,Sportybet,Kwikbet and Chezacash")
			arb.Send(msg.Sender, "2 accounts if possible.")

		} else {
			arb.Send(msg.Sender, "You're already a member!")
		}
	})

	arb.Handle(tbot.OnChannelPost, func(msg *tbot.Message) {
		//Treat as query
		utils.Debug(msg.Text, msg.Chat.ID, PREM_SUPPORT, msg.IsReply())
		data := strings.Split(msg.Text, "::")
		if int64(msg.Chat.ID) == GEN_SUPPORT || int64(msg.Chat.ID) == PREM_SUPPORT { // SAFETY NET, only listen to commands from known channels
			if len(data) == 2 {
				query := data[1]
				switch data[0] {
				case "F": //SBF CLIENTS
					feedback <- map[string]interface{}{
						"admin":          true,
						"broadcast":      true,
						"chatID":         0,
						"text":           query,
						"active_premium": true,
						"active_general": true,
						"fixed":          true,
						"or":             false,
					}
				case "P": //PAYING CLIENTS
					feedback <- map[string]interface{}{
						"admin":     true,
						"broadcast": true,
						"chatID":    0,
						"text":      query,

						"active_premium": true,
						"active_general": true,
						"fixed":          false,
						"or":             false,
					}
				case "PF": //PAYING CLIENTS INCL. SBF
					feedback <- map[string]interface{}{
						"admin":          true,
						"broadcast":      true,
						"chatID":         0,
						"text":           query,
						"active_premium": true,
						"active_general": true,
						"fixed":          true,
						"or":             true,
					}
				case "N": //NON-PAYING CLIENTS
					// chatID, _ := strconv.Atoi(data[1])

					// utils.Debug(query, chatID)
					feedback <- map[string]interface{}{
						"admin":          true,
						"broadcast":      true,
						"chatID":         0,
						"text":           query,
						"active_premium": false,
						"active_general": false,
						"fixed":          false,
						"or":             false,
					}
				case "E": //EVERYONE
					// utils.Debug(query, chatID)
					feedback <- map[string]interface{}{
						"admin":          true,
						"broadcast":      true,
						"chatID":         0,
						"text":           query,
						"subs":           false,
						"active_premium": false,
						"active_general": false,
						"fixed":          false,
						"or":             true,
					}

				case "q": //QUERY CHAT ID
					chatID, err := strconv.Atoi(query)
					if err == nil {
						feedback <- map[string]interface{}{
							"admin":     true,
							"broadcast": false,
							"chatID":    int64(chatID),
							"text":      query,
							"query":     true,
						}
						return
					}
				case "t": //TOGGLE FORMAT
					chatID, err := strconv.Atoi(query)
					if err == nil {
						feedback <- map[string]interface{}{
							"admin":     true,
							"broadcast": false,
							"chatID":    int64(chatID),
							"text":      query,
							"toggle":    true,
						}
						return
					}

				case "AG": //ADD GENERAL
					dets := strings.Split(data[1], ";")
					if len(dets) != 3 {
						break
					}
					chatID, err := strconv.Atoi(dets[0])
					if err == nil {
						feedback <- map[string]interface{}{
							"admin":     true,
							"broadcast": false,
							"chatID":    int64(chatID),
							"name":      dets[1],
							"phone":     strings.Replace(dets[2], " ", "", -1),
							"add":       "AG",
						}
						return
					}
				case "AGP": //ADD ENTRY PREMIUM
					dets := strings.Split(data[1], ";")
					if len(dets) != 3 {
						break
					}
					chatID, err := strconv.Atoi(dets[0])
					if err == nil {
						feedback <- map[string]interface{}{
							"admin":     true,
							"broadcast": false,
							"chatID":    int64(chatID),
							"name":      dets[1],
							"phone":     strings.Replace(dets[2], " ", "", -1),
							"add":       "AGP",
						}
						return
					}

				case "AP": //ADD PREMIUM
					dets := strings.Split(data[1], ";")
					if len(dets) != 3 {
						break
					}
					chatID, err := strconv.Atoi(dets[0])
					if err == nil {
						feedback <- map[string]interface{}{
							"admin":     true,
							"broadcast": false,
							"chatID":    int64(chatID),
							"name":      dets[1],
							"phone":     strings.Replace(dets[2], " ", "", -1),
							"add":       "AP",
						}
						return
					}
				default: // Forwards text to specified chat id

					chatID, err := strconv.Atoi(data[0])
					if err == nil {
						query := data[1]
						utils.Debug(query, chatID)
						feedback <- map[string]interface{}{
							"admin":     true,
							"broadcast": false,
							"chatID":    int64(chatID),
							"text":      query,
						}
					}

				}

			}
			fullText := msg.Text
			if msg.IsReply() {
				data := strings.Split(msg.ReplyTo.Text, ",")
				id := strings.Split(data[0], ":")
				idStr := ""
				utils.Debug("REPLY", id, len(id))
				if len(id) == 2 {
					idStr = strings.Trim(id[1], " ")
				}
				chatID, err := strconv.Atoi(idStr)
				// utils.Error(err)
				if err == nil {
					fullText += fmt.Sprintf("\nREPLYTO: %s", idStr)
					query := msg.Text
					// utils.Debug(query, chatID)
					feedback <- map[string]interface{}{
						"admin":     true,
						"broadcast": false,
						"chatID":    int64(chatID),
						"text":      query,
					}
				}

			}
			feedback <- map[string]interface{}{
				"admin":  false,
				"chatID": int64(msg.Chat.ID),
				"text":   fullText,
			}
		}

		//MESSAGES FROM RIVAL MARKETING CHANNEL
		if int64(msg.Chat.ID) == -1001135499409 {
			if !strings.Contains(msg.Text, "@arbitragekenya_bot") {
				utils.Debug("SPOTPESA COUNT", spotPesaCount)
				spotPesaCount++
				if spotPesaCount >= 3 {
					msg := getSpotPesaMessage()
					utils.Debug("SPOTPESA ", msg)
					feedback <- map[string]interface{}{
						"admin":     true,
						"broadcast": false,
						"chatID":    int64(-1001135499409),
						"text":      msg,
					}
					spotPesaCount = 0
				}

			} else {
				spotPesaCount = 0
			}
			return
		}
	})

	arb.Handle(tbot.OnText, func(msg *tbot.Message) {
		utils.Debug(msg.Text, int64(msg.Sender.ID))
		isSub := func(user *models.User) bool {
			if user.ActivePremium {
				return true
			}
			if user.ActiveGeneral {
				return true
			}
			if user.ActiveBonus {
				return true
			}
			return false
		}
		//check if it's a user setting their stake
		if !strings.HasPrefix(msg.Text, "07") {
			stake, err := strconv.ParseFloat(msg.Text, 64)
			if err == nil {

				query := models.NewQuery(db)
				user := models.GetUserChatID(query, int64(msg.Sender.ID))

				if user != nil {
					if !isSub(user) {
						arbServer.Send(msg.Sender, pricingMsg)
						return
					}
					user.Stake = stake
					if user.Stake > user.HighestStake {
						user.HighestStake = user.Stake
					}
					user.NumStakes++
					if err := models.UpdateUser(user, query); err != nil {
						utils.Info(err)
					}

					//if there were previous arbs send them with recalculated stake
					stakeNotif <- user
					return
				}
			}
		}

		userMsg := msg.Text
		if msg.IsReply() {
			userMsg += "\nREPLY TO: " + msg.ReplyTo.Text
		}
		feedback <- map[string]interface{}{
			"admin":  false,
			"chatID": int64(msg.Sender.ID),
			"text":   userMsg,
		}
		return

	})

	arb.Handle(tbot.OnPhoto, func(msg *tbot.Message) {
		isSub := func(user *models.User) bool {
			if user.ActivePremium {
				return true
			}
			if user.ActiveGeneral {
				return true
			}
			if user.ActiveBonus {
				return true
			}
			return false
		}
		query := models.NewQuery(db)
		user := models.GetUserChatID(query, int64(msg.Sender.ID))

		if user != nil {
			if !isSub(user) {
				arbServer.Send(b.GetRecipient(GEN_SUPPORT), msg.Photo)
				arbServer.Send(b.GetRecipient(GEN_SUPPORT), fmt.Sprintf("PHOTO: %d", msg.Sender.ID))
				return
			}
			arbServer.Send(b.GetRecipient(PREM_SUPPORT), msg.Photo)
			arbServer.Send(b.GetRecipient(PREM_SUPPORT), fmt.Sprintf("PHOTO: %d", msg.Sender.ID))

		}
	})

	arb.Handle("/help", func(msg *tbot.Message) {
		arb.Send(msg.Sender, arbIntroMsg1)
		// arb.Send(msg.Sender,pricingMsg)
	})

	arb.Handle("/faq", func(msg *tbot.Message) {
		arb.Send(msg.Sender, faq)
	})

	arb.Handle("/tips", func(msg *tbot.Message) {
		arb.Send(msg.Sender, tips)
	})

	arb.Handle("/price", func(msg *tbot.Message) {
		arb.Send(msg.Sender, pricingMsg)
	})

	arb.Handle("/ziskun", func(msg *tbot.Message) {
		data := strings.Split(msg.Text, ":")
		if len(data) < 2 {
			return
		}
		chatID, _ := strconv.Atoi(data[0])
		query := data[1]
		utils.Debug(query, chatID)
		feedback <- map[string]interface{}{
			"admin":  true,
			"chatID": int64(chatID),
			"text":   query,
		}
	})
	arbServer = arb
	return arbServer
	// utils.Error(arb.ListenAndServe())

}

var (
	startMsg = `Hi,
I'm Arb bot. I'll be sending you
value bets for free.If you have
an interest in Arbitrage bets (Sure Bets)
text me /arb`

	arbIntroMsg = `
INTRO TO ARBITRAGE BETTING

Game: Mladost Lucani V Backa
DRAW NO BET 
Elitebet Home=5.95 Away=1.5
Kwikbet Home=5.7 Away=1.79

If You invest 10000 like this:
Elitebet - Home - KSH 2313
Kwikbet  - Away - KSH 7687

If home win: 2313 x 5.95 = KSH 13762
If Away win: 7687 x 1.79 = KSH 13759

Regardless of the match outcome you still
make profit = 13759-10000 = KSH 3759
`
	arbIntroMsg1 = `My format is as follows:

-------------------------
Game: Mladost Lucani V Backa
Percentage: 37.60
Market: Draw No Bet
Time:Sun May 13 20:00:00 2018
Combination:
Site: Elitebet Outcome: 1 Odd: 5.95 Stake: 0.2313
Site: Kwikbet Outcome: 2 Odd: 1.79 Stake: 0.7687
-------------------------

Investment = total amount you want to stake = KSH 10000
Profit = percentage/100 x investment = 10000 x 37.60/100 = KSH 3760	
Combination shows you how to split your cash:
Elitebet: Stake x investment = 0.2313 x 10000 = KSH 2313
Kwikbet: Stake x investment = 0.7687 x 10000 = KSH 7687

More Options:
/faq  /tips  /price  /help`

	pricingMsg = `Pricing:
Choose plan:
✔️Ksh 500 for 2 weeks upto 50% odds 
✔️Ksh 1000 monthly unlimited odds (Arbs or Sure tips) 🔥🔥
✔️Ksh 1500 monthly unlimited odds (Arbs and Sure tips at the same time).
✔️Ksh 4000 Half-Season UNLIMITED ALL SPORTS OFFER ⚡️

Pay using M-Pesa:
☑️ Send money to: 0742354792
☑️ Name: CAROLYNE
☑️ Enter amount: 500/1000/1500/4000
☑️ Send me Transaction Code or your phonenumber.
`

	faq = `1. How long are the arbs available?
	ONLY the arbs available in the last message are available.
	The arbs in the last message are available until the message
	"All previous arbs are obsolete!"

2. What does StakeSplit do?
	StakeSplit gives you the amount of money to stake on a particular
	site. Multiply StakeSplit x Investment.

3. What is the best Investment?
	I recommend 5,000 KSH and above to enjoy meaningful profits. Minimum 500 Per day guaranteed

4. Won't I lose money on one outcome?
	NO. The profit you make on any of the outcomes is more than your
	total investment. You make profit regardless.

5. When are there many arbs?
	From wednesday.
`

	tips = `1. Place bets from least amount to highest.
2. Use common sense
3. Sportpesa odds change fast, be alert.
4. Have multiple accounts.
5. Look through all odds first before placing.
6. Optimize your betting times!
7. PAY CLOSE ATTENTION
`
)
