package bot

import (
	"fmt"
	"net"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/docker/libchan/spdy"
	tbot "gopkg.in/tucnak/telebot.v2"

	b "gitlab.com/arbke/bot"
	"gitlab.com/arbke/premium/models"
	"gitlab.com/arbke/utils"
)

var (
	premium = "127.0.0.1:9323"
)

type FixedBet struct {
	Home string
	Away string
	Odd  float64
	Sent bool
	Msg  string
}
type Bot struct {
	lock sync.Mutex

	lastUpdate  int64
	generalArbs []b.Arb
	premiumArbs []b.Arb
	oneOffArbs  []b.Arb

	generalArbsV  map[int][]b.Arb
	premiumArbsV  map[int][]b.Arb
	valueBets     map[string]b.ValueBet
	verifierDelay int

	lastGroupUpdate   map[int]int64
	lastGroupObsolete map[int]int64

	grouper *Grouper

	stakeChangeChan chan *models.User

	promoPercent    float64
	encodedBetCount int
	bonusTotal      float64 //to track changes in arbs for bonus accounts

	fixedBets map[int]FixedBet
	fbi       int

	server string

	query *models.Query
}

func NewBot(db *models.DB, stakeNotif chan *models.User, verifierDelay int) *Bot {

	return &Bot{
		lock:         sync.Mutex{},
		generalArbsV: map[int][]b.Arb{},
		premiumArbsV: map[int][]b.Arb{},

		premiumArbs: []b.Arb{},
		valueBets:   map[string]b.ValueBet{},

		lastGroupUpdate:   map[int]int64{},
		lastGroupObsolete: map[int]int64{},

		query:           models.NewQuery(db),
		grouper:         NewGrouper(db),
		fixedBets:       map[int]FixedBet{},
		stakeChangeChan: stakeNotif,
		verifierDelay:   verifierDelay,
	}
}

func (bot *Bot) MessageDispatch(msg chan map[string]interface{}) {
	for txt := range msg {
		if _, err := arbServer.Send(b.GetRecipient(txt["id"].(int64)), txt["msg"].(string)); err != nil {
			utils.Error(err)
		}
	}
}

func (bot *Bot) timePassed(now, subscribed int64) bool {
	return (now >= subscribed)

}

func (bot *Bot) StakeUpdateNotifService() {
	for user := range bot.stakeChangeChan {

		arbs := []b.Arb{}
		if user.ActiveGeneral {
			arbs = bot.GetGeneralArbs(user.GroupID)
			if len(arbs) == 0 {
				continue
			}
		}
		if user.ActivePremium {
			arbs = bot.GetPremiumArbs(user.GroupID)
			if len(arbs) == 0 {
				continue
			}
		}

		msg, msg2, msg3 := GenMessage(arbs, false, user.Stake)

		if user.ActiveBonus {
			msg, msg2, msg3 = "", "", ""
			allArbs := []b.Arb{}
			i := 0
			data := map[string]string{}
			for groupID, arbs := range bot.premiumArbsV {
				for _, arb := range arbs {
					allArbs = append(allArbs, arb)
					tmp, _, _ := GenMessage([]b.Arb{arb}, false, user.Stake, groupID)
					if t, ok := data[arb.ID]; ok {
						tmp = strings.Replace(t, "\n\n", strconv.Itoa(groupID), -1)
						tmp += "\n\n"
					}
					data[arb.ID] = tmp
				}
			}

			for _, tmp := range data {
				if i > 10 && i < 20 {
					msg2 += tmp
					i++
					continue
				}
				if i > 20 {
					msg3 += tmp
					i++
					continue
				}
				msg += tmp
				i++
			}

		}

		if _, err := arbServer.Send(b.GetRecipient(user.ChatID), msg, &tbot.SendOptions{ParseMode: tbot.ModeMarkdown}); err != nil {
			utils.Error(err)
			continue
		}
		if len(msg2) > 0 {
			arbServer.Send(b.GetRecipient(user.ChatID), fmt.Sprintf("CONTINUATION:\n\n %s", msg2), &tbot.SendOptions{ParseMode: tbot.ModeMarkdown})
			if len(msg3) > 0 {
				arbServer.Send(b.GetRecipient(user.ChatID), fmt.Sprintf("CONTINUATION:\n\n %s", msg3), &tbot.SendOptions{ParseMode: tbot.ModeMarkdown})
			}
		}
		arbServer.Send(b.GetRecipient(ACK_CHAN), fmt.Sprintf("Setting Stake: %.2f For: %d", user.Stake, user.ChatID))
	}
}

func (bot *Bot) updateArbs(arbs []b.Arb, t int64, from string, gid ...int) {
	bot.lock.Lock()
	defer bot.lock.Unlock()
	if t > bot.lastUpdate {
		utils.Debug("UPDATING ARBS FROM:", from)
		bot.premiumArbs = arbs

		bot.lastUpdate = t
	}
}

func (bot *Bot) updateValueBets(vbs []b.ValueBet) {
	bot.lock.Lock()
	defer bot.lock.Unlock()

	utils.Debug("UPDATING VALUE BETS FROM ARB CORE")
	for _, valueBet := range vbs {
		if vb, ok := bot.valueBets[valueBet.ID]; ok {
			valueBet.Updated = vb.Updated
			bot.valueBets[valueBet.ID] = valueBet
			continue
		}
		bot.valueBets[valueBet.ID] = valueBet
	}

	//delete already started games
	toDelete := []string{}
	for _, valueBet := range bot.valueBets {
		if time.Now().Unix() >= valueBet.TimeObj.Unix() {
			toDelete = append(toDelete, valueBet.ID)
		}
	}
	for _, id := range toDelete {
		delete(bot.valueBets, id)
	}

}

func (bot *Bot) PromoNotify(promo b.Arb) {
	if promo.Percentage >= 30.0 {
		promoGroups := bot.getPromoGroups()
		msg := GenEncodedMessage([]b.Arb{promo})
		for _, group := range promoGroups {
			arbServer.Send(b.GetRecipient(group), fmt.Sprintf("%s\n\n @arbitragekenya_bot for instructions and more.", msg))
		}
	}

	//send encoded game

	if promo.Percentage != bot.promoPercent {
		if promo.Percentage >= 30.0 {
			nonSubs := bot.getNonSubs()
			msg := GenEncodedMessage([]b.Arb{promo})
			msg += "\n For help & support click here: /help"
			// utils.Debug(msg)
			for _, user := range nonSubs {
				time.Sleep(25 * time.Millisecond)
				utils.Info("Sending encoded to", user.ChatID)
				arbServer.Send(b.GetRecipient(user.ChatID), msg)
			}
			bot.encodedBetCount++
			if bot.encodedBetCount >= 5 {
				for _, user := range nonSubs {
					time.Sleep(25 * time.Millisecond)
					utils.Info("Sending encoded to", user.ChatID)
					arbServer.Send(b.GetRecipient(user.ChatID), pricingMsg)
				}
				bot.encodedBetCount = 0
			}
		}
	}
}

func (bot *Bot) Notify(gen, prem []b.Arb, groupID int) {
	expired := "Your subscription has expired.\n Text me /price for pricing details."
	premUsers := bot.getPremiumUsers(groupID)
	genUsers := bot.getGeneralUsers(groupID)

	///PREMIUM SUBS
	eq, z := bot.checkEqual(prem, bot.premiumArbsV[groupID], groupID)
	if !eq { //send to premium subscribers
		for _, user := range premUsers {
			msg, msg2, msg3 := "", "", ""
			if user.ActiveBonus {
				msg, msg2, msg3 = GenMessage(prem, z, user.Stake, groupID)
			} else {
				msg, msg2, msg3 = GenMessage(prem, z, user.Stake)
			}
			// utils.Debug(msg, eq, z)
			if !user.ActiveBonus {
				if bot.timePassed(time.Now().Unix(), user.PremiumTime) { //Prem expired
					user.ActivePremium = false
					user.PremiumTime = 0
					models.UpdateUser(&user, bot.query)
					arbServer.Send(b.GetRecipient(user.ChatID), expired)
					continue
				}

			}

			utils.Info("Sending to", user.ChatID)
			arbServer.Send(b.GetRecipient(user.ChatID), msg, &tbot.SendOptions{ParseMode: tbot.ModeMarkdown})
			if len(msg2) > 0 {
				arbServer.Send(b.GetRecipient(user.ChatID), fmt.Sprintf("CONTINUATION:\n\n%s", msg2), &tbot.SendOptions{ParseMode: tbot.ModeMarkdown})
				if len(msg3) > 0 {
					arbServer.Send(b.GetRecipient(user.ChatID), fmt.Sprintf("CONTINUATION:\n\n%s", msg3), &tbot.SendOptions{ParseMode: tbot.ModeMarkdown})
				}
			}
			time.Sleep(35 * time.Millisecond)
		}

	}

	//GEN SUBS
	eq, z = bot.checkEqual(gen, bot.generalArbsV[groupID], groupID)
	if !eq { //send to gen subscribers
		for _, user := range genUsers {
			msg, msg2, msg3 := GenMessage(gen, z, user.Stake)

			if bot.timePassed(time.Now().Unix(), user.GeneralTime) { //Gen expired
				user.ActiveGeneral = false
				user.GeneralTime = 0
				models.UpdateUser(&user, bot.query)
				arbServer.Send(b.GetRecipient(user.ChatID), expired)
				continue
			}
			arbServer.Send(b.GetRecipient(user.ChatID), msg, &tbot.SendOptions{ParseMode: tbot.ModeMarkdown})
			if len(msg2) > 0 {
				arbServer.Send(b.GetRecipient(user.ChatID), fmt.Sprintf("CONTINUATION:\n\n%s", msg2), &tbot.SendOptions{ParseMode: tbot.ModeMarkdown})
				if len(msg3) > 0 {

					arbServer.Send(b.GetRecipient(user.ChatID), fmt.Sprintf("CONTINUATION:\n\n%s", msg3), &tbot.SendOptions{ParseMode: tbot.ModeMarkdown})
				}
			}
			time.Sleep(35 * time.Millisecond)
		}

	}

	tmp := len(bot.premiumArbsV[groupID])

	if len(prem) > 0 {
		if len(bot.premiumArbsV[groupID]) == 0 {
			bot.lastGroupUpdate[groupID] = time.Now().Unix()
		}
	}
	bot.premiumArbsV[groupID] = prem
	bot.generalArbsV[groupID] = gen

	if z {
		bot.lastGroupObsolete[groupID] = time.Now().Unix()
		bot.grouper.RecordStat(tmp, bot.lastGroupUpdate, bot.lastGroupObsolete, groupID)
		return
	}

}

func (bot *Bot) NotifyBonus(arbs map[string][]int, arbRegistry map[string]*b.Arb) {
	bonus := bot.getBonusUsers()
	allArbs := []b.Arb{}
	uq := map[string]int{}
	newTotal := 0.0
	now := time.Now().UnixNano()
	for _, arbs := range bot.premiumArbsV {
		for _, arb := range arbs {
			if _, ok := uq[arb.ID]; ok {
				continue
			}
			allArbs = append(allArbs, arb)
			newTotal += arb.Percentage
			uq[arb.ID] = 1
		}

	}

	utils.Debug("New total", newTotal, "Previous", bot.bonusTotal)
	var z bool
	if newTotal == 0.0 {
		if bot.bonusTotal != 0 {
			//obsolete
			z = true
		}
	}
	if newTotal != bot.bonusTotal {
		for _, user := range bonus {
			msg, msg2, msg3 := "", "", ""
			i := 0
			for arbID, groups := range arbs {
				if _, ok := bot.grouper.validArbs[arbID]; !ok {
					continue
				}
				if arb, ok := arbRegistry[arbID]; ok {
					tmp, _, _ := GenMessage([]b.Arb{*arb}, z, user.Stake, groups...)
					if i > 10 && i < 20 {
						msg2 += tmp
						i++
						continue
					}
					if i > 20 {
						msg3 += tmp
						i++
						continue
					}
					msg += tmp
					i++

				}
			}
			if z {
				msg = "Previous arbs now obsolete!"
				msg2 = ""
				msg3 = ""
			}
			if len(msg) > 0 {
				arbServer.Send(b.GetRecipient(user.ChatID), msg, &tbot.SendOptions{ParseMode: tbot.ModeMarkdown})
			}
			if len(msg2) > 0 {
				arbServer.Send(b.GetRecipient(user.ChatID), fmt.Sprintf("CONTINUATION:\n\n %s", msg2), &tbot.SendOptions{ParseMode: tbot.ModeMarkdown})
			}
			if len(msg3) > 0 {
				arbServer.Send(b.GetRecipient(user.ChatID), fmt.Sprintf("CONTINUATION:\n\n %s", msg3), &tbot.SendOptions{ParseMode: tbot.ModeMarkdown})
			}
		}
	}
	bot.bonusTotal = newTotal
	bot.updateArbs(allArbs, now, "VERIFIER")
}
func (bot *Bot) NotifyValueBets() {
	expired := "Your subscription has expired.\n Text me /price for pricing details."
	//send fixed bets
	fixedUsers := bot.getFixedUsers()
	fixedPremUsers := bot.getFixedPremUsers()

	vbs := []b.ValueBet{}
	for _, vb := range bot.valueBets {
		if !vb.Updated {
			vbs = append(vbs, vb)
		}
	}
	msg := GenValueBetMessage(vbs)
	// utils.Debug(msg)
	if len(msg) > 0 {
		for _, user := range fixedUsers {

			if bot.timePassed(time.Now().Unix(), user.PremiumTime) && bot.timePassed(time.Now().Unix(), user.GeneralTime) { //Prem expired
				user.ActivePremium = false
				user.ActiveGeneral = false
				user.PremiumTime = 0
				user.GeneralTime = 0
				user.Fixed = false
				models.UpdateUser(&user, bot.query)
				arbServer.Send(b.GetRecipient(user.ChatID), expired)
				continue
			}
			// utils.Debug("Sending SFB to:", user.ChatID)
			arbServer.Send(b.GetRecipient(user.ChatID), msg)

			time.Sleep(35 * time.Millisecond)

		}
		for _, user := range fixedPremUsers {

			if bot.timePassed(time.Now().Unix(), user.PremiumTime) { //Prem expired
				user.ActivePremium = false
				user.ActiveGeneral = false
				user.PremiumTime = 0
				user.GeneralTime = 0
				user.FixedPremium = false
				models.UpdateUser(&user, bot.query)
				arbServer.Send(b.GetRecipient(user.ChatID), expired)
				continue
			}
			// utils.Debug("Sending PREM SFB to:", user.ChatID)
			arbServer.Send(b.GetRecipient(user.ChatID), fmt.Sprintf("✅VALUE BET\n%s", msg))
			time.Sleep(35 * time.Millisecond)
		}
		arbServer.Send(b.GetRecipient(-221105295), msg) //SILOK FIXED BET
	}
	bot.flagSentValueBets(vbs)

}

func (bot *Bot) flagSentValueBets(vbs []b.ValueBet) {
	bot.lock.Lock()
	defer bot.lock.Unlock()
	for _, vb := range vbs {
		delete(bot.valueBets, vb.ID)
		vb.Updated = true
		bot.valueBets[vb.ID] = vb
	}
}
func (bot *Bot) Start() {
	var err error
	var listener net.Listener
	listener, err = net.Listen("tcp", premium)
	if err != nil {
		utils.Error(err)
		return
	}

	b.CurrentArbs = map[string]*b.Arb{}
	b.CurrentValueBets = map[string]*b.ValueBet{}

	stop := make(chan struct{})
	verifier := NewVerifier(bot, bot.verifierDelay)
	go verifier.Start(stop)
	go bot.grouper.persistenceService()

	for {
		c, err := listener.Accept()
		if err != nil {
			utils.Error(err)
			break
		}

		t, _ := spdy.NewServerTransport(c)

		receiver, err := t.WaitReceiveChannel()
		if err != nil {
			utils.Error(err)
			continue
		}
		for {
			payload := b.PremiumPayload{}
			err = receiver.Receive(&payload)
			if err != nil {
				utils.Error(err)
				break
			}
			utils.Debug("RECEIVED ARBS", payload.Arbs)
			utils.Debug("RECEIVED VALUE BETS", payload.Arbs)

			arbs := []b.Arb{}
			vbs := []b.ValueBet{}

			for _, arb := range payload.Arbs {
				if strings.Contains(arb.Market, "MIDDLE") {
					arbs = append(arbs, arb)
					continue
				}
				if arb.Percentage > 3.5 {
					arbs = append(arbs, arb)
				}
			}
			for _, vb := range payload.ValueBets {
				vb.Updated = false
				vbs = append(vbs, vb)
			}
			bot.updateArbs(arbs, time.Now().UnixNano(), "ARB_CORE")
			bot.updateValueBets(vbs)

		}

	}
	stop <- struct{}{}
}

func (bot *Bot) RegFixedBet(index int, fb FixedBet) {
	bot.fbi++
	if index < 0 { //new
		bot.fixedBets[bot.fbi] = fb
	} else {
		bot.fixedBets[index] = fb
	}
}
func (bot *Bot) Match(name string, odd float64, msg string) bool {
	teams := strings.Split(name, " V ")
	matched := false
	for i, fbet := range bot.fixedBets {
		home := strings.ToUpper(teams[0])
		away := strings.ToUpper(teams[1])
		rhome := strings.ToUpper(fbet.Home)
		raway := strings.ToUpper(fbet.Away)
		homematch := strings.Contains(rhome, home) || strings.Contains(home, rhome)
		awaymatch := strings.Contains(raway, away) || strings.Contains(away, raway)

		if homematch && awaymatch {
			matched = true

			if odd != fbet.Odd {
				bot.RegFixedBet(i, FixedBet{
					Home: strings.Trim(teams[0], " "),
					Away: strings.Trim(teams[1], " "),
					Odd:  odd,
					Msg:  msg,
					Sent: false,
				})
			}
			break
		}
	}
	if matched {
		return true
	}
	bot.RegFixedBet(-1, FixedBet{
		Home: strings.Trim(teams[0], " "),
		Away: strings.Trim(teams[1], " "),
		Odd:  odd,
		Msg:  msg,
	})
	return false

}

func (bot *Bot) checkEqual(new, current []b.Arb, groupID int) (bool, bool) {
	currentTotal, newTotal := 0.0, 0.0
	for _, arb := range current {
		currentTotal += arb.Percentage
	}
	for _, arb := range new {
		newTotal += arb.Percentage
	}

	utils.Debug("Group", groupID, currentTotal, newTotal)
	zero := false
	if newTotal == 0 && currentTotal != 0 {
		zero = true
	}
	return currentTotal == newTotal, zero

}

func (bot *Bot) classify(arbs []b.Arb) ([]b.Arb, []b.Arb, b.Arb) {
	prem, gen := []b.Arb{}, []b.Arb{}
	larb := b.Arb{}
	largest, li := 0.0, -1
	for i, arb := range arbs {
		// groups := bot.grouper.ResolveArb(arb.ID)

		if strings.Contains(arb.Market, "MIDDLE") {
			if arb.Percentage >= 2.5 {
				prem = append(prem, arb)
			}
			continue
		}
		if arb.Percentage >= 3.0 && arb.Percentage <= 50.0 {
			gen = append(gen, arb)
		}

		if arb.Percentage >= 2.5 {
			prem = append(prem, arb)
		}
		if arb.Percentage > largest {
			largest = arb.Percentage
			li = i
		}
	}
	if li > -1 {
		larb = arbs[li]
	}
	return gen, prem, larb
}
func (bot *Bot) GetGeneralArbs(groupID int) []b.Arb {
	bot.lock.Lock()
	defer bot.lock.Unlock()
	return bot.generalArbsV[groupID]
}

func (bot *Bot) GetPremiumArbs(groupID int) []b.Arb {
	bot.lock.Lock()
	defer bot.lock.Unlock()
	return bot.premiumArbsV[groupID]
}

func (bot *Bot) GetPremiumArbsFCORE() []b.Arb {
	bot.lock.Lock()
	defer bot.lock.Unlock()
	return bot.premiumArbs
}

func (bot *Bot) getPremiumUsers(groups ...int) []models.User {
	users := []models.User{}
	for _, group := range groups {
		_users := []models.User{}
		bot.query.GetMany("users", map[string]interface{}{
			"active_premium": true,
			"fixed":          false,
			"group_id":       group,
		}, &users)
		users = append(users, _users...)
	}

	return users
}

func (bot *Bot) getBonusUsers() []models.User {
	bonus := []models.User{}
	bot.query.GetMany("users", map[string]interface{}{
		"active_bonus": true,
	}, &bonus)
	return bonus

}

func (bot *Bot) getGeneralUsers(groups ...int) []models.User {
	users := []models.User{}
	for _, group := range groups {
		_users := []models.User{}
		bot.query.GetMany("users", map[string]interface{}{
			"active_general": true,
			"fixed":          false,
			"group_id":       group,
		}, &users)
		users = append(users, _users...)
	}
	return users
}
func (bot *Bot) getNonSubs() []models.User {
	users := []models.User{}
	bot.query.GetMany("users", map[string]interface{}{
		"active_general": false,
		"active_premium": false,
	}, &users)
	return users
}
func (bot *Bot) getFixedUsers() []models.User {
	users := []models.User{}
	bot.query.GetMany("users", map[string]interface{}{
		"fixed": true,
	}, &users)
	return users
}
func (bot *Bot) getFixedPremUsers() []models.User {
	users := []models.User{}
	bot.query.GetMany("users", map[string]interface{}{
		"fixed_premium": true,
	}, &users)
	return users
}

func (bot *Bot) getOneOffUsers() []models.User {
	users := []models.User{}
	bot.query.GetMany("users", map[string]interface{}{
		"active_oneoff": true,
	}, &users)
	return users
}

func (bot *Bot) getPromoGroups() []int64 {
	return []int64{-1001135499409}
}
