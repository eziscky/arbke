package bot

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/arbke/utils"
)

type Payload struct {
	ConfirmationURL string `json:"ConfirmationURL"`
	ResponseType    string `json:"ResponseType"`
	ShortCode       string `json:"ShortCode"`
	ValidationURL   string `json:"ValidationURL"`
}

type Saf struct {
	accessToken    string
	tokenReqURL    string
	consumerKey    string
	host           string
	regURL         string
	consumerSecret string
	expiry         int64
	expiryChan     chan int64
	serverAddr     string
}

func NewSaf(addr string) *Saf {
	return &Saf{
		accessToken:    "",
		host:           "sandbox.safaricom.co.ke",
		tokenReqURL:    "https://sandbox.safaricom.co.ke/oauth/v1/generate",
		regURL:         "https://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl",
		consumerKey:    "jDPYqNqOFSF3g2ZUcJEfuhAgtBo9ZmNH",
		consumerSecret: "jknPgECvQAxontWG",
		expiryChan:     make(chan int64),
		serverAddr:     addr,
	}
}

func (saf *Saf) Start() {
	go saf.service()
}

func (saf *Saf) registerURL() error {
	data := Payload{
		ShortCode:       "910068",
		ResponseType:    "Completed",
		ConfirmationURL: fmt.Sprintf("%s/payment/confirm", saf.serverAddr),
		ValidationURL:   fmt.Sprintf("%s/payment/validate", saf.serverAddr),
	}
	payloadBytes, _ := json.Marshal(data)

	body := bytes.NewReader(payloadBytes)

	req, _ := http.NewRequest("POST", "https://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl", body)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", saf.accessToken))

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		return err
	}
	d, _ := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	utils.Debug(string(d))
	parsed := map[string]string{}
	if err := json.Unmarshal(d, &parsed); err != nil {
		utils.Error(err)
		return err
	}
	if parsed["ResponseDescription"] != "success" {
		utils.Error(fmt.Errorf("%s", parsed["ResponseDescription"]))
		return fmt.Errorf("%s", parsed["ResponseDescription"])
	}
	return nil
}
func (saf *Saf) service() {
	for t := range saf.expiryChan {
		go saf.requestToken()
		time.Sleep(time.Second * time.Duration(t))

	}
}

func (saf *Saf) getAuthKey() string {
	return base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s%s", saf.consumerKey, saf.consumerSecret)))
}
func (saf *Saf) requestToken() {
	req, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("%s?grant_type=client_credentials", saf.tokenReqURL), nil)
	// req.Header.Add("Authorization", saf.getAuthKey())
	req.SetBasicAuth(saf.consumerKey, saf.consumerSecret)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		utils.Error(err)
		saf.expiryChan <- -1
		return
	}
	data, _ := ioutil.ReadAll(resp.Body)
	utils.Debug(string(data))
	parsed := map[string]interface{}{}
	if err := json.Unmarshal(data, &parsed); err != nil {
		utils.Error(err)
		saf.expiryChan <- -1
		return
	}

	saf.accessToken = parsed["access_token"].(string)
	saf.expiry, _ = strconv.ParseInt(parsed["expires_in"].(string), 10, 64)
	utils.Debug(saf.accessToken, saf.expiry)
	// saf.expiryChan <- saf.expiry
	saf.registerURL()
}
