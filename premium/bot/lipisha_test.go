package bot

import (
	"log"
	"net/http"
	"os"
	"testing"

	"github.com/gorilla/mux"
	"gitlab.com/arbke/premium/models"
	"gitlab.com/arbke/utils"
	r "gopkg.in/gorethink/gorethink.v3"
)

func connectDB() *models.DB {
	addr := os.Getenv("arb_db")
	if len(addr) == 0 {
		addr = "127.0.0.1:28015"
	}
	session, err := r.Connect(r.ConnectOpts{
		Address:  addr,
		Username: "admin",
		Password: "nitrade.nitride",
	})
	if err != nil {
		log.Panic(err)
	}
	c, err := r.DBList().Run(session)
	if err != nil {
		log.Fatal(err)
	}
	defer c.Close()
	var db string
	var create int
	for c.Next(&db) {
		utils.Info(db)
		if db == models.DB_NAME {
			create = -1
		}
	}
	if create >= 0 {
		utils.Info("Creating")
		_, err = r.DBCreate(models.DB_NAME).RunWrite(session)
	}
	utils.Debug("DB:", addr)
	return &models.DB{Session: session}
}

func setRoutes(db *models.DB) *mux.Router {
	router := mux.NewRouter()

	router.HandleFunc("/payment/ack", func(rw http.ResponseWriter, r *http.Request) {
		r.ParseForm()
		lp := NewLipisha(db)

		switch r.FormValue("api_type") {
		case "Initiate":
			rw.Write(lp.HandleInitiate(r))
		case "Acknowledge":
			lp.HandleACK(r)
		}
	})
	return router
}

func testBill(t *testing.T) {
	db := connectDB()
	routes := setRoutes(db)
	// lip := NewLipisha(db)

	srv := &http.Server{Addr: ":8000", Handler: routes}
	// go func() {
	// 	lip.Bill("254723200817", "ref", 10)
	// }()
	utils.Error(srv.ListenAndServe())
}
