package bot

import (
	"testing"

	b "gitlab.com/arbke/bot"
	"gitlab.com/arbke/utils"
	tbot "gopkg.in/tucnak/telebot.v2"
)

func testArbFormat(t *testing.T) {
	arbServer = NewTBOTServer("561929995:AAHBVJnybP4I8R8ANasg3oOICOkptlj6Qwo")
	utils.LogDebug, utils.LogError = true, true
	msg, _, _ := GenMessage([]b.Arb{b.Arb{
		ID:     "TEST",
		Home:   "Home",
		Away:   "Away",
		League: "League",
		Market: "Market",
		Combinations: []b.Container{
			b.Container{
				"link":    "https://www.link.com",
				"Site":    "Test/TT",
				"Outcome": "Over 4.5",
				"Odd":     9.9,
				"Stake":   0.5,
			},
			b.Container{
				"link":    "https://www.link2.com",
				"Site":    "Test2",
				"Outcome": "Under 4.5",
				"Odd":     7.7,
				"Stake":   0.5,
			},
		},
	}}, false, 0.0)
	arbServer.Send(b.GetRecipient(543847056), msg, &tbot.SendOptions{
		ParseMode: tbot.ModeMarkdown,
	})
}
