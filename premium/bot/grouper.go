package bot

import (
	"sync"
	"time"

	b "gitlab.com/arbke/bot"
	"gitlab.com/arbke/premium/models"
	"gitlab.com/arbke/utils"
)

type Grouper struct {
	groups    [][]int
	current   int
	arbMap    map[string][]int
	validArbs map[string]int
	rrBias    map[int]float64

	query *models.Query
	lock  *sync.Mutex

	pchan chan int
}

func NewGrouper(db *models.DB) *Grouper {
	query := models.NewQuery(db)

	groups := [][]int{
		[]int{1, 4},
		[]int{2, 10},
		[]int{5, 7},
		[]int{6, 8},
		[]int{3, 9},
	}

	//setup stats
	bias := models.GetAGSel(query)

	for i := range groups {
		if _, ok := bias[i]; !ok {
			bias[i] = 0.0
		}
	}
	return &Grouper{
		arbMap:    map[string][]int{},
		rrBias:    bias,
		pchan:     make(chan int),
		lock:      &sync.Mutex{},
		validArbs: map[string]int{},
		query:     query,
		groups:    groups,
	}
}

func (g *Grouper) persistenceService() {
	for id := range g.pchan {

		if err := models.UpdateGSel(models.GroupSelect{Total: g.rrBias[id], GroupID: id}, g.query); err != nil {
			utils.Error(err)
		}

	}
}

//GroupedArbs returns grouped and ungrouped arbs stored in arbMap
func (g *Grouper) GroupedArbs() (map[int][]string, map[string][]int) {
	data := map[int][]string{}
	for _, group := range g.groups {
		for _, groupID := range group {
			data[groupID] = []string{}
		}
	}
	for arbID, group := range g.arbMap {
		if _, ok := g.validArbs[arbID]; !ok {
			utils.Debug("SKIPPING", arbID)
			continue
		}
		for _, groupID := range group {
			if _, exists := data[groupID]; exists {
				data[groupID] = append(data[groupID], arbID)
				continue
			}
			data[groupID] = []string{arbID}
		}
	}
	return data, g.arbMap
}

func (g *Grouper) RecordStat(numArbs int, lastUpdate, lastObsolete map[int]int64, groupID int) {

	lo, e := lastObsolete[groupID]
	lu, e1 := lastUpdate[groupID]

	if e && e1 {
		utils.Debug(groupID, lo-lu)
		gs := models.GetGroupStats(groupID, g.query)
		if gs != nil {
			gs.LastArb = time.Now().Unix()
			gs.ArbCyc = gs.ArbCyc + 1
			num := (lo - lu)
			if num > 0 {
				gs.ArbCycT = num
			}
			gs.NumArbs += numArbs
			gs.ArbLatency = gs.ArbCycT / gs.ArbCyc
			models.UpdateGroupStats(*gs, g.query)
		} else {
			gs = &models.GroupStats{}
			gs.GroupID = groupID
			gs.LastArb = time.Now().Unix()
			gs.ArbCyc = 1
			num := (lo - lu)
			if num > 0 {
				gs.ArbCycT = num
			}
			gs.NumArbs = numArbs
			gs.ArbLatency = gs.ArbCycT
			models.RegisterGroupStats(*gs, g.query)
		}
	}

}
func (g *Grouper) least() int {
	least := 0.0
	if t, ok := g.rrBias[0]; ok {
		least = t
	}
	li := 0
	for i := range g.groups {
		if total, ok := g.rrBias[i]; ok {

			if total < least {
				least = total
				li = i
			}
		} else {
			utils.Error("BIAS ERROR", g.rrBias, g.current)
		}
	}
	utils.Debug(g.rrBias, least, li)
	return li
}
func (g *Grouper) next(arbs ...b.Arb) []int {
	g.lock.Lock()
	defer g.lock.Unlock()

	g.current = g.least()

	t := 0.0
	for _, arb := range arbs {
		t += arb.Percentage
	}
	if _, ok := g.rrBias[g.current]; ok {
		g.rrBias[g.current] += t
	} else {
		g.rrBias[g.current] = t
	}
	g.pchan <- g.current
	return g.groups[g.current]
}

func (g *Grouper) Register(arbs ...b.Arb) {
	valid := map[string]int{}
	for _, arb := range arbs {
		val, exists := g.arbMap[arb.ID]
		if !exists {
			g.next(arb)
			utils.Debug("ASSIGNING", arb.ID, "TO GROUPINDEX", g.current)
			val = g.groups[g.current]
		}
		g.arbMap[arb.ID] = val
		valid[arb.ID] = 1

	}
	g.validArbs = valid

	// toRemove := []string{}
	// for arbID := range g.arbMap {
	// 	if _, exists := valid[arbID]; !exists {
	// 		toRemove = append(toRemove, arbID)
	// 	}
	// }
	// for _, r := range toRemove {
	// 	delete(g.arbMap, r)
	// }
}

func (g *Grouper) ResolveArb(arbID string) []int {
	if gid, exists := g.arbMap[arbID]; exists {
		utils.Debug("RESOLVED", arbID, "GROUP", gid)
		return gid
	}
	return []int{}
}
