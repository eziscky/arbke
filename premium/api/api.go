package api

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/arbke/premium/bot"
	"gitlab.com/arbke/premium/models"
	"gitlab.com/arbke/utils"
)

var (
	ONEOFF_PRICE   = 100.0
	WEEKLY_PRICE   = 250.0  //4% and below
	PREMIUM_WEEKLY = 1000.0 //Unlimited
)

/*

//initiate
[api_key=792ace3f7167fd96044a7729487e5b67&api_signature=CvCxlZEs4J4OhgJxpRgpGFqD7kFztA79hSfmpYyLAeCrexeqwB2c6%2BCUUgFEPnqasfZUHaC06VoiHtFeXt4jTv6NCv6sw4SsItdS67Ld108d07OXfTEHXqN8kwlCsKolm9NP6Zm964XW2llHT8IohrYY%2BvP4MPh5VEorFplo6fY%3D&api_version=1.0.4&api_type=Initiate&transaction_date=2018-05-26+09%3A12%3A27&transaction_amount=10.00&transaction_type=Payment&transaction_method=Paybill+%28M-Pesa%29&transaction=DIL0G7CQOH&transaction_reference=DIL0G7CQOH&transaction_name=ERIC+M+MOKAYA&transaction_mobile=254723200817&transaction_paybill=961700&transaction_paybill_type=Shared&transaction_account=13544&transaction_account_number=13544&transaction_merchant_reference=13544&transaction_account_name=Primary&transaction_status=Completed&transaction_code=MEQ5LQO2QB&transaction_country=KE&transaction_currency=KES&transaction_gateway_code=&transaction_account_keyword=]
[map[transaction_merchant_reference:[13544] api_version:[1.0.4] api_type:[Initiate] transaction_date:[2018-05-26 09:12:27] transaction:[DIL0G7CQOH] transaction_paybill_type:[Shared] transaction_status:[Completed] transaction_gateway_code:[] transaction_amount:[10.00] transaction_type:[Payment] transaction_reference:[DIL0G7CQOH] transaction_name:[ERIC M MOKAYA] transaction_mobile:[254723200817] transaction_code:[MEQ5LQO2QB] api_signature:[CvCxlZEs4J4OhgJxpRgpGFqD7kFztA79hSfmpYyLAeCrexeqwB2c6+CUUgFEPnqasfZUHaC06VoiHtFeXt4jTv6NCv6sw4SsItdS67Ld108d07OXfTEHXqN8kwlCsKolm9NP6Zm964XW2llHT8IohrYY+vP4MPh5VEorFplo6fY=] transaction_method:[Paybill (M-Pesa)] transaction_paybill:[961700] transaction_account:[13544] transaction_account_name:[Primary] api_key:[792ace3f7167fd96044a7729487e5b67] transaction_account_number:[13544] transaction_country:[KE] transaction_currency:[KES] transaction_account_keyword:[]] <nil>]



[api_key=792ace3f7167fd96044a7729487e5b67&api_signature=CvCxlZEs4J4OhgJxpRgpGFqD7kFztA79hSfmpYyLAeCrexeqwB2c6%2BCUUgFEPnqasfZUHaC06VoiHtFeXt4jTv6NCv6sw4SsItdS67Ld108d07OXfTEHXqN8kwlCsKolm9NP6Zm964XW2llHT8IohrYY%2BvP4MPh5VEorFplo6fY%3D&api_version=1.0.4&api_type=Acknowledge&transaction_status_code=005&transaction_status=Fail&transaction_status_description=Invalid+response+parameters.&transaction_reference=DIL0G7CQOH&transaction_code=MEQ5LQO2QB&transaction_gateway_code=&transaction_status_action=ACCEPT&transaction_status_reason=VALID_TRANSACTION]
[map[transaction_status_code:[005] transaction_status:[Fail] transaction_gateway_code:[] transaction_status_reason:[VALID_TRANSACTION] transaction_status_action:[ACCEPT] api_key:[792ace3f7167fd96044a7729487e5b67] api_signature:[CvCxlZEs4J4OhgJxpRgpGFqD7kFztA79hSfmpYyLAeCrexeqwB2c6+CUUgFEPnqasfZUHaC06VoiHtFeXt4jTv6NCv6sw4SsItdS67Ld108d07OXfTEHXqN8kwlCsKolm9NP6Zm964XW2llHT8IohrYY+vP4MPh5VEorFplo6fY=] api_version:[1.0.4] api_type:[Acknowledge] transaction_status_description:[Invalid response parameters.] transaction_reference:[DIL0G7CQOH] transaction_code:[MEQ5LQO2QB]] <nil>]

*/

func SetRoutes(db *models.DB, msg chan map[string]interface{}, prem *bot.Bot) *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/payment/ack", func(rw http.ResponseWriter, r *http.Request) {
		r.ParseForm()
		lp := bot.NewLipisha(db)

		switch r.FormValue("api_type") {
		case "Initiate":
			rw.Write(lp.HandleInitiate(r))
		case "Acknowledge":
			success, txt, user := lp.HandleACK(r)
			if success {
				msg <- map[string]interface{}{
					"id":    user.ChatID,
					"msg":   txt,
					"reset": true,
				}
			}
		}
	})

	router.HandleFunc("/data", func(rw http.ResponseWriter, r *http.Request) {
		rw.Header().Add("Access-Control-Allow-Origin", "*")
		arbs := prem.GetPremiumArbsFCORE()
		query := models.NewQuery(db)
		users, _ := models.GetUsersFilter(query, map[string]interface{}{})
		premU, genU := []models.User{}, []models.User{}
		newU := []map[string]interface{}{}
		for i, user := range users {
			if user.ActivePremium {
				premU = append(premU, user)
			}
			if user.ActiveGeneral {
				genU = append(genU, user)
			}

			if i <= 7 {
				newU = append(newU, map[string]interface{}{
					"name":        user.FName,
					"phoneNumber": user.PhoneNumber,
				})
			}
		}

		resp := map[string]interface{}{
			"arbs":     len(arbs),
			"clients":  len(users),
			"premSubs": len(premU),
			"gensubs":  len(genU),
			"newUsers": newU,
		}

		data, _ := json.Marshal(resp)
		rw.Write(data)
	})

	router.HandleFunc("/groups", func(rw http.ResponseWriter, r *http.Request) {
		query := models.NewQuery(db)
		final := []map[string]interface{}{}
		for i := 1; i <= 10; i++ {
			users := models.GetUsersGroup(i, query)
			groupStats := models.GetGroupStats(i, query)

			userData := []map[string]interface{}{}
			for _, user := range users {
				userData = append(userData, map[string]interface{}{
					"phoneNumber":  user.PhoneNumber,
					"name":         user.FName,
					"highestStake": user.Stake,
				})
			}
			data := map[string]interface{}{
				"groupID":  i,
				"numUsers": len(users),
				"users":    userData,
				"stats":    groupStats,
			}

			final = append(final, data)
		}

		data, err := json.Marshal(final)
		if err != nil {
			utils.Error(err)
		}
		rw.Write(data)
	})

	// router.HandleFunc("/messages", GetMessages)

	return router
}
