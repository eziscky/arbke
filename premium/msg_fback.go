package main

import (
	"fmt"
	"time"

	tbot "gopkg.in/tucnak/telebot.v2"

	b "gitlab.com/arbke/bot"
	"gitlab.com/arbke/premium/bot"
	"gitlab.com/arbke/premium/models"
	"gitlab.com/arbke/utils"
)

func feedbackService(query *models.Query, arbServer *tbot.Bot, feedback chan map[string]interface{}) {
	for f := range feedback {
		if f["admin"].(bool) { //from admin to ...
			if f["broadcast"].(bool) { //to multiple targets
				fix := f["fixed"].(bool)
				prem := f["active_premium"].(bool)
				gen := f["active_general"].(bool)
				or := f["or"].(bool)

				selected := map[string]models.User{}
				all := []models.User{}
				filter := map[string]interface{}{}
				if or { //prem or fixed and gen or fixed
					if prem && gen && fix { //paying with sbf
						users, _ := models.GetUsersFilter(query, map[string]interface{}{
							"active_premium": true,
						})
						all = append(all, users...)
						users, _ = models.GetUsersFilter(query, map[string]interface{}{
							"active_general": true,
						})
						all = append(all, users...)
						users, _ = models.GetUsersFilter(query, map[string]interface{}{
							"fixed": true,
						})
						all = append(all, users...)
					}
					if !prem && !gen && !fix { //EVERYONE
						users, _ := models.GetUsersFilter(query, filter)
						all = append(all, users...)
					}

				} else {
					if prem && gen && fix { //sbf
						users, _ := models.GetUsersFilter(query, map[string]interface{}{
							"fixed": true,
						})
						all = append(all, users...)
					}

					if prem && gen && !fix { //paying
						users, _ := models.GetUsersFilter(query, map[string]interface{}{
							"active_premium": true,
							"fixed":          false,
						})
						all = append(all, users...)
						users, _ = models.GetUsersFilter(query, map[string]interface{}{
							"active_general": true,
							"fixed":          false,
						})
						all = append(all, users...)
					}
					if !prem && !gen && !fix { //NON PAYING
						users, _ := models.GetUsersFilter(query, map[string]interface{}{
							"active_premium": false,
							"active_general": false,
						})
						all = append(all, users...)
					}
				}

				t := 0
				for _, u := range all {
					if _, ok := selected[u.PhoneNumber]; !ok {
						t++
						selected[u.PhoneNumber] = u
					}
				}

				utils.Debug("Sending to ", t)
				for _, user := range selected {
					utils.Info(user)
					arbServer.Send(b.GetRecipient(user.ChatID), f["text"].(string))
					time.Sleep(100 * time.Millisecond)
				}
			} else {

				//query user details
				if _, ok := f["query"]; ok {
					user := models.GetUserChatID(query, f["chatID"].(int64))
					//forward to admin
					arbServer.Send(b.GetRecipient(bot.PREM_SUPPORT), user.ToInfo())
					continue
				}

				//toggle between Arbs and SBF
				if _, ok := f["toggle"]; ok {
					user := models.GetUserChatID(query, f["chatID"].(int64))
					msg := ""
					if user.Fixed {
						user.Fixed = false
						msg = fmt.Sprintf("Switching ID: %d To: %s", user.ChatID, "ARBS")
					} else {
						user.Fixed = true
						msg = fmt.Sprintf("Switching ID: %d To: %s", user.ChatID, "SURETIP")
					}
					if err := models.UpdateUser(user, query); err != nil {
						utils.Error(err)
						msg = err.Error()
					}
					//forward to admin
					arbServer.Send(b.GetRecipient(bot.PREM_SUPPORT), msg)
					continue
				}

				//fasttracks user addition
				if addT, ok := f["add"]; ok {
					_, user, err := models.AddUser(f["chatID"].(int64), f["phone"].(string), f["name"].(string), query)
					if err != nil {
						utils.Error(err)
						arbServer.Send(b.GetRecipient(bot.PREM_SUPPORT), err.Error())
						continue
					}

					regID := models.NextID(query, "arb_registry")
					utils.Debug("REGISTRATION", regID)
					if regID > 10 {
						utils.Debug("RESETTING REGISTRATION", regID)
						models.SetID(query, "arb_registry", 0)
						regID = 1
					}
					switch addT.(string) {
					// case "AG": //add 1-week package
					// 	user.GeneralTime = time.Now().Add(168 * time.Hour).Unix()
					// 	user.ActiveGeneral = true
					case "AGP": //add 2-week package
						user.GeneralTime = time.Now().Add(168 * 2 * time.Hour).Unix()
						user.ActiveGeneral = true

						utils.Debug("ASSIGNING", user.PhoneNumber, "GROUP", regID)
						user.GroupID = regID
					case "AP": //add 1-month package
						user.PremiumTime = time.Now().Add(720 * time.Hour).Unix()
						user.ActivePremium = true
						utils.Debug("ASSIGNING", user.PhoneNumber, "GROUP", regID)
						user.GroupID = regID

					case "APF": //add 1-month sbf package
						user.PremiumTime = time.Now().Add(720 * time.Hour).Unix()
						user.ActivePremium = true
						user.FixedPremium = true
						utils.Debug("ASSIGNING", user.PhoneNumber, "GROUP", regID)
						user.GroupID = regID

					}

					msg := user.ToInfo()
					if err := models.UpdateUser(user, query); err != nil {
						msg = err.Error()
					}
					//forward to admin
					arbServer.Send(b.GetRecipient(bot.PREM_SUPPORT), msg)
					continue
				}

				// forward to user
				if _, err := arbServer.Send(b.GetRecipient(f["chatID"].(int64)), f["text"].(string)); err != nil {
					utils.Error(err)
				}
			}
		} else {
			// forward to admin

			//exclude marketing group
			if f["chatID"].(int64) == -1001135499409 {
				continue
			}

			user := models.GetUserChatID(query, f["chatID"].(int64))
			name := "N/A"
			if user != nil {
				name = user.FName
			}

			//reroute support based on user subscription
			channel := bot.GEN_SUPPORT
			if user != nil {
				if user.ActivePremium || user.ActiveGeneral {
					channel = bot.PREM_SUPPORT
				}
			}

			//ensure messages are acknowledged on the channel they are sent on
			if f["chatID"].(int64) < 0 {
				channel = bot.ACK_CHAN
			}
			//543847056 --> SA
			//SILOK SUPPORT
			// arbServer.Send(543847056, fmt.Sprintf("ID: %d, TEXT: %s", f["chatID"].(int64), f["text"].(string)))
			arbServer.Send(b.GetRecipient(channel), fmt.Sprintf("ID: %d,NAME:%s\nTEXT: %s", f["chatID"].(int64), name, f["text"].(string)))
		}
	}
}
