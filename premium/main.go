package main

import (
	"log"
	"net/http"
	"os"
	"time"

	metrics "github.com/tevjef/go-runtime-metrics"
	"gitlab.com/arbke/premium/api"
	"gitlab.com/arbke/premium/bot"
	"gitlab.com/arbke/premium/models"
	"gitlab.com/arbke/utils"
	r "gopkg.in/gorethink/gorethink.v3"
)

//connectDB connects to the specified rethinkdb server
//returns the models.DB type
func connectDB() *models.DB {
	addr := os.Getenv("arb_db")
	if len(addr) == 0 {
		addr = "127.0.0.1:28015"
	}
	session, err := r.Connect(r.ConnectOpts{
		Address:  addr,
		Username: "admin",
		Password: "nitrade.nitride",
	})
	if err != nil {
		log.Panic(err)
	}
	c, err := r.DBList().Run(session)
	if err != nil {
		log.Fatal(err)
	}
	defer c.Close()
	var db string
	var create int
	for c.Next(&db) {
		utils.Info(db)
		if db == models.DB_NAME {
			create = -1
		}
	}
	if create >= 0 {
		utils.Info("Creating")
		_, err = r.DBCreate(models.DB_NAME).RunWrite(session)
	}
	utils.Debug("DB:", addr)
	return &models.DB{Session: session}
}

func main() {
	//ENABLE FOR SYS TRACE
	metrics.DefaultConfig.CollectionInterval = time.Second
	metrics.DefaultConfig.Database = "stats2_db"
	metrics.DefaultConfig.Logger = &metrics.DefaultLogger{}
	metricStop := make(chan struct{})
	if err := metrics.RunCollector(metrics.DefaultConfig, metricStop); err != nil {
		utils.Error(err)
	}

	utils.LogDebug, utils.LogError = true, true
	db := connectDB()
	models.Setup(db)
	msg := make(chan map[string]interface{}, 2)
	stakeNotif := make(chan *models.User, 5)

	premium := bot.NewBot(db, stakeNotif, 30)
	go premium.Start()
	go premium.MessageDispatch(msg)
	go premium.StakeUpdateNotifService()

	routes := api.SetRoutes(db, msg, premium)

	arbitragekenya := bot.NewTBOTServer("561929995:AAHBVJnybP4I8R8ANasg3oOICOkptlj6Qwo")
	feedback := make(chan map[string]interface{})
	// stop := make(chan int)
	arbServer := bot.GetMessageServer(arbitragekenya, db, feedback, stakeNotif)
	go func() {
		arbServer.Start()
	}()

	go feedbackService(models.NewQuery(db), arbServer, feedback)

	srv := &http.Server{Addr: ":5555", Handler: routes}

	utils.Error(srv.ListenAndServe())
	// <-stop
}
