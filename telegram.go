package main

import (
	"fmt"
	"sync"
	"time"

	tbot "gopkg.in/tucnak/telebot.v2"

	"gitlab.com/arbke/bot"
	"gitlab.com/arbke/utils"
)

func newTBOTServer(key string) *tbot.Bot {
	tb, err := tbot.NewBot(tbot.Settings{
		Token:  key,
		Poller: &tbot.LongPoller{Timeout: 10 * time.Second},
	})
	if err != nil {
		utils.Error(err)
		return nil
	}
	return tb
}

func postToTelegram(arbs []bot.Arb, zero bool) {
	// return
	finalMsg := ""
	finalMsg2 := ""
	finalMsg3 := ""
	for i, arb := range arbs {
		msg := fmt.Sprintf("Game: %s\nPercentage: %.2f\nMarket: %s\nTime:%s\nCombination:\n\t", arb.Name, arb.Percentage, arb.Market, arb.Time)
		// arb.Time
		for _, c := range arb.Combinations {
			site := ""
			v := ""
			if val, ok := c["*Site"]; ok {
				site = "*Site"
				v = val.(string)
			} else {
				v = c["Site"].(string)
				site = "Site"
			}

			msg += fmt.Sprintf("%s: %s Outcome: %s Odd: %.2f Stake: %.4f\n\t", site, v, c["Outcome"].(string), c["Odd"].(float64), c["Stake"].(float64))

		}
		msg += "\n\n"
		if i > 5 && i < 10 {
			finalMsg2 += msg
			continue
		}
		if i > 10 {
			finalMsg3 += msg
			continue
		}
		finalMsg += msg
	}
	if zero {
		finalMsg = "Previous arbs now obsolete!"
	}

	//debug arb
	fmt.Println(finalMsg)

	if len(finalMsg2) > 0 {
		utils.Error(princessPeach.Send(bot.GetRecipient(-231695084), finalMsg))
		utils.Error(princessPeach.Send(bot.GetRecipient(-231695084), "CONTINUATION: \n\n"+finalMsg2))
		if len(finalMsg3) > 0 {
			utils.Error(princessPeach.Send(bot.GetRecipient(-231695084), "CONTINUATION: \n\n"+finalMsg3))
		}

		utils.Error(princessPeach.Send(bot.GetRecipient(-1001324873209), finalMsg))
		utils.Error(princessPeach.Send(bot.GetRecipient(-1001324873209), "CONTINUATION: \n\n"+finalMsg2))
		if len(finalMsg3) > 0 {
			utils.Error(princessPeach.Send(bot.GetRecipient(-1001324873209), "CONTINUATION: \n\n"+finalMsg3))
		}
	} else {
		//Send off one off message
		if _, err := princessPeach.Send(bot.GetRecipient(-231695084), finalMsg); err != nil {
			utils.Error(err)

		}
		if _, err := arbke.Send(bot.GetRecipient(-1001324873209), finalMsg); err != nil {
			utils.Error(err)
		}
	}

}

func postCustomTel(arbs []bot.Arb, zero bool) {
	finalMsg := ""

	for _, arb := range arbs {
		msg := fmt.Sprintf("Game: %s\nPercentage: %.2f\nMarket: %s\nTime:%s\nCombination:\n\t", arb.Name, arb.Percentage, arb.Market, arb.Time)
		// arb.Time
		for _, c := range arb.Combinations {
			site := ""
			v := ""
			if val, ok := c["*Site"]; ok {
				site = "*Site"
				v = val.(string)
			} else {
				v = c["Site"].(string)
				site = "Site"
			}
			msg += fmt.Sprintf("%s: [%s](%s) Outcome: %s Odd: %.2f Stake: %.4f\n\t", site, v, c["link"].(string), c["Outcome"].(string), c["Odd"].(float64), c["Stake"].(float64))
		}
		msg += "\n\n"
		finalMsg += msg
	}
	if zero {
		finalMsg = "Previous arbs now obsolete!"
	}
	fmt.Println("BONUSARB:\n", finalMsg)
	princessPeach.Send(bot.GetRecipient(-301400406), finalMsg, &tbot.SendOptions{ParseMode: tbot.ModeMarkdown})
	// arbke.Send(bot.GetRecipient(-1001360364329), finalMsg, &tbot.SendOptions{ParseMode: tbot.ModeMarkdown})
}

func startMessageServers(_bot *bot.Bot, bots ...*tbot.Bot) {
	wg := sync.WaitGroup{}
	wg.Add(len(bots))
	construct := func(data []map[string]string) string {
		utils.Debug(_bot.Stats())
		proxies := _bot.GetProxies()
		message := ""
		for _, v := range data {
			message += fmt.Sprintf("Site:  %s\n", v["site"])
			message += fmt.Sprintf("Games(Avg): %s\n", v["num"])
			message += fmt.Sprintf("Usage(Avg): %s MB\n", v["avusage"])
			message += fmt.Sprintf("Usage(Total): %s MB\n", v["usage"])
			message += fmt.Sprintf("Latency(Avg): %s Seconds\n\n", v["avlatency"])
		}
		for _, proxy := range proxies {
			message += fmt.Sprintf("Proxy: %s\n", proxy.Addr.String())
			message += fmt.Sprintf("Up: %t\n", proxy.Up)
			message += fmt.Sprintf("Latency: %.2f Seconds\n\n", float64(proxy.ResponseTime)/1000000000.0)
		}

		utils.Debug(message)
		return message
	}

	for _, telbot := range bots {
		go func(telbot *tbot.Bot, bot *bot.Bot) {
			telbot.Handle("/stats", func(msg *tbot.Message) {
				telbot.Send(msg.Sender, construct(bot.Stats()))
			})
			telbot.Start()
			wg.Done()
		}(telbot, _bot)
	}
	wg.Wait()
}

func postToTelegramCustom(msg string) {
	utils.LogDebug, utils.LogError = true, true
	// princessPeach.Send(bot.GetRecipient(-231695084), msg, &tbot.SendOptions{
	// 	ParseMode: tbot.ModeMarkdown,
	// })
	utils.Error(arbke.Send(bot.GetRecipient(-1001360364329), msg, &tbot.SendOptions{ParseMode: tbot.ModeMarkdown}))

}
