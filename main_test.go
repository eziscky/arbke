package main

import (
	"fmt"
	"testing"
	"time"

	client "github.com/influxdata/influxdb/client/v2"
	master "gitlab.com/arbke/bot"
)

func testSPDY(t *testing.T) {
	arbc := make(chan []master.Arb)

	go startPremiumConnection(arbc)

	// b := bot.NewBot()
	// go b.Start()

	for {
		time.Sleep(5 * time.Second)
		arbc <- []master.Arb{
			master.Arb{
				Name:       "Man City V Liverpool",
				Percentage: 42.4,
			},
		}
	}
}

func testPostMarkdown(t *testing.T) {
	princessPeach = newTBOTServer("563927613:AAE7THPjOQliL0KwtjE7d9Ct3qwLJ0FfTLA")
	arbke = newTBOTServer("592329288:AAEK0nWKB5fB6sjSD6oWIXrtgrK6rKSG8do")

	postToTelegramCustom(`Kwik Test: [Kwikbet](https://www.kwikbet.co.ke)`)
}

func TestInflux(t *testing.T) {
	c, err := client.NewHTTPClient(client.HTTPConfig{
		Addr: "http://localhost:8086",
	})
	if err != nil {
		fmt.Println("Error creating InfluxDB Client: ", err.Error())
	}
	defer c.Close()

	q := client.Query{
		Command: "SHOW DATABASES",
	}
	response, err := c.Query(q)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(response.Error())
}
